//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <conio.h>
#include <assert.h>

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
FILE *out;
FILE *protoout;

char curr_label[2050];

//#define GET_FLAGS_AFTER

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
enum
{
	TYP_DONE,
	TYP_SUB,
	TYP_DB,
	TYP_DW,
	TYP_DD,
	TYP_LABEL,
	TYP_CODE,
	TYP_DATA,
};

enum
{
	STATE_NONE,
	STATE_DB,
	STATE_DW,
	STATE_DD,
	STATE_SUB,
	STATE_DATA,
};

//	typedef struct
//	{
//		union
//		{
//			unsigned char  a,f,b,c,d,e,h,l;
//			unsigned short af,bc,de,hl;
//		};
//	} REG_PACK;

//	"typedef struct\n"
//	"{\n"
//	"\tunsigned char a,f,b,c,d,e,h,l;\n"
//	"} REG_PACK;\n"

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char header[]=
{
	"//--------------------------------------------------------------------------------------\n"
	"// Auto-generated output from z80toC\n"
	"//--------------------------------------------------------------------------------------\n"
	"\n"
	"#include <windows.h>\n"
	"#include <assert.h>\n"
	"\n"
	"#include \"%s.h\"\n"
	"\n"
	"enum\n"
	"{\n"
	"\tFLAGS_CF=1,\n"
	"\tFLAGS_NF=2,\n"
	"\tFLAGS_PF=4,\n"
	"\tFLAGS_XF=8,\n"
	"\tFLAGS_HF=16,\n"
	"\tFLAGS_YF=32,\n"
	"\tFLAGS_ZF=64,\n"
	"\tFLAGS_SF=128,\n"
	"};\n"
	"\n"
	"\n"
	"unsigned char memory[65536];\n"
	"int z80_stack[2048];\n"
	"int *sp = z80_stack;\n"
	"\n"
	"#pragma pack(push,1)\n"
	"\n"
	"typedef struct\n"
	"{\n"
	"\tunion\n"
	"\t{\n"
	"\t\tunsigned char\tf,a,c,b,e,d,l,h;\n"
	"\t\tunsigned short\taf,bc,de,hl;\n"
	"\t};\n"
	"\tunsigned short sp;\n"
	"\tunsigned char i, r;\n"
	"} REG_PACK;\n"
	"\n"
	"typedef union\n"
	"{\n"
	"\tunsigned char ixl, ixh, iyl, iyh;\n"
	"\tunsigned short ix, iy;\n"
	"} IXIY_PACK;\n"
	"\n"
	"#pragma pack(pop)\n"
	"\n"
	"REG_PACK regs;\n"
	"REG_PACK exregs;\n"
	"IXIY_PACK ixiyregs;\n"
	"\n"
	"int main(void)\n"
	"{\n"
	"return 0;\n"
	"}\n"
	"\n"
	"void rol(unsigned char *a)\n"
	"{\n"
	"\tunsigned char b;\n"
	"\tb = *a&0x80;\n"
	"\t*a<<=1;\n"
	"\t*a |= b>>7;\n"
	"return;\n"
	"}\n"
	"\n"
	"void ror(unsigned char *a)\n"
	"{\n"
	"\tunsigned char b;\n"
	"\tb = *a&1;\n"
	"\t*a>>=1;\n"
	"\t*a |= b<<7;\n"
	"return;\n"
	"}\n"
	"\n"
	"void rolc(unsigned char *a)\n"
	"{\n"
	"\tunsigned char b;\n"
	"\tb = *a&0x80;\n"
	"\t*a<<=1;\n"
	"\t*a |= b>>7;\n"
	"\tif (b)\n"
	"\t\tregs.f |= FLAGS_CF;\n"
	"\telse\n"
	"\t\tregs.f &= ~FLAGS_CF;\n"
	"return;\n"
	"}\n"
	"\n"
	"void rorc(unsigned char *a)\n"
	"{\n"
	"\tunsigned char b;\n"
	"\tb = *a&1;\n"
	"\t*a>>=1;\n"
	"\t*a |= b<<7;\n"
	"\tif (b)\n"
	"\t\tregs.f |= FLAGS_CF;\n"
	"\telse\n"
	"\t\tregs.f &= ~FLAGS_CF;\n"
	"return;\n"
	"}\n"
	"\n"
	"void exx()\n"
	"{\n"
	"\tREG_PACK tmp;\n"
	"\ttmp = regs;\n"
	"\tregs = exregs;\n"
	"\texregs = tmp;\n"
	"return;\n"
	"}\n"
	"\n"
	"void push(int a)\n"
	"{\n"
	"\t*sp++ = a;\n"
	"return;\n"
	"}\n"
	"\n"
	"int pop()\n"
	"{\n"
	"return *--sp;\n"
	"}\n"
	"\n"
	"void get_flags()\n"
	"{\n"
	"\t__asm\n"
	"\t{\n"
	"\t\tlahf\n"
	"\t\tmov [regs.f],ah\n"
	"\t};\n"
	"return;\n"
	"}\n"
	"\n"
	"void set_flags(int v, int c)\n"
	"{\n"
	"\t__asm\n"
	"\t{\n"
	"\t\tmov	eax,v\n"
	"\t\tmov	ebx,c\n"
	"\t\tcmp	eax,ebx\n"
	"\t\tlahf\n"
	"\t\tmov [regs.f],ah\n"
	"\t};\n"
	"return;\n"
	"}\n"
	"\n"
};

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void write_file_header(char *inc_name)
{
	fprintf(out, header, inc_name);
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
int label_line;
int id_line(char *p, int len)
{
	char *q;
	if (len == 0)
		return TYP_DONE;

	if (q=strchr(p, ':'))
	{
		if (!strncmp(p, "locret_", 7))
			return TYP_LABEL;
		if (!strncmp(p, "sub_", 4))
			return TYP_SUB;
		if (!strncmp(p, "loc_", 4))
			return TYP_LABEL;

		if (q[1]=='\0')
			return TYP_SUB;//user defined lable

		label_line = 1;
		return TYP_DATA;
	}

	while (isspace(*p)) p++;

	if (p[0] == 'd' && p[1] == 'b')
		return TYP_DB;

	if (p[0] == 'd' && p[1] == 'w')
		return TYP_DW;

	if (p[0] == 'd' && p[1] == 'd')
		return TYP_DD;

	if (isdigit(p[0]))
		return TYP_DONE;

return TYP_CODE;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
int state;
int array;
int glob_array;
//int newlabel;
int last_state;
void change_state(int new_state)
{
	if (state != new_state)
	{
		switch (state)
		{
			case STATE_SUB:
				fprintf(out, "return;\n}\n");
				break;
			case STATE_DB:
				if (array) fprintf(out, "};\n");
				break;
			case STATE_DW:
				if (array) fprintf(out, "};\n");
				break;
			case STATE_DD:
				if (array) fprintf(out, "};\n");
				break;
			case STATE_DATA:
				break;
		}

		fprintf(out, "\n");

		switch (new_state)
		{
			case STATE_DB:
				break;
			case STATE_DW:
				break;
			case STATE_DD:
				break;
			case STATE_SUB:
				break;
			case STATE_DATA:
				break;
		}

		last_state = state;
		state = new_state;
	}
	else
	{
		if (state == STATE_SUB)
			fprintf(out, "return;\n}\n");
	}

	/*
	else
	{
		if (array && newlabel)
		{
			switch (state)
			{
			case STATE_DB:
			case STATE_DW:
			case STATE_DD:
				fprintf(out, "};\n");
				break;
			}
		}
	}
	*/
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char fixnum_wks[32];
char *fixnum(char *p, int bytes)
{
	int x, l, n;
	int fecked = 0;

	strcpy(fixnum_wks, p);
//	if (dst)
//	{
//		l = strlen(fixnum_wks);
//		fixnum_wks[--l] = '\0';
//	}
	
	l = strlen(fixnum_wks);
	for (x = 0; x < l; x++)
	{
		if (!isxdigit(fixnum_wks[x]) && fixnum_wks[x] != 'h')
		{
			fecked = 1;
			break;
		}
	}
	if (!fecked)
	{
		l = strlen(fixnum_wks);
		if (fixnum_wks[l-1] == 'h')
		{
			fixnum_wks[l-1] = '\0';
			n = strtoul(fixnum_wks, NULL, 16);
			sprintf(fixnum_wks, "0x%x", n);
		}
		else
		{
			sprintf(fixnum_wks, "0x%0*X", bytes, atol(p));
		}
	}
	else
	{
		sprintf(fixnum_wks, "0x%0*X", bytes, atol(p));
	}
return fixnum_wks;
}
//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char *process_dx(char *p, char *type, char *srch, int bytes)
{
	char *q;
	int iter;
	char db_size[12]="";

	if (q=strchr(p, '['))
	{
		char *r;

		r=q+1;
		q = strchr(r, ']');
		if (q)
		{
			*(q-1) = '\0';
			db_size[0] ='0';
			db_size[1] ='x';
			strcpy(db_size+2, r);
			*(q-1) = 'h';

			strcpy(p+2, q+1);
		}
	}


	p = strstr(p, srch);
	assert(p);
	p += 2;

	change_state(STATE_DW);

	iter = 0;
	do
	{
		q = strchr(p, ',');
		if (q)
		{
			*q = '\0';

			if (iter == 0)
			{
				if (label_line)
				{
					if (glob_array)// && (last_state == STATE_DW || last_state == STATE_DB || last_state == STATE_DD))
						fprintf(out, "};\n\n");
					fprintf(out, "unsigned %s %s[%s] =\n\t{", type, curr_label, db_size);

					fprintf(protoout, "unsigned %s %s[%s];\n", type, curr_label, db_size);

					glob_array = 1;
				}
				else
					fprintf(out, "\t");
				array = 1;
			}

			if (glob_array)
				//fprintf(out, "0x%0*X,", bytes, atol(p));
				fprintf(out, "%s,", fixnum(p, bytes));
			else
				//fprintf(out, "//0x%0*X,", bytes, atol(p));
				fprintf(out, "//%s,", fixnum(p, bytes));
			p = q+1;
		}
		else
		{
			if (iter == 0)
			{
				if (label_line)
				{
					if (glob_array)// && (last_state == STATE_DW || last_state == STATE_DB || last_state == STATE_DD))
						fprintf(out, "};\n\n");
					fprintf(out, "unsigned %s %s = ", type, curr_label);

					fprintf(protoout, "unsigned %s %s;\n", type, curr_label);

				glob_array = 0;
				}
				else
					fprintf(out, "\t");
				array = 0;
			}

			if (array)
				//fprintf(out, "0x%0*X,\n", bytes, atol(p));
				fprintf(out, "%s,\n", fixnum(p, bytes));
			else
			{
				if (label_line)
					//fprintf(out, "0x%0*X;\n", bytes, atol(p));
					fprintf(out, "%s;\n", fixnum(p, bytes));
				else
					//fprintf(out, "//0x%0*X;\n", bytes, atol(p));
					fprintf(out, "//%s;\n", fixnum(p, bytes));
			}


			p = NULL;
		}
		iter=1;
	} while (q);

//	newlabel = 0;
return p;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char *process_db(char *p)
{
return process_dx(p, "char ", "db", 2);
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char *process_dw(char *p)
{
return process_dx(p, "short", "dw", 4);
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char *process_dd(char *p)
{
return process_dx(p, "int  ", "dd", 8);
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char *process_label(char *p)
{
	char *q;

	q = strchr(p, ':');
	assert(q);
	*q = '\0';

	if (state != STATE_SUB)
	{
		//promote into subroutine
		change_state(STATE_SUB);

		fprintf(out, "//--------------------------------------------------------------------------------------\n");
		fprintf(out, "//\n");
		fprintf(out, "//--------------------------------------------------------------------------------------\n");
		fprintf(out, "void _%s()\n{\n", p);
	}

	fprintf(out, "\n%s:\n", p);

return q+1;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char *process_data(char *p)
{
	char *q;

	q = strchr(p, ':');
	assert(q);
	*q = '\0';

	strcpy(curr_label, p);
	array = 0;
//	newlabel = 1;
return q+1;
}


//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char *process_subr(char *p)
{
	char *q;

	change_state(STATE_SUB);

	q = strchr(p, ':');
	assert(q);
	*q = '\0';

	fprintf(out, "//--------------------------------------------------------------------------------------\n");
	fprintf(out, "//\n");
	fprintf(out, "//--------------------------------------------------------------------------------------\n");
	fprintf(out, "void _%s()\n{\n", p);

	p = q+1;
return p;
}

typedef struct
{
	char *reg;
	char *rep;
} REG_THING;

REG_THING regs[]=
{
	{"af\'","exregs.af"},
	{"af","regs.af"},
	{"bc)","regs.bc"},
	{"de)","regs.de"},
	{"hl)","regs.hl"},
	{"sp)","regs.sp"},
	{"bc","regs.bc"},
	{"de","regs.de"},
	{"hl","regs.hl"},
	{"sp","regs.sp"},
	{"ix","ixiyregs.ix"},
	{"iy","ixiyregs.iy"},
	{"a","regs.a"},
	{"f","regs.f"},
	{"b","regs.b"},
	{"c","regs.c"},
	{"d","regs.d"},
	{"e","regs.e"},
	{"h","regs.h"},
	{"l","regs.l"},
	{"i","regs.i"},
	{"r","regs.r"},

	{NULL,NULL},
};

char *find_reg(char *p)
{
	REG_THING *r = regs;

	int len = strlen(p);

	while (r->reg)
	{
		//if (!strncmp(p, r->reg, strlen(r->reg)))
		if (!strcmp(p, r->reg))
			return r->rep;
		r++;
	}
return NULL;
}

char reg_dst_wks[256];
char reg_src_wks[256];

char *get_reg(char *p, char *dest)
{
	int dst;
	char *reg;

	dst = 0;
	if (p[0] == '(')
	{
		p++;
		dst = 1;
	}

	reg = find_reg(p);
	if (reg)
	{
		if (dst)
		{
			sprintf(dest, "memory[%s]", reg);
		}
		else
		{
			sprintf(dest, "%s", reg);
		}
	}
	else
	{
		int x, l, n;
		int fecked = 0;

		strcpy(dest, p);
		if (dst)
		{
			l = strlen(dest);
			dest[--l] = '\0';
		}
		
		l = strlen(dest);
		for (x = 0; x < l; x++)
		{
			if (!isxdigit(dest[x]) && dest[x] != 'h')
			{
				fecked = 1;
				break;
			}
		}
		if (!fecked)
		{
			l = strlen(dest);
			if (dest[l-1] == 'h')
			{
				dest[l-1] = '\0';
				n = strtoul(dest, NULL, 16);
				sprintf(dest, "0x%x", n);
			}
			
		}
		else
		{
			//ok, do we have a sum
			l = strlen(dest);
			if (dest[l-1] == 'h')
			{
				for (x = l-2; x > 0; x--)
				{
					if (!isxdigit(dest[x]))
					{
						//what is it?

						if (dest[x] == '+')
						{
							dest[l-1] = '\0';
							n = strtoul(dest+x+1, NULL, 16);
							sprintf(dest+x+1,"0x%x", n);
						}

						break;
					}
				}
			}
		}
	}

return dest;
}

char *get_dst(char *p)
{
return get_reg(p, reg_dst_wks);
}

char *get_src(char *p)
{
return get_reg(p, reg_src_wks);
}


//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void get_both(char *p, char **dst, char **src)
{
	char *q;
	q = strchr(p, ',');
	if (q)
	{
		*q = '\0';
		*dst = get_dst(p);
		p = q+1;
		*src = get_src(p);
	}
	else
	{
		sprintf(reg_dst_wks, "regs.a");//implied 'a'
		*dst = reg_dst_wks;
		*src = get_src(p);
	}
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void getretcallflags(char *p, int is_call)
{
	char flags[256];
	char *q;

	if (!isalpha(*p))
		return;

	strcpy(flags, p);
	_strupr(flags);

	if (is_call)
	{
		q = strchr(flags, ',');
		if (!q)
			return;
		*q = '\0';
	}

#ifndef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();\n");
#endif

	if (flags[0] == 'N')
	{
		fprintf(out, 
			"\tif (!(regs.f & FLAGS_%sF))\n"
			"\t\t", flags+1);
	}
	else
	{
		fprintf(out, 
			"\tif (regs.f & FLAGS_%sF)\n"
			"\t\t", flags);
	}
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_ld(char *p)
{
	char *dst;
	char *src;
	char *q;

	q = strchr(p, ',');
	assert(q);
	*q = '\0';
	dst = get_dst(p);
	p = q+1;
	src = get_src(p);

	fprintf(out, "\t%s = %s;", dst, src);

return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_call(char *p)
{
	fprintf(protoout, "void _%s(void);\n", p);
	getretcallflags(p, 1);
	fprintf(out, "\t_%s();", p);
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_ldir(char *p)
{
	fprintf(out,
		"\twhile (regs.bc--)\n"
		"\t\tmemory[regs.de++] = memory[regs.hl++];");
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_lddr(char *p)
{
	fprintf(out,
		"\twhile (regs.bc--)\n"
		"\t\tmemory[regs.de--] = memory[regs.hl--];");
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_inc(char *p)
{
	char *reg = get_src(p);
	fprintf(out, "\t%s++;", reg);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_dec(char *p)
{
	char *reg = get_src(p);
	fprintf(out, "\t%s--;", reg);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_neg(char *p)
{
	char *reg = get_src(p);

	if (reg[0] == '\0')
		fprintf(out, "\tregs.a = -regs.a;", reg, reg);
	else
		fprintf(out, "\t%s = -%s;", reg, reg);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_add(char *p)
{
	char *src;
	char *dst;
	get_both(p, &dst, &src);
	fprintf(out, "\t%s += %s;", dst, src);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_sub(char *p)
{
	char *src;
	char *dst;
	get_both(p, &dst, &src);
	fprintf(out, "\t%s -= %s;", dst, src);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_sbc(char *p)
{
	char *src;
	char *dst;
	get_both(p, &dst, &src);
	fprintf(out, "\t%s -= %s + (regs.f&FLAGS_CF);", dst, src);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_adc(char *p)
{
	char *src;
	char *dst;
	get_both(p, &dst, &src);
	fprintf(out, "\t%s += %s + (regs.f&FLAGS_CF);", dst, src);

#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_and(char *p)
{
	char *src;
	char *dst;
	get_both(p, &dst, &src);
	fprintf(out, "\t%s &= %s;", dst, src);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_or(char *p)
{
	char *src;
	char *dst;
	get_both(p, &dst, &src);
	fprintf(out, "\t%s |= %s;", dst, src);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_xor(char *p)
{
	char *src;
	char *dst;
	get_both(p, &dst, &src);
	fprintf(out, "\t%s ^= %s;", dst, src);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_cp(char *p)
{
	char *src;
	char *dst;
	get_both(p, &dst, &src);
	fprintf(out, "\tset_flags(%s, %s);", dst, src);
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_not(char *p)
{
	char *reg;
	reg = get_src(p);
	fprintf(out, "\t%s = ~%s;", reg, reg);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_rst(char *p)
{
	char *reg;
	reg = get_src(p);
	fprintf(out, "\t//rst %s", reg);
return;
}


//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_ret(char *p)
{
	getretcallflags(p, 0);
	fprintf(out, "return;");
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_jr(char *p)
{
	char flags[3];
	char *q;

	q = strchr(p, ',');

	if (!q)
		fprintf(out, "\tgoto %s;\n", p);
	else
	{
		*q = '\0';
		strcpy(flags, p);
		_strupr(flags);

#ifndef GET_FLAGS_AFTER
		fprintf(out, "\tget_flags();\n");
#endif

		if (flags[0] == 'N')
		{
			fprintf(out, 
				"\tif (!(regs.f & FLAGS_%sF))\n"
				"\t\tgoto %s;", flags+1, q+1);
				*q = ',';
		}
		else
		{
			fprintf(out, 
				"\tif (regs.f & FLAGS_%sF)\n"
				"\t\tgoto %s;", flags, q+1);
				*q = ',';
		}
	}

return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_jp(char *p)
{
	char flags[3];
	char *q;

	q = strchr(p, ',');

	if (!q)
		fprintf(out, "\tgoto %s;\n", p);
	else
	{
		*q = '\0';
		strcpy(flags, p);
		_strupr(flags);

#ifndef GET_FLAGS_AFTER
		fprintf(out, "\tget_flags();\n");
#endif

		if (flags[0] == 'N')
		{
			fprintf(out, 
				"\tif (!(regs.f & FLAGS_%sF))\n"
				"\t\tgoto %s;", flags+1, q+1);
				*q = ',';
		}
		else
		{
			fprintf(out, 
				"\tif (regs.f & FLAGS_%sF)\n"
				"\t\tgoto %s;", flags, q+1);
				*q = ',';
		}
	}

return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_ei(char *p)
{
	fprintf(out, "\t//ei");
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_di(char *p)
{
	fprintf(out, "\t//di");
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_im(char *p)
{
	char *reg;
	reg = get_src(p);
	fprintf(out, "\t//im %s", reg);
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_rrc(char *p)
{
	char *reg;
	reg = get_src(p);
	if (reg[0] == '\0')
		fprintf(out, "\trorc(&regs.a);", reg);
	else
		fprintf(out, "\trorc(&%s);", reg);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_rlc(char *p)
{
	char *reg;
	reg = get_src(p);
	if (reg[0] == '\0')
		fprintf(out, "\trolc(&regs.a);", reg);
	else
		fprintf(out, "\trolc(&%s);", reg);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_rr(char *p)
{
	char *reg;
	reg = get_src(p);
	if (reg[0] == '\0')
		fprintf(out, "\tror(&regs.a);", reg);
	else
		fprintf(out, "\tror(&%s);", reg);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_rl(char *p)
{
	char *reg;
	reg = get_src(p);
	if (reg[0] == '\0')
		fprintf(out, "\trol(&regs.a);", reg);
	else
		fprintf(out, "\trol(&%s);", reg);
#ifdef GET_FLAGS_AFTER
	fprintf(out, "\tget_flags();");
#endif
return;
}


//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_djnz(char *p)
{
	fprintf(out, "\tif (--regs.b)\n\t\tgoto %s;", p);
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_push(char *p)
{
	char *reg;
	reg = get_src(p);
	fprintf(out, "\tpush(%s);", reg);
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_nop(char *p)
{
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_pop(char *p)
{
	char *reg;
	reg = get_src(p);
	fprintf(out, "\t%s = pop();", reg);
return;
}


//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_out(char *p)
{
	fprintf(out, "\t//out %s", p);
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_in(char *p)
{
	fprintf(out, "\t//in %s", p);
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_exx(char *p)
{
	fprintf(out, "\texx();");
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_ex(char *p)
{
	char *src;
	char *dst;
	get_both(p, &dst, &src);
	fprintf(out, "\t{\n");
	fprintf(out, "\tint tmp=%s;\n", dst);
	fprintf(out, "\t%s = %s;\n", dst, src);
	fprintf(out, "\t%s = tmp;\n", src);
	fprintf(out, "\t}");
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_scf(char *p)
{
	fprintf(out, "\tregs.f |= FLAGS_CF;");
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_ccf(char *p)
{
	fprintf(out, "\tregs.f &= ~FLAGS_CF;");
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_sra(char *p)
{
	char *reg;
	reg = get_src(p);
	fprintf(out, "\t%s = (int)%s>>1;", reg, reg);
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_sla(char *p)
{
	char *reg;
	reg = get_src(p);
	fprintf(out, "\t%s = (int)%s<<1;", reg, reg);
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_srl(char *p)
{
	char *reg;
	reg = get_src(p);
	fprintf(out, "\t%s = %s>>1;", reg, reg);
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_sll(char *p)
{
	char *reg;
	reg = get_src(p);
	fprintf(out, "\t%s = %s<<1;", reg, reg);
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_daa(char *p)
{
	fprintf(out, "\tassert(0);");
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_bit(char *p)
{
	char *src;
	char *dst;
	get_both(p, &dst, &src);
	fprintf(out, "\tif (%s & (1<<%s))\n"
		         "\t\tregs.f |= FLAGS_CF;\n"
				 "\telse\n"
				 "\t\tregs.f &= ~FLAGS_CF;\n" , dst, src);
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_res(char *p)
{
	char *src;
	char *dst;
	get_both(p, &dst, &src);
	fprintf(out, "\t%s &= ~(1<<%s);\n", dst, src);
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_set(char *p)
{
	char *src;
	char *dst;
	get_both(p, &dst, &src);
	fprintf(out, "\t%s |= 1<<%s;\n", dst, src);
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void process_null(char *p)
{
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
typedef struct
{
	char *ins;
	int len;
	void (*decode)(char *);
} INS_JUMP;

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char *process_code(char *p, char *q)
{
	INS_JUMP *j;
	int done;
	static INS_JUMP jump_tab[]=
	{
		{"segment", 7, process_null},
		{"ldir", 4, process_ldir},
		{"lddr", 4, process_lddr},
		{"djnz", 4, process_djnz},
		{"call", 4, process_call},
		{"push", 4, process_push},
		{"exx", 3, process_exx},
		{"inc", 3, process_inc},
		{"dec", 3, process_dec},
		{"neg", 3, process_neg},
		{"add", 3, process_add},
		{"sub", 3, process_sub},
		{"xor", 3, process_xor},
		{"and", 3, process_and},
		{"not", 3, process_not},
		{"ret", 3, process_ret},
		{"rst", 3, process_rst},
		{"pop", 3, process_pop},
		{"nop", 3, process_nop},
		{"out", 3, process_out},
		{"sbc", 3, process_sbc},
		{"adc", 3, process_adc},
		{"scf", 3, process_scf},
		{"ccf", 3, process_ccf},
		{"sra", 3, process_sra},
		{"sla", 3, process_sla},
		{"srl", 3, process_srl},
		{"sll", 3, process_sll},
		{"daa", 3, process_daa},
		{"end", 3, process_null},
		{"org", 3, process_null},
		{"rrc", 3, process_rrc},
		{"rlc", 3, process_rlc},
		{"bit", 3, process_bit},
		{"res", 3, process_res},
		{"set", 3, process_set},
		{"rr", 2, process_rr},
		{"rl", 2, process_rl},
		{"or", 2, process_or},
		{"ld", 2, process_ld},
		{"jr", 2, process_jr},
		{"jp", 2, process_jp},
		{"cp", 2, process_cp},
		{"ei", 2, process_ei},
		{"di", 2, process_di},
		{"im", 2, process_im},
		{"in", 2, process_in},
		{"ex", 2, process_ex},

		{NULL, 0, NULL},
	};

	if (state != STATE_SUB)//don't output code in non-code area
		return NULL;

	j = jump_tab;
	done = 0;
	while (j->ins)
	{
		if (!strncmp(p, j->ins, j->len))
		{
			j->decode(p+j->len);
			done = 1;
			break;
		}
		j++;
	}

	if (!done)
	{
		fprintf(out, "UNKNOWN INSTRUCTION %s;\n", p);
		fflush(out);
		fflush(protoout);
		assert(0);
	}
	else
	{
		fprintf(out, "\t// %s\n", q);
	}

return NULL;
}

//--------------------------------------------------------------------------------------
// Completely strip whitespace from a line
//--------------------------------------------------------------------------------------
void stripit(char *line)
{
	char *s;
	char *d;
	int l;

	l = strlen(line);

	s = line;
	d = line;

	while (l--)
	{
		if (*s == 0xd || *s == 0xa || *s == ';')
		{
			*d = '\0';
			break;
		}

		if (!isspace(*s))
			*d++ = *s;
		s++;
	}
return;
}

//--------------------------------------------------------------------------------------
// Strip only EOL whitespace
//--------------------------------------------------------------------------------------
void stripit2(char *line)
{
	char *s;
	char *d;
	int l;

	l = strlen(line);

	s = line;
	d = line;

	while (l--)
	{
		if (*s == 0xd || *s == 0xa || *s == ';')
		{
			*d = '\0';
			break;
		}

//		if (!isspace(*s))
			*d++ = *s;
		s++;
	}
return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char infile[_MAX_PATH];
char coutfile[_MAX_PATH];
char houtfile[_MAX_PATH];

int main(int argc, char **argv)
{
	FILE *f;
	int quit;
	char *p;
	char line[2050];
	char line2[2050];
	int full_debug;

	full_debug = 0;
	sprintf(infile, "quaz.asm");
	sprintf(coutfile, "quaz.c");
	sprintf(houtfile, "quaz.h");

	if (argc > 1)
	{
		if (!strcmp(argv[1], "-d"))
			full_debug = 1;
		else if (!strcmp(argv[1], "-f"))
		{
			sprintf(infile, "%s.asm", argv[2]);
			sprintf(coutfile, "%s.c", argv[2]);
			sprintf(houtfile, "%s.h", argv[2]);
		}
	}

	f = fopen(infile, "rb");
	out = fopen(coutfile, "w");
	protoout = fopen(houtfile, "w");

	state = STATE_NONE;

	write_file_header("quaz");

	do
	{
		fgets(line, 2048, f);
		label_line = 0;
		if (feof(f)) break;
		strcpy(line2, line);
		stripit(line);
		stripit2(line2);

		p = line;
		quit = 0;
		do
		{
			switch (id_line(p, strlen(p)))
			{
				case TYP_DB:
					p = process_db(p);
					break;

				case TYP_DW:
					p = process_dw(p);
					break;

				case TYP_DD:
					p = process_dd(p);
					break;

				case TYP_LABEL:
					p = process_label(p);
					break;

				case TYP_SUB:
					p = process_subr(p);
					break;

				case TYP_CODE:
					p = process_code(p, line2);
					break;

				case TYP_DATA:
					p = process_data(p);
					break;

				case TYP_DONE:
				default:
					quit = 1;
			}

		} while (!quit && p && *p);

		if (full_debug)
			fprintf(out, "\t// %s\n", line2);

	} while (1);

	if (glob_array)
		fprintf(out, "};\n");
	fprintf(out, "\n");

	fclose(f);
	fclose(out);
	fclose(protoout);

	//while (!_kbhit());
/*
	f = fopen(coutfile, "rb");
	out = fopen("C:\\tmp.txt", "wb");
	if (f && out)
	{
		do
		{
			fgets(line, 2048, f);
			if (feof(f)) break;
			fputs(line, 2048, out);
		} while (1);
		fclose(f);
	}
*/
return 0;
}
