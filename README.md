# SpecConv

## A ZX Spectrum Emulator with built-in code and graphics reverse engineering tools, with the aim of exporting the results to assembler and C.
By Jim Shaw.

Main features:  
A full ZX Spectrum 48K and 128K Emulator able to play most Spectrum games from tape or snapshot.  
Static and dynamic analysis of the game being played.  
Graph of game control flow.  
A graphics explorer.  
Full interactive disassembler able to define labels and datatypes.  
SQLite storage for persistent game analysis.  
JSON database import/export.  
Console control mode with built-in assembler.  
Disassembly to Z80 assembler.  
Decompilation to C.  

Most of these features are experimental and work in progress.


### Binaries built from the following sources are included in this project:  

cJSON https://github.com/DaveGamble/cJSON  
Copyright 2009-2017 Dave Gamble and cJSON contributors. MIT License.

SQLite https://www.sqlite.org  
Public Domain.

Kubazip https://github.com/kuba--/zip  
Public Domain.  

Graphviz https://graphviz.org/  
Common Public License V1.0.

Many, many thanks to the people who have contributed to these projects.

### Binaries of ZX Spectrum ROMs for 48K and 128K are included.
Amstrad have kindly given their permission for the redistribution of their copyrighted material but retain that copyright - see the LICENSE.md file.

Released under the MIT License.

>Copyright 2020 James Shaw. All Rights Reserved.
>
>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
>
>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
>
>THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.