//--------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//--------------------------------------------------------------------------------------
#include <windows.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

//#include "dbgmem.h"
#include "logger.h"
#include "machine.h"
#include "dasm.h"
#include "daos.h"
#include "trace.h"
#include "coverage.h"
#include "labels.h"
#include "gfxview.h"
#include "basic.h"
#include "disview.h"
#include "emu.h"

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void preamble(FILE *f)
{
	fputs(
		"\
#include <windows.h>\n\
#include <process.h>\n\
\n\
static unsigned char memory[65536];\n\
static unsigned short *memoryw = (unsigned short *)memory;\n\
static unsigned short af,bc,de,hl,sp,ix,iy;\n\
static unsigned char i,r;\n\
static unsigned short af_,bc_,de_,hl_;\n\
static unsigned char Z,C,S,P;\n\
static unsigned char im_,iff1,iff2;\n\
volatile char hlt;\n\
\n\
#define a (*((unsigned char *)&af+1))\n\
#define f (*(unsigned char *)&af)\n\
#define b (*((unsigned char *)&bc+1))\n\
#define c (*(unsigned char *)&bc)\n\
#define d (*((unsigned char *)&de+1))\n\
#define e (*(unsigned char *)&de)\n\
#define h (*((unsigned char *)&hl+1))\n\
#define l (*(unsigned char *)&hl)\n\
#define ixh (*((unsigned char *)&ix+1))\n\
#define ixl (*(unsigned char *)&ix)\n\
#define iyh (*((unsigned char *)&iy+1))\n\
#define iyl (*(unsigned char *)&iy)\n\
\n\
static const unsigned char parity[256]=\n\
{	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,\n\
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,\n\
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,\n\
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,\n\
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,\n\
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,\n\
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,\n\
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,\n\
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,\n\
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,\n\
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,\n\
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,\n\
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,\n\
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,\n\
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,\n\
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,\n\
};\n\
\n\
static void exx_af_af_(void){unsigned short tmp=af;af=af_;af_=tmp;}\n\
static void exx_sp_hl(void){unsigned short tmp=memoryw[sp];memoryw[sp]=hl;hl=tmp;}\n\
static void exx_sp_ix(void){unsigned short tmp=memoryw[sp];memoryw[sp]=ix;ix=tmp;}\n\
static void exx_sp_iy(void){unsigned short tmp=memoryw[sp];memoryw[sp]=iy;iy=tmp;}\n\
static void exx_de_hl(void){unsigned short tmp=de;de=hl;hl=tmp;}\n\
static void exx(void){unsigned short tmp=bc;bc_=bc;bc_=tmp;tmp=de;de_=de;de_=tmp;tmp=hl;hl_=hl;hl_=tmp;}\n\
static void push(unsigned short p){sp-=2;memoryw[sp]=p;}\n\
static unsigned short pop(void){unsigned short tmp=memory[sp];sp+=2;return tmp;}\n\
static unsigned char rlc(unsigned char v){C=v>>7;return (v<<1)|C;}\n\
static unsigned char rrc(unsigned char v){C=v&1; return (v>>1)|(C<<7);}\n\
static unsigned char rl(unsigned char v){unsigned char OC=C;C=v>>7;return (v<<1)|OC;}\n\
static unsigned char rr(unsigned char v){unsigned char OC=C;C=v&1;return (v>>1)|(OC<<7);}\n\
static unsigned char sla(unsigned char v){C=v>>7;return v<<1;}\n\
static unsigned char sra(unsigned char v){C=v&1;return (v>>1)|(v&0x80);}\n\
static unsigned char sll(unsigned char v){C=v>>7;return (v<<1)|1;}\n\
static unsigned char srl(unsigned char v){C=v&1;return v>>1;}\n\
static unsigned char daa(unsigned char v){return v;}\n\
static void im(char m){im_=m;}\n\
static void di(void){iff1=iff2=0;}\n\
static void ei(void){iff1=iff2=1;}\n\
static void reti(void){/*iff1=iff2;*/}\n\
static void retn(void){iff1=iff2;}\n\
static void halt(void){hlt=1;while(hlt) YieldProcessor();}\n\
static unsigned char in(unsigned char p){p;return 0xFF;}\n\
static void out(unsigned char p, unsigned char v){p;v;}\n\
\n\
static void updatescreen(void){}\n\
static void init_memory(void);\n\
static void run(void *);\n\
int main(void)\n\
{\n\
	init_memory();\n\
	HANDLE t = _beginthread(run,0,NULL);\n\
	for (;;) {\n\
		Sleep(50);\n\
		if (iff1) {\n\
		SuspendThread(t);\n\
		iff1 = iff2 = 0;\n\
		if (im_ == 0 || im_ == 1) _0x0038();\n\
		else ISR();//memoryw[((unsigned short)i<<8)|0xff]\n\
		hlt=0;\n\
		ResumeThread(t);\n\
		}\n\
	}\n\
	return 0;\n\
}\n\n\
", f);
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char *Csafe(char *name)
{
	static char csafe[1000];

	strcpy_s(csafe, 1000, name);

	if (isdigit((unsigned char)csafe[0])) csafe[0] = '_';

	for (unsigned int i = 0; i < strlen(csafe); i++)
	{
		if (ispunct((unsigned char)csafe[i])) csafe[i] = '_';
	}

	return csafe;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
typedef enum
{
	CR_IS_DJNZ_TARGET = 1,
	CR_IS_DJNZ = 2,
} CROWFLAGS;

typedef struct
{
	unsigned short address;
	ZX_LABEL *label;
	char *code;
	char *xrefs;
	char *comment;
	char *autocomment;
	CROWFLAGS flags;
	unsigned short jumpto;
} C_ROW;

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void GetC(DB_RES *labelres, DB_RES *typeres)
{
	ZX_LABEL *label = (ZX_LABEL *)labelres->rows;
	ZX_LABEL *finallabel = label + labelres->count;

	static char code[10000000];
	static char data[10000000];
	static DASM_RES dis[65536];
	static C_ROW rows[65536];
	int n_rows = 0;
	int n_dasm = 0;
	code[0] = '\0';
	data[0] = '\0';
	char *tc = code;
	char *td = data;

	td += sprintf_s(td, 1000, "static void init_memory(void)\n{\n");

	int pc = 0;

	C_ROW row = { 0 };

	static ZX_TYPE *types[65536];
	memset(types, 0, sizeof types);
	{
		//todo: optimise this
		ZX_TYPE *type = (ZX_TYPE *)typeres->rows;

		static ZX_TYPE builtin_code_type = { 0 };
		builtin_code_type.address = 0;
		builtin_code_type.size = 16384;
		builtin_code_type.type = CODE;
		builtin_code_type.source = SR_BUILTIN;

		static ZX_TYPE builtin_default_type = { 0 };
		builtin_default_type.address = 16384;
		builtin_default_type.size = 49152;
		builtin_default_type.type = DEFB;
		builtin_default_type.source = SR_BUILTIN;

		for (int i = 0; i < 16384; i++)
			types[i] = &builtin_code_type;
		for (int i = 16384; i < 65536; i++)
			types[i] = &builtin_default_type;

		int count = typeres->count;
		while (count--)
		{
			for (unsigned int i = type->address; i < min(65536, type->address + type->size); i++)
				types[i] = type;
			type++;
		}
	}

	pc = 0;
	char in_sub = 0;
	while (pc < 65536)
	{
		memset(&row, 0, sizeof row);

		//address
		row.address = (unsigned short)pc;

		int size = 0;

		//comment
		row.comment = NULL;

		if (types[pc]->type == CODE)
		{
			DASM_RES comment;
			DASM_RES asm;

			DAsmEx((unsigned short)pc, &comment);
			size = DCEx((unsigned short)pc, &asm);
			dis[n_dasm++] = asm;

			row.comment = _strdup(comment.instr);

			if (asm.bytes[0] == 0x10) //16 djnz
			{
				for (int x = n_rows - 1; x >= 0; x--)
				{
					if (rows[x].address == asm.target)
					{
						rows[x].flags |= CR_IS_DJNZ_TARGET;
						row.flags |= CR_IS_DJNZ;
						break;
					}
				}
			}

			//if this instruction is a jump, keep the destination
			switch (asm.type)
			{
				case M_CDSTR:
				//case M_CDST:
					row.jumpto = (unsigned short)asm.target;
					break;
			}

			if (asm.flags.in)
			{
				//this instruction needs flags, work out where to get them from
				for (int x = n_dasm - 2; x >= 0; x--)
				{
					free(row.comment);
					row.comment = _strdup("needs flags");
					unsigned char mask = dis[x].flags.out & asm.flags.in;
					if (mask)
					{
						//dis[x] instruction is the supplier of flags to this instruction
						if ((mask & FLAGS_ZF && !strstr(dis[x].instr, "Z=")))
						{
							if (dis[x].instr[0] != 'Z')
							{
								char tmp[20];
								sprintf_s(tmp, 20, "Z=(%c==0);", dis[x].instr[0]);
								strcat_s(dis[x].instr, 100, tmp);
							}
						}
												//dis[x] instruction is the supplier of flags to this instruction
						if ((mask & FLAGS_SF) && !strstr(dis[x].instr, "S="))
						{
							char tmp[20];
							sprintf_s(tmp, 20, "S=(%c&0x80);", dis[x].instr[0]);
							strcat_s(dis[x].instr, 100, tmp);
						}

						if ((mask & FLAGS_CF) && !strstr(dis[x].instr, "C="))
						{
							if (dis[x].instr[0] != 'C'
								&& !strstr(dis[x].instr, "rl(")
								&& !strstr(dis[x].instr, "rr(")
								&& !strstr(dis[x].instr, "rlc(")
								&& !strstr(dis[x].instr, "rrc(")
								&& !strstr(dis[x].instr, "sla(")
								&& !strstr(dis[x].instr, "sra(")
								&& !strstr(dis[x].instr, "sll(")
								&& !strstr(dis[x].instr, "srl(")
								)
							{
								char tmp[40];
								if (strstr(dis[x].instr, "&=") || strstr(dis[x].instr, "^=") || strstr(dis[x].instr, "|="))
								{
									//and/xor/or clear the CF
									sprintf_s(tmp, 40, "C=0;");
								}
								else
								{
									//cp is like sub
									if (dis[x].instr[0] == 'Z' && strstr(dis[x].instr, "=="))
									{
										char *cmp = strstr(dis[x].instr, "==");
										char *e = strstr(dis[x].instr, ")");
										char t2[100]; int k = 0;
										for (char *c = cmp + 2; c < e; c++)
											t2[k++] = *c;
										t2[k++] = '\0';
										sprintf_s(tmp, 40, "C=(%c<%s);", *(cmp - 1), t2);
									}
									else if (strncmp(dis[x].instr, "hl+=", 4) == 0)
									{
										char *sxt = strstr(dis[x].instr, "hl+=");
										if (*(sxt+6)=='+')
											sprintf_s(tmp, 40, "C=((int)hl+%c%c+C>=0x10000);", *(sxt + 4), *(sxt + 5));
										else
											sprintf_s(tmp, 40, "C=((int)hl+%c%c>=0x10000);", *(sxt+4), *(sxt + 5));
									}
									else if (strncmp(dis[x].instr, "hl-=", 4) == 0)
									{
										char *sxt = strstr(dis[x].instr, "hl-=");
										sprintf_s(tmp, 40, "C=((int)hl<(int)%c%c+C);", *(sxt + 4), *(sxt + 5));
									}
									else
									{
										sprintf_s(tmp, 40, "C=??;");
									}
								}
								strcat_s(dis[x].instr, 100, tmp);
							}
						}

						if ((mask & FLAGS_PF) && !strstr(dis[x].instr, "P="))
						{
							char tmp[30];
							sprintf_s(tmp, 30, "P=parity[%c];", dis[x].instr[0]);
							strcat_s(dis[x].instr, 100, tmp);
						}

						mask &= ~(FLAGS_CF | FLAGS_ZF | FLAGS_SF | FLAGS_PF);

						assert(mask == 0);
						break;
					}
				}
			}

			row.code = dis[n_dasm-1].instr;

			//autocomment
			row.autocomment = dis[n_dasm - 1].autocomment[0] != '\0' ? dis[n_dasm - 1].autocomment : NULL;
		}
		else if (types[pc]->type == DEFB || types[pc]->type == DEFM || types[pc]->type == STRUCT)
		{
			//code
			static char defb[100];
			unsigned char b = peek_fast((unsigned short)pc);
			sprintf_s(defb, 100, "memory[0x%04X]=0x%02X;", pc, b);
			row.code = defb;

			//autocomment
			static char autoc[100];
			if (b >= 32 && b <= 127)
			{
				sprintf_s(autoc, 100, "%c", b);
				row.autocomment = autoc;
			}
			else if (b >= 165)
			{
				row.autocomment = (char *)get_keyword_txt(b);
			}
			else
			{
				row.autocomment = NULL;
			}

			size = 1;
		}
		else if (types[pc]->type == DEFN)
		{
			//code
			static char defn[500];
			unsigned char bytes[5];
			for (int i = 0; i < 5; i++)
				bytes[i] = peek_fast((unsigned short)(pc + i));
			sprintf_s(defn, 500, "memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X;", pc, bytes[0], pc + 1, bytes[1], pc + 2, bytes[2], pc + 3, bytes[3], pc + 4, bytes[4]);
			row.code = defn;

			//autocomment
			static char number[100];
			sprintf_s(number, 100, "%g", decodeFP(bytes));
			row.autocomment = number;

			size = 5;
		}
		else if (types[pc]->type == DEFFPSTK)
		{
			//first byte / 0x40 + 1 is how many bytes follow

			//code
			static char defn[500];
			unsigned char bytes[6] = { 0 };
			bytes[0] = peek_fast((unsigned short)pc);
			unsigned char extrabytes = (bytes[0] >> 6) + 1;
			for (int i = 1; i < extrabytes; i++)
				bytes[i] = peek_fast((unsigned short)(pc + i));
			switch (extrabytes)
			{
				case 1: sprintf_s(defn, 500, "memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X;", pc, bytes[0], pc + 1, bytes[1]);
				case 2: sprintf_s(defn, 500, "memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X;", pc, bytes[0], pc + 1, bytes[1], pc + 2, bytes[2]);
				case 3: sprintf_s(defn, 500, "memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X;", pc, bytes[0], pc + 1, bytes[1], pc + 2, bytes[2], pc + 3, bytes[3]);
				case 4: sprintf_s(defn, 500, "memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X; memory[0x%04X]=0x%02X;", pc, bytes[0], pc + 1, bytes[1], pc + 2, bytes[2], pc + 3, bytes[3], pc + 4, bytes[4]);
			}
			row.code = defn;
			size = (bytes[0] >> 6) + 2;

			//autocomment
			static char number[100];
			if ((bytes[0] & 0x3F) == 0)
			{
				bytes[1] += 0x50;
				sprintf_s(number, 100, "%g", decodeFP(bytes + 1));
			}
			else
			{
				bytes[0] &= 0x3F;
				bytes[0] += 0x50;
				sprintf_s(number, 100, "%g", decodeFP(bytes));
			}
			row.autocomment = number;
		}
		else if (types[pc]->type == DEFC)
		{
			//code
			static char defb[100];
			unsigned char b = peek_fast((unsigned short)pc);
			sprintf_s(defb, 100, "memory[0x%04X]=0x%02X;", pc, b);
			row.code = defb;

			//autocomment
			row.autocomment = (char *)get_calculator_label(b);

			size = 1;
		}
		else if (types[pc]->type == DEFW)
		{
			//code
			static char defw[100];
			unsigned short b = peek_fastw((unsigned short)pc);
			sprintf_s(defw, 100, "memoryw[0x%04X]=0x%04X;", pc, b);
			row.code = defw;

			//autocomment
			row.autocomment = pc_to_label(b, 0, 0, 0);

			size = 2;
		}
		else if (types[pc]->type == DEFS)
		{
			ZX_TYPE *curr_type = types[pc];
			int defb = pc;
			while (defb < 65536 && types[defb++] == curr_type)
				size++;

			//code
			static char defs[100];
			sprintf_s(defs, 100, "//defs %d", size);
			row.code = defs;

			//autocomment
			row.autocomment = NULL;
		}
		else if (types[pc]->type == GFX)
		{
			ZX_TYPE *curr_type = types[pc];

			size = get_gfx_size(curr_type->gfxtype, curr_type->gfxwidth, curr_type->gfxheight);

			//code
			static char defs[100];
			sprintf_s(defs, 100, "//gfx %d x %d %s (%d bytes) ", curr_type->gfxwidth, curr_type->gfxheight, get_gfx_type(curr_type->gfxtype), size);
			row.code = defs;

			//autocomment
			row.autocomment = NULL;
		}
		else
		{
			assert(0);
		}

		//label
		//step through the labels as we step through the code, rather than looking from the start.
		while (label != NULL && pc > label->address)
		{
			label++;
			if (label == finallabel)
			{
				label = NULL;
				break;
			}
		}

		if (label != NULL)
		{
			if (pc == label->address)
				row.label = label;
			else
				row.label = NULL;
		}
		else
		{
			row.label = NULL;
		}

		if (types[pc]->type == CODE)
		{
			rows[n_rows++] = row;
		}
		else
		{
			if (row.label != NULL && row.label->address == pc)// && row.label->type == LT_DATA)
				td += sprintf_s(td, 1000, "#define %s memory[0x%04X]\n", Csafe(row.label->name), row.address);

			td += sprintf_s(td, 1000, "%s\t\t//0x%04X %s %s", row.code, row.address, row.autocomment ? row.autocomment : "", row.comment ? row.comment : "");
			td += sprintf_s(td, 1000, "\n");
			in_sub = 0;
		}

		pc += size;
	}
	td += sprintf_s(td, 1000, "}\n\n");

	ZX_LABEL *r = labelres->rows;
	for (int i = 0; i < labelres->count; i++, r++)
	{
		if (r->address > 0x38) break;
		if ((r->address & 0x38) == r->address)
			tc += sprintf_s(tc, 1000, "#define _0x%04X() %s()\n", r->address, Csafe(r->name));
	}
	tc += sprintf_s(tc, 1000, "\n");

	tc += sprintf_s(tc, 1000, "static void run(void *p)\n{\n\tp;\n");
	tc += sprintf_s(tc, 1000, "\t_0x0000();\n");
	tc += sprintf_s(tc, 1000, "}\n\n");

	unsigned short max_jump = 0;
	for (int i = 0; i < n_rows; i++)
	{
		row = rows[i];

		//label part

		if (in_sub)
		{
			if (row.jumpto > max_jump)
			{
				max_jump = row.jumpto;
				//elog("%04X jumps to %04X\n", row.address, max_jump);
			}
		}

		if (row.flags & CR_IS_DJNZ_TARGET)
		{
			tc += sprintf_s(tc, 1000, "\tdo {\n");
		}
		else if (!in_sub && row.label != NULL && row.label->address == row.address /* && (row.label->type == LT_SUB || row.label->type == LT_JUMP)*/)
		{
			tc += sprintf_s(tc, 1000, "void %s(void)\n{\n", Csafe(row.label->name));
			in_sub = 1;
			max_jump = 0;
		}
		else
		{
			if (row.label != NULL && row.label->address == row.address)
				tc += sprintf_s(tc, 1000, "%s:\n", Csafe(row.label->name));
		}
		//tc += sprintf_s(tc, 1000, "_0x%04X:\n", row.address);

		//code part
		tc += sprintf_s(tc, 1000 , "\t");
		if (row.flags & CR_IS_DJNZ)
		{
			tc += sprintf_s(tc, 1000, "} while (--b);//%s", row.code);
		}
		else
		{
			tc += sprintf_s(tc, 1000, "%-45s", row.code);
		}
		tc += sprintf_s(tc, 1000, "//0x%04X %s %s", row.address, row.autocomment ? row.autocomment : "", row.comment ? row.comment : "");

		tc += sprintf_s(tc, 1000, "\n");

		if (in_sub && max_jump <= row.address && (strcmp(row.code, "return;") == 0 || strncmp(row.code, "goto ", 5) == 0))
		{
			tc += sprintf_s(tc, 1000, "}\n\n");
			in_sub = 0;
		}
	}

	for (int i = 0; i < n_rows; i++)
	{
		row = rows[i];
		if (row.comment) free(row.comment);
	}

	FILE *f;
	fopen_s(&f, "quazatron.c", "w");
	if (f != NULL)
	{
		preamble(f);
		fputs(data, f);
		fputs(code, f);
		fclose(f);
		ShellExecute(NULL, "open", "code.exe", "quazatron.c", NULL, SW_SHOW);
	}
}

