//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>

#include "daos.h"
#include "coverage.h"
#include "trace.h"
#include "labels.h"
#include "logger.h"
#include "dbgmem.h"
#include "basic.h"

typedef struct
{
	unsigned short address;
	char *const label;
} ROM_LABEL;

const ROM_LABEL rom_labels[]=
{
	{0x0000, "START"},
	{0x0008, "ERROR-1"},
	{0x0010, "PRINT-A-1"},
	{0x0018, "GET-CHAR"},
	{0x001C, "TEST-CHAR"},
	{0x0020, "NEXT-CHAR"},
	{0x0028, "FP-CALC"},
	{0x0030, "BC-SPACES"},
	{0x0038, "MASK-INT"},
	{0x0048, "KEY-INT"},
	{0x0053, "ERROR-2"},
	{0x0055, "ERROR-3"},
	{0x0066, "RESET"},
	{0x0070, "NO-RESET"},
	{0x0074, "CH-ADD+1"},
	{0x0077, "TEMP-PTR1"},
	{0x0078, "TEMP-PTR2"},
	{0x007D, "SKIP-OVER"},
	{0x0090, "SKIPS"},
	{0x028E, "KEY-SCAN"},
	{0x0296, "KEY-LINE"},
	{0x029F, "KEY-3KEYS"},
	{0x02A1, "KEY-BITS"},
	{0x02AB, "KEY-DONE"},
	{0x02BF, "KEYBOARD"},
	{0x02C6, "K-ST-LOOP"},
	{0x02D1, "K-CH-SET"},
	{0x02F1, "K-NEW"},
	{0x0308, "K-END"},
	{0x0310, "K-REPEAT"},
	{0x031E, "K-TEST"},
	{0x032C, "K-MAIN"},
	{0x0333, "K-DECODE"},
	{0x0341, "K-E-LET"},
	{0x034A, "K-LOOK-UP"},
	{0x034F, "K-KLC-LET"},
	{0x0364, "K-TOKENS"},
	{0x0367, "K-DIGIT"},
	{0x0382, "K-8-&-9"},
	{0x0389, "K-GRA-DGT"},
	{0x039D, "K-KLC-DGT"},
	{0x03B2, "K-@-CHAR"},
	{0x03B5, "BEEPER"},
	{0x03D1, "BE-IX+3"},
	{0x03D2, "BE-IX+2"},
	{0x03D3, "BE-IX+1"},
	{0x03D4, "BE-IX+0"},
	{0x03D6, "BE-H&L-LP"},
	{0x03F2, "BE-AGAIN"},
	{0x03F6, "BE-END"},
	{0x03F8, "BEEP"},
	{0x0425, "BE-I-OK"},
	{0x0427, "BE-OCTAVE"},
	{0x046C, "REPORT-B"},
	{0x04C2, "SA-BYTES"},
	{0x04D0, "SA-FLAG"},
	{0x04D8, "SA-LEADER"},
	{0x04EA, "SA-SYNC-1"},
	{0x04F2, "SA-SYNC-2"},
	{0x04FE, "SA-LOOP"},
	{0x0505, "SA-LOOP-P"},
	{0x0507, "SA-START"},
	{0x050E, "SA-PARITY"},
	{0x0511, "SA-BIT-2"},
	{0x0514, "SA-BIT-1"},
	{0x051A, "SA-SET"},
	{0x051C, "SA-OUT"},
	{0x0525, "SA-8-BITS"},
	{0x053C, "SA-DELAY"},
	{0x053F, "SA/LD-RET"},
	{0x0552, "REPORT-D"},
	{0x0554, "SA/LD-END"},
	{0x0556, "LD-BYTES"},
	{0x056B, "LD-BREAK"},
	{0x056C, "LD-START"},
	{0x0574, "LD-WAIT"},
	{0x0580, "LD-LEADER"},
	{0x058F, "LD-SYNC"},
	{0x05A9, "LD-LOOP"},
	{0x05B3, "LD-FLAG"},
	{0x05BD, "LD-VERIFY"},
	{0x05C2, "LD-NEXT"},
	{0x05C4, "LD-DEC"},
	{0x05C8, "LD-MARKER"},
	{0x05CA, "LD-8-BITS"},
	{0x05E3, "LD-EDGE-2"},
	{0x05E7, "LD-EDGE-1"},
	{0x05E9, "LD-DELAY"},
	{0x05ED, "LD-SAMPLE"},
	{0x0605, "SAVE-ETC"},
	{0x0621, "SA-SPACE"},
	{0x0629, "SA-BLANK"},
	{0x0642, "REPORT-F"},
	{0x0644, "SA-NULL"},
	{0x064B, "SA-NAME"},
	{0x0652, "SA-DATA"},
	{0x0670, "REPORT-2"},
	{0x0672, "SA-V-OLD"},
	{0x0685, "SA-V-NEW"},
	{0x068F, "SA-V-TYPE"},
	{0x0692, "SA-DATA-1"},
	{0x06A0, "SA-SCR$"},
	{0x06C3, "SA-CODE"},
	{0x06E1, "SA-CODE-1"},
	{0x06F0, "SA-CODE-2"},
	{0x06F5, "SA-CODE-3"},
	{0x06F9, "SA-CODE-4"},
	{0x0710, "SA-TYPE-3"},
	{0x0716, "SA-LINE"},
	{0x0723, "SA-LINE-1"},
	{0x073A, "SA-TYPE-0"},
	{0x075A, "SA-ALL"},
	{0x0767, "LD-LOOK-H"},
	{0x078A, "LD-TYPE"},
	{0x07A6, "LD-NAME"},
	{0x07AD, "LD-CH-PR"},
	{0x07CB, "VR-CONTRL"},
	{0x07E9, "VR-CONT-1"},
	{0x07F4, "VR-CONT-2"},
	{0x0800, "VR-CONT-3"},
	{0x0802, "LD-BLOCK"},
	{0x0806, "REPORT-R"},
	{0x0808, "LD-CONTRL"},
	{0x0819, "LD-CONT-1"},
	{0x0825, "LD-CONT-2"},
	{0x082E, "LD-DATA"},
	{0x084C, "LD-DATA-1"},
	{0x0873, "LD-PROG"},
	{0x08AD, "LD-PROG-1"},
	{0x08B6, "ME-CONTRL"},
	{0x08D2, "ME-NEW-LP"},
	{0x08D7, "ME-OLD-LP"},
	{0x08DF, "ME-OLD-L1"},
	{0x08EB, "ME-NEW-L2"},
	{0x08F0, "ME-VAR-LP"},
	{0x08F9, "ME-OLD-VP"},
	{0x0901, "ME-OLD-V1"},
	{0x0909, "ME-OLD-V2"},
	{0x0912, "ME-OLD-V3"},
	{0x091E, "ME-OLD-V4"},
	{0x0921, "ME-VAR-L1"},
	{0x0923, "ME-VAR-L2"},
	{0x092C, "ME-ENTER"},
	{0x093E, "ME-ENT-1"},
	{0x0955, "ME-ENT-2"},
	{0x0958, "ME-ENT-3"},
	{0x0970, "SA-CONTRL"},
	{0x0991, "SA-1-SEC"},
	{0x09F4, "PRINT-OUT"},
	{0x0A23, "PO-BACK-1"},
	{0x0A38, "PO-BACK-2"},
	{0x0A3A, "PO-BACK-3"},
	{0x0A3D, "PO-RIGHT"},
	{0x0A4F, "PO-ENTER"},
	{0x0A5F, "PO-COMMA"},
	{0x0A69, "PO-QUEST"},
	{0x0A6D, "PO-TV-2"},
	{0x0A75, "PO-2-OPER"},
	{0x0A7A, "PO-1-OPER"},
	{0x0A7D, "PO-TV-1"},
	{0x0A80, "PO-CHANGE"},
	{0x0A87, "PO-CONT"},
	{0x0AAC, "PO-AT-ERR"},
	{0x0ABF, "PO-AT-SET"},
	{0x0AC2, "PO-TAB"},
	{0x0AC3, "PO-FILL"},
	{0x0AD0, "PO-SPACE"},
	{0x0AD9, "PO-ABLE"},
	{0x0ADC, "PO-STORE"},
	{0x0AF0, "PO-ST-E"},
	{0x0AFC, "PO-ST-PR"},
	{0x0B03, "PO-FETCH"},
	{0x0B1D, "PO-F-PR"},
	{0x0B24, "PO-ANY"},
	{0x0B38, "PO-GR-1"},
	{0x0B3E, "PO-GR-2"},
	{0x0B4C, "PO-GR-3"},
	{0x0B52, "PO-T&UDG"},
	{0x0B5F, "PO-T"},
	{0x0B65, "PO-CHAR"},
	{0x0B6A, "PO-CHAR-2"},
	{0x0B76, "PO-CHAR-3"},
	{0x0B7F, "PR-ALL"},
	{0x0B93, "PR-ALL-1"},
	{0x0BA4, "PR-ALL-2"},
	{0x0BB6, "PR-ALL-3"},
	{0x0BB7, "PR-ALL-4"},
	{0x0BC1, "PR-ALL-5"},
	{0x0BD3, "PR-ALL-6"},
	{0x0BDB, "PO-ATTR"},
	{0x0BFA, "PO-ATTR-1"},
	{0x0C08, "PO-ATTR-2"},
	{0x0C0A, "PO-MSG"},
	{0x0C10, "PO-TOKENS"},
	{0x0C14, "PO-TABLE"},
	{0x0C22, "PO-EACH"},
	{0x0C35, "PO-TR-SP"},
	{0x0C3B, "PO-SAVE"},
	{0x0C41, "PO-SEARCH"},
	{0x0C44, "PO-STEP"},
	{0x0C55, "PO-SCR"},
	{0x0C86, "REPORT-5"},
	{0x0C88, "PO-SCR-2"},
	{0x0CD2, "PO-SCR-3"},
	{0x0CF0, "PO-SCR-3A"},
	{0x0D00, "REPORT-D"},
	{0x0D02, "PO-SCR-4"},
	{0x0D1C, "PO-SCR-4A"},
	{0x0D2D, "PO-SCR-4B"},
	{0x0D4D, "TEMPS"},
	{0x0D5B, "TEMPS-1"},
	{0x0D65, "TEMPS-2"},
	{0x0D6B, "CLS"},
	{0x0D6E, "CLS-LOWER"},
	{0x0D87, "CLS-1"},
	{0x0D89, "CLS-2"},
	{0x0D8E, "CLS-3"},
	{0x0D94, "CL-CHAN"},
	{0x0DA0, "CL-CHAN-A"},
	{0x0DAF, "CL-ALL"},
	{0x0DD9, "CL-SET"},
	{0x0DEE, "CL-SET-1"},
	{0x0DF4, "CL-SET-2"},
	{0x0E00, "CL-SCROLL"},
	{0x0E05, "CL-SCR-1"},
	{0x0E0D, "CL-SCR-2"},
	{0x0E19, "CL-SCR-3"},
	{0x0E44, "CL-LINE"},
	{0x0E4A, "CL-LINE-1"},
	{0x0E4D, "CL-LINE-2"},
	{0x0E80, "CL-LINE-3"},
	{0x0E88, "CL-ATTR"},
	{0x0E9B, "CL-ADDR"},
	{0x0EAC, "COPY"},
	{0x0EB2, "COPY-1"},
	{0x0EC9, "COPY-2"},
	{0x0ECD, "COPY-BUFF"},
	{0x0ED3, "COPY-3"},
	{0x0EDA, "COPY-END"},
	{0x0EDF, "CLEAR-PRB"},
	{0x0EE7, "PRB-BYTES"},
	{0x0EF4, "COPY-LINE"},
	{0x0EFD, "COPY-L-1"},
	{0x0F0C, "COPY-L-2"},
	{0x0F14, "COPY-L-3"},
	{0x0F18, "COPY-L-4"},
	{0x0F1E, "COPY-L-5"},
	{0x0F2C, "EDITOR"},
	{0x0F30, "ED-AGAIN"},
	{0x0F38, "ED-LOOP"},
	{0x0F6C, "ED-CONTR"},
	{0x0F81, "ADD-CHAR"},
	{0x0F8B, "ADD-CH-1"},
	{0x0F92, "ED-KEYS"},
	{0x0FA9, "ED-EDIT"},
	{0x0FF3, "ED-DOWN"},
	{0x1001, "ED-STOP"},
	{0x1007, "ED-LEFT"},
	{0x100C, "ED-RIGHT"},
	{0x1011, "ED-CUR"},
	{0x1015, "ED-DELETE"},
	{0x101E, "ED-IGNORE"},
	{0x1024, "ED-ENTER"},
	{0x1026, "ED-END"},
	{0x1031, "ED-EDGE"},
	{0x103E, "ED-EDGE-1"},
	{0x1051, "ED-EDGE-2"},
	{0x1059, "ED-UP"},
	{0x106E, "ED-LIST"},
	{0x1076, "ED-SYMBOL"},
	{0x107C, "ED-GRAPH"},
	{0x107F, "ED-ERROR"},
	{0x1097, "CLEAR-SP"},
	{0x10A8, "KEY-INPUT"},
	{0x10DB, "KEY-M&CL"},
	{0x10E6, "KEY-MODE"},
	{0x10F4, "KEY-FLAG"},
	{0x10FA, "KEY-CONTR"},
	{0x1105, "KEY-DATA"},
	{0x110D, "KEY-NEXT"},
	{0x1113, "KEY-CHAN"},
	{0x111B, "KEY-DONE"},
	{0x111D, "ED-COPY"},
	{0x1150, "ED-BLANK"},
	{0x115E, "ED-SPACES"},
	{0x1167, "ED-FULL"},
	{0x117C, "ED-C-DONE"},
	{0x117E, "ED-C-END"},
	{0x1195, "SET-DE"},
	{0x11A7, "REMOVE-FP"},
	{0x11B7, "NEW"},
	{0x11CB, "START/NEW"},
	{0x11DA, "RAM-CHECK"},
	{0x11DC, "RAM-FILL"},
	{0x11E2, "RAM-READ"},
	{0x11EF, "RAM-DONE"},
	{0x1219, "RAM-SET"},
	{0x12A2, "MAIN-EXEC"},
	{0x12A9, "MAIN-1"},
	{0x12AC, "MAIN-2"},
	{0x12CF, "MAIN-3"},
	{0x1303, "MAIN-4"},
	{0x1313, "MAIN-G"},
	{0x133C, "MAIN-5"},
	{0x1373, "MAIN-6"},
	{0x1376, "MAIN-7"},
	{0x1384, "MAIN-8"},
	{0x1386, "MAIN-9"},
	{0x1555, "REPORT-G"},
	{0x155D, "MAIN-ADD"},
	{0x157D, "MAIN-ADD1"},
	{0x15AB, "MAIN-ADD2"},
	{0x15C4, "REPORT-J"},
	{0x15D4, "WAIT-KEY"},
	{0x15DE, "WAIT-KEY1"},
	{0x15E4, "REPORT-8"},
	{0x15E6, "INPUT-AD"},
	{0x15EF, "OUT-CODE"},
	{0x15F2, "PRINT-A-2"},
	{0x1601, "CHAN-OPEN"},
	{0x1610, "CHAN-OP-1"},
	{0x1615, "CHAN-FLAG"},
	{0x162C, "CALL-JUMP"},
	{0x1634, "CHAN-K"},
	{0x1642, "CHAN-S"},
	{0x1646, "CHAN-S-1"},
	{0x164D, "CHAN-P"},
	{0x1652, "ONE-SPACE"},
	{0x1655, "MAKE-ROOM"},
	{0x1664, "POINTERS"},
	{0x166B, "PTR-NEXT"},
	{0x167F, "PTR-DONE"},
	{0x168F, "LINE-ZERO"},
	{0x1695, "LINE-NO"},
	{0x169E, "RESERVE"},
	{0x16B0, "SET-MIN"},
	{0x16BF, "SET-WORK"},
	{0x16C5, "SET-STK"},
	{0x16D4, "REC-EDIT"},
	{0x16DB, "INDEXER-1"},
	{0x16DC, "INDEXER"},
	{0x16E5, "CLOSE"},
	{0x16FC, "CLOSE-1"},
	{0x1701, "CLOSE-2"},
	{0x171C, "CLOSE-STR"},
	{0x171E, "STR-DATA"},
	{0x1725, "REPORT-O"},
	{0x1727, "STR-DATA1"},
	{0x1736, "OPEN"},
	{0x1756, "OPEN-1"},
	{0x175D, "OPEN-2"},
	{0x1765, "REPORT-F"},
	{0x1767, "OPEN-3"},
	{0x1781, "OPEN-K"},
	{0x1785, "OPEN-S"},
	{0x1789, "OPEN-P"},
	{0x178B, "OPEN-END"},
	{0x1795, "AUTO-LIST"},
	{0x17CE, "AUTO-L-1"},
	{0x17E1, "AUTO-L-2"},
	{0x17E4, "AUTO-L-3"},
	{0x17ED, "AUTO-L-4"},
	{0x17F5, "LLIST"},
	{0x17FB, "LIST"},
	{0x1814, "LIST-2"},
	{0x181A, "LIST-3"},
	{0x181F, "LIST-4"},
	{0x1822, "LIST-5"},
	{0x1833, "LIST-ALL"},
	{0x1835, "LIST-ALL-1"},
	{0x1855, "OUT-LINE"},
	{0x1865, "OUT-LINE1"},
	{0x187D, "OUT-LINE2"},
	{0x1881, "OUT-LINE3"},
	{0x1894, "OUT-LINE4"},
	{0x18A1, "OUT-LINE5"},
	{0x18B4, "OUT-LINE6"},
	{0x18B6, "NUMBER"},
	{0x18C1, "OUT-FLASH"},
	{0x18E1, "OUT-CURS"},
	{0x18F3, "OUT-C-1"},
	{0x1909, "OUT-C-2"},
	{0x190F, "LN-FETCH"},
	{0x191C, "LN-STORE"},
	{0x1925, "OUT-SP-2"},
	{0x192A, "OUT-SP-NO"},
	{0x192B, "OUT-SP-1"},
	{0x1937, "OUT-CHAR"},
	{0x195A, "OUT-CH-1"},
	{0x1968, "OUT-CH-2"},
	{0x196C, "OUT-CH-3"},
	{0x196E, "LINE-ADDR"},
	{0x1974, "LINE-AD-1"},
	{0x1980, "CP-LINES"},
	{0x198B, "EACH-STMT"},
	{0x1990, "EACH-S-1"},
	{0x199A, "EACH-S-2"},
	{0x19A5, "EACH-S-4"},
	{0x19AD, "EACH-S-5"},
	{0x19B1, "EACH-S-6"},
	{0x19B8, "NEXT-ONE"},
	{0x19C7, "NEXT-O-1"},
	{0x19CE, "NEXT-O-2"},
	{0x19D5, "NEXT-O-3"},
	{0x19D6, "NEXT-O-4"},
	{0x19DB, "NEXT-O-5"},
	{0x19DD, "DIFFER"},
	{0x19E5, "RECLAIM-1"},
	{0x19E8, "RECLAIM-2"},
	{0x19FB, "E-LINE-NO"},
	{0x1A15, "E-L-1"},
	{0x1A1B, "OUT-NUM-1"},
	{0x1A28, "OUT-NUM-2"},
	{0x1A30, "OUT-NUM-3"},
	{0x1A42, "OUT-NUM-4"},
	{0x1A7A, "P-LET"},
	{0x1A7D, "P-GO-TO"},
	{0x1A81, "P-IF"},
	{0x1A86, "P-GO-SUB"},
	{0x1A8A, "P-STOP"},
	{0x1A8D, "P-RETURN"},
	{0x1A90, "P-FOR"},
	{0x1A98, "P-NEXT"},
	{0x1A9C, "P-PRINT"},
	{0x1A9F, "P-INPUT"},
	{0x1AA2, "P-DIM"},
	{0x1AA5, "P-REM"},
	{0x1AA8, "P-NEW"},
	{0x1AAB, "P-RUN"},
	{0x1AAE, "P-LIST"},
	{0x1AB1, "P-POKE"},
	{0x1AB5, "P-RANDOM"},
	{0x1AB8, "P-CONT"},
	{0x1ABB, "P-CLEAR"},
	{0x1ABE, "P-CLS"},
	{0x1AC1, "P-PLOT"},
	{0x1AC5, "P-PAUSE"},
	{0x1AC9, "P-READ"},
	{0x1ACC, "P-DATA"},
	{0x1ACF, "P-RESTORE"},
	{0x1AD2, "P-DRAW"},
	{0x1AD6, "P-COPY"},
	{0x1AD9, "P-LPRINT"},
	{0x1ADC, "P-LLIST"},
	{0x1ADF, "P-SAVE"},
	{0x1AE0, "P-LOAD"},
	{0x1AE1, "P-VERIFY"},
	{0x1AE2, "P-MERGE"},
	{0x1AE3, "P-BEEP"},
	{0x1AE7, "P-CIRCLE"},
	{0x1AEB, "P-INK"},
	{0x1AEC, "P-PAPER"},
	{0x1AED, "P-FLASH"},
	{0x1AEE, "P-BRIGHT"},
	{0x1AEF, "P-INVERSE"},
	{0x1AF0, "P-OVER"},
	{0x1AF1, "P-OUT"},
	{0x1AF5, "P-BORDER"},
	{0x1AF9, "P-DEF-FN"},
	{0x1AFC, "P-OPEN"},
	{0x1B02, "P-CLOSE"},
	{0x1B06, "P-FORMAT"},
	{0x1B0A, "P-MOVE"},
	{0x1B10, "P-ERASE"},
	{0x1B14, "P-CAT"},
	{0x1B17, "LINE-SCAN"},
	{0x1B29, "STMT-LOOP"},
	{0x1B52, "SCAN-LOOP"},
	{0x1B55, "GET-PARAM"},
	{0x1B6F, "SEPARATOR"},
	{0x1B76, "STMT-RET"},
	{0x1B7B, "REPORT-L"},
	{0x1B7D, "STMT-R-1"},
	{0x1B8A, "LINE-RUN"},
	{0x1B9E, "LINE-NEW"},
	{0x1BB0, "REPORT-0"},
	{0x1BB3, "LINE-END"},
	{0x1BBF, "LINE-USE"},
	{0x1BD1, "NEXT-LINE"},
	{0x1BEC, "REPORT-N"},
	{0x1BEE, "CHECK-END"},
	{0x1BF4, "STMT-NEXT"},
	{0x1C0D, "CLASS-03"},
	{0x1C10, "CLASS-00"},
	{0x1C11, "CLASS-05"},
	{0x1C16, "JUMP-C-R"},
	{0x1C1F, "CLASS-01"},
	{0x1C22, "VAR-A-1"},
	{0x1C2E, "REPORT-2"},
	{0x1C30, "VAR-A-2"},
	{0x1C46, "VAR-A-3"},
	{0x1C4E, "CLASS-02"},
	{0x1C59, "VAL-FET-1"},
	{0x1C6C, "CLASS-04"},
	{0x1C79, "NEXT-2NUM"},
	{0x1C7A, "EXPT-2NUM"},
	{0x1C82, "EXPT-1NUM"},
	{0x1C8A, "REPORT-C"},
	{0x1C8C, "EXPT-EXP"},
	{0x1C96, "PERMS"},
	{0x1CBE, "CLASS-09"},
	{0x1CD6, "CL-09-1"},
	{0x1CDB, "CLASS-0B"},
	{0x1CDE, "FETCH-NUM"},
	{0x1CE6, "USE-ZERO"},
	{0x1CEE, "STOP"},
	{0x1CF0, "IF"},
	{0x1D00, "IF-1"},
	{0x1D03, "FOR"},
	{0x1D10, "F-USE-1"},
	{0x1D16, "F-REORDER"},
	{0x1D34, "F-L&S"},
	{0x1D64, "F-LOOP"},
	{0x1D7C, "F-FOUND"},
	{0x1D84, "REPORT-I"},
	{0x1D86, "LOOK-PROG"},
	{0x1D8B, "LOOK-P-1"},
	{0x1DA3, "LOOK-P-2"},
	{0x1DAB, "NEXT"},
	{0x1DD8, "REPORT-1"},
	{0x1DDA, "NEXT-LOOP"},
	{0x1DE2, "NEXT-1"},
	{0x1DE9, "NEXT-2"},
	{0x1DEC, "READ-3"},
	{0x1DED, "READ"},
	{0x1E08, "REPORT-E"},
	{0x1E0A, "READ-1"},
	{0x1E1E, "READ-2"},
	{0x1E27, "DATA"},
	{0x1E2C, "DATA-1"},
	{0x1E37, "DATA-2"},
	{0x1E39, "PASS-BY"},
	{0x1E42, "RESTORE"},
	{0x1E45, "REST-RUN"},
	{0x1E4F, "RANDOMIZE"},
	{0x1E5A, "RAND-1"},
	{0x1E5F, "CONTINUE"},
	{0x1E73, "GO-TO-2"},
	{0x1E7A, "OUT"},
	{0x1E80, "POKE"},
	{0x1E85, "TWO-PARAM"},
	{0x1E8E, "TWO-P-1"},
	{0x1E94, "FIND-INT1"},
	{0x1E99, "FIND-INT2"},
	{0x1E9C, "FIND-I-1"},
	{0x1E9F, "REPORT-B"},
	{0x1EA1, "RUN"},
	{0x1EAC, "CLEAR"},
	{0x1EAF, "CLEAR-RUN"},
	{0x1EB7, "CLEAR-1"},
	{0x1EDA, "REPORT-M"},
	{0x1EDC, "CLEAR-2"},
	{0x1F05, "TEST-ROOM"},
	{0x1F15, "REPORT-4"},
	{0x1F1A, "FREE-MEM"},
	{0x1F23, "RETURN"},
	{0x1F36, "REPORT-7"},
	{0x1F3A, "PAUSE"},
	{0x1F3D, "PAUSE-1"},
	{0x1F49, "PAUSE-2"},
	{0x1F4F, "PAUSE-END"},
	{0x1F54, "BREAK-KEY"},
	{0x1F60, "DEF-FN"},
	{0x1F6A, "DEF-FN-1"},
	{0x1F7D, "DEF-FN-2"},
	{0x1F86, "DEF-FN-3"},
	{0x1F89, "DEF-FN-4"},
	{0x1F94, "DEF-FN-5"},
	{0x1FA6, "DEF-FN-6"},
	{0x1FBD, "DEF-FN-7"},
	{0x1FC3, "UNSTACK-Z"},
	{0x1FC9, "LPRINT"},
	{0x1FCD, "PRINT"},
	{0x1FCF, "PRINT-1"},
	{0x1FDF, "PRINT-2"},
	{0x1FE5, "PRINT-3"},
	{0x1FF2, "PRINT-4"},
	{0x1FF5, "PRINT-CR"},
	{0x1FFC, "PR-ITEM-1"},
	{0x200E, "PR-ITEM-2"},
	{0x201E, "PR-AT-TAB"},
	{0x2024, "PR-ITEM-3"},
	{0x203C, "PR-STRING"},
	{0x2045, "PR-END-Z"},
	{0x2048, "PR-ST-END"},
	{0x204E, "PR-POSN-1"},
	{0x2061, "PR-POSN-2"},
	{0x2067, "PR-POSN-3"},
	{0x206E, "PR-POSN-4"},
	{0x2089, "INPUT"},
	{0x2096, "INPUT-1"},
	{0x20AD, "INPUT-2"},
	{0x20C1, "IN-ITEM-1"},
	{0x20D8, "IN-ITEM-2"},
	{0x20ED, "IN-ITEM-3"},
	{0x20FA, "IN-PROMPT"},
	{0x211A, "IN-PR-1"},
	{0x211C, "IN-PR-2"},
	{0x2129, "IN-PR-3"},
	{0x213A, "IN-VAR-1"},
	{0x2148, "IN-VAR-2"},
	{0x215E, "IN-VAR-3"},
	{0x2161, "IN-VAR-4"},
	{0x2174, "IN-VAR-5"},
	{0x219B, "IN-VAR-6"},
	{0x21AF, "IN-NEXT-1"},
	{0x21B2, "IN-NEXT-2"},
	{0x21D6, "IN-CHAN-K"},
	{0x21E1, "CO-TEMP-1"},
	{0x21F2, "CO-TEMP-3"},
	{0x2211, "CO-TEMP-5"},
	{0x2228, "CO-TEMP-6"},
	{0x2234, "CO-TEMP-7"},
	{0x223E, "CO-TEMP-8"},
	{0x2244, "REPORT-K"},
	{0x2246, "CO-TEMP-9"},
	{0x2257, "CO-TEMP-A"},
	{0x2258, "CO-TEMP-B"},
	{0x226C, "CO-CHANGE"},
	{0x2294, "BORDER"},
	{0x22A6, "BORDER-1"},
	{0x22AA, "PIXEL-ADD"},
	{0x22CB, "POINT-SUB"},
	{0x22D4, "POINT-LP"},
	{0x22DC, "PLOT"},
	{0x22E5, "PLOT-SUB"},
	{0x22F0, "PLOT-LOOP"},
	{0x22FD, "PL-TST-IN"},
	{0x2303, "PLOT-END"},
	{0x2307, "STK-TO-BC"},
	{0x2314, "STK-TO-A"},
	{0x2320, "CIRCLE"},
	{0x233B, "C-R-GRE-1"},
	{0x2347, "C-ARC-GE1"},
	{0x2382, "DRAW"},
	{0x238D, "DR-3-PRMS"},
	{0x23A3, "DR-SIN-NZ"},
	{0x23C1, "DR-PRMS"},
	{0x2420, "DRW-STEPS"},
	{0x2425, "ARC-LOOP"},
	{0x2439, "ARC-START"},
	{0x245F, "ARC-END"},
	{0x2477, "LINE-DRAW"},
	{0x247D, "CD-PRMS1"},
	{0x2495, "USE-252"},
	{0x2497, "DRAW-SAVE"},
	{0x24B7, "DRAW-LINE"},
	{0x24C4, "DL-X-GE-Y"},
	{0x24CB, "DL-LARGER"},
	{0x24CE, "D-L-LOOP"},
	{0x24D4, "D-L-DIAG"},
	{0x24DB, "D-L-HR-VT"},
	{0x24DF, "D-L-STEP"},
	{0x24EC, "D-L-PLOT"},
	{0x24F7, "D-L-RANGE"},
	{0x24F9, "REPORT-B"},
	{0x24FB, "SCANNING"},
	{0x24FF, "S-LOOP-1"},
	{0x250F, "S-QUOTE-S"},
	{0x2522, "S-2-COORD"},
	{0x252D, "S-RPORT-C"},
	{0x2530, "SYNTAX-Z"},
	{0x2535, "S-SCRN$-S"},
	{0x254F, "S-SCRN-LP"},
	{0x255A, "S-SC-MTCH"},
	{0x255D, "S-SC-ROWS"},
	{0x257D, "S-SCR-STO"},
	{0x2580, "S-ATTR-S"},
	{0x25AF, "S-U-PLUS"},
	{0x25B3, "S-QUOTE"},
	{0x25BE, "S-Q-AGAIN"},
	{0x25CB, "S-Q-COPY"},
	{0x25D9, "S-Q-PRMS"},
	{0x25DB, "S-STRING"},
	{0x25E8, "S-BRACKET"},
	{0x25F5, "S-FN"},
	{0x25F8, "S-RND"},
	{0x2625, "S-RND-END"},
	{0x2627, "S-PI"},
	{0x2630, "S-PI-END"},
	{0x2634, "S-INKEY$"},
	{0x2660, "S-IK$-STK"},
	{0x2665, "S-INK$-EN"},
	{0x2668, "S-SCREEN$"},
	{0x2672, "S-ATTR"},
	{0x267B, "S-POINT"},
	{0x2684, "S-ALPHNUM"},
	{0x268D, "S-DECIMAL"},
	{0x26B6, "S-SD-SKIP"},
	{0x26C3, "S-NUMERIC"},
	{0x26C9, "S-LETTER"},
	{0x26DD, "S-CONT-1"},
	{0x26DF, "S-NEGATE"},
	{0x2707, "S-NO-TO-S"},
	{0x270D, "S-PUSH-PO"},
	{0x2712, "S-CONT-2"},
	{0x2713, "S-CONT-3"},
	{0x2723, "S-OPERTR"},
	{0x2734, "S-LOOP"},
	{0x274C, "S-STK-LST"},
	{0x275B, "S-SYNTEST"},
	{0x2761, "S-RPORT-C"},
	{0x2764, "S-RUNTEST"},
	{0x2770, "S-LOOPEND"},
	{0x2773, "S-TIGHTER"},
	{0x2788, "S-NOT-AND"},
	{0x2790, "S-NEXT"},
	{0x27BD, "S-FN-SBRN"},
	{0x27D0, "SF-BRKT-1"},
	{0x27D9, "SF-ARGMTS"},
	{0x27E4, "SF-BRKT-2"},
	{0x27E6, "SF-RPRT-C"},
	{0x27E9, "SF-FLAG-6"},
	{0x27F4, "SF-SYN-EN"},
	{0x27F7, "SF-RUN"},
	{0x2802, "SF-ARGMT1"},
	{0x2808, "SF-FND-DF"},
	{0x2812, "REPORT-P"},
	{0x2814, "SF-CP-DEF"},
	{0x2825, "SF-NOT-FD"},
	{0x2831, "SF-VALUES"},
	{0x2843, "SF-ARG-LP"},
	{0x2852, "SF-ARG-VL"},
	{0x2885, "SF-R-BR-2"},
	{0x288B, "REPORT-Q"},
	{0x288D, "SF-VALUE"},
	{0x28AB, "FN-SKPOVR"},
	{0x28B2, "LOOK-VARS"},
	{0x28D4, "V-CHAR"},
	{0x28DE, "V-STR-VAR"},
	{0x28E3, "V-TEST-FN"},
	{0x28EF, "V-RUN/SYN"},
	{0x28FD, "V-RUN"},
	{0x2900, "V-EACH"},
	{0x2912, "V-MATCHES"},
	{0x2913, "V-SPACES"},
	{0x2929, "V-GET-PTR"},
	{0x292A, "V-NEXT"},
	{0x2932, "V-80-BYTE"},
	{0x2934, "V-SYNTAX"},
	{0x293E, "V-FOUND-1"},
	{0x293F, "V-FOUND-2"},
	{0x2943, "V-PASS"},
	{0x294B, "V-END"},
	{0x2951, "STK-F-ARG"},
	{0x295A, "SFA-LOOP"},
	{0x296B, "SFA-CP-VR"},
	{0x2981, "SFA-MATCH"},
	{0x2991, "SFA-END"},
	{0x2996, "STK-VAR"},
	{0x29A1, "SV-SIMPLE$"},
	{0x29AE, "SV-ARRAYS"},
	{0x29C0, "SV-PTR"},
	{0x29C3, "SV-COMMA"},
	{0x29D8, "SV-CLOSE"},
	{0x29E0, "SV-CH-ADD"},
	{0x29EA, "SV-COUNT"},
	{0x29FB, "SV-MULT"},
	{0x2A12, "SV-RPT-C"},
	{0x2A20, "REPORT-3"},
	{0x2A22, "SV-NUMBER"},
	{0x2A2C, "SV-ELEM$"},
	{0x2A45, "SV-SLICE"},
	{0x2A48, "SV-DIM"},
	{0x2A49, "SV-SLICE?"},
	{0x2A52, "SLICING"},
	{0x2A7A, "SL-RPT-C"},
	{0x2A81, "SL-SECOND"},
	{0x2A94, "SL-DEFINE"},
	{0x2AA8, "SL-OVER"},
	{0x2AAD, "SL-STORE"},
	{0x2AB1, "STK-ST-0"},
	{0x2AB2, "STK-STO-$"},
	{0x2AB6, "STK-STORE"},
	{0x2ACD, "INT-EXP1"},
	{0x2AE8, "I-CARRY"},
	{0x2AEB, "I-RESTORE"},
	{0x2AEE, "DE,(DE+1)"},
	{0x2AF4, "GET-HL*DE"},
	{0x2AFF, "LET"},
	{0x2B0B, "L-EACH-CH"},
	{0x2B0C, "L-NO-SP"},
	{0x2B1F, "L-TEST"},
	{0x2B29, "L-SPACES"},
	{0x2B3E, "L-CHAR"},
	{0x2B4F, "L-SINGLE"},
	{0x2B59, "L-NUMERIC"},
	{0x2B66, "L-EXISTS"},
	{0x2B72, "L-DELETE$"},
	{0x2B9B, "L-LENGTH"},
	{0x2BA3, "L-IN-W/S"},
	{0x2BA6, "L-ENTER"},
	{0x2BAF, "L-ADD$"},
	{0x2BC0, "L-NEW$"},
	{0x2BC6, "L-STRING"},
	{0x2BEA, "L-FIRST"},
	{0x2BF1, "STK-FETCH"},
	{0x2C05, "DIM"},
	{0x2C15, "D-RUN"},
	{0x2C1F, "D-LETTER"},
	{0x2C2D, "D-SIZE"},
	{0x2C2E, "D-NO-LOOP"},
	{0x2C7C, "DIM-CLEAR"},
	{0x2C7F, "DIM-SIZES"},
	{0x2C88, "ALPHANUM"},
	{0x2C8D, "ALPHA"},
	{0x2C9B, "DEC-TO-FP"},
	{0x2CA2, "BIN-DIGIT"},
	{0x2CB3, "BIN-END"},
	{0x2CB8, "NOT-BIN"},
	{0x2CCB, "DECIMAL"},
	{0x2CCF, "DEC-RPT-C"},
	{0x2CD5, "DEC-STO-1"},
	{0x2CDA, "NXT-DGT-1"},
	{0x2CEB, "E-FORMAT"},
	{0x2CF2, "SIGN-FLAG"},
	{0x2CFE, "SIGN-DONE"},
	{0x2CFF, "ST-E-PART"},
	{0x2D18, "E-FP-JUMP"},
	{0x2D1B, "NUMERIC"},
	{0x2D22, "STK-DIGIT"},
	{0x2D28, "STACK-A"},
	{0x2D2B, "STACK-BC"},
	{0x2D3B, "INT-TO-FP"},
	{0x2D40, "NXT-DGT-2"},
	{0x2D4F, "E-TO-FP"},
	{0x2D55, "E-SAVE"},
	{0x2D60, "E-LOOP"},
	{0x2D6D, "E-DIVSN"},
	{0x2D6E, "E-FETCH"},
	{0x2D71, "E-TST-END"},
	{0x2D7B, "E-END"},
	{0x2D7F, "INT-FETCH"},
	{0x2D8C, "P-INT-STO"},
	{0x2D8E, "INT-STORE"},
	{0x2DA2, "FP-TO-BC"},
	{0x2DAD, "FP-DELETE"},
	{0x2DC1, "LOG(2^A)"},
	{0x2DD5, "FP-TO-A"},
	{0x2DE1, "FP-A-END"},
	{0x2DE3, "PRINT-FP"},
	{0x2DF2, "PF-NEGTVE"},
	{0x2DF8, "PF-POSTVE"},
	{0x2E01, "PF-LOOP"},
	{0x2E1E, "PF-SAVE"},
	{0x2E24, "PF-SMALL"},
	{0x2E56, "PF-LARGE"},
	{0x2E6F, "PF-MEDIUM"},
	{0x2E7B, "PF-BITS"},
	{0x2E8A, "PF-BYTES"},
	{0x2EA1, "PF-DIGITS"},
	{0x2EA9, "PF-INSERT"},
	{0x2EB3, "PF-TEST-2"},
	{0x2EB8, "PF-ALL-9"},
	{0x2ECB, "PF-MORE"},
	{0x2ECF, "PF-FRACTN"},
	{0x2EDF, "PF-FRN-LP"},
	{0x2EEC, "PF-FR-DGT"},
	{0x2EEF, "PF-FR-EXX"},
	{0x2F0C, "PF-ROUND"},
	{0x2F18, "PF-RND-LP"},
	{0x2F25, "PF-R-BACK"},
	{0x2F2D, "PF-COUNT"},
	{0x2F46, "PF-NOT-E"},
	{0x2F4A, "PF-E-SBRN"},
	{0x2F52, "PF-OUT-LP"},
	{0x2F59, "PF-OUT-DT"},
	{0x2F5E, "PF-DC-OUT"},
	{0x2F64, "PF-DEC-0S"},
	{0x2F6C, "PF-E-FRMT"},
	{0x2F83, "PF-E-POS"},
	{0x2F85, "PF-E-SIGN"},
	{0x2F8B, "CA=10*A+C"},
	{0x2F9B, "PREP-ADD"},
	{0x2FAF, "NEG-BYTE"},
	{0x2FBA, "FETCH-TWO"},
	{0x2FDD, "SHIFT-FP"},
	{0x2FE5, "ONE-SHIFT"},
	{0x2FF9, "ADDEND-0"},
	{0x2FFB, "ZEROS-4/5"},
	{0x3004, "ADD-BACK"},
	{0x300D, "ALL-ADDED"},
	{0x300F, "SUBTRACT"},
	{0x3014, "addition"},
	{0x303C, "ADDN-OFLW"},
	{0x303E, "FULL-ADDN"},
	{0x3055, "SHIFT-LEN"},
	{0x307C, "TEST-NEG"},
	{0x309F, "ADD-REP-6"},
	{0x30A3, "END-COMPL"},
	{0x30A5, "GO-NC-MLT"},
	{0x30A9, "HL=HL*DE"},
	{0x30B1, "HL-LOOP"},
	{0x30BC, "HL-AGAIN"},
	{0x30BE, "HL-END"},
	{0x30C0, "PREP-M/D"},
	{0x30CA, "multiply"},
	{0x30EA, "MULT-RSLT"},
	{0x30EF, "MULT-OFLW"},
	{0x30F0, "MULT-LONG"},
	{0x3114, "MLT-LOOP"},
	{0x311B, "NO-ADD"},
	{0x3125, "STRT-MLT"},
	{0x313B, "MAKE-EXPT"},
	{0x313D, "DIVN-EXPT"},
	{0x3146, "OFLW1-CLR"},
	{0x3151, "OFLW2-CLR"},
	{0x3155, "TEST-NORM"},
	{0x3159, "NEAR-ZERO"},
	{0x315D, "ZERO-RSLT"},
	{0x315E, "SKIP-ZERO"},
	{0x316E, "NORMALISE"},
	{0x3186, "NORML-NOW"},
	{0x3195, "OFLOW-CLR"},
	{0x31AD, "REPORT-6"},
	{0x31AF, "division"},
	{0x31D2, "DIV-LOOP"},
	{0x31DB, "DIV-34TH"},
	{0x31E2, "DIV-START"},
	{0x31F9, "NO-RSTORE"},
	{0x31FA, "COUNT-ONE"},
	{0x3214, "truncate"},
	{0x3221, "T-GR-ZERO"},
	{0x3233, "T-FIRST"},
	{0x323F, "T-SMALL"},
	{0x3252, "T-NUMERIC"},
	{0x325E, "T-TEST"},
	{0x3261, "T-SHIFT"},
	{0x3267, "T-STORE"},
	{0x326C, "T-EXPNENT"},
	{0x326D, "X-LARGE"},
	{0x3272, "NIL-BYTES"},
	{0x327E, "BYTE-ZERO"},
	{0x3283, "BITS-ZERO"},
	{0x328A, "LESS-MASK"},
	{0x3290, "IX-END"},
	{0x3293, "RE-ST-TWO"},
	{0x3296, "RESTK-SUB"},
	{0x3297, "RE-STACK"},
	{0x32B1, "RS-NRMLSE"},
	{0x32B2, "RSTK-LOOP"},
	{0x32BD, "RS-STORE"},
	{0x32C5, "stk-zero"},
	{0x32C8, "stk-one"},
	{0x32CC, "stk-half"},
	{0x32CE, "stk-pi/2"},
	{0x32D3, "stk-ten"},
	{0x335B, "CALCULATE"},
	{0x335E, "GEN-ENT-1"},
	{0x3362, "GEN-ENT-2"},
	{0x3365, "RE-ENTRY"},
	{0x336C, "SCAN-ENT"},
	{0x3380, "FIRST-3D"},
	{0x338C, "DOUBLE-A"},
	{0x338E, "ENT-TABLE"},
	{0x33A1, "delete"},
	{0x33A2, "fp-calc-2"},
	{0x33A9, "TEST-5-SP"},
	{0x33B4, "STACK-NUM"},
	{0x33C0, "MOVE-FP"},
	{0x33C6, "STK-DATA"},
	{0x33C8, "STK-CONST"},
	{0x33DE, "FORM-EXP"},
	{0x33F1, "STK-ZEROS"},
	{0x33F7, "SKIP-CONS"},
	{0x33F8, "SKIP-NEXT"},
	{0x3406, "LOC-MEM"},
	{0x340F, "get-mem-0"},
	{0x341B, "stk-zero"},
	{0x342D, "st-mem-0"},
	{0x343C, "EXCHANGE"},
	{0x343E, "SWAP-BYTE"},
	{0x3449, "series-06"},
	{0x3453, "G-LOOP"},
	{0x346A, "abs"},
	{0x346E, "NEGATE"},
	{0x3474, "NEG-TEST"},
	{0x3483, "INT-CASE"},
	{0x3492, "sgn"},
	{0x34A5, "in"},
	{0x34AC, "peek"},
	{0x34B0, "IN-PK-STK"},
	{0x34B3, "usr-no"},
	{0x34BC, "usr-$"},
	{0x34D3, "USR-RANGE"},
	{0x34E4, "USR-STACK"},
	{0x34E7, "REPORT-A"},
	{0x34E9, "TEST-ZERO"},
	{0x34F9, "GREATER-0"},
	{0x3501, "NOT"},
	{0x3506, "less-0"},
	{0x3507, "SIGN-TO-C"},
	{0x350B, "FP-0/1"},
	{0x351B, "or"},
	{0x3524, "no-&-no"},
	{0x352D, "str-&-no"},
	{0x353B, "no-l-eql"},
	{0x3543, "EX-OR-NOT"},
	{0x354E, "NU-OR-STR"},
	{0x3559, "STRINGS"},
	{0x3564, "BYTE-COMP"},
	{0x356B, "SECND-LOW"},
	{0x3572, "BOTH-NULL"},
	{0x3575, "SEC-PLUS"},
	{0x3585, "FRST-LESS"},
	{0x3588, "STR-TEST"},
	{0x358C, "END-TESTS"},
	{0x359C, "strs-add"},
	{0x35B7, "OTHER-STR"},
	{0x35BF, "STK-PNTRS"},
	{0x35C9, "chrs"},
	{0x35DC, "REPORT-B"},
	{0x35DE, "val"},
	{0x360C, "V-RPORT-C"},
	{0x361F, "str$"},
	{0x3645, "read-in"},
	{0x365F, "R-I-STORE"},
	{0x3669, "code"},
	{0x3671, "STK-CODE"},
	{0x3674, "len"},
	{0x367A, "dec-jr-nz"},
	{0x3686, "JUMP"},
	{0x3687, "JUMP-2"},
	{0x368F, "jump-true"},
	{0x369B, "end-calc"},
	{0x36A0, "n-mod-m"},
	{0x36AF, "int"},
	{0x36C2, "EXIT"},
	{0x36C4, "EXP"},
	{0x3705, "REPORT-6"},
	{0x370C, "RESULT-OK"},
	{0x370E, "RSLT-ZERO"},
	{0x3713, "ln"},
	{0x371A, "REPORT-A"},
	{0x371C, "VALID"},
	{0x373D, "GRE.8"},
	{0x3783, "get-argt"},
	{0x37A1, "ZPLUS"},
	{0x37A8, "YNEG"},
	{0x37AA, "cos"},
	{0x37B5, "sin"},
	{0x37B7, "C-ENT"},
	{0x37DA, "tan"},
	{0x37E2, "atn"},
	{0x37F8, "SMALL"},
	{0x37FA, "CASES"},
	{0x3833, "asn"},
	{0x3843, "acs"},
	{0x384A, "sqr"},
	{0x3851, "to-power"},
	{0x385D, "XIS0"},
	{0x386A, "ONE"},
	{0x386C, "LAST"},
	{0,NULL}
};

/*
void loadcsrd()
{
	FILE *f;
	fopen_s(&f, "c:\\users\\jim\\desktop\\CompleteSpectrumROMDisassemblyThe_v2.txt", "r");
	if (f == NULL) return;
	unsigned char line[1000];
	while (!feof(f))
	{
		fgets((char *)line, 1000, f);
		if (isxdigit((unsigned char)line[0]) && isxdigit((unsigned char)line[1]) && isxdigit((unsigned char)line[2]) && isxdigit((unsigned char)line[3]))
		{
			if (line[4] == '\t')
			{
				unsigned short address = (unsigned short)strtoul((char *)line, NULL, 16);
				if (address == 0xDEFB)//defb isn't a label
					continue;

				unsigned char *l = line + 5;
				if (line[5] == '\n')
				{
					fgets((char *)line, 1000, f);
					l = line;
				}
				unsigned char *k = l;
				while (!isspace((unsigned char)*k)) k++;
				char label[1000];
				strncpy_s(label, 1000, (char *)l, k - l);
				label[k - l] = '\0';

				if (isdigit((unsigned char)label[0]))
					continue;

				elog("\t{0x%04X, \"%s\"},\n", (unsigned int)address, label);
			}
		}
	}
	fclose(f);
}
*/

static void gen_rom_labels_48k()
{
	const ROM_LABEL *lab = &rom_labels[0];
	//ZX_LABEL l = { 0 };

	while (lab->label != NULL)
	{
		//memset(&l, 0, sizeof l);
		//l.address = lab->address;
		//l.name = (char *)lab->label;
		//l.source = SR_LIBRARY;
		//l.type = LT_SUB;
		//save_label(&l);
		make_label(lab->label, SR_LIBRARY, lab->address, LT_SUB);
		lab++;
	}
}

static void gen_rom_data_48k(void)
{
	//some ROM data areas
	static const struct
	{
		unsigned short start;
		unsigned short end;
		ZX_DATA_TYPE type;
	} data[] = {
		{0x13, 0x17, DEFB},//unused
		{0x25, 0x27, DEFB},//unused
		{0x2b, 0x2f, DEFB},//unused
		{0x5f, 0x65, DEFB},//unused
		{0x95, 0x204, DEFB},//token table
		{0x205, 0x28d, DEFB},//key tables
		{0x46e, 0x4a9, DEFB},//semi tones
		{0x4aa, 0x4c1, DEFB},//zx81 code
		{0x9a1, 0x9f3, DEFB},//cassett messages (uses DEFM - a string ending with last character + 0x80)
		{0xa11, 0xa22, DEFB},//control character table
		{0xfa0, 0xfa8, DEFB},//editing keys table
		{0x1391, 0x1554, DEFM},//report messages (DEFM)
		{0x15af,0x15c3, DEFB},//initial channel information
		{0x15c6,0x15d2, DEFB},//initial stream data
		{0x162d, 0x1633, DEFB}, //channel code look-up table
		{0x1716,0x171b, DEFB},//close stream look-up table
		{0x177a,0x1780, DEFB},//open stream lookup table
		{0x1a48, 0x1a79, DEFB},//syntax tables
		{0x1a7a,0x1b16, DEFB},//parameter table (CLASS-xx)
		{0x1c01,0x1c0c, DEFB},//command class table
		{0x2596,0x25ae, DEFB},//scanning function table
		{0x2795,0x27af, DEFB},//table of operators
		{0x2780,0x27bc, DEFB},//table of priorities
		{0x32c5,0x32d6, DEFB},//fp constants
		{0x32d7,0x335a, DEFW},//fp routines (words)
		{0x386e,0x3cff, DEFB},//spare
		{0x3d00,0x3fff, DEFB},//character set
		{0x344d,0x345a, DEFC},//two hard-to-detect calculator stacks
		{0x3460,0x3469, DEFC},
		{0x345A,0x345F, CODE},
		{0x3469,0x3469, CODE},
		{0x32C5,0x32d6, DEFFPSTK},
		{0x36b7,0x36c2, DEFC},
	};

	for (int i = 0; i < sizeof(data) / sizeof data[0]; i++)
	{
		//ZX_TYPE zx = { 0 };
		//zx.address = data[i].start;
		//zx.size = data[i].end - data[i].start + 1;
		//zx.type = data[i].type;
		//zx.source = SR_LIBRARY;
		//save_type(&zx);
		if (data[i].type == CODE)
			make_code_with_size(SR_LIBRARY, data[i].start, data[i].end - data[i].start + 1);
		else if (data[i].type == DEFFPSTK)
			make_fpstknumber(SR_LIBRARY, data[i].start, data[i].end - data[i].start + 1);
		else
			make_data_type(data[i].type, SR_LIBRARY, data[i].start, data[i].end - data[i].start + 1);
	}
}

static void gen_screen_labels(void)
{
	//ZX_LABEL label = { 0 };
	//label.address = 16384;
	//label.name = "SCREEN";
	//label.source = SR_LIBRARY;
	//label.type = LT_DATA;
	//save_label(&label);
	make_label("SCREEN", SR_LIBRARY, 16384, LT_DATA);

	//label.address = 22528;
	//label.name = "ATTRIBUTES";
	//save_label(&label);
	make_label("ATTRIBUTES", SR_LIBRARY, 22528, LT_DATA);

	//ZX_TYPE type = { 0 };
	//type.address = 16384;
	//type.type = DEFS;
	//type.size = 6144;
	//type.source = SR_LIBRARY;
	//save_type(&type);
	make_data_type(DEFS, SR_LIBRARY, 16384, 6144);

	//type.address = 22528;
	//type.size = 768;
	//save_type(&type);
	make_data_type(DEFS, SR_LIBRARY, 22528, 768);
}

void apply_library_labels(void)
{
	bulk_insert_begin();

	gen_sysvar_if1_labels();
	gen_sysvar_labels_48k();
	gen_rom_data_48k();
	gen_rom_labels_48k();
	gen_screen_labels();

	bulk_insert_end();
}

void gen_rom_labels(void)
{
	ZX_LABEL start;
	if (!get_label_by_name("START", &start))
	{
		make_code_with_size(SR_LIBRARY, 0, 16384);
		make_data(SR_LIBRARY, 16384, 49152);

		apply_library_labels();
	}

	fill_type_gaps();
}

//at 32d7h
const char *const calculator_labels[66]={
"jump-true",
"exchange",
"delete",
"subtract",
"multiply",
"division",
"to-power",
"or",
"no-&-no",
"no-l-eql",
"no-gr-eq",
"nos-neql",
"no-grtr",
"no-less",
"nos-eql",
"addition",
"str-&-no",
"str-l-eql",
"str-gr-eq",
"strs-neql",
"str-grtr",
"str-less",
"strs-eql",
"strs-add",
"val$",
"usr-$",
"read-in",
"negate",
"code",
"val",
"len",
"sin",
"cos",
"tan",
"asn",
"acs",
"atn",
"ln",
"exp",
"int",
"sqr",
"sgn",
"abs",
"peek",
"in",
"usr-no",
"str$",
"chr$",
"not",
"duplicate",
"n-mod-m",
"jump",
"stk-data",
"dec-jr-nz",
"less-0",
"greater-0",
"end-calc",
"get-argt",
"truncate",
"fp-calc-2",
"e-to-fp",
"re-stack",

"series-06",
"stk-zero",
"st-mem-0",
"get-mem-0",
};

const char *get_calculator_label(unsigned char b)
{
	switch (b)
	{
		case 0x86: return "series-06";
		case 0x88: return "series-08";
		case 0x8C: return "series-0C";

		case 0xA0: return "stk-zero";
		case 0xA1: return "stk-one";
		case 0xA2: return "stk-half";
		case 0xA3: return "stk-pi/2";
		case 0xA4: return "stk-ten";

		case 0xC0: return "stk-mem-0";
		case 0xC1: return "stk-mem-1";
		case 0xC2: return "stk-mem-2";
		case 0xC3: return "stk-mem-3";
		case 0xC4: return "stk-mem-4";
		case 0xC5: return "stk-mem-5";

		case 0xE0: return "get-mem-0";
		case 0xE1: return "get-mem-1";
		case 0xE2: return "get-mem-2";
		case 0xE3: return "get-mem-3";
		case 0xE4: return "get-mem-4";
		case 0xE5: return "get-mem-5";

		default:
			if (b <= 65)
				return calculator_labels[b];
			return "(unknown)";
	}
}


#define IS_LIBRARY(x) ((x).source == SR_LIBRARY)
#define IS_MANUAL(x) ((x).source == SR_MANUAL)

static void dedupe_labels()
{
	int counter = 0;

	bulk_insert_begin();

	DB_RES *res = search_labels();
	ZX_LABEL *label = (ZX_LABEL *)res->rows;

	for (int i = 0; i < res->count; i++)
	{
		for (int j = 0; j < res->count; j++)
		{
			if (j != i)
			{
	/*
	SR_LIBRARY=0,//rom labels
	SR_AUTO=1,//unused
	SR_MANUAL=2,//typed in by hand
	SR_BUILTIN=3,//used only while disassembling unknown areas
	SR_COVERAGE=4,//generated by coverage
	SR_PCTRACE=5,//generated by PC tracing
	SR_DATAACCESS = 6
	*/
				if (label[i].address == label[j].address)
				{
					//manual beats everything
					//library beats auto (anything other than manual or library)
					//newest beats oldest
					if (IS_MANUAL(label[i]) && !IS_MANUAL(label[j]))
						delete_label(&label[j]);
					else if (!IS_MANUAL(label[i]) && IS_MANUAL(label[j]))
						delete_label(&label[i]);
					else if (IS_LIBRARY(label[i]) && !IS_LIBRARY(label[j]))
						delete_label(&label[j]);
					else if (!IS_LIBRARY(label[i]) && IS_LIBRARY(label[j]))
						delete_label(&label[i]);
					else if (label[i].time > label[j].time)
						delete_label(&label[j]);
					else
						delete_label(&label[i]);

					counter++;
				}
			}
		}
	}

	bulk_insert_end();
	free_result(res);

	elog("[LABEL] deduped %d labels\n", counter);
}

static void dedupe_types()
{
	int counter = 0;

	bulk_insert_begin();

	DB_RES *res = search_types();
	ZX_TYPE *type = res->rows;

	for (int i = 0; i < res->count; i++)
	{
		for (int j = 0; j < res->count; j++)
		{
			if (j != i)
			{
	/*
	LIBRARY=0,//rom labels
	AUTO=1,//unused
	MANUAL=2,//typed in by hand
	BUILTIN=3,//used only while disassembling unknown areas
	COVERAGE=4,//generated by coverage
	PCTRACE=5,//generated by PC tracing
	DATAACCESS=6,//generated by seeing where code access data
	*/
				if (type[i].address == type[j].address)
				{
					//manual beats everything
					//library beats auto
					//newest beats oldest
					if (IS_MANUAL(type[i]) && !IS_MANUAL(type[j]))
						delete_type(&type[j]);
					else if (!IS_MANUAL(type[i]) && IS_MANUAL(type[j]))
						delete_type(&type[i]);
					else if (IS_LIBRARY(type[i]) && !IS_LIBRARY(type[j]))
						delete_type(&type[j]);
					else if (!IS_LIBRARY(type[i]) && IS_LIBRARY(type[j]))
						delete_type(&type[i]);
					else if (type[i].time > type[j].time)
						delete_type(&type[j]);
					else
						delete_type(&type[i]);
					counter++;
				}
			}
		}
	}

	bulk_insert_end();
	free_result(res);

	elog("[LABEL] deduped %d types\n", counter);
}

void dedupe(void)
{
	elog("[LABEL] deduping...\n");
	dedupe_labels();
	dedupe_types();
}

static int format = 0;
void format_label(char *dst, size_t len, unsigned short address, LABEL_TYPE type)
{
	switch (format)
	{
		case 0:
			switch (type)
			{
				case LBL_SUB: sprintf_s(dst, len, "SUB%04X", address); break;
				case LBL_JUMP: sprintf_s(dst, len, "L%04X", address); break;
			}
		break;

		case 1:
			switch (type)
			{
				case LBL_SUB: sprintf_s(dst, len, "S%04X", address); break;
				case LBL_JUMP: sprintf_s(dst, len, "L%04X", address); break;
			}
			break;

		case 2:
			switch (type)
			{
				case LBL_SUB: sprintf_s(dst, len, "sub_%04X", address); break;
				case LBL_JUMP: sprintf_s(dst, len, "l_%04X", address); break;
			}
			break;
	}
}

ZX_LABELTYPE label_type_from_jump_type(JUMP_TYPE jumptype)
{
	if (jumptype == JT_CALL) return LT_SUB;
	if (jumptype == JT_JUMP) return LT_JUMP;
	assert(0);
	return LT_DATA;
}

ZX_LABELTYPE label_type_from_branch_type(BRANCH_TYPE branchtype)
{
	if (branchtype == BT_CALL) return LT_SUB;
	if (branchtype == BT_JP) return LT_JUMP;
	if (branchtype == BT_JR) return LT_LOCAL;
	assert(0);
	return LT_DATA;
}

static char is_null_or_whitespace(const char *s)
{
	if (s == NULL) return 1;

	while (*s != '\0')
		if (!isspace(*s++)) return 0;
	return 1;
}

void make_label(const char *labelname, ZX_SOURCE source, unsigned short address, ZX_LABELTYPE type)
{
	ZX_LABEL existing_label;
	if (get_label_by_address(address, &existing_label))
	{
		if (is_null_or_whitespace(labelname))
		{
			delete_label(&existing_label);
		}
		else if (source == SR_MANUAL || existing_label.source != SR_LIBRARY)
		{
			//if new label is manual, or the existing label isn't library
			//go ahead and overwrite the existing one
			existing_label.name = (char *)labelname;
			existing_label.source = source;
			existing_label.type = type;
			update_label(&existing_label);
		}
		return;
	}

	if (is_null_or_whitespace(labelname))
		return;

	//it's a new one
	ZX_LABEL label = { 0 };
	label.name = (char *)labelname;
	label.source = source;
	label.address = address;
	label.type = type;
	save_label(&label);
}

typedef struct
{
	ZX_TYPE t;
	int start;
	int end;
} TYPE_RANGE;

#define MAX_RANGES 30
static TYPE_RANGE ranges[MAX_RANGES];
int n_ranges;

static void test_dump_type(const char *name, ZX_TYPE *type);
static void test_dump_type_range(const char *name, TYPE_RANGE *type);
static char *test_type(ZX_DATA_TYPE type);

/* 
overlaps

1) Start of B is in A
A|    |
B   |    |

2) End of B is in A
A   |    |
B|    |

3) A contains B => implies 1) and 2)
A|    |
B  | |

4) B contains A
A  | |
B|    |

5) A equals B
A|   |
B|   |

*/
typedef enum
{
	OVERLAP_NONE,
	OVERLAP_EQUAL,
	OVERLAP_ALL_B_IN_A,
	OVERLAP_ALL_A_IN_B,
	OVERLAP_B_STARTS_IN_A,
	OVERLAP_B_ENDS_IN_A
} OVERLAP_TYPE;

//bool overlap = AStart <= BEnd && BStart <= AEnd;

OVERLAP_TYPE overlap(TYPE_RANGE *A, int Bstart, int Bend)
{
	//A equals B
	if (A->start == Bstart && A->end == Bend) return OVERLAP_EQUAL;

	//A contains B
	if (A->start <= Bstart && A->end >= Bend) return OVERLAP_ALL_B_IN_A;

	//B contains A
	if (Bstart <= A->start && Bend >= A->end) return OVERLAP_ALL_A_IN_B;

	//start of B in A
	if (A->start <= Bstart && A->end >= Bstart) return OVERLAP_B_STARTS_IN_A;

	//end of B in A
	if (A->start <= Bend && A->end >= Bend) return OVERLAP_B_ENDS_IN_A;

	return OVERLAP_NONE;
}

static void dump_ranges()
{
	for (int i = 0; i < n_ranges; i++)
		test_dump_type_range(NULL, &ranges[i]);
}

static char noisy_validate = 0;
static int validate_ranges(char *id, int fatal)
{
	if (noisy_validate)
		dump_ranges();

	if (noisy_validate)
		elog("validating %s...", id);
	for (int i = 0; i < n_ranges - 1; i++)
	{
		if (ranges[i].end >= ranges[i + 1].start)
		{
			if (!noisy_validate)
			{
				dump_ranges();
				elog("validating %s...", id);
			}
			elog("FAILED!\n");
			eloglevel(LOG_ERROR, "[LABEL] type ranges overlap!\n");
			if (fatal)
				assert(0);
			eloglevel(LOG_ERROR, "[LABEL] attempting repair (shrink earlier range not to overlap)\n");
			ranges[i].t.size = ranges[i + 1].t.address - ranges[i].t.address;
			update_type(&ranges[i].t);
			return 1;
		}
	}
	if (noisy_validate)
		elog("OK\n");
	return 0;
}

static char *overlap_type(OVERLAP_TYPE type)
{
	switch (type)
	{
		case OVERLAP_NONE: return "OVERLAP_NONE";
		case OVERLAP_EQUAL: return "OVERLAP_EQUAL";
		case OVERLAP_ALL_B_IN_A: return "OVERLAP_ALL_B_IN_A";
		case OVERLAP_ALL_A_IN_B: return "OVERLAP_ALL_A_IN_B";
		case OVERLAP_B_STARTS_IN_A: return "OVERLAP_B_STARTS_IN_A";
		case OVERLAP_B_ENDS_IN_A: return "OVERLAP_B_ENDS_IN_A";
		default: return "UNKNOWN!";
	}
}

static void apply_new_range(int start, int end, ZX_DATA_TYPE type, ZX_SOURCE source)
{
	//the blocks are in address order. intersect the new block with each row
	char dirty;

	if (validate_ranges("before apply", 1)) return;

	//early-out - we've done this before and this range is already covered
	for (int i = 0; i < n_ranges; i++)
	{
		if (ranges[i].start <= start && ranges[i].end >= start)
		{
			//the start of the incoming label starts within this range
			if (ranges[i].t.type == type && ranges[i].t.source == source)
			{
				//it's the same type and source!
				if (ranges[i].start <= end && ranges[i].end >= end)
				{
					//and the end is within the range
					if (noisy_validate) elog("range already mapped!\n");
					return;
				}
			}
		}
		else if (ranges[i].start > start)
		{
			//ranges from here on in are beyond the incoming start
			break;
		}
	}

	//early out - incoming source is not MANUAL and it overlaps with something that is MANUAL
	//todo: fix this so it works properly, and incoming would fill in some gaps
	if (source != SR_MANUAL)
	{
		for (int i = 0; i < n_ranges; i++)
		{
			if (ranges[i].t.source == SR_MANUAL)
			{
				OVERLAP_TYPE overlaptype = overlap(&ranges[i], start, end);
				if (overlaptype != OVERLAP_NONE)
					return;
			}
		}
	}

	//do the splitting
	do
	{
		dirty = 0;
		for (int i = 0; i < n_ranges; i++)
		{
			OVERLAP_TYPE overlaptype = overlap(&ranges[i], start, end);
			if (overlaptype != OVERLAP_NONE)
			{
				//elog("%s\n", overlap_type(overlaptype));

				switch (overlaptype)
				{
					case OVERLAP_EQUAL:
					case OVERLAP_ALL_A_IN_B://old type is completely enclosed by new type, so it won't exist
						delete_type(&ranges[i].t);
						memmove(&ranges[i], &ranges[i + 1], (n_ranges - i - 1) * sizeof ranges[0]);
						memset(&ranges[n_ranges - 1], 0, sizeof ranges[0]);
						n_ranges--;
						break;

					case OVERLAP_ALL_B_IN_A://new type is completely enclosed by old range
						//the old range needs to be split, possibly into two

						if (ranges[i].t.type == GFX)
						{
							//if old range is GFX type, remove it instead of splitting
							delete_type(&ranges[i].t);
							memmove(&ranges[i], &ranges[i + 1], (n_ranges - i - 1) * sizeof ranges[0]);
							memset(&ranges[n_ranges - 1], 0, sizeof ranges[0]);
							n_ranges--;
						}
						else
						{
							if (ranges[i].start == start)
							{
								//does the existing type extend past the end of the new type?
								if (ranges[i].end > end)
								{
									ranges[i].start = end + 1;
									ranges[i].t.source = source;
								}
							}
							else
							{
								//does the existing type extend past the end of the new type?
								if (ranges[i].end > end)
								{
									//we need a new range
									memmove(&ranges[i + 2], &ranges[i + 1], (n_ranges - i - 1) * sizeof ranges[0]);
									memset(&ranges[i + 1], 0, sizeof ranges[0]);
									memcpy(&ranges[i + 1], &ranges[i], sizeof ranges[0]);
									ranges[i + 1].start = end + 1;
									ranges[i + 1].t.source = source;
									ranges[i + 1].t.id = 0;
									n_ranges++;
								}

								//existing type needs to stop at the start of the new type
								ranges[i].end = start - 1;
								ranges[i].t.source = source;
							}
						}
						break;

					case OVERLAP_B_ENDS_IN_A://new type overlaps the start of old one
						if (ranges[i].t.type == GFX)
						{
							//if old range is GFX type, remove it instead
							delete_type(&ranges[i].t);
							memmove(&ranges[i], &ranges[i + 1], (n_ranges - i - 1) * sizeof ranges[0]);
							memset(&ranges[n_ranges - 1], 0, sizeof ranges[0]);
							n_ranges--;
						}
						else
						{
							ranges[i].start = end + 1;
							ranges[i].t.source = source;
						}
						break;

					case OVERLAP_B_STARTS_IN_A://new type overlaps the end of old one
						if (ranges[i].t.type == GFX)
						{
							//if old range is GFX type, remove it instead of splitting
							delete_type(&ranges[i].t);
							memmove(&ranges[i], &ranges[i + 1], (n_ranges - i - 1) * sizeof ranges[0]);
							memset(&ranges[n_ranges - 1], 0, sizeof ranges[0]);
							n_ranges--;
						}
						else
						{
							ranges[i].end = start - 1;
							ranges[i].t.source = source;
						}
						break;
				}
				dirty = 1;
				break;
			}
		}
		validate_ranges("during split", 1);
	} while (dirty);

	validate_ranges("after split", 1);

	//todo:
	// now fix any odd sized DEFW by following even length DEFW with a single length DEFB


	{
	//now there's a gap for the new range to slot in
	int i = 0;
	int j = i;
	while (i < n_ranges && ranges[i].start < start)
		j = ++i;
	memmove(&ranges[j + 1], &ranges[j], (n_ranges - j) * sizeof ranges[0]);
	memset(&ranges[j], 0, sizeof ranges[0]);
	ranges[j].start = start;
	ranges[j].end = end;
	ranges[j].t.source = source;
	ranges[j].t.type = type;
	ranges[j].t.id = 0;
	n_ranges++;
	}

	if (validate_ranges("after add new", 1)) return;

	//now we can merge neighbouring ranges of the same type
	do
	{
		dirty = 0;
		for (int i = 0; i < n_ranges - 1; i++)
		{
			if (ranges[i].t.type == ranges[i + 1].t.type && ranges[i].end+1 == ranges[i+1].start && ranges[i].t.type != GFX)
			{
				ranges[i].end = ranges[i + 1].end;
				ranges[i].t.source = source;
				if (ranges[i+1].t.id != 0)
					delete_type(&ranges[i + 1].t);
				memmove(&ranges[i + 1], &ranges[i + 2], (n_ranges - i - 2) * sizeof ranges[0]);
				memset(&ranges[n_ranges - 1], 0, sizeof ranges[0]);
				n_ranges--;
				dirty = 1;
				break;
			}
		}
	} while (dirty);

	if (validate_ranges("after merging", 1)) return;

	//finally, we can save them
	for (int i = 0; i < n_ranges; i++)
	{
		ranges[i].t.address = (unsigned short)ranges[i].start;
		ranges[i].t.size = ranges[i].end - ranges[i].start + 1;

		if (ranges[i].t.size == 0)
			delete_type(&ranges[i].t);
		else if (ranges[i].t.id == 0)
			save_type(&ranges[i].t);
		else
			update_type(&ranges[i].t);
	}

	validate_ranges("after save", 1);
}

static int get_neighbours(unsigned int address, unsigned int size)
{
	DB_RES *typeres = search_types_neighbour(address, size);
	ZX_TYPE *type = typeres->rows;
	if (typeres->count > MAX_RANGES)
		elog("typecount %d, @ %u size %u\n", typeres->count, address, size);

	//assert(typeres->count < MAX_RANGES);
	if (typeres->count >= MAX_RANGES)
		return -1;

	memset(ranges, 0, sizeof ranges);

	n_ranges = 0;
	while (typeres->count--)
	{
		ranges[n_ranges].t = *type;
		ranges[n_ranges].start = type->address;
		ranges[n_ranges].end = type->address + type->size - 1;
		n_ranges++;
		type++;
	}

	free_result(typeres);

	return validate_ranges("after loading", 0);
}

static void make_type(ZX_DATA_TYPE datatype, ZX_SOURCE source, unsigned short address, unsigned int size)
{
	if (size == 0) return;
	if (noisy_validate)
		elog("[LABEL] make2 %s %04Xh %u %u\n", test_type(datatype), address, address, size);
	ZX_TYPE zx;
	if (get_type_at_address(address, &zx))
	{
		//early out (already know address is within this label's range)
		if (zx.type == datatype && zx.source == source && zx.address + zx.size >= address + size)
			return;
	}
	if (get_neighbours(address, size) == 0)
		apply_new_range(address, address + size - 1, datatype, source);
}

void make_code_with_size(ZX_SOURCE source, unsigned short address, unsigned int size)
{
	make_type(CODE, source, address, size);
}

void make_code(ZX_SOURCE source, unsigned short address)
{
	//find the size, code extends until the next defined type or EOM
	unsigned int size = 65536 - address;
	DB_RES *typeres = search_types();
	ZX_TYPE *type = typeres->rows;
	int count = typeres->count;

	ZX_TYPE *prev_type = NULL;
	ZX_TYPE *curr_type = NULL;
	ZX_TYPE *next_type = NULL;

	if (count)
	{
		while (count && type->address < address)
			prev_type = type++, count--;

		if (count && type->address == address)
			curr_type = type++, count--;

		if (count)
			next_type = type;
	}

	//finished at the next type block, no matter what it is
	if (next_type)
		size = next_type->address - address;

	free_result(typeres);

	make_type(CODE, source, address, size);
}

void make_data(ZX_SOURCE source, unsigned short address, unsigned int size)
{
	make_type(DEFB, source, address, size);
}

void make_dataw(ZX_SOURCE source, unsigned short address, unsigned int size)
{
	make_type(DEFW, source, address, size&(~1UL));
	if (size & 1UL)
		make_type(DEFB, source, (unsigned short)(address + (size & (~1UL))), 1);
}

void make_datac(ZX_SOURCE source, unsigned short address, unsigned int size)
{
	make_type(DEFC, source, address, size);
}

void make_number(ZX_SOURCE source, unsigned short address, unsigned int size)
{
	assert((size % 5) == 0);
	make_type(DEFN, source, address, size);
}

void make_data_type(ZX_DATA_TYPE datatype, ZX_SOURCE source, unsigned short address, unsigned int size)
{
	assert(datatype == DEFB ||
		datatype == DEFW ||
		datatype == DEFM ||
		datatype == DEFS ||
		datatype == DEFC);
	make_type(datatype, source, address, size);
}

void make_fpstknumber(ZX_SOURCE source, unsigned short address, unsigned int size)
{
	make_type(DEFFPSTK, source, address, size);
}

static void test_begin(void)
{
	char *dbname = "test";

	delete_db(dbname);
	create_db(dbname);

	DB_RES *typeres = search_types();
	ZX_TYPE *type = typeres->rows;
	bulk_insert_begin();
	while (typeres->count--)
		delete_type(type++);
	bulk_insert_end();
	free_result(typeres);
}

static char *test_source(ZX_SOURCE source)
{
	switch (source)
	{
		case SR_LIBRARY: return "rom labels";
		case SR_AUTO: return "unused";
		case SR_MANUAL: return "typed in by hand";
		case SR_BUILTIN: return "used only while disassembling unknown areas";
		case SR_COVERAGE: return "generated by coverage";
		case SR_PCTRACE: return "generated by PC tracing";
		case SR_DATAACCESS: return "generated by dataaccess";
		default: return "UNKNOWN!";
	}
}

static void test_dump_type(const char *name, ZX_TYPE *type)
{
	if (name) elog("%s ", name);

	if (type == NULL)
	{
		elog("null\n");
		return;
	}

	elog("id: %6d ", type->id);
	elog("address: %04X %5d ", type->address, type->address);
	elog("size: %5d ", type->size);
	elog("type: %s ", type->type == 2 ? "DEFB" : "CODE");
	elog("source: %s ", test_source(type->source));
	elog("\n");
}

static char *test_type(ZX_DATA_TYPE type)
{
	switch (type)
	{
		case UNKNOWN: return "UNKNOWN";
		case CODE: return "CODE";
		case DEFB: return "DEFB";
		case DEFW: return "DEFW";
		case GFX: return "GFX";
		case DEFM: return "DEFM";
		case DEFS: return "DEFS";
		case STRUCT: return "STRUCT";
		case DEFC: return "DEFC";
		default:
			assert(0);
			return "undefined!";
	}
}
static void test_dump_type_range(const char *name, TYPE_RANGE *type)
{
	if (name) elog("%s ", name);

	if (type == NULL)
	{
		elog("null\n");
		return;
	}

	elog("id: %6d ", type->t.id);
	elog("address: %04X %5d ", type->start, type->start);
	elog("end: %04X %5d ", type->end, type->end);
	elog("size: %5d ", type->end - type->start + 1);
	elog("type: %s ", test_type(type->t.type));
	elog("source: %s ", test_source(type->t.source));
	elog("\n");
}

static void test_dump(int when)
{
	elog(when?"after\n":"before\n");
	DB_RES *typeres = search_types();
	ZX_TYPE *type = typeres->rows;
	while (typeres->count--)
		test_dump_type(NULL, type++);

	free_result(typeres);
}

static void test_end(void)
{
	test_dump(1);
	close_db();
}

static void test_1(void)
{
	ZX_TYPE t0 = { 0 };
	ZX_TYPE t1 = { 0 };
	ZX_TYPE t2 = { 0 };

	t0.address = 10;
	t0.size = 50;
	t0.type = DEFB;

	t1.address = 90;
	t1.size = 10;
	t1.type = DEFB;

	t2.address = 110;
	t2.size = 20;
	t2.type = DEFB;

	save_type(&t0);
	save_type(&t1);
	save_type(&t2);
	test_dump(0);

	make_data(SR_MANUAL, 100, 3);
}

static void test_2(void)
{
	ZX_TYPE t0 = { 0 };

	t0.address = 10;
	t0.size = 50;
	t0.type = DEFB;

	save_type(&t0);
	test_dump(0);

	make_data(SR_MANUAL, 100, 3);
}

static void test_3(void)
{
	ZX_TYPE t2 = { 0 };

	t2.address = 110;
	t2.size = 20;
	t2.type = DEFB;

	save_type(&t2);
	test_dump(0);

	make_data(SR_MANUAL, 100, 3);
}

static void test_4(void)
{
	ZX_TYPE t2 = { 0 };

	t2.address = 110;
	t2.size = 20;
	t2.type = DEFB;

	save_type(&t2);
	test_dump(0);

	make_data(SR_MANUAL, 100, 10);
}

static void test_5(void)
{
	ZX_TYPE t0 = { 0 };
	ZX_TYPE t1 = { 0 };
	ZX_TYPE t2 = { 0 };

	t0.address = 10;
	t0.size = 50;
	t0.type = DEFB;

	t1.address = 100;
	t1.size = 10;
	t1.type = CODE;

	t2.address = 110;
	t2.size = 20;
	t2.type = DEFB;

	save_type(&t0);
	save_type(&t1);
	save_type(&t2);
	test_dump(0);

	make_data(SR_MANUAL, 100, 3);
}

static void test_6(void)
{
	ZX_TYPE t2 = { 0 };

	t2.address = 100;
	t2.size = 100;
	t2.type = CODE;

	save_type(&t2);
	test_dump(0);

	make_data(SR_MANUAL, 130, 10);
}

static void test_7(void)
{
	ZX_TYPE t0 = { 0 };
	ZX_TYPE t1 = { 0 };
	ZX_TYPE t2 = { 0 };

	t0.address = 10;
	t0.size = 50;
	t0.type = DEFB;

	t1.address = 90;
	t1.size = 10;
	t1.type = DEFB;

	t2.address = 110;
	t2.size = 20;
	t2.type = DEFB;

	save_type(&t0);
	save_type(&t1);
	save_type(&t2);
	test_dump(0);

	make_data(SR_MANUAL, 100, 10);
}

static void test_8(void)
{
	ZX_TYPE t2 = { 0 };

	t2.address = 100;
	t2.size = 100;
	t2.type = CODE;

	save_type(&t2);
	test_dump(0);

	make_code(SR_MANUAL, 200);
}

static void test_9(void)
{
	ZX_TYPE t2 = { 0 };

	t2.address = 100;
	t2.size = 100;
	t2.type = CODE;

	save_type(&t2);
	test_dump(0);

	make_code(SR_MANUAL, 300);
}

static void test_10(void)
{
	ZX_TYPE t2 = { 0 };

	t2.address = 100;
	t2.size = 100;
	t2.type = CODE;

	save_type(&t2);
	test_dump(0);

	make_code(SR_MANUAL, 1);
}

static void test_11(void)
{
	ZX_TYPE t2 = { 0 };

	t2.address = 100;
	t2.size = 100;
	t2.type = DEFB;

	save_type(&t2);
	test_dump(0);

	make_code(SR_MANUAL, 1);
}

/*
[LABEL] make DEFB 15DAh 5594 4
prev id : 71001 address : 15D4 5588 size : 10 type : DEFB source : typed in by hand
curr id : 55289 address : 15DA 5594 size : 7 type : CODE source : generated by PC tracing
next id : 54341 address : 15DE 5598 size : 6 type : CODE source : typed in by hand
*/

/*
[LABEL] make CODE 1642h 5698 11
prev id: 79100 address: 1640 5696 size: 13 type: CODE source: typed in by hand
curr id: 79087 address: 1642 5698 size: 15 type: DEFB source: typed in by hand
next id: 79088 address: 164D 5709 size: 19 type: CODE source: typed in by hand 
*/
static void test_12(void)
{
	ZX_TYPE t0 = { 0 };
	ZX_TYPE t1 = { 0 };
	ZX_TYPE t2 = { 0 };

	t0.address = 56;
	t0.size = 6;
	t0.type = DEFB;
	t0.source = SR_MANUAL;

	t1.address = 5594;
	t1.size = 4;
	t1.type = CODE;
	t1.source = SR_PCTRACE;

	t2.address = 5598;
	t2.size = 6;
	t2.type = CODE;
	t2.source = SR_MANUAL;

	save_type(&t0);
	save_type(&t1);
	save_type(&t2);
	test_dump(0);

	make_data(SR_MANUAL, 5593, 4);

	elog("desired\n");
	elog("56 6 defb manual\n");
	elog("5593 4 defb manual\n");
	elog("5597 1 code manual\n");
	elog("5598 6 code manual\n");
}

static void (*testcases[])(void) = {
	test_1,
	test_2,
	test_3,
	test_4,
	test_5,
	test_6,
	test_7,
	test_8,
	test_9,
	test_10,
	test_11,
	test_12,
};

int tests_enabled = 0;
void typetest(void)
{
	if (!tests_enabled) return;

	for (int i = 0; i < sizeof testcases / sizeof * testcases; i++)
	{
		elog("\nTest case %d\n", i+1);
		test_begin();
		testcases[i]();
		test_end();
	}
}

void fill_type_gaps(void)
{
	ZX_TYPE **types = calloc(1, 65536 * sizeof * types);
	{
		DB_RES *res = search_types();
		ZX_TYPE *type = res->rows;
		while (res->count--)
		{
			for (int i = (int)type->address; i < min(65536, (int)type->address + (int)type->size); i++)
				types[i] = type;
			type++;
		}
		free_result(res);
	}

	{
		elog("[LABEL] filling in the blanks...");

		bulk_insert_begin();
		int i = 0;
		int counter = 0;
		while (i < 65536)
		{
			if (types[i] == NULL)
			{
				int start = i++;
				while (types[i] == NULL && i < 65536) i++;
				int end = i;

				ZX_TYPE t = { 0 };
				if (i >= 16384)
					t.type = DEFB;
				else
					t.type = CODE;
				t.address = (unsigned short)start;
				t.size = (unsigned short)(end - start + 1);
				t.source = SR_LIBRARY;
				save_type(&t);
				counter++;
			}
			i++;
		}
		bulk_insert_end();

		elog("%d\n", counter);
	}
	free(types);
	types = NULL;

	{
		elog("[LABEL] repairing overlaps...");
		DB_RES *res = search_types();
		ZX_TYPE *type = res->rows;
		int counter = 0;
		bulk_insert_begin();
		for (int i = 0; i < res->count - 1; i++)
		{
			if (type[i].address + type[i].size - 1 >= type[i + 1].address)
			{
				type[i].size = type[i + 1].address - type[i].address;
				update_type(&type[i]);
				counter++;
			}
		}
		bulk_insert_end();
		free_result(res);
		elog("%d\n", counter);
	}

	{
		elog("[LABEL] repairing zero sized types...");
		DB_RES *res = search_types();
		ZX_TYPE *type = res->rows;
		int counter = 0;
		bulk_insert_begin();
		for (int i = 0; i < res->count - 1; i++)
		{
			if (type[i].size == 0)
				delete_type(&type[i]);
		}
		bulk_insert_end();
		free_result(res);
		elog("%d\n", counter);
	}

	{
		elog("[LABEL] merging neighbours...");
		
		int counter = 0;
		for (;;)
		{
			char dirty = 0;

			bulk_insert_begin();

			DB_RES *res = search_types();
			ZX_TYPE *type = res->rows;

			for (int i = 0; i < res->count - 1; i++)
			{
				if (type[i].source == type[i + 1].source && type[i].type == type[i + 1].type)
				{

					type[i].size += type[i + 1].size;
					update_type(&type[i]);
					delete_type(&type[i + 1]);
					counter++;
					dirty = 1;
					i++;//skip merging the deleted block with its neighbour
				}
			}

			free_result(res);
			bulk_insert_end();

			if (!dirty) break;
		}

		elog("%d\n", counter);
	}
}
