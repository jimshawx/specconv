//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
void test_asm(void);
int expr_eval(char *exp, unsigned short pc, int *value, DB_RES *labels);
char *smart_strtok_s(char *str, const char *delimiters, char **context);
