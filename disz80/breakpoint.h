//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

typedef enum
{
	BP_ACTIVE = 1,
	BP_TEMP = 2
} BREAKPOINT_FLAGS;

typedef struct
{
	unsigned short pc;
	BREAKPOINT_FLAGS flags;
} BREAKPOINT;

int get_breakpoints(BREAKPOINT **breakpoint);
void remove_all_temp_breakpoints(void);
void remove_breakpoint(int index);
int check_breakpoints(void);
int toggle_breakpoint(unsigned short pc);
void set_breakpoint(unsigned short pc);
void set_temp_breakpoint(unsigned short pc);
BREAKPOINT_FLAGS get_breakpoint_status(unsigned short pc);
