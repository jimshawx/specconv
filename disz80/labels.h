//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

void gen_rom_labels(void);
const char *get_calculator_label(unsigned char b);
void dedupe(void);
void fill_type_gaps(void);
void apply_library_labels(void);

typedef enum
{
	LBL_SUB,
	LBL_JUMP
} LABEL_TYPE;

void format_label(char *dst, size_t len, unsigned short address, LABEL_TYPE type);
ZX_LABELTYPE label_type_from_jump_type(JUMP_TYPE jumptype);
ZX_LABELTYPE label_type_from_branch_type(BRANCH_TYPE branchtype);

void make_label(const char *labelname, ZX_SOURCE source, unsigned short address, ZX_LABELTYPE type);
void make_code(ZX_SOURCE source, unsigned short address);
void make_code_with_size(ZX_SOURCE source, unsigned short address, unsigned int size);
void make_data(ZX_SOURCE source, unsigned short address, unsigned int size);
void make_dataw(ZX_SOURCE source, unsigned short address, unsigned int size);
void make_datac(ZX_SOURCE source, unsigned short address, unsigned int size);
void make_data_type(ZX_DATA_TYPE datatype, ZX_SOURCE source, unsigned short address, unsigned int size);
void make_number(ZX_SOURCE source, unsigned short address, unsigned int size);
void make_fpstknumber(ZX_SOURCE source, unsigned short address, unsigned int size);