//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

#include <windows.h>
#include <commctrl.h>

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "resource.h"

#include "machine.h"
#include "daos.h"
#include "gfxview.h"
#include "specem.h"
#include "logger.h"
#include "dbgmem.h"

static int curr_width = 256;
static int curr_height = 192;
static int curr_offset = 16384;

static ZX_GFX_TYPE screen_format = SCREEN;
static int use_attributes = 1;

static HBITMAP bitmap;
static BITMAPINFO bmi;

static HBITMAP bitmap;

static unsigned int *gfx_out;

static unsigned char *binary = NULL;
static int binary_size;

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
extern HWND zxgfx_dialog;
extern HWND dis_window;
void toggle_dialog(HWND dlg, DIALOG_ID id, int toggleset);

void set_dimensions(int width, int height, int offset)
{
	curr_width = width;
	curr_height = height;
	curr_offset = offset;

	SetDlgItemInt(zxgfx_dialog, IDC_WIDTH_EDIT, width, 0);
	SetDlgItemInt(zxgfx_dialog, IDC_HEIGHT_EDIT, height, 0);
	SetScrollPos(GetDlgItem(zxgfx_dialog, IDC_OFFSETSCROLL), SB_CTL, curr_offset, 1);
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void killbitmap(void)
{
	if (bitmap)
	{
		DeleteObject(bitmap);//delete new bitmap
		bitmap = 0;
	}
return;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void kill_gfxview(void)
{
	killbitmap();

	if (gfx_out)
	{
		free(gfx_out);
		gfx_out = NULL;
	}
	
	if (binary)
	{
		free(binary);
		binary = NULL;
	}
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void makebitmap(unsigned int *image, int bpp, int w, int h, int src_w, int src_h)
{
	void *bits;

	h; w;

	killbitmap();

	bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth = src_w;
	bmi.bmiHeader.biHeight = -src_h;
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = (WORD)bpp;
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biSizeImage = 0;
	bmi.bmiHeader.biXPelsPerMeter = 75;
	bmi.bmiHeader.biYPelsPerMeter = 75;
	bmi.bmiHeader.biClrUsed = 0;
	bmi.bmiHeader.biClrImportant = 0;

	HDC hdc = CreateCompatibleDC(NULL);

	bitmap = CreateDIBSection(hdc, &bmi, DIB_RGB_COLORS, &bits, NULL, 0);
	assert(bitmap);

	memcpy(bits, image, src_w*src_h*(bpp>>3));
	GdiFlush();//force bits to get into DC!

	DeleteDC(hdc);

	SendDlgItemMessage(zxgfx_dialog, IDC_PICCY, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)bitmap);
	InvalidateRect(zxgfx_dialog, NULL, TRUE);

return;
}

static void copy_memory(void)
{
	//hacky - copy zx spectrum memory locally for speed
	if (binary != NULL) free(binary);
	binary_size = 65536;
	binary = malloc(binary_size);
	assert(binary);
	peek_fast_memcpy(binary, 0, 65536);
}

static const unsigned int zx_colours[16] =
{
	0x00000000, 0x000000df, 0x00df0000, 0x00df00df, 0x00df00, 0x00dfdf, 0xdfdf00, 0xdfdfdf,//normal
	0x00000000, 0x000000ff, 0x00ff0000, 0x00ff00ff, 0x00ff00, 0x00ffff, 0xffff00, 0xffffff,//bright
};

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void generate()
{
	unsigned int *gfx;

	screen_format = ((SendDlgItemMessage(zxgfx_dialog, IDC_SCREEN_FORMAT, BM_GETCHECK, 0, 0)==BST_CHECKED)*SCREEN)+
						((SendDlgItemMessage(zxgfx_dialog, IDC_SPRITE_FORMAT, BM_GETCHECK, 0, 0)==BST_CHECKED)*SPRITE)+
						((SendDlgItemMessage(zxgfx_dialog, IDC_CHAR_FORMAT, BM_GETCHECK, 0, 0)==BST_CHECKED)*CHARACTER)+
						((SendDlgItemMessage(zxgfx_dialog, IDC_RAW_FORMAT, BM_GETCHECK, 0, 0) == BST_CHECKED) * RAW);

	use_attributes = SendDlgItemMessage(zxgfx_dialog, IDC_ATTRIBUTES, BM_GETCHECK, 0, 0)==BST_CHECKED;

	curr_width = GetDlgItemInt(zxgfx_dialog, IDC_WIDTH_EDIT, NULL, 0);
	curr_height = GetDlgItemInt(zxgfx_dialog, IDC_HEIGHT_EDIT, NULL, 0);
	curr_offset = GetScrollPos(GetDlgItem(zxgfx_dialog, IDC_OFFSETSCROLL), SB_CTL);

	if (!curr_width)
		return;

	gfx_out = calloc(1, 65536 * 8 * sizeof * gfx_out);
	if (gfx_out == NULL) return;

	//transform the remaining part of the screen into 32bit rgb
	{
		copy_memory();

		gfx = gfx_out;
		unsigned char *src = binary + curr_offset;
		for (int x = 0; x < binary_size - curr_offset; x++)
		{
			unsigned char pix = *src++;
			for (unsigned char b = 0x80; b >= 1; b >>= 1)
				*gfx++ = (pix & b) ? zx_colours[0] : zx_colours[7];
		}
	}

	if (screen_format == SCREEN)
	{
		int size;
		unsigned int *src;
		unsigned int *dst, *dst2;

		size = curr_width * curr_height;
		dst = dst2 = malloc(size*sizeof(unsigned int));

		for (int s = 0; s < 3; s++)
		{
			for (int y0 = 0; y0 < 8; y0++)
			{
				for (int y1 = 0; y1 < 8; y1++)
				{
					if (s*64+y1*8+y0 >= curr_height) break;
					src = gfx_out + ((s*64+y1*8+y0)*curr_width);
					memcpy(dst, src, curr_width * sizeof(unsigned int));
					dst += curr_width;
				}
			}
		}
		memcpy(gfx_out, dst2, size*sizeof(unsigned int));
		free(dst2);
	}
	else if (screen_format == SPRITE)
	{
		int size;
		unsigned int *src;
		unsigned int *dst, *dst2;

		size = curr_width*curr_height;
		dst = dst2 = malloc(size*sizeof(unsigned int));

		src = gfx_out;
		for (int x = 0; x < curr_width/8; x++)
		{
			dst = dst2 + 8*x;
			for (int y = 0; y < curr_height; y++)
			{
				if (dst+8>dst2+size)
					break;
				memcpy(dst, src, 8*sizeof(unsigned int));
				src += 8;
				dst += curr_width;
			}
		}

		memcpy(gfx_out, dst2, size*sizeof(unsigned int));
		free(dst2);
	}
	else if (screen_format == CHARACTER)
	{
		int size;
		unsigned int *src;
		unsigned int *dst, *dst2;

		size = curr_width*curr_height;
		dst = dst2 = malloc(size*sizeof(unsigned int));

		src = gfx_out;
		for (int s = 0; s < curr_height; s += 8)
		{
			for (int x = 0; x < curr_width/8; x++)
			{
				dst = dst2 + s*curr_width + 8*x;
				for (int y = 0; y < 8; y++)
				{
					if (dst+8>dst2+size)
						break;
					memcpy(dst, src, 8*sizeof(unsigned int));
					src += 8;
					dst += curr_width;
				}
			}
		}

		memcpy(gfx_out, dst2, size*sizeof(unsigned int));
		free(dst2);
	}
	else if (screen_format == RAW)
	{
		int size;
		unsigned int *src = gfx_out;
		unsigned int *dst, *dst2;

		size = curr_width * curr_height;
		dst = dst2 = malloc(size * sizeof(unsigned int));

		for (int y = 0; y < curr_height; y++)
		{
			memcpy(dst, src, curr_width * sizeof(unsigned int));
			dst += curr_width;
			src += curr_width;
		}
		memcpy(gfx_out, dst2, size * sizeof(unsigned int));
		free(dst2);
	}

	if (use_attributes)
	{
		unsigned char *src;
		unsigned int *dst;
		unsigned int ink, paper;

		unsigned char col;
		unsigned char bright;

		src = (unsigned char *)(binary + curr_width*curr_height/8 + curr_offset);
		for (int y = 0; y < curr_height/8; y++)
		{
			for (int x = 0; x < curr_width/8; x++)
			{
				dst = gfx_out+y*8*curr_width+x*8;
				col = (src < binary + binary_size) ? *src++ : (7<<3);
				
				bright = (col & 0x40) >> 3;

				ink = zx_colours[(col&7)+bright];
				paper = zx_colours[((col>>3)&7)+bright];

				for (int py = 0; py < 8; py++)
				{
					for (int px = 0; px < 8; px++)
					{
						if (!dst[px])
							dst[px] = ink;
						else
							dst[px] = paper;
					}
					dst+=curr_width;
				}
			}
		}
	}

	makebitmap(gfx_out, 32, curr_width, curr_height, curr_width, curr_height);

	free(gfx_out);
	gfx_out = NULL;

return;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void savegfx()
{
	OPENFILENAME ofn;
	char filename[_MAX_PATH];
	BOOL res;

	filename[0] = '\0';
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = dis_window;
	ofn.hInstance = NULL;
	ofn.lpstrFilter = "BMP Files\0*.bmp\0All Files\0*.*\0";
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = 0;
	ofn.lpstrFile = filename;
	ofn.nMaxFile = _MAX_PATH;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrTitle = "Select BMP File";
	ofn.Flags = OFN_FILEMUSTEXIST|OFN_EXPLORER;
	ofn.nFileOffset=0;
	ofn.nFileExtension=0;
	ofn.lpstrDefExt="bmp";
	ofn.lCustData=0;
	ofn.lpfnHook=NULL;
	ofn.lpTemplateName="";

	res = GetSaveFileName(&ofn);
	if (res)
	{
		FILE *f;
		BITMAPFILEHEADER bmfh;

		bmfh.bfType = ('M'<<8)|'B';
		bmfh.bfSize = sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFO)+curr_width*curr_height*sizeof(unsigned int);
		bmfh.bfReserved1 = 0;
		bmfh.bfReserved2 = 0;
		bmfh.bfOffBits = sizeof(BITMAPFILEHEADER)+sizeof(BITMAPINFO);

		fopen_s(&f, ofn.lpstrFile, "wb");
		if (f)
		{
			fwrite(&bmfh, 1, sizeof(BITMAPFILEHEADER), f);
			fwrite(&bmi, 1, sizeof(BITMAPINFO), f);
			fwrite(gfx_out, 1, curr_width*curr_height*sizeof(unsigned int), f);
			fclose(f);
		}
	}

return;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
int get_gfx_size(ZX_GFX_TYPE type, int width, int height)
{
	return (width * height) / 8 + (!!(type&ATTRIBUTES))*(width * height) / 64;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
char *get_gfx_type(ZX_GFX_TYPE type)
{
	type &= ~ATTRIBUTES;
	switch (type)
	{
		case SCREEN: return "SCREEN";
		case SPRITE: return "SPRITE";
		case CHARACTER: return "CHARACTER";
		case RAW: return "RAW";
		default: assert(0);
	}
	return "UNKNOWN";
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void load_gfx_list(HWND hwndDlg)
{
	SendDlgItemMessage(hwndDlg, IDC_GFXLIST, LB_RESETCONTENT, 0, 0);
	DB_RES *gfx = search_types_by_type(GFX);
	ZX_TYPE *type = gfx->rows;
	while (gfx->count--)
	{
		ZX_LABEL label;
		if (get_label_by_address(type->address, &label))
		{
			SendDlgItemMessage(hwndDlg, IDC_GFXLIST, LB_ADDSTRING, 0, (LPARAM)label.name);
		}
		else
		{
			char text[100];
			sprintf_s(text, 100, "GFX_%04X", type->address);
			SendDlgItemMessage(hwndDlg, IDC_GFXLIST, LB_ADDSTRING, 100, (LPARAM)text);
			memset(&label, 0, sizeof label);
			label.address = type->address;
			label.name = text;
			label.source = SR_MANUAL;
			label.type = LT_DATA;
			save_label(&label);
		}
		type++;
	}
	free_result(gfx);

}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void set_dialog_status(HWND hwndDlg)
{
	SetDlgItemInt(hwndDlg, IDC_WIDTH_EDIT, curr_width, FALSE);
	SetDlgItemInt(hwndDlg, IDC_HEIGHT_EDIT, curr_height, FALSE);
	SetDlgItemInt(hwndDlg, IDC_OFFSET_EDIT, curr_offset, FALSE);

	{
		SCROLLINFO si = { 0 };
		si.cbSize = sizeof si;
		si.fMask = SIF_POS| SIF_TRACKPOS;
		si.nPos = si.nTrackPos = curr_offset;
		SetScrollInfo(GetDlgItem(hwndDlg, IDC_OFFSETSCROLL), SB_CTL, &si, 1);
	}

	SendDlgItemMessage(hwndDlg, IDC_SCREEN_FORMAT, BM_SETCHECK, screen_format == SCREEN ? BST_CHECKED : BST_UNCHECKED, 0);
	SendDlgItemMessage(hwndDlg, IDC_SPRITE_FORMAT, BM_SETCHECK, screen_format == SPRITE ? BST_CHECKED : BST_UNCHECKED, 0);
	SendDlgItemMessage(hwndDlg, IDC_CHAR_FORMAT, BM_SETCHECK, screen_format == CHARACTER ? BST_CHECKED : BST_UNCHECKED, 0);
	SendDlgItemMessage(hwndDlg, IDC_RAW_FORMAT, BM_SETCHECK, screen_format == RAW ? BST_CHECKED : BST_UNCHECKED, 0);

	SendDlgItemMessage(hwndDlg, IDC_ATTRIBUTES, BM_SETCHECK, use_attributes ? BST_CHECKED : BST_UNCHECKED, 0);
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void gfx_database_changed(void)
{
	curr_width = 256;
	curr_height = 192;
	curr_offset = 16384;

	screen_format = SCREEN;
	use_attributes = 1;

	load_gfx_list(zxgfx_dialog);
	set_dialog_status(zxgfx_dialog);
	copy_memory();
	generate();
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK GfxDialogProc(
  HWND hwndDlg,  // handle to dialog box
  UINT uMsg,     // message
  WPARAM wParam, // first message parameter
  LPARAM lParam  // second message parameter
  )
{
	static const UDACCEL accelh[] = {{0,(UINT)-1}, {2,(UINT)-2},  {3,(UINT)-8}};
	static const UDACCEL accelw[] = {{0,(UINT)-8}, {2,(UINT)-16}, {3,(UINT)-32}};

	switch (uMsg)
	{
		case WM_INITDIALOG:
			set_dialog_status(hwndDlg);

			SendDlgItemMessage(hwndDlg, IDC_WIDTH, UDM_SETRANGE, 0, (1024<<16)|8);
			SendDlgItemMessage(hwndDlg, IDC_WIDTH, UDM_SETACCEL, 3, (LPARAM)accelw);

			SendDlgItemMessage(hwndDlg, IDC_HEIGHT, UDM_SETRANGE, 0, (1024<<16)|1);
			SendDlgItemMessage(hwndDlg, IDC_HEIGHT, UDM_SETACCEL, 3, (LPARAM)accelh);
			{
			SCROLLINFO si = { 0 };
			si.cbSize = sizeof si;
			si.fMask = SIF_ALL;
			si.nMin = 0;
			si.nMax = 65536;
			si.nPos = si.nTrackPos = curr_offset;
			si.nPage = 32;
			SetScrollInfo(GetDlgItem(hwndDlg, IDC_OFFSETSCROLL), SB_CTL, &si, 1);
			}

			bitmap = CreateBitmap(32,32,1,32,NULL);
			SendDlgItemMessage(hwndDlg, IDC_PICCY, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)bitmap);

			copy_memory();
			generate();
			
			return 0;

		case WM_SHOWWINDOW:
			if (wParam)
			{
				generate();
				load_gfx_list(hwndDlg);
			}
			break;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDC_GENERATE:
					generate();
					break;

				case IDC_SAVE:
					savegfx();
					break;

				case IDC_SCREEN_FORMAT:
				case IDC_SPRITE_FORMAT:
				case IDC_CHAR_FORMAT:
				case IDC_RAW_FORMAT:
				case IDC_ATTRIBUTES:
					generate();
					break;

				case IDC_UPDATEGFX:
					{
						//save the label
						{
							static char text[100];
							SendDlgItemMessage(hwndDlg, IDC_GFXNAME, WM_GETTEXT, 100, (LPARAM)text);
							if (text[0] == '\0')
							{
								sprintf_s(text, 100, "GFX_%04X", curr_offset);
								SendDlgItemMessage(hwndDlg, IDC_GFXNAME, WM_SETTEXT, 100, (LPARAM)text);
							}
							ZX_LABEL label = { 0 };
							label.address = (unsigned short)curr_offset;
							label.source = SR_MANUAL;
							label.name = text;
							label.type = LT_DATA;

							ZX_LABEL existing_label;
							if (get_label_by_name(text, &existing_label))
							{
								if (curr_offset != existing_label.address)
								{
									int mb = MessageBox(hwndDlg, "There is a label with the same name at a different address, OK to move it?", "Name Conflict", MB_OKCANCEL | MB_ICONWARNING);
									if (mb != MB_OK)
										break;
									existing_label.address = (unsigned short)curr_offset;
									existing_label.source = SR_MANUAL;
									save_label(&existing_label);
								}
							}

							if (get_label_by_address((unsigned short)curr_offset, &existing_label))
							{
								if (!compare_labels(&label, &existing_label) != 0)
								{
									delete_label(&existing_label);
									save_label(&label);
								}
							}
							else
							{
								save_label(&label);
							}
						}

						//save the graphic type
						{
							ZX_TYPE type = { 0 };
							type.address = (unsigned short)curr_offset;
							type.gfxwidth = (unsigned short)curr_width;
							type.gfxheight = (unsigned short)curr_height;
							type.gfxtype = screen_format + (use_attributes * ATTRIBUTES);
							type.size = get_gfx_size(screen_format + use_attributes * ATTRIBUTES, curr_width, curr_height);
							type.source = SR_MANUAL;
							type.type = GFX;

							ZX_TYPE existing_type;
							if (get_type_by_address((unsigned short)curr_offset, &existing_type))
							{
								if (!compare_types(&type, &existing_type))
								{
									delete_type(&existing_type);
									save_type(&type);
								}
							}
							else
							{
								save_type(&type);
							}
						}

						load_gfx_list(hwndDlg);
						//SendMessage(hwndDlg, WM_CLOSE, 0, 0);
					}
					break;

				case IDC_CANCELUPDATEGFX:
					SendMessage(hwndDlg, WM_CLOSE, 0, 0);
					break;

				//problem - how to detect if user did it, or code did it.
					/*
				case IDC_WIDTH_EDIT:
					if (HIWORD(wParam) == EN_CHANGE)
					{
						curr_width = (int)GetDlgItemInt(hwndDlg, IDC_WIDTH_EDIT, NULL, FALSE);
						generate();
					}
					break;
				case IDC_HEIGHT_EDIT:
					if (HIWORD(wParam) == EN_CHANGE)
					{
						curr_height = (int)GetDlgItemInt(hwndDlg, IDC_WIDTH_EDIT, NULL, FALSE);
						generate();
					}
					break;
				case IDC_OFFSET_EDIT:
					if (HIWORD(wParam) == EN_CHANGE)
					{
						int offset = (int)GetDlgItemInt(hwndDlg, IDC_OFFSET_EDIT, NULL, FALSE);
						SetScrollPos(GetDlgItem(hwndDlg, IDC_OFFSETSLIDER), SB_CTL, offset, TRUE);
						generate();
					}
					break;
					*/
				case IDC_GFXLIST:
				{
					if (HIWORD(wParam) == LBN_DBLCLK)
					{
						char name[100];
						int sel = (int)SendDlgItemMessage(hwndDlg, IDC_GFXLIST, LB_GETCURSEL, 0, 0);
						SendDlgItemMessage(hwndDlg, IDC_GFXLIST, LB_GETTEXT, sel, (LPARAM)name);
						ZX_LABEL label;
						if (get_label_by_name(name, &label))
						{
							ZX_TYPE type;
							if (get_type_by_address(label.address, &type))
							{
								curr_width = type.gfxwidth;
								curr_height = type.gfxheight;
								curr_offset = type.address;
								screen_format = type.gfxtype & ~ATTRIBUTES;
								use_attributes = !!(type.gfxtype & ATTRIBUTES);
								set_dialog_status(hwndDlg);
								SendDlgItemMessage(hwndDlg, IDC_GFXNAME, WM_SETTEXT, 0, (LPARAM)name);
								generate();
							}
						}
					}
				}
			}
			break;

		case WM_VSCROLL:
		case WM_HSCROLL:
			{
				int id = GetDlgCtrlID((HWND)lParam);
				if (id == IDC_WIDTH || id == IDC_HEIGHT)
				{
					generate();
				}
				else if (id == IDC_OFFSETSCROLL)
				{
					SCROLLINFO si = { 0 };
					si.cbSize = sizeof si;
					si.fMask = SIF_ALL;
					GetScrollInfo(GetDlgItem(hwndDlg, IDC_OFFSETSCROLL), SB_CTL, &si);

					switch (LOWORD(wParam))
					{
						case SB_TOP: si.nPos = si.nMin; break;
						case SB_BOTTOM: si.nPos = si.nMax; break;
						case SB_LINEUP: si.nPos--; break;
						case SB_LINEDOWN: si.nPos++; break;
						case SB_PAGEUP: si.nPos -= si.nPage; break;
						case SB_PAGEDOWN: si.nPos += si.nPage; break;
						case SB_THUMBTRACK: si.nPos = si.nTrackPos; break;
						case SB_ENDSCROLL:
						case SB_THUMBPOSITION: return 0;
						default:
							assert(0);
					}
					si.fMask = SIF_POS;
					SetScrollInfo(GetDlgItem(hwndDlg, IDC_OFFSETSCROLL), SB_CTL, &si, TRUE);
					GetScrollInfo(GetDlgItem(hwndDlg, IDC_OFFSETSCROLL), SB_CTL, &si);
					
					int pos = GetScrollPos(GetDlgItem(hwndDlg, IDC_OFFSETSCROLL), SB_CTL);
					SetDlgItemInt(hwndDlg, IDC_OFFSET_EDIT, pos, FALSE);

					generate();
				}
			}
			break;

		case WM_CLOSE:
			toggle_dialog(hwndDlg, DLG_ZXGFX, 0);

			if (binary)
			{
				free(binary);
				binary = NULL;
			}

			killbitmap();
			break;
	}

return 0;
}
