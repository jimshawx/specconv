//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

LRESULT CALLBACK GfxDialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
void set_dimensions(int width, int height, int offset);
void kill_gfxview(void);
int get_gfx_size(ZX_GFX_TYPE type, int width, int height);
char *get_gfx_type(ZX_GFX_TYPE type);
void gfx_database_changed(void);