//---------------------------------------------------------------------------------------
// (C) Copyright Jim Shaw 2004
//---------------------------------------------------------------------------------------
int toggle_breakpoint(unsigned short pc);
int toggle_breakpoint_line(unsigned int line);
void set_breakpoint(unsigned short pc);
void set_breakpoint_line(unsigned int line);
void set_temp_breakpoint(unsigned short pc);
void do_breakpoint_dialog(HWND);
int check_breakpoints(void);
