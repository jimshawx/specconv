//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

typedef enum
{
	FS_SCANLINE = 0,//convert each scanline at the end of the scanline
	FS_SCREEN = 1,//convert each frame at the end of the frame
	FS_IMMEDIATE = 2,//write the pixels into the display exactly when the cpu does
	FS_OFF = 3,//don't update the screen
	FS_ULA = 4,//copy the screen in realtime using the ula
	FS_MAX = FS_ULA
} FASTSCREEN;

typedef struct
{
	unsigned char I;
	unsigned short HL_alt;
	unsigned short DE_alt;
	unsigned short BC_alt;
	unsigned short AF_alt;
	unsigned short HL;
	unsigned short DE;
	unsigned short BC;
	unsigned short IY;
	unsigned short IX;
	unsigned char interrupt;
	unsigned char R;
	unsigned short AF;
	unsigned short SP;
	unsigned char interrupt_mode;
	unsigned char border_colour;

	//extend this to your heart's content.
	unsigned short PC;
	unsigned char port_7ffd;
	unsigned char R7;

} START_REGS;

enum
{
	SPEC_16K,
	SPEC_48K,
	SPEC_128K,
	SPEC_PLUS2,
	SPEC_PLUS2A,
	SPEC_PLUS3,
};

#define BF_CONTENDED 1
#define BF_READONLY  2

typedef struct
{
	int bank_no;
	unsigned int flags;
	unsigned char *memory;
} BANK;

typedef struct
{
	BANK bank;
	BANK *prevbank;
	char ff_paged_in;
	char ff_nmi_pending;
} MULTIFACE;

typedef struct
{
	BANK bank;
	BANK *prevbank;
} INTERFACE1;

typedef struct
{
	int machine_id;
	unsigned char border;
	unsigned char mic;
	unsigned char ear;
	unsigned long long cycle;

	int cpu_hz;

	BANK *membank[4];
	BANK rambanks[8];
	BANK rombanks[4];
	BANK nullbank;

	unsigned int first_scanline;
	unsigned int screen_start;
	unsigned int line_length;
	unsigned int frame_length;
	unsigned int scanlines;
	unsigned int ear_cycle;
	unsigned int screen_cycle;

	unsigned char port_7FFD;
	unsigned char port_1FFD;

	int ear_timer;
	int interrupt_timer;
	int line_timer;
	int frame_cycle;
	int ay_timer, ay_countdown;

	int tape_playing;

	int last_was_ei;

	char has_ay;

	char has_if1;
	char has_mf1;
	char has_mf128;
	char has_mf3;
	char has_shadow_roms;

	INTERFACE1 if1;

	MULTIFACE mf1;
	MULTIFACE mf128;
	MULTIFACE mf3;

	char nmi;

} MACHINE;

#define READ_CONTENTION 1
#define WRITE_CONTENTION 0

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
unsigned short emulate_start_game(START_REGS *start_regs);
unsigned short emulate_next(void);
void emulate_init(void);
void emulate_stop(void);
void kill_machine(void);
void init_machine(int machine_id);
void reset_machine(void);
unsigned char peek_fast(unsigned short a);
unsigned short peek_fastw(unsigned short a);
void poke_fastw(unsigned short a, unsigned short b);
void poke_fast(unsigned short a, unsigned char b);
void *peeka_fast(unsigned short a);
void peek_fast_memcpy(void *vdst, unsigned short src, size_t length);
void poke_fast_memcpy(unsigned short dst, void *vsrc, size_t length);
void update_screen(void);
void set_ram(unsigned short dest, unsigned char *src, unsigned int length);
void machine_play_tape(int motor);
void set_machine_rate(int mult);
void machine_page_rom(void);
void page_ram(int bank);
unsigned int get_cycle(void);
void toggle_fastscreen(void);
void set_screen_mode(FASTSCREEN mode);
FASTSCREEN get_screen_mode(void);
unsigned int get_first_scanline(void);
unsigned char in(unsigned short p);
void out(unsigned short p, unsigned char d);
void page_interface1(void);
void page_multiface1(unsigned char in);
void page_multiface128(unsigned char in);
void page_multiface3(unsigned char in);
void multiface_nmi(void);
