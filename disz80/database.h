//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

sqlite3 *db_init(void);
void db_shutdown(void);
char *local_time(double julianday);

void db_free(DB_RES *res);

//---------------------------------------------------------------------------------------
int begin(sqlite3 *db);
int commit(sqlite3 *db);
int rollback(sqlite3 *db);
//---------------------------------------------------------------------------------------

DB_RES *search_ZXDB(sqlite3 *db);
int get_ZXDB_id_by_name(sqlite3 *db, char *name);
DB_RES *get_ZXDB_by_dbid(sqlite3 *db, int dbid);
int save_ZXDB(sqlite3 *db, ZX_DB *zx);
int delete_ZXDB(sqlite3 *db, int dbid);

//---------------------------------------------------------------------------------------

DB_RES *search_ZXLABEL_by_name(sqlite3 *db, int dbid, char *name);
DB_RES *search_ZXLABEL_by_dbid(sqlite3 *db, int dbid);
DB_RES *search_ZXLABEL_by_address(sqlite3 *db, int dbid, unsigned int address);
void save_ZXLABEL_batch(sqlite3 *db, ZX_LABEL *labels, int count);
int save_ZXLABEL(sqlite3 *db, ZX_LABEL *zx);
int delete_ZXLABEL(sqlite3 *db, int id);
int update_ZXLABEL(sqlite3 *db, ZX_LABEL *zx);

//---------------------------------------------------------------------------------------

DB_RES *search_ZXTYPE_by_dbid(sqlite3 *db, int dbid);
void save_ZXTYPE_batch(sqlite3 *db, ZX_TYPE *types, int count);
int save_ZXTYPE(sqlite3 *db, ZX_TYPE *zx);
int delete_ZXTYPE(sqlite3 *db, int id);
int update_ZXTYPE(sqlite3 *db, ZX_TYPE *zx);
DB_RES *search_ZXTYPE_neighbours(sqlite3 *db, int dbid, unsigned int address, unsigned int size);
DB_RES *search_ZXTYPE_by_address(sqlite3 *db, int dbid, unsigned int address);
DB_RES *search_ZXTYPE_at_address(sqlite3 *db, int dbid, unsigned int address);
DB_RES *search_ZXTYPE_by_type(sqlite3 *db, int dbid, ZX_DATA_TYPE type);

//---------------------------------------------------------------------------------------
DB_RES *search_ZXSETTING(sqlite3 *db);
DB_RES *search_ZXSETTING_by_name(sqlite3 *db, char *name);
int save_or_update_ZXSETTING(sqlite3 *db, ZX_SETTING *zx);
