//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

void disassemble_advanced(void);
unsigned short find_next_pc(void);
unsigned short get_pc_at_line(int line);
void reload_database(void);

void disview_database_changed(void);
void disview_get_labels(void);
void disview_skip_to_label(char *name);
void disview_skip_to_pc(void);
void disview_skip_to_address(unsigned short pc);
char *pc_to_label(unsigned short target, signed char o, char is_offset, int hexfmt);
void disview_close(void);
void disview_apply_coverage(void);
DB_RES *get_db_labels(void);

