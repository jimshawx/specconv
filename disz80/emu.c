//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <assert.h>

#include "machine.h"
#include "files.h"
#include "zxgfx.h"
#include "emu.h"
#include "sound.h"
#include "specem.h"
#include "ay8912.h"
#include "coverage.h"
#include "dbgmem.h"
#include "logger.h"
#include "loaders.h"

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void pfx_dd(void);
static void pfx_fd(void);
static void dec8(unsigned char *r);

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
extern MACHINE machine;

static REG_PACK regs;
static unsigned char ix_iy=R_IX;
unsigned int cycles_used;
unsigned int tape_cycles;

static const unsigned char parity[256]=
{
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04,
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,
	0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00,
	0x04, 0x00, 0x00, 0x04, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x04, 0x00, 0x00, 0x04, 
};

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
REG_PACK *get_regs(void)
{
	return &regs;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void page_ram(int bank)
{
	machine.membank[3] = &machine.rambanks[bank];
}

void page_interface1(void)
{
	//elog("[IF1 ] PAGE %s %04X %u\n", machine.membank[0] != &machine.if1rombank ? "in from":"out  to", (unsigned int)regs.pc, (unsigned int)regs.pc);

	if (machine.membank[0] == &machine.if1.bank)
	{
		//page out
		machine.membank[0] = machine.if1.prevbank;
	}
	else
	{
		//page in
		machine.if1.prevbank = machine.membank[0];
		machine.membank[0] = &machine.if1.bank;

		//hack, copy in the second 8K of the just-paged-out ROM, because we only handle 16K bank swaps
		memcpy(machine.membank[0]->memory + 0x2000, machine.if1.prevbank->memory + 0x2000, 0x2000);
	}
}

void page_multiface1(unsigned char in)
{
	//elog("[MF1 ] PAGE %s %04X %u\n", machine.membank[0] != &machine.if1rombank ? "in from":"out  to", (unsigned int)regs.pc, (unsigned int)regs.pc);

	if (!in)
	{
		if (machine.mf1.ff_paged_in)
		{
			//page out
			machine.membank[0] = machine.mf1.prevbank;
			machine.mf1.ff_paged_in = 0;
		}
	}
	else
	{
		if (!machine.mf1.ff_paged_in)
		{
			//page in
			machine.mf1.ff_paged_in = 1;
			machine.mf1.prevbank = machine.membank[0];
			machine.membank[0] = &machine.mf1.bank;
		}
	}
}

void page_multiface128(unsigned char in)
{
	//elog("[MF128 ] PAGE %s %04X %u\n", machine.membank[0] != &machine.if1rombank ? "in from":"out  to", (unsigned int)regs.pc, (unsigned int)regs.pc);

	if (!in)
	{
		if (machine.mf128.ff_paged_in)
		{
			//page out
			machine.membank[0] = machine.mf128.prevbank;
			machine.mf128.ff_paged_in = 0;
		}
	}
	else
	{
		if (!machine.mf128.ff_paged_in)
		{
			//page in
			machine.mf128.ff_paged_in = 1;
			machine.mf128.prevbank = machine.membank[0];
			machine.membank[0] = &machine.mf128.bank;
		}
	}
}

void page_multiface3(unsigned char in)
{
	//elog("[MF3 ] PAGE %s %04X %u\n", machine.membank[0] != &machine.if1rombank ? "in from":"out  to", (unsigned int)regs.pc, (unsigned int)regs.pc);

	if (!in)
	{
		if (machine.mf3.ff_paged_in)
		{
			//page out
			machine.membank[0] = machine.mf3.prevbank;
			machine.mf3.ff_paged_in = 0;
		}
	}
	else
	{
		if (!machine.mf3.ff_paged_in)
		{
			//page in
			machine.mf3.ff_paged_in = 1;
			machine.mf3.prevbank = machine.membank[0];
			machine.membank[0] = &machine.mf3.bank;
		}
	}
}

void multiface_nmi(void)
{
	if (machine.has_mf1)
	{
		machine.nmi = 1;
		machine.mf1.ff_nmi_pending = 1;
	}
	if (machine.has_mf128)
	{
		machine.nmi = 1;
		machine.mf128.ff_nmi_pending = 1;
	}
	if (machine.has_mf3)
	{
		machine.nmi = 1;
		machine.mf3.ff_nmi_pending = 1;
	}
}

void init_parity(void)
{
	/*
//	FILE *f = fopen("parity.txt", "w");
	int x, b, c;
	for (x = 0; x < 256; x++)
	{
		c = 1;
		for (b = 1; b < 256; b<<=1)
		{
			c ^= !!(x&b);
		}
//		c ^= 1;
		parity[x] = c * FLAGS_PF;
//		fprintf(f, "%02X, ", parity[x]);
//		fputc('\n', f);
	}
//	fclose(f);
	*/
}

//this one used by non-emulation code to access ram at speed, without contention or anything
void poke_fast(unsigned short a, unsigned char b)
{
	machine.membank[a>>14]->memory[a&0x3fff]=b;
}

//this one used by non-emulation code to access ram at speed, without contention or anything
void poke_fastw(unsigned short a, unsigned short b)
{
	poke_fast(a, (unsigned char)b);
	poke_fast(a + 1, (unsigned char)(b >> 8));
}

//this one used by emulation code to access ram with contention etc.
void poke(unsigned short a, unsigned char b)
{
	int bank;

	mark_type(a, MK_DATA_WR);

	bank = a>>14;

	if (bank == 1)
		dirty_screen(a, b);

	if (machine.membank[bank]->flags&BF_READONLY)
	{
		//write to ROM! (spectrum ROM does this in normal operation!)
	}
	else if (machine.membank[bank]->flags&BF_CONTENDED)
	{
#if WRITE_CONTENTION
#endif
		//contended write
		machine.membank[bank]->memory[a&0x3fff]=b;
	}
	else
	{
		//non-contended write
		machine.membank[bank]->memory[a&0x3fff]=b;
	}
}

//this one used by non-emulation code to access ram at speed, without contention or anything
unsigned char peek_fast(unsigned short a)
{
	return machine.membank[a>>14]->memory[a&0x3fff];
}

//this one used by non-emulation code to access ram at speed, without contention or anything
unsigned short peek_fastw(unsigned short a)
{
	unsigned short p;
	p = peek_fast(a);
	p |= (unsigned short)peek_fast(a + 1) << 8;
	return p;
}

//this one used by non-emulation code to get the linear access of ram at speed, without contention or anything
void *peeka_fast(unsigned short a)
{
	//limit access to screen
	assert((a >> 14) == 1);
	return &machine.membank[a >> 14]->memory[a & 0x3fff];
}

//this one used by non-emulation code to get a copy of a block of zx memory
void peek_fast_memcpy(void *vdst, unsigned short src, size_t length)
{
	char *dst = (char *)vdst;
	while (length--)
		*dst++ = peek_fast(src++);
}

void poke_fast_memcpy(unsigned short dst, void *vsrc, size_t length)
{
	char *src = (char *)vsrc;
	while (length--)
		poke_fast(dst++, *src++);
}

static void contend(int bank)
{
	bank;
#if READ_CONTENTION
	if (bank==1 || (machine.membank[bank]->flags&BF_CONTENDED))
	{
		int scanline;
		int scanpixel;

		unsigned int cycle = machine.frame_cycle + cycles_used;

		scanline = (cycle%machine.frame_length) / machine.line_length;
		scanpixel = (cycle%machine.frame_length) % machine.line_length;
		if (scanline >= (int)machine.first_scanline && scanline < (int)machine.first_scanline+192)
		{
			//we're on a visible scanline, check for borders
			if (scanpixel >= 47 && scanpixel < 47+128)
			{
				int contention;
				contention = (scanpixel-47)&7;
				switch (machine.machine_id)
				{
					case SPEC_48K:
					case SPEC_16K:
						cycles_used += max(0, contention-2);
						break;
					default:
						cycles_used += max(0, contention-1);
				}
			}
		}
	}
#endif
}

//this one used by emulation code to access ram with contention etc. (peeked as DATA)
static unsigned char peek(unsigned short a)
{
	int bank;

	mark_type(a, MK_DATA_RD);

	bank = a>>14;
	contend(bank);
	
	return machine.membank[bank]->memory[a&0x3fff];
}

//this one used by emulation code to access ram with contention etc. (peeked as CODE)
static unsigned char peekc(unsigned short a)
{
	int bank;

	mark_type(a, MK_CODE);

	bank = a>>14;
	contend(bank);
	return machine.membank[bank]->memory[a&0x3fff];
}

//get the linear address of the memory
//NB! only allowed to read 1 byte from returned address
static unsigned char rom;
static unsigned char *peeka(unsigned short a)
{
	int bank;

	mark_type(a, MK_DATA_RD);

	bank = a>>14;
	contend(bank);

	//ensure we don't corrupt rom
	if (machine.membank[bank]->flags & BF_READONLY)
	{
		rom = machine.membank[bank]->memory[a&0x3fff];
		return &rom;
	}

	return &machine.membank[bank]->memory[a&0x3fff];
}

static void pokew(unsigned short a, unsigned short b)
{
	poke(a, (unsigned char)b);
	poke(a+1, (unsigned char)(b>>8));
}

static unsigned short peekw(unsigned short a)
{
	unsigned short p;
	p = peek(a);
	p |= (unsigned short)peek(a+1)<<8;
return p;
}

static unsigned short peekwc(unsigned short a)
{
	unsigned short p;
	p = peekc(a);
	p |= (unsigned short)peekc(a+1)<<8;
return p;
}

static void push(unsigned short a)
{
	regs.sp -= 2;
	pokew(regs.sp,a);
}

static unsigned short pop(void)
{
	unsigned short t;
	t = peekw(regs.sp);
	regs.sp += 2;
return t;
}

static unsigned short inline jumptargetJ(unsigned short target)
{
	mark_jump(target, JT_JUMP);
	return target;
}

static unsigned short inline jumptargetC(unsigned short target)
{
	mark_jump(target, JT_CALL);
	return target;
}

//---------------------------------------------------------------------------------------
// rotates and shifts
//---------------------------------------------------------------------------------------
//SZ503P0C
static void rl(unsigned char *a)
{
	unsigned char b, c;
	c = (unsigned char)(*a&0x80);
	b = (unsigned char)!!(regs.b.f&FLAGS_CF);
	*a<<=1;
	*a |= b;

	regs.b.f = *a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
	if (c)
		regs.b.f |= FLAGS_CF;
	if (!*a)
		regs.b.f |= FLAGS_ZF;
	regs.b.f |= parity[*a];

return;
}

//SZ503P0C
static void rr(unsigned char *a)
{
	unsigned char b, c;
	c = (unsigned char)(*a&1);
	b = (unsigned char)!!(regs.b.f&FLAGS_CF);
	*a>>=1;
	*a |= b<<7;

	regs.b.f = *a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
	if (c)
		regs.b.f |= FLAGS_CF;
	if (!*a)
		regs.b.f |= FLAGS_ZF;
	regs.b.f |= parity[*a];

return;
}

//SZ503P0C
static void rlc(unsigned char *a)
{
	unsigned char c;

	c=(unsigned char)!!(*a&0x80);
	*a<<=1;
	*a|=c;
	regs.b.f = *a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
	if (!*a)
		regs.b.f |= FLAGS_ZF;
	regs.b.f |= parity[*a];
	if(c)
		regs.b.f |= FLAGS_CF;
}

//SZ503P0C
static void rrc(unsigned char *a)
{
	unsigned char c;

	c=*a&1;
	*a>>=1;
	*a|=c<<7;
	regs.b.f = *a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
	if (!*a)
		regs.b.f |= FLAGS_ZF;
	regs.b.f |= parity[*a];
	if(c)
		regs.b.f |= FLAGS_CF;
}

//SZ503P0C
static void sla(unsigned char *a)
{
	unsigned char b;
	b = *a&0x80;
	*a <<= 1;

	regs.b.f = *a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
	regs.b.f |= parity[*a];
	if (!*a)
		regs.b.f |= FLAGS_ZF;
	if (b)
		regs.b.f |= FLAGS_CF;
}

//SZ503P0C
static void sra(unsigned char *a)
{
	unsigned char b;
	unsigned char c;
	b = *a&0x80;
	c = *a&1;
	*a >>= 1;
	*a |= b;

	regs.b.f = *a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
	regs.b.f |= parity[*a];
	if (!*a)
		regs.b.f |= FLAGS_ZF;
	if (c)
		regs.b.f |= FLAGS_CF;
}

//SZ503P0C
static void sll(unsigned char *a)
{
	unsigned char c;
	c = *a&0x80;
	*a <<= 1;
	*a |= 1;
	
	regs.b.f = *a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
	regs.b.f |= parity[*a];
	if (!*a)
		regs.b.f |= FLAGS_ZF;
	if (c)
		regs.b.f |= FLAGS_CF;
}

//SZ503P0C
static void srl(unsigned char *a)
{
	unsigned char b;
	b = *a&1;
	*a >>= 1;

	regs.b.f = *a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
	regs.b.f |= parity[*a];
	if (!*a)
		regs.b.f |= FLAGS_ZF;
	if (b)
		regs.b.f |= FLAGS_CF;
}

//---------------------------------------------------------------------------------------
// arithmetic
//---------------------------------------------------------------------------------------
//SZ5H3VNC
static void adc(unsigned char b)
{
	int c;
	int c2;
	unsigned char d;

	d = (unsigned char)!!(regs.b.f & FLAGS_CF);
	c = (int)regs.b.a + b + d;

	regs.b.f = (unsigned char)(c&(FLAGS_SF|FLAGS_XF|FLAGS_YF));
	if (c&0x100)
		regs.b.f |= FLAGS_CF;
//	if (~(regs.b.a ^ b)&(regs.b.a ^ c)&0x80)
//	if( (val^oldval^0x80) & (val^newval) & 0x80 )
//	if( (c^regs.b.a^0x80) & (c^b) & 0x80 )

	c2 = (int)(signed char)regs.b.a + (signed char)b + d;
	if (c2 <-128 || c2 > 127)
		regs.b.f |= FLAGS_PF;
	regs.b.f |= ((regs.b.a & 0xf) + (b & 0xf) + d) & FLAGS_HF;

	regs.b.a = (unsigned char)c;

	if (!regs.b.a)
		regs.b.f |= FLAGS_ZF;
}

//SZ5H3VNC
static void add(unsigned char b)
{
	int c;
	int c2;
	c = (int)regs.b.a + b;

	regs.b.f = (unsigned char)(c&(FLAGS_SF|FLAGS_XF|FLAGS_YF));
	if (c&0x100)
		regs.b.f |= FLAGS_CF;
//	if (~(regs.b.a ^ b)&(regs.b.a ^ c)&0x80)
//	if( (val^oldval^0x80) & (val^newval) & 0x80 )
//	if( (c^regs.b.a^0x80) & (c^b) & 0x80 )

	c2 = (int)(signed char)regs.b.a + (signed char)b;
	if (c2 <-128 || c2 > 127)
		regs.b.f |= FLAGS_PF;

	regs.b.f |= ((regs.b.a & 0xf) + (b & 0xf)) & FLAGS_HF;

	regs.b.a = (unsigned char)c;

	if (!regs.b.a)
		regs.b.f |= FLAGS_ZF;
}

//SZ5H3VNC
static void sbc(unsigned char b)
{
	int c;
	int c2;
	unsigned char d;

	d = (unsigned char)!!(regs.b.f & FLAGS_CF);
	c = (int)regs.b.a - b - d;

	regs.b.f = (unsigned char)((c&(FLAGS_SF|FLAGS_XF|FLAGS_YF))|FLAGS_NF);
	if (c&0x100)
		regs.b.f |= FLAGS_CF;
//	if ((regs.b.a ^ b)&(regs.b.a ^ c)&0x80)
//	if( (val^oldval) & (oldval^newval) & 0x80 )
//	if( (c^regs.b.a) & (regs.b.a^b) & 0x80 )
	c2 = (int)(signed char)regs.b.a - (signed char)b - d;
	if (c2 <-128 || c2 > 127)
		regs.b.f |= FLAGS_PF;
	regs.b.f |= ((regs.b.a & 0xf) - (b & 0xf) - d) & FLAGS_HF;
	
	regs.b.a = (unsigned char)c;

	if (!regs.b.a)
		regs.b.f |= FLAGS_ZF;
}

//SZ5H3VNC
static void sub(unsigned char b)
{
	int c;
	int c2;

	c = (int)regs.b.a - b;

	regs.b.f = (unsigned char)((c&(FLAGS_SF|FLAGS_XF|FLAGS_YF))|FLAGS_NF);
	if (c&0x100)
		regs.b.f |= FLAGS_CF;
//	if ((regs.b.a ^ b)&(regs.b.a ^ c)&0x80)
//	if( (val^oldval) & (oldval^newval) & 0x80 )
//	if( (c^regs.b.a) & (regs.b.a^b) & 0x80 )

	c2 = (int)(signed char)regs.b.a - (signed char)b;
	if (c2 <-128 || c2 > 127)
		regs.b.f |= FLAGS_PF;
	
	regs.b.f |= ((regs.b.a & 0xf) - (b & 0xf)) & FLAGS_HF;
	
	regs.b.a = (unsigned char)c;

	if (!regs.b.a)
		regs.b.f |= FLAGS_ZF;
}

//SZ*H*VNC
static void cp(unsigned char b)
{
	int c;
	int c2;

	c = (int)regs.b.a - b;

	regs.b.f = (unsigned char)((c&FLAGS_SF)|FLAGS_NF);
	regs.b.f |= b&(FLAGS_XF|FLAGS_YF);

	if (c&0x100)
		regs.b.f |= FLAGS_CF;
//	if ((regs.b.a ^ b)&(regs.b.a ^ c)&0x80)
//	if( (val^oldval) & (oldval^newval) & 0x80 )
//	if( (c^regs.b.a) & (regs.b.a^b) & 0x80 )

	c2 = (int)(signed char)regs.b.a - (signed char)b;
	if (c2 <-128 || c2 > 127)
		regs.b.f |= FLAGS_PF;
	
	regs.b.f |= ((regs.b.a & 0xf) - (b & 0xf)) & FLAGS_HF;
	
	if (!(c&0xff))
		regs.b.f |= FLAGS_ZF;
}

//SZ513P00
static void and(unsigned char b)
{
	regs.b.a &= b;
	regs.b.f = (unsigned char)((regs.b.a&(FLAGS_SF|FLAGS_XF|FLAGS_YF))|FLAGS_HF);
	regs.b.f |= parity[regs.b.a];
	if (!regs.b.a)
		regs.b.f |= FLAGS_ZF;
}

//SZ503P00
static void or(unsigned char b)
{
	regs.b.a |= b;
	regs.b.f = regs.b.a&(FLAGS_SF|FLAGS_XF|FLAGS_YF);
	regs.b.f |= parity[regs.b.a];
	if (!regs.b.a)
		regs.b.f |= FLAGS_ZF;
}

//SZ503P00
static void xor(unsigned char b)
{
	regs.b.a ^= b;
	regs.b.f = regs.b.a&(FLAGS_SF|FLAGS_XF|FLAGS_YF);
	regs.b.f |= parity[regs.b.a];
	if (!regs.b.a)
		regs.b.f |= FLAGS_ZF;
}

//SZ5H3V1C
static void neg(void)
{
	unsigned char c;
	c = regs.b.a;
	regs.b.a=0;
	sub(c);
}

//---------------------------------------------------------------------------------------
// interrupt handling
//---------------------------------------------------------------------------------------
static void retn(void)
{
	regs.iff1 = regs.iff2;
	regs.pc = pop();
	//regs.memptr = regs.pc;?
}

static void reti(void)
{
	//regs.iff1 = regs.iff2;
	regs.pc = pop();
	regs.memptr = regs.pc;
}

static void im(unsigned char m)
{
	regs.im = m;
}

//---------------------------------------------------------------------------------------
// block move
//---------------------------------------------------------------------------------------
/*
static const unsigned char Dparity[16]={0,1,0,0,1,0,0,1,0,0,1,0,0,1,0,1};
static const unsigned char Iparity[16]={0,0,1,0,0,1,0,1,1,0,1,1,0,1,1,0};

static unsigned char get_parity(unsigned char b, unsigned char c, unsigned char v, const unsigned char *table)
{
	unsigned char i;
	unsigned char temp1, temp2;
	unsigned char c2;
	unsigned char v2;
	
	c2 = (unsigned char)!!(c&(1<<2));
	v2 = (unsigned char)!!(v&(1<<2));

	i = ((c&3)<<2)|(v&3);
	temp1 = table[i];

	if ((b&0xF)==0)
	{
		unsigned char b4,b5,b6;
		b4 = (unsigned char)!!(b&(1<<4));
		b5 = (unsigned char)!!(b&(1<<5));
		b6 = (unsigned char)!!(b&(1<<6));
		temp2 = ((unsigned char)!!parity[b]) ^ (b4 | (b6 & (!b5)));
	}
	else
	{
		unsigned char b0,b1,b2;
		b0 = (unsigned char)!!(b&(1<<0));
		b1 = (unsigned char)!!(b&(1<<1));
		b2 = (unsigned char)!!(b&(1<<2));
		temp2 = ((unsigned char)!!parity[b]) ^ (b0 | (b2 & (!b1)));
	}

	return (unsigned char)((temp1 ^ temp2 ^ c2 ^ v2)?FLAGS_HF:0);
}

static unsigned char get_I_parity(unsigned char b, unsigned char c, unsigned char v)
{
return get_parity(b,c,v,Iparity);
}

static unsigned char get_D_parity(unsigned char b, unsigned char c, unsigned char v)
{
return get_parity(b,c,v,Dparity);
}
*/
static void ldi(void)
{
	unsigned char c;

	c = peek(regs.w.hl);
	poke(regs.w.de, c);
	regs.w.de++;
	regs.w.hl++;
	regs.w.bc--;

	regs.b.f &= FLAGS_SF|FLAGS_ZF|FLAGS_CF;

	c += regs.b.a;
	if (regs.w.bc)
		regs.b.f |= FLAGS_PF;
	if (c&2)
		regs.b.f |= FLAGS_YF;
	regs.b.f |= FLAGS_XF&c;
}

//SZ*H**1-
static void cpi(void)
{
	unsigned char c, p;
	c = (unsigned char)(regs.b.f & FLAGS_CF);
	p = peek(regs.w.hl);
	cp(p);
	regs.b.f &= FLAGS_SF|FLAGS_ZF|FLAGS_HF|FLAGS_NF;

	regs.w.hl++;
	regs.w.bc--;

	p = regs.b.a - p - (unsigned char)!!(regs.b.f&FLAGS_HF);
	regs.b.f |= p&FLAGS_XF;
	if (p&2)
		regs.b.f |= FLAGS_YF;

	if (regs.w.bc)
		regs.b.f |= FLAGS_PF;
	regs.b.f |= c;

	regs.memptr++;
}

static void ini(void)
{
	unsigned char c;

	regs.memptr = regs.w.bc+1;

	dec8(&regs.b.b);
	c = in(regs.w.bc);

	poke(regs.w.hl, c);
	regs.w.hl++;
	regs.b.f &= FLAGS_SF|FLAGS_ZF|FLAGS_XF|FLAGS_YF;

	if (((unsigned char)(regs.b.c+1) + (int)c)>0xff)// & 0x100)
		regs.b.f |= FLAGS_CF|FLAGS_HF;
	if (c&0x80)
		regs.b.f |= FLAGS_NF;

	//regs.b.f |= get_I_parity(regs.b.b, regs.b.c, c);
	regs.b.f |= parity[((c+regs.b.c+1)&7)^regs.b.b];
}

static void outi(void)
{
	unsigned char c;
	c = peek(regs.w.hl);
	out(regs.w.bc, c);
	dec8(&regs.b.b);
	regs.memptr = regs.w.bc+1;
	regs.w.hl++;
	regs.b.f &= FLAGS_SF|FLAGS_ZF|FLAGS_XF|FLAGS_YF;
	if (((int)c + regs.b.l)>0xff)//&0x100)
		regs.b.f |= FLAGS_CF|FLAGS_HF;
	if (c&0x80)
		regs.b.f |= FLAGS_NF;
	
	//regs.b.f |= get_I_parity(regs.b.b, regs.b.c, c);
	regs.b.f |= parity[((c+regs.b.l)&7)^regs.b.b];
}
      
static void ldd(void)
{
	unsigned char c;

	c = peek(regs.w.hl);
	poke(regs.w.de, c);
	regs.w.de--;
	regs.w.hl--;
	regs.w.bc--;

	regs.b.f &= FLAGS_SF|FLAGS_ZF|FLAGS_CF;

	c += regs.b.a;
	if (regs.w.bc)
		regs.b.f |= FLAGS_PF;
	if (c&2)
		regs.b.f |= FLAGS_YF;
	regs.b.f |= FLAGS_XF&c;
}

static void cpd(void)
{
	unsigned char c,p;
	c = (unsigned char)(regs.b.f & FLAGS_CF);
	p = peek(regs.w.hl);
	cp(p);
	regs.b.f &= FLAGS_SF|FLAGS_ZF|FLAGS_HF|FLAGS_NF;

	regs.w.hl--;
	regs.w.bc--;

	p = regs.b.a - p - (unsigned char)!!(regs.b.f&FLAGS_HF);
	regs.b.f |= p&FLAGS_XF;
	if (p&2)
		regs.b.f |= FLAGS_YF;

	if (regs.w.bc)
		regs.b.f |= FLAGS_PF;
	regs.b.f |= c;

	regs.memptr--;
}

static void ind(void)
{
	unsigned char c;
	regs.memptr = regs.w.bc-1;
	dec8(&regs.b.b);
	c = in(regs.w.bc);

	poke(regs.w.hl, c);
	regs.w.hl--;
	regs.b.f &= FLAGS_SF|FLAGS_ZF|FLAGS_XF|FLAGS_YF;

	if (((unsigned char)(regs.b.c-1) + (int)c)>0xff)// & 0x100)
		regs.b.f |= FLAGS_CF|FLAGS_HF;
	if (c&0x80)
		regs.b.f |= FLAGS_NF;

	//regs.b.f |= get_D_parity(regs.b.b, regs.b.c, c);
	regs.b.f |= parity[(c+((regs.b.c-1)&0xff)&7)^regs.b.b];
}

static void outd(void)
{
	unsigned char c;
	c = peek(regs.w.hl);	
	out(regs.w.bc, c);
	dec8(&regs.b.b);
	regs.memptr = regs.w.bc-1;
	regs.w.hl--;
	regs.b.f &= FLAGS_SF|FLAGS_ZF|FLAGS_XF|FLAGS_YF;
	if (((int)c + regs.b.l)>0xff)//&0x100)
		regs.b.f |= FLAGS_CF|FLAGS_HF;
	if (c&0x80)
		regs.b.f |= FLAGS_NF;
	
	//regs.b.f |= get_D_parity(regs.b.b, regs.b.c, c);
	regs.b.f |= parity[((c+regs.b.l)&7)^regs.b.b];
}

//---------------------------------------------------------------------------------------
// inc/dec
//---------------------------------------------------------------------------------------
//SZ5H3VN-
static void inc8(unsigned char *r)
{
	regs.b.f &= FLAGS_CF;
	if (*r==0x7f)
		regs.b.f |= FLAGS_PF;
	regs.b.f |= ((*r&0xf)+1)&FLAGS_HF;
	(*r)++;
	regs.b.f |= *r&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
	if (!*r)
		regs.b.f |= FLAGS_ZF;
}

static void dec8(unsigned char *r)
{
	regs.b.f &= FLAGS_CF;
	regs.b.f |= FLAGS_NF;
	if (*r==0x80)
		regs.b.f |= FLAGS_PF;
	regs.b.f |= ((*r&0xf)-1)&FLAGS_HF;
	(*r)--;
	regs.b.f |= *r&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
	if (!*r)
		regs.b.f |= FLAGS_ZF;
}

//---------------------------------------------------------------------------------------
// 16 bit arithmetic
//---------------------------------------------------------------------------------------
//--***-0C
static void add16(unsigned short *d, unsigned short r)
{
	unsigned int v;

	regs.b.f &= FLAGS_SF|FLAGS_ZF|FLAGS_PF;
	v = (unsigned int)*d+r;
	if (v&0x10000)
		regs.b.f |= FLAGS_CF;
	
	if (((*d & 0xfff) + (r & 0xfff)) & 0x1000)
		regs.b.f |= FLAGS_HF;

	*d = (unsigned short)v;
	regs.b.f |= (*d>>8)&(FLAGS_XF|FLAGS_YF);
}

//SZ***V0C
static void adc16(unsigned short *a, unsigned short *b)
{
	int v;
	int v2;
	unsigned char c;

	c = (unsigned char)!!(regs.b.f&FLAGS_CF);
	v = (int)*a + *b + c;

	regs.b.f = (unsigned char)((v>>8)&(FLAGS_XF|FLAGS_YF|FLAGS_SF));
	if (((*a & 0xfff) + (*b & 0xfff) + c) & 0x1000)
		regs.b.f |= FLAGS_HF;

//	if (~(*a^*b)&(*a^v)&0x8000)
//	if( (c^regs.b.a^0x80) & (c^b) & 0x80 )
//	if( (c^*a^0x8000) & (c^*b) & 0x8000 )
	v2 = (int)(signed short)*a + (signed short)*b + c;
	if (v2 < -32768 || v2 > 32767)
		regs.b.f |= FLAGS_PF;

	*a = (unsigned short)v;

	if (v & 0x10000)
		regs.b.f |= FLAGS_CF;
	if (!*a)
		regs.b.f |= FLAGS_ZF;
}

//SZ***VNC
static void sbc16(unsigned short *a, unsigned short *b)
{
	unsigned short c;
	int v;
	int v2;

	c = (unsigned char)!!(regs.b.f&FLAGS_CF);
	v = (int)*a - *b - c;

	regs.b.f = (unsigned char)((v>>8)&(FLAGS_XF|FLAGS_YF|FLAGS_SF));
	regs.b.f |= FLAGS_NF;
	if (((*a & 0xfff) - (*b & 0xfff) - c) & 0x1000)
		regs.b.f |= FLAGS_HF;

//	if ((*a^*b)&(*a^v)&0x8000)
//	if( (c^regs.b.a) & (regs.b.a^b) & 0x80 )
//	if( (c^*a) & (*a^*b) & 0x8000 )
	v2 = (int)(signed short)*a - (signed short)*b - c;
	if (v2 < -32768 || v2 > 32767)
		regs.b.f |= FLAGS_PF;

	*a = (unsigned short)v;

	if (v&0x10000)
		regs.b.f |= FLAGS_CF;
	if (!*a)
		regs.b.f |= FLAGS_ZF;

}

//---------------------------------------------------------------------------------------
// instruction decoding
//---------------------------------------------------------------------------------------
static unsigned short get_imm16(void)
{
	unsigned short v;
	v = peekwc(regs.pc);
	regs.pc += 2;
return v;
}

unsigned char get_imm8(void)
{
return peekc(regs.pc++);
}

static unsigned char getr(void)
{
return (regs.r&0x7f)|regs.r7;
}

void setr(unsigned char r)
{
	regs.r = r;
	regs.r7 = r&0x80;
}

void inline incr(void)
{
	unsigned char c;
	c = regs.r&0x80;
	regs.r++;
	regs.r&=0x7f;
	regs.r |= c;
}

//---------------------------------------------------------------------------------------
// unshifted op-codes
//---------------------------------------------------------------------------------------
static void nop(void)
{
	cycles_used += 4;
}

static void ld_bci16(void)
{
	cycles_used += 10;
	regs.w.bc = get_imm16();
	//regs.memptr = regs.w.bc+1;
}

static void ld_bca(void)
{
	cycles_used += 7;
	regs.memptr = ((regs.w.bc+1)&0xff)|(((unsigned short)regs.b.a)<<8);
	poke(regs.w.bc, regs.b.a);
}

static void inc_bc(void)
{
	cycles_used += 6;
	regs.w.bc++;
}

static void inc_b(void)
{
	cycles_used += 4;
	inc8(&regs.b.b);
}

static void dec_b(void)
{
	cycles_used += 4;
	dec8(&regs.b.b);
}

static void ld_bi8(void)
{
	cycles_used += 7;
	regs.b.b = get_imm8();
}

//--503-0C
static void rlca(void)
{
	unsigned char b;
 
	cycles_used += 4;

	b = regs.b.a&0x80;

	regs.b.f &= FLAGS_PF|FLAGS_SF|FLAGS_ZF;
	if (b)
		regs.b.f |= FLAGS_CF;

	regs.b.a<<=1;
	regs.b.a |= !!b;
	regs.b.f |= regs.b.a&(FLAGS_XF|FLAGS_YF);
}

static void exafaf(void)
{
	unsigned short t;

	cycles_used += 4;

	t = regs.w.af;
	regs.w.af = regs.exregs.af;
	regs.exregs.af = t;
}

static void add_hlbc(void)
{
	cycles_used += 11;
	regs.memptr = regs.w.hl+1;
	add16(&regs.w.hl, regs.w.bc);
}

static void ld_abc(void)
{
	cycles_used += 7;
	regs.memptr = regs.w.bc+1;
	regs.b.a = peek(regs.w.bc);
}

static void dec_bc(void)
{
	cycles_used += 6;
	regs.w.bc--;
}

static void inc_c(void)
{
	cycles_used += 4;
	inc8(&regs.b.c);
}

static void dec_c(void)
{
	cycles_used += 4;
	dec8(&regs.b.c);
}

static void ld_ci8(void)
{
	cycles_used += 7;
	regs.b.c = get_imm8();
}

static void rrca(void)
{
	unsigned char b;
	
	cycles_used += 4;
	b = regs.b.a&0x1;
	regs.b.f &= FLAGS_PF|FLAGS_SF|FLAGS_ZF;
	if (b)
		regs.b.f |= FLAGS_CF;

	regs.b.a>>=1;
	regs.b.a |= b<<7;

	regs.b.f |= regs.b.a&(FLAGS_XF|FLAGS_YF);
}

static void djnz(void)
{
	regs.b.b--;
	if (regs.b.b)
	{
		cycles_used += 13;
		regs.pc += (signed char)get_imm8();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 8;
		regs.pc++;
	}
}

static void ld_dei16(void)
{
	cycles_used += 10;
	regs.w.de = get_imm16();
	//regs.memptr = regs.w.de+1;
}

static void ld_dea(void)
{
	cycles_used += 7;
	regs.memptr = ((regs.w.de+1)&0xff)|(((unsigned short)regs.b.a)<<8);
	poke(regs.w.de, regs.b.a);
}

static void inc_de(void)
{
	cycles_used += 6;
	regs.w.de++;
}

static void inc_d(void)
{
	cycles_used += 4;
	inc8(&regs.b.d);
}

static void dec_d(void)
{
	cycles_used += 4;
	dec8(&regs.b.d);
}

static void ld_di8(void)
{
	cycles_used += 7;
	regs.b.d = get_imm8();
}

static void rla(void)
{
	unsigned char b;
	unsigned char c;

	cycles_used += 4;
	
	c = (unsigned char)!!(regs.b.f&FLAGS_CF);

	regs.b.f &= FLAGS_PF|FLAGS_SF|FLAGS_ZF;

	b = regs.b.a&0x80;
	regs.b.a<<=1;
	regs.b.a |= c;

	if (b)
		regs.b.f |= FLAGS_CF;
	regs.b.f |= regs.b.a&(FLAGS_XF|FLAGS_YF);
}

static void jr(void)
{
	cycles_used += 12;
	regs.pc += (signed char)get_imm8();
	regs.memptr = regs.pc;
}

static void add_hlde(void)
{
	cycles_used += 11;
	regs.memptr = regs.w.hl+1;
	add16(&regs.w.hl, regs.w.de);
}

static void ld_ade(void)
{
	cycles_used += 7;
	regs.memptr = regs.w.de+1;
	regs.b.a = peek(regs.w.de);
}

static void dec_de(void)
{
	cycles_used += 6;
	regs.w.de--;
}

static void inc_e(void)
{
	cycles_used += 4;
	inc8(&regs.b.e);
}

static void dec_e(void)
{
	cycles_used += 4;
	dec8(&regs.b.e);
}

static void ld_ei8(void)
{
	cycles_used += 7;
	regs.b.e = get_imm8();
}

static void rra(void)
{
	unsigned char b;
	unsigned char c;

	cycles_used += 4;

	c = (unsigned char)!!(regs.b.f & FLAGS_CF);

	regs.b.f &= FLAGS_PF|FLAGS_SF|FLAGS_ZF;
	b = regs.b.a&1;

	regs.b.a>>=1;
	regs.b.a |= c<<7;

	if (b)
		regs.b.f |= FLAGS_CF;
	regs.b.f |= regs.b.a&(FLAGS_XF|FLAGS_YF);
}

static void jr_nz(void)
{
	if (!(regs.b.f & FLAGS_ZF))
	{
		cycles_used += 12;
		regs.pc += (signed char)get_imm8();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 7;
		regs.pc++;
	}
}

static void ld_hli16(void)
{
	cycles_used += 10;
	regs.w.hl = get_imm16();
	//regs.memptr = regs.w.hl;
}

static void ld_i16hl(void)
{
	unsigned short ea = get_imm16();
	cycles_used += 16;
	regs.memptr = ea+1;
	pokew(ea, regs.w.hl);
}

static void inc_hl(void)
{
	cycles_used += 6;
	regs.w.hl++;
}

static void inc_h(void)
{
	cycles_used += 4;
	inc8(&regs.b.h);
}

static void dec_h(void)
{
	cycles_used += 4;
	dec8(&regs.b.h);
}

static void ld_hi8(void)
{
	cycles_used += 7;
	regs.b.h = get_imm8();
}

static void daa(void)
{
	unsigned char c = regs.b.f & FLAGS_CF;
	unsigned char h = !!(regs.b.f & FLAGS_HF);
	unsigned char lo = regs.b.a&0xf;
	unsigned char hi = regs.b.a>>4;

	unsigned char d=0;

	cycles_used += 4;

	regs.b.f &= ~(FLAGS_XF|FLAGS_YF|FLAGS_SF|FLAGS_ZF|FLAGS_CF|FLAGS_HF);

	if (c)
	{
		d = 0x60;
		if (h || lo>9) d |= 0x06;

		regs.b.f |= FLAGS_CF;
	}
	else
	{
		if (hi <= 9 && !h && lo <= 9) d = 0x00;
		else if (hi <= 9 &&  h && lo <= 9) d = 0x06;
		else if (hi <= 8 &&  1 && lo >  9) d = 0x06;
		else if (hi >  9 && !h && lo <= 9) d = 0x60;
		else if (hi >= 9 &&  1 && lo >  9) d = 0x66;
		else if (hi >  9 &&  h && lo <= 9) d = 0x66;

		if (hi >= 9 && lo > 9) regs.b.f |= FLAGS_CF;
		else if (hi > 9 && lo <= 9) regs.b.f |= FLAGS_CF;
	}

	if (regs.b.f & FLAGS_NF)
	{
		regs.b.a -= d;
		if (h && lo <= 5) regs.b.f |= FLAGS_HF;
	}
	else
	{
		regs.b.a += d;
		if (lo > 9) regs.b.f |= FLAGS_HF;
	}
	if (!regs.b.a)
		regs.b.f |= FLAGS_ZF;

	regs.b.f |= regs.b.a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);

	regs.b.f &= ~FLAGS_PF;
	regs.b.f |= parity[regs.b.a];
}

static void jr_z(void)
{
	if (regs.b.f & FLAGS_ZF)
	{
		cycles_used += 12;
		regs.pc += (signed char)get_imm8();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 7;
		regs.pc++;
	}
}

static void add_hlhl(void)
{
	cycles_used += 11;
	regs.memptr = regs.w.hl+1;
	add16(&regs.w.hl, regs.w.hl);
}

static void ld_hlci16(void)
{
	unsigned short ea = get_imm16();
	cycles_used += 16;
	regs.memptr = ea+1;
	regs.w.hl = peekw(ea);
}

static void dec_hl(void)
{
	cycles_used += 6;
	regs.w.hl--;
}

static void inc_l(void)
{
	cycles_used += 4;
	inc8(&regs.b.l);
}

static void dec_l(void)
{
	cycles_used += 4;
	dec8(&regs.b.l);
}

static void ld_li8(void)
{
	cycles_used += 7;
	regs.b.l = get_imm8();
}

static void cpl(void)
{
	cycles_used += 4;
	regs.b.a ^= 0xff;
	regs.b.f &= ~(FLAGS_XF|FLAGS_YF);
	regs.b.f |= regs.b.a&(FLAGS_XF|FLAGS_YF);
	regs.b.f |= FLAGS_HF|FLAGS_NF;
}

static void jr_nc(void)
{
	if (!(regs.b.f & FLAGS_CF))
	{
		cycles_used += 12;
		regs.pc += (signed char)get_imm8();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 7;
		regs.pc++;
	}
}

static void ld_spi16(void)
{
	cycles_used += 10;
	regs.sp = get_imm16();
	//regs.memptr = regs.sp+1;
}

static void ld_i16a(void)
{
	unsigned short ea = get_imm16();
	cycles_used += 13;
	regs.memptr = ((ea+1)&0xff)|(((unsigned short)regs.b.a)<<8);
	poke(ea, regs.b.a);
}

static void inc_sp(void)
{
	cycles_used += 6;
	regs.sp++;
}

static void inc_chl(void)
{
	cycles_used += 11;
	inc8(peeka(regs.w.hl));
}

static void dec_chl(void)
{
	cycles_used += 11;
	dec8(peeka(regs.w.hl));
}

static void ld_hli8(void)
{
	cycles_used += 10;
	poke(regs.w.hl, get_imm8());
}

static void scf(void)
{
	unsigned char q = regs.b.f;
	cycles_used += 4;
	regs.b.f &= ~(FLAGS_XF | FLAGS_YF | FLAGS_NF | FLAGS_HF);
	regs.b.f |= FLAGS_CF | (regs.b.a & (FLAGS_XF | FLAGS_YF));

	q = ((q ^ regs.b.f) | regs.b.a) & (FLAGS_XF | FLAGS_YF);
	regs.b.f = (regs.b.f & ~(FLAGS_XF | FLAGS_YF)) | q;
}

static void jr_c(void)
{
	if (regs.b.f & FLAGS_CF)
	{
		cycles_used += 12;
		regs.pc += (signed char)get_imm8();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 7;
		regs.pc++;
	}
}

static void add_hlsp(void)
{
	cycles_used += 11;
	regs.memptr = regs.w.hl+1;
	add16(&regs.w.hl, regs.sp);
}

static void ld_ai16(void)
{
	unsigned short ea = get_imm16();
	cycles_used += 13;
	regs.memptr = ea+1;
	regs.b.a = peek(ea);
}

static void dec_sp(void)
{
	cycles_used += 6;
	regs.sp--;
}

static void inc_a(void)
{
	cycles_used += 4;
	inc8(&regs.b.a);
}

static void dec_a(void)
{
	cycles_used += 4;
	dec8(&regs.b.a);
}

static void ld_ai8(void)
{
	cycles_used += 7;
	regs.b.a = get_imm8();
}

static void ccf(void)
{
	unsigned char q = regs.b.f;
	unsigned char c;
	cycles_used += 4;
	c = (unsigned char)(regs.b.f&FLAGS_CF);
	regs.b.f &= ~(FLAGS_XF|FLAGS_YF|FLAGS_NF|FLAGS_CF|FLAGS_HF);
	regs.b.f |= regs.b.a&(FLAGS_XF|FLAGS_YF);
	if (c)
		regs.b.f |= FLAGS_HF;
	else
		regs.b.f |= FLAGS_CF;

	q = ((q ^ regs.b.f) | regs.b.a) & (FLAGS_XF | FLAGS_YF);
	regs.b.f = (regs.b.f & ~(FLAGS_XF | FLAGS_YF)) | q;
}

static void ld_bb(void)
{
	cycles_used += 4;
}

static void ld_bc(void)
{
	cycles_used += 4;
	regs.b.b = regs.b.c;
}
static void ld_bd(void)
{
	cycles_used += 4;
	regs.b.b = regs.b.d;
}

static void ld_be(void)
{
	cycles_used += 4;
	regs.b.b = regs.b.e;
}

static void ld_bh(void)
{
	cycles_used += 4;
	regs.b.b = regs.b.h;
}

static void ld_bl(void)
{
	cycles_used += 4;
	regs.b.b = regs.b.l;
}
static void ld_bhl(void)
{
	cycles_used += 7;
	regs.b.b = peek(regs.w.hl);
}
static void ld_ba(void)
{
	cycles_used += 4;
	regs.b.b = regs.b.a;
}

static void ld_cb(void)
{
	cycles_used += 4;
	regs.b.c = regs.b.b;
}
static void ld_cc(void)
{
	cycles_used += 4;
}
static void ld_cd(void)
{
	cycles_used += 4;
	regs.b.c = regs.b.d;
}
static void ld_ce(void)
{
	cycles_used += 4;
	regs.b.c = regs.b.e;
}
static void ld_ch(void)
{
	cycles_used += 4;
	regs.b.c = regs.b.h;
}
static void ld_cl(void)
{
	cycles_used += 4;
	regs.b.c = regs.b.l;
}
static void ld_chl(void)
{
	cycles_used += 7;
	regs.b.c = peek(regs.w.hl);
}
static void ld_ca(void)
{
	cycles_used += 4;
	regs.b.c = regs.b.a;
}
static void ld_db(void)
{
	cycles_used += 4;
	regs.b.d = regs.b.b;
}
static void ld_dc(void)
{
	cycles_used += 4;
	regs.b.d = regs.b.c;
}
static void ld_dd(void)
{
	cycles_used += 4;
}
static void ld_de(void)
{
	cycles_used += 4;
	regs.b.d = regs.b.e;
}
static void ld_dh(void)
{
	cycles_used += 4;
	regs.b.d = regs.b.h;
}
static void ld_dl(void)
{
	cycles_used += 4;
	regs.b.d = regs.b.l;
}
static void ld_dhl(void)
{
	cycles_used += 7;
	regs.b.d = peek(regs.w.hl);
}
static void ld_da(void)
{
	cycles_used += 4;
	regs.b.d = regs.b.a;
}
static void ld_eb(void)
{
	cycles_used += 4;
	regs.b.e = regs.b.b;
}
static void ld_ec(void)
{
	cycles_used += 4;
	regs.b.e = regs.b.c;
}
static void ld_ed(void)
{
	cycles_used += 4;
	regs.b.e = regs.b.d;
}
static void ld_ee(void)
{
	cycles_used += 4;
}
static void ld_eh(void)
{
	cycles_used += 4;
	regs.b.e = regs.b.h;
}
static void ld_el(void)
{
	cycles_used += 4;
	regs.b.e = regs.b.l;
}
static void ld_ehl(void)
{
	cycles_used += 7;
	regs.b.e = peek(regs.w.hl);
}
static void ld_ea(void)
{
	cycles_used += 4;
	regs.b.e = regs.b.a;
}
static void ld_hb(void)
{
	cycles_used += 4;
	regs.b.h = regs.b.b;
}
static void ld_hc(void)
{
	cycles_used += 4;
	regs.b.h = regs.b.c;
}
static void ld_hd(void)
{
	cycles_used += 4;
	regs.b.h = regs.b.d;
}
static void ld_he(void)
{
	cycles_used += 4;
	regs.b.h = regs.b.e;
}
static void ld_hh(void)
{
	cycles_used += 4;
}
static void ld_hl(void)
{
	cycles_used += 4;
	regs.b.h = regs.b.l;
}
static void ld_hhl(void)
{
	cycles_used += 7;
	regs.b.h = peek(regs.w.hl);
}
static void ld_ha(void)
{
	cycles_used += 4;
	regs.b.h = regs.b.a;
}
static void ld_lb(void)
{
	cycles_used += 4;
	regs.b.l = regs.b.b;
}
static void ld_lc(void)
{
	cycles_used += 4;
	regs.b.l = regs.b.c;
}
static void ld_ld(void)
{
	cycles_used += 4;
	regs.b.l = regs.b.d;
}
static void ld_le(void)
{
	cycles_used += 4;
	regs.b.l = regs.b.e;
}
static void ld_lh(void)
{
	cycles_used += 4;
	regs.b.l = regs.b.h;
}
static void ld_ll(void)
{
	cycles_used += 4;
}
static void ld_lhl(void)
{
	cycles_used += 7;
	regs.b.l = peek(regs.w.hl);
}
static void ld_la(void)
{
	cycles_used += 4;
	regs.b.l = regs.b.a;
}
static void ld_hlb(void)
{
	cycles_used += 7;
	poke(regs.w.hl, regs.b.b);
}
static void ld_hlc(void)
{
	cycles_used += 7;
	poke(regs.w.hl, regs.b.c);
}
static void ld_hld(void)
{
	cycles_used += 7;
	poke(regs.w.hl, regs.b.d);
}
static void ld_hle(void)
{
	cycles_used += 7;
	poke(regs.w.hl, regs.b.e);
}
static void ld_hlh(void)
{
	cycles_used += 7;
	poke(regs.w.hl, regs.b.h);
}
static void ld_hll(void)
{
	cycles_used += 7;
	poke(regs.w.hl, regs.b.l);
}
static void halt(void)
{
	//cycles_used += 4;//four for halt, four for nop
	nop();
	regs.pc-=1;
}

static void ld_hla(void)
{
	cycles_used += 7;
	poke(regs.w.hl, regs.b.a);
}

static void ld_ab(void)
{
	cycles_used += 4;
	regs.b.a = regs.b.b;
}
static void ld_ac(void)
{
	cycles_used += 4;
	regs.b.a = regs.b.c;
}
static void ld_ad(void)
{
	cycles_used += 4;
	regs.b.a = regs.b.d;
}
static void ld_ae(void)
{
	cycles_used += 4;
	regs.b.a = regs.b.e;
}
static void ld_ah(void)
{
	cycles_used += 4;
	regs.b.a = regs.b.h;
}
static void ld_al(void)
{
	cycles_used += 4;
	regs.b.a = regs.b.l;
}
static void ld_ahl(void)
{
	cycles_used += 7;
	regs.b.a = peek(regs.w.hl);
}
static void ld_aa(void)
{
	cycles_used += 4;
}

static void add_b(void)
{
	cycles_used += 4;
	add(regs.b.b);
}
static void add_c(void)
{
	cycles_used += 4;
	add(regs.b.c);
}
static void add_d(void)
{
	cycles_used += 4;
	add(regs.b.d);
}
static void add_e(void)
{
	cycles_used += 4;
	add(regs.b.e);
}
static void add_h(void)
{
	cycles_used += 4;
	add(regs.b.h);
}
static void add_l(void)
{
	cycles_used += 4;
	add(regs.b.l);
}
static void add_hl(void)
{
	cycles_used += 7;
	add(peek(regs.w.hl));
}
static void add_a(void)
{
	cycles_used += 4;
	add(regs.b.a);
} 
static void adc_b(void)
{
	cycles_used += 4;
	adc(regs.b.b);
}
static void adc_c(void)
{
	cycles_used += 4;
	adc(regs.b.c);
}
static void adc_d(void)
{
	cycles_used += 4;
	adc(regs.b.d);
}
static void adc_e(void)
{
	cycles_used += 4;
	adc(regs.b.e);
}
static void adc_h(void)
{
	cycles_used += 4;
	adc(regs.b.h);
}
static void adc_l(void)
{
	cycles_used += 4;
	adc(regs.b.l);
}
static void adc_hl(void)
{
	cycles_used += 7;
	adc(peek(regs.w.hl));
}
static void adc_a(void)
{
	cycles_used += 4;
	adc(regs.b.a);
} 
static void sub_b(void)
{
	cycles_used += 4;
	sub(regs.b.b);
}
static void sub_c(void)
{
	cycles_used += 4;
	sub(regs.b.c);
}
static void sub_d(void)
{
	cycles_used += 4;
	sub(regs.b.d);
}
static void sub_e(void)
{
	cycles_used += 4;
	sub(regs.b.e);
}
static void sub_h(void)
{
	cycles_used += 4;
	sub(regs.b.h);
}
static void sub_l(void)
{
	cycles_used += 4;
	sub(regs.b.l);
}
static void sub_hl(void)
{
	cycles_used += 7;
	sub(peek(regs.w.hl));
}
static void sub_a(void)
{
	cycles_used += 4;
	sub(regs.b.a);
} 

static void sbc_b(void)
{
	cycles_used += 4;
	sbc(regs.b.b);
}
static void sbc_c(void)
{
	cycles_used += 4;
	sbc(regs.b.c);
}
static void sbc_d(void)
{
	cycles_used += 4;
	sbc(regs.b.d);
}
static void sbc_e(void)
{
	cycles_used += 4;
	sbc(regs.b.e);
}
static void sbc_h(void)
{
	cycles_used += 4;
	sbc(regs.b.h);
}
static void sbc_l(void)
{
	cycles_used += 4;
	sbc(regs.b.l);
}
static void sbc_hl(void)
{
	cycles_used += 7;
	sbc(peek(regs.w.hl));
}
static void sbc_a(void)
{
	cycles_used += 4;
	sbc(regs.b.a);
} 
static void and_b(void)
{
	cycles_used += 4;
	and(regs.b.b);
}
static void and_c(void)
{
	cycles_used += 4;
	and(regs.b.c);
}
static void and_d(void)
{
	cycles_used += 4;
	and(regs.b.d);
}
static void and_e(void)
{
	cycles_used += 4;
	and(regs.b.e);
}
static void and_h(void)
{
	cycles_used += 4;
	and(regs.b.h);
}
static void and_l(void)
{
	cycles_used += 4;
	and(regs.b.l);
}
static void and_hl(void)
{
	cycles_used += 7;
	and(peek(regs.w.hl));
}
static void and_a(void)
{
	cycles_used += 4;
	and(regs.b.a);
} 
static void xor_b(void)
{
	cycles_used += 4;
	xor(regs.b.b);
}
static void xor_c(void)
{
	cycles_used += 4;
	xor(regs.b.c);
}
static void xor_d(void)
{
	cycles_used += 4;
	xor(regs.b.d);
}
static void xor_e(void)
{
	cycles_used += 4;
	xor(regs.b.e);
}
static void xor_h(void)
{
	cycles_used += 4;
	xor(regs.b.h);
}
static void xor_l(void)
{
	cycles_used += 4;
	xor(regs.b.l);
}
static void xor_hl(void)
{
	cycles_used += 7;
	xor(peek(regs.w.hl));
}
static void xor_a(void)
{
	cycles_used += 4;
	xor(regs.b.a);
} 

static void or_b(void)
{
	cycles_used += 4;
	or(regs.b.b);
}
static void or_c(void)
{
	cycles_used += 4;
	or(regs.b.c);
}
static void or_d(void)
{
	cycles_used += 4;
	or(regs.b.d);
}
static void or_e(void)
{
	cycles_used += 4;
	or(regs.b.e);
}
static void or_h(void)
{
	cycles_used += 4;
	or(regs.b.h);
}
static void or_l(void)
{
	cycles_used += 4;
	or(regs.b.l);
}
static void or_hl(void)
{
	cycles_used += 7;
	or(peek(regs.w.hl));
}
static void or_a(void)
{
	cycles_used += 4;
	or(regs.b.a);
} 
static void cp_b(void)
{
	cycles_used += 4;
	cp(regs.b.b);
}
static void cp_c(void)
{
	cycles_used += 4;
	cp(regs.b.c);
}
static void cp_d(void)
{
	cycles_used += 4;
	cp(regs.b.d);
}
static void cp_e(void)
{
	cycles_used += 4;
	cp(regs.b.e);
}
static void cp_h(void)
{
	cycles_used += 4;
	cp(regs.b.h);
}
static void cp_l(void)
{
	cycles_used += 4;
	cp(regs.b.l);
}
static void cp_hl(void)
{
	cycles_used += 7;
	cp(peek(regs.w.hl));
}
static void cp_a(void)
{
	cycles_used += 4;
	cp(regs.b.a);
} 

static void retnz(void)
{
	if (!(regs.b.f & FLAGS_ZF))
	{
		cycles_used += 11;
		regs.pc = pop();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 5;
	}
}

static void pop_bc(void)
{
	cycles_used += 10;
	regs.w.bc = pop();
}

static void jp_nzi16(void)
{
	regs.memptr = get_imm16();
	cycles_used += 10;
	if (!(regs.b.f & FLAGS_ZF))
		regs.pc = jumptargetJ(regs.memptr);
}

static void jp_i16(void)
{
	cycles_used += 10;
	regs.pc = regs.memptr = jumptargetJ(get_imm16());
}

static void call_nzi16(void)
{
	regs.memptr = get_imm16();
	if (!(regs.b.f & FLAGS_ZF))
	{
		cycles_used += 17;
		push(regs.pc);
		regs.pc = jumptargetC(regs.memptr);
	}
	else
	{
		cycles_used += 10;
	}
}

static void push_bc(void)
{
	cycles_used += 11;
	push(regs.w.bc);
}

static void add_i8(void)
{
	cycles_used += 7;
	add(get_imm8());
}

static void rst_00h(void)
{
	cycles_used += 11;
	push(regs.pc);
	regs.pc = 0;
	regs.memptr = regs.pc;
} 

static void retz(void)
{
	if (regs.b.f & FLAGS_ZF)
	{
		cycles_used += 11;
		regs.pc = pop();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 5;
	}
}

static void ret(void)
{
	if (machine.has_if1 && regs.pc == 0x0701)
	{
		regs.pc = peek_fastw(regs.sp);
		page_interface1();
	}
	cycles_used += 10;
	regs.pc = pop();
	regs.memptr = regs.pc;
}

static void jp_zi16(void)
{
	regs.memptr = get_imm16();
	cycles_used += 10;
	if (regs.b.f & FLAGS_ZF)
		regs.pc = jumptargetJ(regs.memptr);
}

//---------------------------------------------------------------------------------------
// CB
//---------------------------------------------------------------------------------------
#pragma warning(push)
#pragma warning(disable: 4701 4703 6001)
static void pfx_cb(void)
{
	unsigned char i;
	unsigned char *r, *r2;
	unsigned char v;
	signed char o;
	unsigned char mask;
	unsigned char b = 0;

	incr();

	if (ix_iy == R_CBIX || ix_iy == R_CBIY)
	{
		o = get_imm8();
		cycles_used += 8;
	}

	i = get_imm8();

	if (ix_iy != R_CBIX && ix_iy != R_CBIY)
	{
		do_stats(CS_CB, i);
	}

	if (ix_iy == R_CBIX)
	{
		unsigned short ea = regs.ix[R_IX].i + o;
		r = peeka(ea);

		regs.memptr = ea;

		//undocumented
		switch (i&7)
		{
			case 0: r2 = &regs.b.b; break;
			case 1: r2 = &regs.b.c; break;
			case 2: r2 = &regs.b.d; break;
			case 3: r2 = &regs.b.e; break;
			case 4: r2 = &regs.b.h; break;
			case 5: r2 = &regs.b.l; break;
			case 6:	r2 = NULL; break;
			case 7: r2 = &regs.b.a; break;
		}
	}
	else if (ix_iy == R_CBIY)
	{
		unsigned short ea = regs.ix[R_IY].i + o;
		r = peeka(ea);

		regs.memptr = ea;

		//undocumented
		switch (i&7)
		{
			case 0: r2 = &regs.b.b; break;
			case 1: r2 = &regs.b.c; break;
			case 2: r2 = &regs.b.d; break;
			case 3: r2 = &regs.b.e; break;
			case 4: r2 = &regs.b.h; break;
			case 5: r2 = &regs.b.l; break;
			case 6:	r2 = NULL; break;
			case 7: r2 = &regs.b.a; break;
		}
	}
	else
	{
		switch (i&7)
		{
			case 0: r = &regs.b.b; break;
			case 1: r = &regs.b.c; break;
			case 2: r = &regs.b.d; break;
			case 3: r = &regs.b.e; break;
			case 4: r = &regs.b.h; break;
			case 5: r = &regs.b.l; break;
			case 6:	r = peeka(regs.w.hl);b=1;break;
			case 7: r = &regs.b.a; break;
		}
		r2 = NULL;
	}

	if (r2) cycles_used += 7;

	switch (i>>6)
	{
		case 0://rlc etc
			switch (i>>3)
			{
				case 0:	rlc(r); /* undocumented */ if (r2) *r2=*r; /* undocumented */ break;
				case 1:	rrc(r); /* undocumented */ if (r2) *r2=*r; /* undocumented */ break;
				case 2:	rl(r);  /* undocumented */ if (r2) *r2=*r; /* undocumented */ break;
				case 3:	rr(r);  /* undocumented */ if (r2) *r2=*r; /* undocumented */ break;
				case 4:	sla(r); /* undocumented */ if (r2) *r2=*r; /* undocumented */ break;
				case 5:	sra(r); /* undocumented */ if (r2) *r2=*r; /* undocumented */ break;
				case 6: sll(r); /* undocumented */ if (r2) *r2=*r; /* undocumented */ break;
				case 7: srl(r); /* undocumented */ if (r2) *r2=*r; /* undocumented */ break;
				default:
					assert(0);
			}
			if ((i&7)==6)
				cycles_used += 15;
			else
				cycles_used += 8;
			break;

		case 1://bit
			if ((i&7)==6)
				cycles_used += 12;
			else
				cycles_used += 8;

			i -= 64;
			i >>= 3;
			assert(i>=0&&i<8);

			mask = 1<<i;
			v = *r & mask;

			regs.b.f &= FLAGS_CF;
			regs.b.f |= (v&FLAGS_SF)|FLAGS_HF;
			if (!v)
				regs.b.f |= FLAGS_ZF|FLAGS_PF;
			
			if (ix_iy == R_CBIX)
			{
				//bit n,(IX+d) get XF and YF from high byte of IX+d
				regs.b.f |= ((regs.ix[R_IX].i + o)>>8)&(FLAGS_XF|FLAGS_YF);
			}
			else if (ix_iy == R_CBIY)
			{
				//bit n,(IY+d) get XF and YF from high byte of IY+d
				regs.b.f |= ((regs.ix[R_IY].i + o)>>8)&(FLAGS_XF|FLAGS_YF);
			}
			else if (b)//(i&7)==6)
			{
				regs.b.f |= (regs.memptr>>8)&(FLAGS_XF|FLAGS_YF);
			}
			else
			{
				regs.b.f |= *r&(FLAGS_XF|FLAGS_YF);
			}
			break;

		case 2://res
#pragma message("res needs to generate write cycles when (hl) is target")
			if ((i&7)==6)
				cycles_used += 15;
			else
				cycles_used += 8;

			i -= 128;
			i >>= 3;
			assert(i>=0&&i<8);
			mask = 1<<i;
			*r &= ~mask;
			if (r2)//undocumented
				*r2 = *r;
			break;

		case 3://set
#pragma message("set needs to generate write cycles when (hl) is target")
			if ((i&7)==6)
				cycles_used += 15;
			else
				cycles_used += 8;

			i -= 192;
			i >>= 3;
			assert(i>=0&&i<8);
			mask = 1<<i;
			*r |= mask;
			if (r2)//undocumented
				*r2 = *r;
			break;
	}
}
#pragma warning(pop)

static void call_zi16(void)
{
	regs.memptr = get_imm16();
	if (regs.b.f & FLAGS_ZF)
	{
		cycles_used += 17;
		push(regs.pc);
		regs.pc = jumptargetC(regs.memptr);
	}
	else
	{
		cycles_used += 10;
	}
}

static void call_i16(void)
{
	cycles_used += 17;
	regs.memptr = get_imm16();
	push(regs.pc);
	regs.pc = jumptargetC(regs.memptr);
}

static void adc_i8(void)
{
	cycles_used += 7;
	adc(get_imm8());
}

static void rst_08h(void)
{
	/*if (machine.has_if1)
		page_interface1();*/

	cycles_used += 11;
	push(regs.pc);
	regs.pc = 8;
	regs.memptr = regs.pc;
} 

static void retnc(void)
{
	if (!(regs.b.f & FLAGS_CF))
	{
		cycles_used += 11;
		regs.pc = pop();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 5;
	}
}

static void pop_de(void)
{
	cycles_used += 10;
	regs.w.de=pop();
}

static void jp_nci16(void)
{
	regs.memptr = get_imm16();
	cycles_used += 10;
	if (!(regs.b.f & FLAGS_CF))
		regs.pc = jumptargetJ(regs.memptr);
}

static void outai8(void)
{
	unsigned short ep = (unsigned short)get_imm8();
	cycles_used += 11;
	regs.memptr = ((ep+1)&0xff) + (((unsigned short)regs.b.a)<<8);
	out(ep, regs.b.a);
}

static void call_nci16(void)
{
	regs.memptr = get_imm16();
	if (!(regs.b.f & FLAGS_CF))
	{
		cycles_used += 17;
		push(regs.pc);
		regs.pc = jumptargetC(regs.memptr);
	}
	else
	{
		cycles_used += 10;
	}
}

static void push_de(void)
{
	cycles_used += 11;
	push(regs.w.de);
}

static void sub_i8(void)
{
	cycles_used += 7;
	sub(get_imm8());
}

static void rst_10h(void)
{
	cycles_used += 11;
	push(regs.pc);
	regs.pc = 0x10;
	regs.memptr = regs.pc;
} 

static void retc(void)
{
	if (regs.b.f & FLAGS_CF)
	{
		cycles_used += 11;
		regs.pc = pop();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 5;
	}
}

static void exx(void)
{
	unsigned short t;

	cycles_used += 4;

	t = regs.w.bc;
	regs.w.bc = regs.exregs.bc;
	regs.exregs.bc = t;

	t = regs.w.de;
	regs.w.de = regs.exregs.de;
	regs.exregs.de = t;

	t = regs.w.hl;
	regs.w.hl = regs.exregs.hl;
	regs.exregs.hl = t;
}

static void jp_ci16(void)
{
	regs.memptr = get_imm16();
	cycles_used += 10;
	if (regs.b.f & FLAGS_CF)
		regs.pc = jumptargetJ(regs.memptr);
}

static void inai8(void)
{
	unsigned short ep = ((unsigned short)regs.b.a<<8)|get_imm8();
	cycles_used += 11;
	regs.memptr = ep+1;
	regs.b.a = in(ep);

	if ((ep & 0x00ff) == 0xfe)
		detect_loader(regs.pc-2);

	/*regs.b.f &= FLAGS_CF;
	regs.b.f |= regs.b.a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
	regs.b.f |= parity[regs.b.a];*/
}

static void call_ci16(void)
{
	regs.memptr = get_imm16();
	if (regs.b.f & FLAGS_CF)
	{
		cycles_used += 17;
		push(regs.pc);
		regs.pc = jumptargetC(regs.memptr);
	}
	else
	{
		cycles_used += 10;
	}
}

//0xDD

static void sbc_i8(void)
{
	cycles_used += 7;
	sbc(get_imm8());
}

static void rst_18h(void)
{
	cycles_used += 11;
	push(regs.pc);
	regs.pc = 0x18;
	regs.memptr = regs.pc;
} 

static void retpo(void)
{
	if (!(regs.b.f & FLAGS_PF))
	{
		cycles_used += 11;
		regs.pc = pop();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 5;
	}
}

static void pop_hl(void)
{
	cycles_used += 10;
	regs.w.hl = pop();
}

static void jp_poi16(void)
{
	regs.memptr = get_imm16();
	cycles_used += 10;
	if (!(regs.b.f & FLAGS_PF))
		regs.pc = jumptargetJ(regs.memptr);
}

static void exhlsp(void)
{
	unsigned short hl;
	cycles_used += 19;
	hl = regs.w.hl;
	regs.w.hl = peekw(regs.sp);
	pokew(regs.sp, hl);
	regs.memptr = regs.w.hl;
}

static void call_poi16(void)
{
	regs.memptr = get_imm16();
	if (!(regs.b.f & FLAGS_PF))
	{
		cycles_used += 17;
		push(regs.pc);
		regs.pc = jumptargetC(regs.memptr);
	}
	else
	{
		cycles_used += 10;
	}
}

static void push_hl(void)
{
	cycles_used += 11;
	push(regs.w.hl);
}

static void and_i8(void)
{
	cycles_used += 7;
	and(get_imm8());
}

static void rst_20h(void)
{
	cycles_used += 11;
	push(regs.pc);
	regs.pc = 0x20;
	regs.memptr = regs.pc;
} 

static void retpe(void)
{
	if (regs.b.f & FLAGS_PF)
	{
		cycles_used += 11;
		regs.pc = pop();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 5;
	}
}

static void ld_pchl(void)//aka jp (hl) - It's not indirect though!
{
	cycles_used += 4;
	regs.pc = jumptargetJ(regs.w.hl);
}

static void jp_pei16(void)
{
	regs.memptr = get_imm16();
	cycles_used += 10;
	if (regs.b.f & FLAGS_PF)
		regs.pc = jumptargetJ(regs.memptr);
}

static void exdehl(void)
{
	unsigned short t;
	cycles_used += 4;
	t = regs.w.de;
	regs.w.de = regs.w.hl;
	regs.w.hl = t;
}

static void call_pei16(void)
{
	regs.memptr = get_imm16();
	if (regs.b.f & FLAGS_PF)
	{
		cycles_used += 17;
		push(regs.pc);
		regs.pc = jumptargetC(regs.memptr);
	}
	else
	{
		cycles_used += 10;
	}
}

//---------------------------------------------------------------------------------------
// ED
//---------------------------------------------------------------------------------------
#pragma warning(push)
#pragma warning (disable: 4701 4703 6001)
static void pfx_ed(void)
{
	unsigned char i;
	unsigned char *r;
	unsigned short *rr;

	incr();

	i = get_imm8();

	do_stats(CS_ED, i);

	if (i < 64)
	{
		run_hotpatch(i);
	}
	else if (i >= 64 && i < 128)
	{
		i-=64;
		switch (i&7)
		{
			case 0:	//in R,(c)
				{
				unsigned char zero=0;
				switch (i>>3)
				{
					case 0:	r = &regs.b.b; break;
					case 1:	r = &regs.b.c; break;
					case 2:	r = &regs.b.d; break;
					case 3:	r = &regs.b.e; break;
					case 4:	r = &regs.b.h; break;
					case 5:	r = &regs.b.l; break;
					case 6:	r = &zero; break;
					case 7:	r = &regs.b.a; break;
				}
				cycles_used += 12;
				*r = in(regs.w.bc);

				regs.memptr = regs.w.bc+1;

				regs.b.f &= FLAGS_CF;
				regs.b.f |= *r&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
				regs.b.f |= parity[*r];

				}
				break;
			case 1: //out (c),R
				{
				static const unsigned char zero=0;
				switch (i>>3)
				{
					case 0:	r = &regs.b.b; break;
					case 1:	r = &regs.b.c; break;
					case 2:	r = &regs.b.d; break;
					case 3:	r = &regs.b.e; break;
					case 4:	r = &regs.b.h; break;
					case 5:	r = &regs.b.l; break;
					case 6:	r = (unsigned char *)&zero; break;
					case 7:	r = &regs.b.a; break;
				}
				cycles_used += 12;
				out(regs.w.bc, *r);
				regs.memptr = regs.w.bc+1;
				}
				break;
			case 2: //sbc hl,RR
				switch (i>>3)
				{
					case 0:
					case 1:	rr = &regs.w.bc; break;
					case 2:
					case 3:	rr = &regs.w.de; break;
					case 4:
					case 5:	rr = &regs.w.hl; break;
					case 6:
					case 7:	rr = &regs.sp; break;
				}
				cycles_used += 15;
				regs.memptr = regs.w.hl+1;
				if (((i-2)>>3)&1)
					adc16(&regs.w.hl, rr);
				else
					sbc16(&regs.w.hl, rr);
				break;

			case 3: //ld RR,(i16) or ld (i16),RR
				{
				unsigned short ea = get_imm16();
				regs.memptr = ea+1;
				switch (i>>3)
				{
					case 0:
					case 1:
						cycles_used += 20;
						rr = &regs.w.bc;
						break;
					case 2:
					case 3:
						cycles_used += 20;
						rr = &regs.w.de;
						break;
					case 4:
					case 5:
						cycles_used += 16;
						rr = &regs.w.hl;
						break;
					case 6:
					case 7:
						cycles_used += 20;
						rr = &regs.sp;
						break;
				}
				if (((i-3)>>3)&1)
				{
					*rr = peekw(ea);
				}
				else
				{
					pokew(ea, *rr);
				}
				}
				break;

			case 4: //neg
				cycles_used += 8;
				//incremented in neg()
				if (((i-4)>>3) == 0)
					neg();
				else
				{
					neg();//undocumented
				}
				break;

			case 5: //retX
				cycles_used += 14;
				if (((i-5)>>3) == 0)
					retn();
				else if (((i-5)>>3) == 1)
					reti();
				else
				{
					//undocumented
					//if (((i-5)>>3)&1)
					//	reti();
					//else
						retn();
				}
				break;

			case 6: //im N
				cycles_used += 8;
				if (((i-6)>>3) == 0)
					im(0);
				else if (((i-5)>>3) == 2)
					im(1);
				else if (((i-5)>>3) == 3)
					im(2);
				else
				{
					//crazy people, undocumented
					switch ((i-6)>>3)
					{
						case 1:
						case 4:
						case 5:
							im(0); break;
						case 6:
							im(1); break;
						case 7:
							im(2); break;
					}
				}
				break;

			case 7: //ld R,R
				switch ((i-7)>>3)
				{
					case 0://ld i,a
						cycles_used += 9;
						regs.i = regs.b.a;
						break;
					case 1://ld r,a
						cycles_used += 9;
						setr(regs.b.a);
						break;
					case 2://ld a,i
						cycles_used += 9;
						regs.b.a = regs.i;
						regs.b.f &= FLAGS_CF;
						regs.b.f = regs.b.a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
						if (!regs.b.a)
							regs.b.f |= FLAGS_ZF;
						if (regs.iff2)
							regs.b.f |= FLAGS_PF;
						break;
					case 3://ld a,r
						cycles_used += 9;
						regs.b.a = getr();
						regs.b.f &= FLAGS_CF;
						regs.b.f |= regs.b.a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
						if (!regs.b.a)
							regs.b.f |= FLAGS_ZF;
						if (regs.iff2)
							regs.b.f |= FLAGS_PF;
						break;
					case 4:
						//rrd
						{
						unsigned char c;
						unsigned char v;

						cycles_used += 18;
						regs.b.f &= FLAGS_CF;

						c = peek(regs.w.hl);
						v = (c>>4)|(regs.b.a<<4);
						regs.b.a = (regs.b.a&0xf0)|(c&0xf);

						poke(regs.w.hl, v);

						regs.b.f |= regs.b.a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
						if (!regs.b.a)
							regs.b.f |= FLAGS_ZF;
						//if (regs.iff2)
						//	regs.b.f |= FLAGS_PF;
						regs.b.f |= parity[regs.b.a];

						regs.memptr = regs.w.hl+1;
						}
						break;
					case 5:
						//rld
						{
						unsigned char c;
						unsigned char v;

						cycles_used += 18;
						regs.b.f &= FLAGS_CF;

						c = peek(regs.w.hl);
						v = (c<<4)|(regs.b.a &0xf);
						regs.b.a = (regs.b.a & 0xf0)|(c>>4);

						poke(regs.w.hl, v);

						regs.b.f |= regs.b.a&(FLAGS_XF|FLAGS_YF|FLAGS_SF);
						if (!regs.b.a)
							regs.b.f |= FLAGS_ZF;
						//if (regs.iff2)
						//	regs.b.f |= FLAGS_PF;
						regs.b.f |= parity[regs.b.a];

						regs.memptr = regs.w.hl+1;
						}
						break;
					default:
						//possibly 6==ld i,i   7==ld r,r
						nop();
						nop();
				}
		}
	}
	else
	{
		switch (i)
		{
			case 160: cycles_used += 16; ldi(); break;
			case 161: cycles_used += 16; cpi(); break;
			case 162: cycles_used += 16; ini(); break;
			case 163: cycles_used += 16; outi();break;

			case 168: cycles_used += 16; ldd(); break;
			case 169: cycles_used += 16; cpd(); break;
			case 170: cycles_used += 16; ind(); break;
			case 171: cycles_used += 16; outd();break;

			case 176: cycles_used += 16; if (regs.w.bc!=1) regs.memptr = regs.pc+1-2; ldi(); if (regs.w.bc) { cycles_used+=5; regs.pc -= 2;} break;
			case 177: cycles_used += 16; if (regs.w.bc!=1) regs.memptr = regs.pc-2; cpi(); if ((regs.b.f&(FLAGS_ZF|FLAGS_PF))==FLAGS_PF) { cycles_used+=5; regs.pc -= 2;} break;
			case 178: cycles_used += 16; ini(); if (regs.b.b) { cycles_used+=5; regs.pc -= 2;} break;
			case 179: cycles_used += 16; outi();if (regs.b.b) { cycles_used+=5; regs.pc -= 2;} break;
				    
			case 184: cycles_used += 16; if (regs.w.bc!=1) regs.memptr = regs.pc+1-2; ldd(); if (regs.w.bc) { cycles_used+=5; regs.pc -= 2;} break;
			case 185: cycles_used += 16; if (regs.w.bc!=1) regs.memptr = regs.pc+1-1; cpd(); if ((regs.b.f&(FLAGS_ZF|FLAGS_PF))==FLAGS_PF) { cycles_used+=5; regs.pc -= 2;} break;
			case 186: cycles_used += 16; ind(); if (regs.b.b) { cycles_used+=5; regs.pc -= 2;} break;
			case 187: cycles_used += 16; outd();if (regs.b.b) { cycles_used+=5; regs.pc -= 2;} break;

			default:
				nop();
				nop();
		}
	}
}
#pragma warning(pop)

static void xor_i8(void)
{
	cycles_used += 7;
	xor(get_imm8());
}

static void rst_28h(void)
{
	cycles_used += 11;
	push(regs.pc);
	regs.pc = 0x28;
	regs.memptr = regs.pc;
} 

static void retp(void)
{
	if (!(regs.b.f & FLAGS_SF))
	{
		cycles_used += 11;
		regs.pc = pop();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 5;
	}
}

static void pop_af(void)
{
	cycles_used += 10;
	regs.w.af = pop();
}

static void jp_pi16(void)
{
	regs.memptr = get_imm16();
	cycles_used += 10;
	if (!(regs.b.f & FLAGS_SF))
		regs.pc = jumptargetJ(regs.memptr);
}

static void di(void)
{
	cycles_used += 4;
	regs.iff1=regs.iff2=0;
	machine.last_was_ei = 1;
}

static void call_pi16(void)
{
	regs.memptr = get_imm16();
	if (!(regs.b.f & FLAGS_SF))
	{
		cycles_used += 17;
		push(regs.pc);
		regs.pc = jumptargetC(regs.memptr);
	}
	else
	{
		cycles_used += 10;
	}
}

static void push_af(void)
{
	cycles_used += 11;
	push(regs.w.af);
}

static void or_i8(void)
{
	cycles_used += 7;
	or(get_imm8());
}

static void rst_30h(void)
{
	cycles_used += 11;
	push(regs.pc);
	regs.pc = 0x30;
	regs.memptr = regs.pc;
} 

static void retm(void)
{
	if (regs.b.f & FLAGS_SF)
	{
		cycles_used += 11;
		regs.pc = pop();
		regs.memptr = regs.pc;
	}
	else
	{
		cycles_used += 5;
	}
}

static void ld_sphl(void)
{
	cycles_used += 6;
	regs.sp = regs.w.hl;
}

static void jp_mi16(void)
{
	regs.memptr = get_imm16();
	cycles_used += 10;
	if (regs.b.f & FLAGS_SF)
		regs.pc = jumptargetJ(regs.memptr);
}

static void ei(void)
{
	cycles_used += 4;
	regs.iff1=regs.iff2=1;
	machine.last_was_ei = 1;
}

static void call_mi16(void)
{
	regs.memptr = get_imm16();
	if (regs.b.f & FLAGS_SF)
	{
		cycles_used += 17;
		push(regs.pc);
		regs.pc = jumptargetC(regs.memptr);
	}
	else
	{
		cycles_used += 10;
	}
}

//0xFD

static void cp_i8(void)
{
	cycles_used += 7;
	cp(get_imm8());
}

static void rst_38h(void)
{
	cycles_used += 11;
	push(regs.pc);
	regs.pc = 0x38;
	regs.memptr = regs.pc;
} 

//---------------------------------------------------------------------------------------
// IX/IY (0xDD , 0xCB)
//---------------------------------------------------------------------------------------
typedef signed char offset_type;

static void nop_ix(void)
{
	cycles_used += 4;
	regs.pc--;//skip back, we don't need the prefix
	//todo: that extra read might have caused contention, best not to do it in the first place
}

static void add_ixbc(void)
{
	cycles_used += 15;
	regs.memptr = regs.ix[ix_iy].i+1;
	add16(&regs.ix[ix_iy].i, regs.w.bc);
}

static void add_ixde(void)
{
	cycles_used += 15;
	regs.memptr = regs.ix[ix_iy].i + 1;
	add16(&regs.ix[ix_iy].i, regs.w.de);
}

static void ld_ixi16(void)
{
	cycles_used += 14;
	regs.ix[ix_iy].i = get_imm16();
}

static void ld_i16ix(void)
{
	unsigned short ea = get_imm16();
	cycles_used += 20;
	regs.memptr = ea+1;
	pokew(ea, regs.ix[ix_iy].i);
}

static void inc_ix(void)
{
	cycles_used += 10;
	regs.ix[ix_iy].i++;
}

static void inc_ixh(void)
{
	cycles_used += 8;
	inc8(&regs.ix[ix_iy].b.h);
}

static void dec_ixh(void)
{
	cycles_used += 8;
	dec8(&regs.ix[ix_iy].b.h);
}

static void ld_ixhi8(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.h = get_imm8();
}

static void add_ixix(void)
{
	cycles_used += 15;
	regs.memptr = regs.ix[ix_iy].i + 1;
	add16(&regs.ix[ix_iy].i, regs.ix[ix_iy].i);
}

static void ld_ixci16(void)
{
	unsigned short ea = get_imm16();
	cycles_used += 20;
	regs.memptr = ea+1;
	regs.ix[ix_iy].i = peekw(ea);
}

static void dec_ix(void)
{
	cycles_used += 10;
	regs.ix[ix_iy].i--;
}

static void inc_ixl(void)
{
	cycles_used += 8;
	inc8(&regs.ix[ix_iy].b.l);
}

static void dec_ixl(void)
{
	cycles_used += 8;
	dec8(&regs.ix[ix_iy].b.l);
}

static void ld_ixli8(void)
{
	cycles_used += 11;
	regs.ix[ix_iy].b.l = get_imm8();
}

static void inc_cix(void)
{
	cycles_used += 23;
	inc8(peeka(regs.ix[ix_iy].i + (offset_type)get_imm8()));
}

static void dec_cix(void)
{
	cycles_used += 23;
	dec8(peeka(regs.ix[ix_iy].i + (offset_type)get_imm8()));
}

static void ld_ixi8(void)
{
	offset_type o;
	cycles_used += 19;
	o = get_imm8();
	poke(regs.ix[ix_iy].i + o, get_imm8());
}

static void add_ixsp(void)
{
	cycles_used += 15;
	regs.memptr = regs.ix[ix_iy].i + 1;
	add16(&regs.ix[ix_iy].i, regs.sp);
}

static void ld_bixh(void)
{
	cycles_used += 8;
	regs.b.b = regs.ix[ix_iy].b.h;
}
static void ld_bixl(void)
{
	cycles_used += 8;
	regs.b.b = regs.ix[ix_iy].b.l;
}
static void ld_bix(void)
{
	unsigned short ea;

	cycles_used += 19;
	ea = regs.ix[ix_iy].i+ (offset_type)get_imm8();
	regs.b.b = peek(ea);
	regs.memptr = (unsigned char)(ea>>8);
}

static void ld_cixh(void)
{
	cycles_used += 8;
	regs.b.c = regs.ix[ix_iy].b.h;
}
static void ld_cixl(void)
{
	cycles_used += 8;
	regs.b.c = regs.ix[ix_iy].b.l;
}
static void ld_cix(void)
{
	unsigned short ea;

	cycles_used += 19;
	ea = regs.ix[ix_iy].i+ (offset_type)get_imm8();
	regs.b.c = peek(ea);
	regs.memptr = (unsigned char)(ea>>8);
}

static void ld_dixh(void)
{
	cycles_used += 8;
	regs.b.d = regs.ix[ix_iy].b.h;
}
static void ld_dixl(void)
{
	cycles_used += 8;
	regs.b.d = regs.ix[ix_iy].b.l;
}
static void ld_dix(void)
{
	unsigned short ea;

	cycles_used += 19;
	ea = regs.ix[ix_iy].i+ (offset_type)get_imm8();
	regs.b.d = peek(ea);
	regs.memptr = (unsigned char)(ea>>8);
}

static void ld_eixh(void)
{
	cycles_used += 8;
	regs.b.e = regs.ix[ix_iy].b.h;
}
static void ld_eixl(void)
{
	cycles_used += 8;
	regs.b.e = regs.ix[ix_iy].b.l;
}
static void ld_eix(void)
{
	unsigned short ea;

	cycles_used += 19;
	ea = regs.ix[ix_iy].i+ (offset_type)get_imm8();
	regs.b.e = peek(ea);
	regs.memptr = (unsigned char)(ea>>8);
}

static void ld_ixhb(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.h = regs.b.b;
}       
static void ld_ixhc(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.h = regs.b.c;
}       
static void ld_ixhd(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.h = regs.b.d;
}       
static void ld_ixhe(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.h = regs.b.e;
}       
static void ld_ixhixh(void)
{
	cycles_used += 8;
}       
static void ld_ixhixl(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.h = regs.ix[ix_iy].b.l;
}       
static void ld_hix(void)
{
	unsigned short ea;

	cycles_used += 19;
	ea = regs.ix[ix_iy].i+ (offset_type)get_imm8();
	regs.b.h = peek(ea);
	regs.memptr = (unsigned char)(ea>>8);
}
static void ld_ixha(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.h = regs.b.a;
}       

static void ld_ixlb(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.l = regs.b.b;
}
static void ld_ixlc(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.l = regs.b.c;
}
static void ld_ixld(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.l = regs.b.d;
}
static void ld_ixle(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.l = regs.b.e;
}
static void ld_ixlixh(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.l = regs.ix[ix_iy].b.h;
}
static void ld_ixlixl(void)
{
	cycles_used += 8;
}
static void ld_lix(void)
{
	unsigned short ea;

	cycles_used += 19;
	ea = regs.ix[ix_iy].i+ (offset_type)get_imm8();
	regs.b.l = peek(ea);
	regs.memptr = (unsigned char)(ea>> 8);
}
static void ld_ixla(void)
{
	cycles_used += 8;
	regs.ix[ix_iy].b.l = regs.b.a;
}

static void ld_ixb(void)
{
	cycles_used += 19;
	poke(regs.ix[ix_iy].i + (offset_type)get_imm8(), regs.b.b);
}
static void ld_ixc(void)
{
	cycles_used += 19;
	poke(regs.ix[ix_iy].i + (offset_type)get_imm8(), regs.b.c);
}
static void ld_ixd(void)
{
	cycles_used += 19;
	poke(regs.ix[ix_iy].i + (offset_type)get_imm8(), regs.b.d);
}
static void ld_ixe(void)
{
	cycles_used += 19;
	poke(regs.ix[ix_iy].i + (offset_type)get_imm8(), regs.b.e);
}
static void ld_ixh(void)
{
	cycles_used += 19;
	poke(regs.ix[ix_iy].i + (offset_type)get_imm8(), regs.b.h);
}
static void ld_ixl(void)
{
	cycles_used += 19;
	poke(regs.ix[ix_iy].i + (offset_type)get_imm8(), regs.b.l);
}
static void ld_ixa(void)
{
	cycles_used += 19;
	poke(regs.ix[ix_iy].i + (offset_type)get_imm8(), regs.b.a);
}

static void ld_aixh(void)
{
	cycles_used += 8;
	regs.b.a = regs.ix[ix_iy].b.h;
}
static void ld_aixl(void)
{
	cycles_used += 8;
	regs.b.a = regs.ix[ix_iy].b.l;
}
static void ld_aix(void)
{
	cycles_used += 19;
	regs.b.a = peek(regs.ix[ix_iy].i + (offset_type)get_imm8());
}

static void add_ix(void)
{
	cycles_used += 19;
	add(peek(regs.ix[ix_iy].i+ (offset_type)get_imm8()));
}
static void add_ixl(void)
{
	cycles_used += 8;
	add(regs.ix[ix_iy].b.l);
}
static void add_ixh(void)
{
	cycles_used += 8;
	add(regs.ix[ix_iy].b.h);
}

static void adc_ix(void)
{
	cycles_used += 19;
	adc(peek(regs.ix[ix_iy].i+ (offset_type)get_imm8()));
}
static void adc_ixl(void)
{
	cycles_used += 8;
	adc(regs.ix[ix_iy].b.l);
}
static void adc_ixh(void)
{
	cycles_used += 8;
	adc(regs.ix[ix_iy].b.h);
}

static void sub_ix(void)
{
	cycles_used += 19;
	sub(peek(regs.ix[ix_iy].i+ (offset_type)get_imm8()));
}
static void sub_ixl(void)
{
	cycles_used += 8;
	sub(regs.ix[ix_iy].b.l);
}
static void sub_ixh(void)
{
	cycles_used += 8;
	sub(regs.ix[ix_iy].b.h);
}

static void sbc_ix(void)
{
	cycles_used += 19;
	sbc(peek(regs.ix[ix_iy].i+ (offset_type)get_imm8()));
}
static void sbc_ixl(void)
{
	cycles_used += 8;
	sbc(regs.ix[ix_iy].b.l);
}
static void sbc_ixh(void)
{
	cycles_used += 8;
	sbc(regs.ix[ix_iy].b.h);
}

static void and_ix(void)
{
	cycles_used += 19;
	and(peek(regs.ix[ix_iy].i+ (offset_type)get_imm8()));
}
static void and_ixl(void)
{
	cycles_used += 8;
	and(regs.ix[ix_iy].b.l);
}
static void and_ixh(void)
{
	cycles_used += 8;
	and(regs.ix[ix_iy].b.h);
}

static void xor_ix(void)
{
	cycles_used += 19;
	xor(peek(regs.ix[ix_iy].i+ (offset_type)get_imm8()));
}
static void xor_ixl(void)
{
	cycles_used += 8;
	xor(regs.ix[ix_iy].b.l);
}
static void xor_ixh(void)
{
	cycles_used += 8;
	xor(regs.ix[ix_iy].b.h);
}

static void or_ix(void)
{
	cycles_used += 19;
	or(peek(regs.ix[ix_iy].i+ (offset_type)get_imm8()));
}
static void or_ixl(void)
{
	cycles_used += 8;
	or(regs.ix[ix_iy].b.l);
}
static void or_ixh(void)
{
	cycles_used += 8;
	or(regs.ix[ix_iy].b.h);
}

static void cp_ix(void)
{
	cycles_used += 19;
	cp(peek(regs.ix[ix_iy].i+ (offset_type)get_imm8()));
}
static void cp_ixl(void)
{
	cycles_used += 8;
	cp(regs.ix[ix_iy].b.l);
}
static void cp_ixh(void)
{
	cycles_used += 8;
	cp(regs.ix[ix_iy].b.h);
}

static void pop_ix(void)
{
	cycles_used += 14;
	regs.ix[ix_iy].i = pop();
}

static void ex_ixsp(void)
{
	unsigned short hl;
	cycles_used += 23;
	hl = regs.ix[ix_iy].i;
	regs.ix[ix_iy].i = peekw(regs.sp);
	regs.memptr = regs.ix[ix_iy].i;
	pokew(regs.sp, hl);
}

static void push_ix(void)
{
	cycles_used += 15;
	push(regs.ix[ix_iy].i);
}

static void ld_pcix(void)//aka jp (ix)
{
	cycles_used += 8;
	//todo: memptr?
	regs.pc = jumptargetJ(regs.ix[ix_iy].i);
}

static void ex_deix(void)
{
	exdehl();
	/*
	unsigned short t;
	cycles_used += 8;//guessed
	t = regs.w.de;
	regs.w.de = regs.ix[ix_iy].i;
	regs.ix[ix_iy].i=t;
	*/
}

static void ld_spix(void)
{
	cycles_used += 10;
	regs.sp = regs.ix[ix_iy].i;
}


//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------

#include "ins.h"

//---------------------------------------------------------------------------------------
// DD
//---------------------------------------------------------------------------------------
static void pfx_dd(void)
{
	unsigned char i;

	i = get_imm8();
	if (i != 0xcb)
	{
		incr();
		//IX
		ix_iy = R_IX;
		do_stats(CS_DD, i);
		ixiyjump_tab[i]();
	}
	else
	{
		//r gets incremented in pfx_cb
		ix_iy=R_CBIX;
		pfx_cb();
		ix_iy=R_IX;
	}
}

//---------------------------------------------------------------------------------------
// FD
//---------------------------------------------------------------------------------------
static void pfx_fd(void)
{
	unsigned char i;
	i = get_imm8();
	if (i != 0xcb)
	{
		incr();
		//IY
		ix_iy = R_IY;
		do_stats(CS_FD, i);
		ixiyjump_tab[i]();
	}
	else
	{
		//r gets incremented in pfx_cb
		ix_iy=R_CBIY;
		pfx_cb();
		ix_iy=R_IX;
	}
	
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void interrupt(void)
{
	if (!regs.iff1)
		return;
	incr();
	switch (regs.im)
	{
		case 0:
		case 1:
			push(regs.pc);
			regs.iff1 = regs.iff2 = 0;
			regs.pc = 56;//0x38
			cycles_used += 13;
			break;
		case 2:
			push(regs.pc);
			regs.iff1 = regs.iff2 = 0;
			regs.pc = jumptargetC(peekw(((unsigned short)regs.i<<8)|0xff));
			cycles_used += 19;
			break;
	}
	regs.memptr = regs.pc;
}

void nmi(void)
{
	//regs.iff2 = regs.iff1;
	regs.iff1 = 0;
	incr();
	push(regs.pc);
	regs.pc = 0x66; // NMI
	cycles_used += 11;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
#include <windows.h>
void execute(unsigned char i)
{
	jump_tab[i]();
}
