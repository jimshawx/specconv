//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

#include <stdlib.h>
#include <assert.h>

#include "sound.h"
#include "ay8912.h"
#include "dbgmem.h"

static void AY_reg0(void);
static void AY_reg1(void);
static void AY_reg2(void);
static void AY_reg3(void);
static void AY_reg4(void);
static void AY_reg5(void);
static void AY_reg6(void);
static void AY_reg7(void);
static void AY_reg8(void);
static void AY_reg9(void);
static void AY_regA(void);
static void AY_regB(void);
static void AY_regC(void);
static void AY_regD(void);
static void AY_regE(void);
static void AY_regF(void);


extern unsigned int get_cycle(void);

#define AY_STEP 0x8000
//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static AY_STATE ay_state;

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void AY_null(void);
static unsigned char curr_reg;
static const void (*curr_fn)(void) = AY_null;

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
/*
; AY-3-8912 registers
; Register    Function              Range
; 0 Channel A fine pitch            8-bit (0-255)
; 1 Channel A coarse pitch          4-bit (0-15)
; 2 Channel B fine pitch            8-bit (0-255)
; 3 Channel B coarse pitch          4-bit (0-15)
; 4 Channel C fine pitch            8-bit (0-255)
; 5 Channel C coarse pitch          4-bit (0-15)
; 6 Noise pitch                     5-bit (0-31)
; 7 Mixer                           8-bit 
; 8 Channel A volume                4-bit (0-15)
; 9 Channel B volume                4-bit (0-15)
; A Channel C volume                4-bit (0-15)
; B Envelope fine duration          8-bit (0-255)
; C Envelope coarse duration        8-bit (0-255)
; D Envelope shape                  4-bit (0-15)
; E I/O port A                      8-bit (0-255)
; F I/O port B                      8-bit (0-255)
*/

static const void (*AY_data_fn[16])(void)=
{
	AY_reg0,
	AY_reg1,
	AY_reg2,
	AY_reg3,
	AY_reg4,
	AY_reg5,
	AY_reg6,
	AY_reg7,
	AY_reg8,
	AY_reg9,
	AY_regA,
	AY_regB,
	AY_regC,
	AY_regD,
	AY_regE,
	AY_regF,
};

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void AY_init(void)
{
	int i;
	float out = 21845.0f;

	ay_state.update_step=(int)((AY_STEP*(float)SOUND_RATE*8.0f + AY_CLOCK/2)/AY_CLOCK);

	ay_state.period[CHANNEL_A] =
	ay_state.period[CHANNEL_B] =
	ay_state.period[CHANNEL_C] = ay_state.update_step;
	ay_state.period[CHANNEL_N] = ay_state.update_step;
	ay_state.period[CHANNEL_E] = ay_state.update_step>>1;
	ay_state.count[CHANNEL_A] =
 	ay_state.count[CHANNEL_B] =
	ay_state.count[CHANNEL_C] = ay_state.update_step;
	ay_state.count[CHANNEL_N] = 1;
	ay_state.count[CHANNEL_E] = 1;
	ay_state.rng = 1;

	for(i=31;i>0;i--)
	{
		ay_state.volTable[i]=out > 21845.0f ? 21845U:(unsigned int)out;
		out/=1.188502227f;
	}
	ay_state.volTable[0]=0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void AY_shutdown(void)
{
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void AY_reg(unsigned char reg)
{
	curr_reg = reg;
	curr_fn = AY_data_fn[curr_reg];
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void AY_data(unsigned char data)
{
	//if (curr_reg == AY_ESHAPE || ay_state.reg[curr_reg] != data)
	//	AY_update(get_cycle());

	ay_state.reg[curr_reg] = data;
	curr_fn();
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
unsigned char AY_read_data(void)
{
	return ay_state.reg[curr_reg];
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void update_volume(int channel)
{
	ay_state.reg[AY_AVOL+channel] &= 0x1f;
	ay_state.envelope[channel] = ay_state.reg[AY_AVOL+channel] & 0x10;
	if (ay_state.envelope[channel] != 0)
		ay_state.vol[channel] = ay_state.vol[CHANNEL_E];
	else
		ay_state.vol[channel] = ay_state.volTable[ay_state.reg[AY_AVOL+channel] != 0 ? (ay_state.reg[AY_AVOL+channel] << 1) + 1 : 0];
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void update_pitch(int channel)
{
	int period;
	int rindex, scale;

	if (channel == CHANNEL_E)
	{
		rindex = AY_EFINE;
		scale = 2;
	}
	else
	{
		rindex = channel*2;
		scale = 1;
	}

	period = ay_state.reg[rindex] + (ay_state.reg[rindex+1] << 8);
	if (period == 0)
		period = 1;
	period *= ay_state.update_step/scale;

	ay_state.count[channel] += period - ay_state.period[channel];
	ay_state.period[channel] = period;
	if (ay_state.count[channel] <= 0)
		ay_state.count[channel] = 1;
}

static void AY_null(void)
{
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------

//A fine
static void AY_reg0(void)
{
	update_pitch(CHANNEL_A);
}

//A coarse
static void AY_reg1(void)
{
	ay_state.reg[AY_ACOARSE] &= 0xf;
	update_pitch(CHANNEL_A);
}

//B fine
static void AY_reg2(void)
{
	update_pitch(CHANNEL_B);
}

//B coarse
static void AY_reg3(void)
{
	ay_state.reg[AY_BCOARSE] &= 0xf;
	update_pitch(CHANNEL_B);
}

//C fine
static void AY_reg4(void)
{
	update_pitch(CHANNEL_C);
}

//C coarse
static void AY_reg5(void)
{
	ay_state.reg[AY_CCOARSE] &= 0xf;
	update_pitch(CHANNEL_C);
}

//noise period
static void AY_reg6(void)
{
	int periodn;

	ay_state.reg[AY_NOISEPER] &= 0x1f;
	periodn = ay_state.reg[AY_NOISEPER];
	if (periodn == 0)
		periodn = 1;

	periodn *= ay_state.update_step;
	ay_state.count[CHANNEL_N] += periodn - ay_state.period[CHANNEL_N];
	ay_state.period[CHANNEL_N] = periodn;
	if (ay_state.count[CHANNEL_N] <= 0)
		ay_state.count[CHANNEL_N] = 1;
}

//enable
static void AY_reg7(void)
{
}

//A vol
static void AY_reg8(void)
{
	update_volume(CHANNEL_A);
}

//B vol
static void AY_reg9(void)
{
	update_volume(CHANNEL_B);
}

//C vol
static void AY_regA(void)
{
	update_volume(CHANNEL_C);
}

//E fine
static void AY_regB(void)
{
	update_pitch(CHANNEL_E);
}

//E coarse
static void AY_regC(void)
{
	update_pitch(CHANNEL_E);
}

//E shape
static void AY_regD(void)
{
	ay_state.reg[AY_ESHAPE] &= 0x0f;
	ay_state.attack = (ay_state.reg[AY_ESHAPE]&0x04)!=0?0x1f:0x00;

	if (ay_state.reg[AY_ESHAPE] & 0x08)
	{
		ay_state.hold = ay_state.reg[AY_ESHAPE]&0x01;
		ay_state.alternate = ay_state.reg[AY_ESHAPE]&0x02;
	}
	else
	{
		ay_state.hold = 1;
		ay_state.alternate = ay_state.attack;
	}
	ay_state.count[CHANNEL_E]=ay_state.period[CHANNEL_E];
	ay_state.countEnv = 0x1f;
	ay_state.holding = 0;
	ay_state.vol[CHANNEL_E] = ay_state.volTable[ay_state.countEnv^ay_state.attack];

	if (ay_state.envelope[CHANNEL_A] != 0) ay_state.vol[CHANNEL_A] = ay_state.vol[CHANNEL_E];
	if (ay_state.envelope[CHANNEL_B] != 0) ay_state.vol[CHANNEL_B] = ay_state.vol[CHANNEL_E];
	if (ay_state.envelope[CHANNEL_C] != 0) ay_state.vol[CHANNEL_C] = ay_state.vol[CHANNEL_E];
}

//port A
static void AY_regE(void)
{
}

//port B
static void AY_regF(void)
{
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void process_output(int channel, int length, unsigned char mask)
{
	if (ay_state.reg[AY_ENABLE] & mask)
	{
		if (ay_state.count[channel] <= length * AY_STEP)
			ay_state.count[channel] += length * AY_STEP;
		ay_state.output[channel] = 1;
	}
	else if (ay_state.reg[AY_AVOL+channel] == 0)
	{
		if (ay_state.count[channel] <= length * AY_STEP)
			ay_state.count[channel] += length * AY_STEP;
	}

}

int process_channel(int channel, unsigned doit, int nextevent)
{
	int vol;
	vol = 0;

	if (doit)
	{
		if (ay_state.output[channel] != 0)
			vol += ay_state.count[channel];
		ay_state.count[channel] -= nextevent;

		while (ay_state.count[channel] <= 0)
		{
			ay_state.count[channel] += ay_state.period[channel];
			if (ay_state.count[channel] > 0)
			{
				ay_state.output[channel] ^= 1;
				if (ay_state.output[channel] != 0)
					vol += ay_state.period[channel];
				break;
			}
			ay_state.count[channel] += ay_state.period[channel];
			vol += ay_state.period[channel];
		}

		if (ay_state.output[channel] != 0)
			vol -= ay_state.count[channel];
	}
	else
	{
		ay_state.count[channel] -= nextevent;
		while (ay_state.count[channel] <= 0)
		{
			ay_state.count[channel] += ay_state.period[channel];
			if (ay_state.count[channel] > 0)
			{
				ay_state.output[channel] ^= 1;
				break;
			}
			ay_state.count[channel] += ay_state.period[channel];
		}
	}
return vol;
}
//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void AY_update(int cycles)
{
	unsigned char outn;
	int length;

	cycles;

	//machine.ay_countdown / machine.cpu_hz * SOUND_RATE
	//10000 / 3546900 * 44100 = 124
	//10000 / 3546900 * 48000 = 135
	length = (10000 * SOUND_RATE) / 3546900;

	process_output(CHANNEL_A, length, 0x01);
	process_output(CHANNEL_B, length, 0x02);
	process_output(CHANNEL_C, length, 0x04);

	if ((ay_state.reg[AY_ENABLE]&0x38)==0x38)
	{
		if (ay_state.count[CHANNEL_N] <= length*AY_STEP)
		{
			ay_state.count[CHANNEL_N] += length*AY_STEP;
		}
	}
	
	outn = (unsigned char)(ay_state.output[CHANNEL_N]|ay_state.reg[AY_ENABLE]);

	while (length-- != 0)
	{
		int vol[3];
		int left, nextevent;

		left=AY_STEP;
		vol[CHANNEL_A] = 0;
		vol[CHANNEL_B] = 0;
		vol[CHANNEL_C] = 0;

		do
		{
			nextevent=ay_state.count[CHANNEL_N]<left?ay_state.count[CHANNEL_N]:left;
			
			vol[CHANNEL_A] += process_channel(CHANNEL_A, 0x08 & outn, nextevent);
			vol[CHANNEL_B] += process_channel(CHANNEL_B, 0x10 & outn, nextevent);
			vol[CHANNEL_C] += process_channel(CHANNEL_C, 0x20 & outn, nextevent);

			ay_state.count[CHANNEL_N] -= nextevent;

			if (ay_state.count[CHANNEL_N]<=0)
			{
				if(((ay_state.rng+1)&2)!=0)
				{
					ay_state.output[CHANNEL_N] = ~ay_state.output[CHANNEL_N];
					outn=(unsigned char)(ay_state.output[CHANNEL_N]|ay_state.reg[AY_ENABLE]);
				}
				if((ay_state.rng&1)!=0)
				{
					ay_state.rng^=0x28000;
				}
				
				ay_state.rng>>=1;
				ay_state.count[CHANNEL_N] += ay_state.period[CHANNEL_N];
			}
			
			left -= nextevent;

		} while (left>0);

		if (ay_state.holding==0)
		{
			ay_state.count[CHANNEL_E] -= AY_STEP;

			if (ay_state.count[CHANNEL_E] <= 0)
			{
				do
				{
					ay_state.countEnv--;
					ay_state.count[CHANNEL_E] += ay_state.period[CHANNEL_E];
				} while (ay_state.count[CHANNEL_E] <= 0);

				if (ay_state.countEnv<0)
				{
					if (ay_state.hold!=0)
					{
						if (ay_state.alternate != 0)
						{
							ay_state.attack ^= 0x1f;
						}
						ay_state.holding = 1;
						ay_state.countEnv = 0;
					}
					else
					{
						if (ay_state.alternate != 0 && (ay_state.countEnv&0x20) != 0)
						{
							ay_state.attack ^= 0x1f;
						}
						ay_state.countEnv &= 0x1f;
					}
				}
				
				ay_state.vol[CHANNEL_E] = ay_state.volTable[ay_state.countEnv^ay_state.attack];

				if (ay_state.envelope[CHANNEL_A]) ay_state.vol[CHANNEL_A] = ay_state.vol[CHANNEL_E];
				if (ay_state.envelope[CHANNEL_B]) ay_state.vol[CHANNEL_B] = ay_state.vol[CHANNEL_E];
				if (ay_state.envelope[CHANNEL_C]) ay_state.vol[CHANNEL_C] = ay_state.vol[CHANNEL_E];
			}
		}
		vol[CHANNEL_A]*=ay_state.vol[CHANNEL_A];
		vol[CHANNEL_B]*=ay_state.vol[CHANNEL_B];
		vol[CHANNEL_C]*=ay_state.vol[CHANNEL_C];

		send_ay_words(vol[CHANNEL_A] / AY_STEP, vol[CHANNEL_B] / AY_STEP, vol[CHANNEL_C] / AY_STEP);
	}
}
