//----------------------------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//----------------------------------------------------------------------------------------------------------
#include <windows.h>
#include <stdlib.h>
#include <string.h>

#include <assert.h>

#include "hqx/hqx.h"

#include "machine.h"
#include "zxgfx.h"
#include "emu.h"
#include "dbgmem.h"
#include "logger.h"

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
typedef struct
{
	unsigned int cycle;
	//48k/128k
	unsigned int num_scanlines;//312 / 311
	unsigned int first_scanline;//64 / 63
	unsigned int scanline_cycle_count;//224 / 228
	unsigned int frame_cycle_count;// 312*224 = 69888 / 311*228 = 70908
	unsigned int narrow_border_time;

	unsigned char port;
	unsigned char last_attribute;
	unsigned char border_colour;
	unsigned char flashmask;

	unsigned char *screen_src;
	unsigned char *attr_src;
	unsigned int *screen_dst;

	unsigned int frame_count;
} ULA_STATE;

static ULA_STATE ula = { 0 };

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
extern HWND emu_window;
extern FASTSCREEN fast_screen;

static unsigned int *output_screen;
static char clean[SCREEN_HEIGHT];

static const unsigned int zx_colours[16]=
{
	0x00000000, 0x000000df, 0x00df0000, 0x00df00df, 0x00df00, 0x00dfdf, 0xdfdf00, 0xdfdfdf,//normal
	0x00000000, 0x000000ff, 0x00ff0000, 0x00ff00ff, 0x00ff00, 0x00ffff, 0xffff00, 0xffffff,//bright
};

static const BITMAPINFO spec_screen_bmi=
{
	sizeof(BITMAPINFOHEADER),
	SCANLINE_WIDTH,
	-SCREEN_HEIGHT,
	1,
	32,
	BI_RGB,
	0,
	75,
	75,
	0,
	0,
};

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
int enable_hqx = 0;
static uint32_t *os2;
static uint32_t *os3;
static uint32_t *os4;
void gfx_init(void)
{

	hqxInit();
	os2 = malloc(SCANLINE_WIDTH * SCREEN_HEIGHT * 2 * 2 * sizeof(unsigned int));
	os3 = malloc(SCANLINE_WIDTH * SCREEN_HEIGHT * 3 * 3 * sizeof(unsigned int));
	os4 = malloc(SCANLINE_WIDTH * SCREEN_HEIGHT * 4 * 4 * sizeof(unsigned int));
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
void gfx_shutdown(void)
{
	free(os4);
	free(os3);
	free(os2);
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
void create_screen(void)
{
	output_screen = malloc(SCANLINE_WIDTH * SCREEN_HEIGHT * sizeof(unsigned int));
	assert(output_screen);
	dirty_whole_screen();
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
void kill_screen(void)
{
	free(output_screen);
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
static void wide_border(unsigned int line, unsigned char border)
{
	unsigned int i;
	unsigned int *dst;
	i = SCANLINE_WIDTH;
	dst = output_screen + SCANLINE_WIDTH *line;
	while (i--)
		*dst++ = zx_colours[border];
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
static void narrow_borders(unsigned int line, unsigned char border)
{
	unsigned int i;
	unsigned int *dst;
	i = ula.narrow_border_time * 2;
	dst = output_screen + SCANLINE_WIDTH * line;
	while (i--) *dst++ = zx_colours[border];
	dst += 256;
	i = SCANLINE_WIDTH - ula.narrow_border_time * 2 - 256;
	while (i--) *dst++ = zx_colours[border];
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
void dirty_screen(unsigned short address, unsigned char value)
{
	if (fast_screen == FS_OFF || fast_screen == FS_ULA)
		return;

	if (peek_fast(address) != value)
	{
		unsigned int first_line = ula.first_scanline;
		unsigned char flash = ula.flashmask;

		address -= 16384;

		if (address >= 0 && address < 6144)
		{
			switch (fast_screen)
			{
				case FS_IMMEDIATE:
				{
					//changed an 8 pixel block
					unsigned short coff;
					unsigned char col, b;
					unsigned short A,B,C,D;
					unsigned int ink, paper, bright;
					unsigned int *dst;

					//address of the colour block that corresponds to this 8 pixel block
					coff = (address / 32) & 7;
					//coff += address & 0x300; //   address/2048*256
					coff += (address/2048)*256;
					col = peek_fast(16384+6144+coff);

					bright = (col&0x40)>>3;
					ink = zx_colours[(col&7)+bright];
					paper = zx_colours[((col>>3)&7)+bright];

					if (flash & col)
					{
						unsigned int t;
						t = ink;
						ink = paper;
						paper = t;
					}

					//address of this line on the output screen

					//pixels
					D = address & 0x1f;
					address /= 32;
					A = address & 7;
					address /= 8;
					B = address & 7;
					address /= 8;
					C = address;

					dst = output_screen+C*64+(D+A)*8+B;
					assert(dst >= output_screen);
					assert(dst < output_screen + SCANLINE_WIDTH * SCREEN_HEIGHT);

					for (b = 0x80; b > 0; b>>=1)
					{
						if (value&b)
							*dst++ = ink;
						else
							*dst++ = paper;
					}
				}
				break;

				case FS_SCREEN:
				case FS_SCANLINE:
				{
					unsigned short A,B,C;
					//pixels
					address /= 32;
					A = address & 7;
					address /= 8;
					B = address & 7;
					address /= 8;
					C = address;

					clean[first_line+C*64+A*8+B] = 0;
				}
				break;

				default:
					assert(0);
					break;

				}
		}
		else if (address >= 6144 && address < 6912)
		{
			switch (fast_screen)
			{
				case FS_IMMEDIATE:
				{
					//changed colours of an 8x8 pixel block
				}
				break;

				case FS_SCREEN:
				case FS_SCANLINE:
				{
					int i;
					//colours
					address -= 6144;
					address /= 32;
					address *= 8;
					for (i = 0; i < 8; i++)
						clean[first_line + address + i] = 0;
				}
				break;

				default:
					assert(0);
					break;
			}
		}
	}
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
void dirty_whole_screen(void)
{
	memset(clean, 0, sizeof clean);
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
void convert_scanline(unsigned int line, unsigned char border)
{
	unsigned int *dst;
	unsigned char *src, *csrc;
	unsigned int s,y1,y0;
	unsigned int x;
	
	unsigned int first_line = ula.first_scanline;
	unsigned char *screen = peeka_fast(16384);
	unsigned char flash = ula.flashmask;

	if (clean[line])
	{
		if (line < first_line || line >= first_line+192)
			wide_border(line, border);
		else
			narrow_borders(line, border);
		return;
	}

	if (line < first_line || line >= first_line+192)
	{
		wide_border(line, border);
		return;
	}

	clean[line]=1;

	dst = output_screen + line*SCANLINE_WIDTH + ula.narrow_border_time * 2;

	line -= first_line;
	s = line/64;
	y0 = (line-(s*64))/8;
	y1 = line&7;

	src = screen + (s*64+y1*8+y0)*32;
	csrc = screen+((256*192)>>3)+(line/8)*32;
	
	unsigned long long *dcsrc = (unsigned long long *)csrc;
	unsigned long long *dsrc = (unsigned long long *)src;
	for (x = 0; x < 4; x++)
	{
		unsigned char b;
		unsigned int ink, paper, bright;

		unsigned long long c;
		unsigned long long col;
		int y;

		c = *dsrc++;
		col = *dcsrc++;

		for (y = 0; y < 8; y++)
		{
			bright = (col & 0x40) >> 3;
			ink = zx_colours[(col & 7) + bright];
			paper = zx_colours[((col >> 3) & 7) + bright];

			if (flash & col)
			{
				unsigned int t;
				t = ink;
				ink = paper;
				paper = t;
			}

			for (b = 0x80; b > 0; b >>= 1)
			{
				if (c & b)
					*dst++ = ink;
				else
					*dst++ = paper;
			}
			c >>= 8;
			col >>= 8;
		}
	}
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
static unsigned char borders[SCREEN_HEIGHT];
void set_border(int scanline, unsigned char value)
{
	borders[scanline] = value;
}

void set_border_colour(unsigned char value)
{
	memset(borders, value, SCREEN_HEIGHT);
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
void convert_screen(void)
{
	unsigned int y;

	for (y = 0; y < ula.num_scanlines; y++)
		convert_scanline(y, borders[y]);
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
void paint_screen(HDC hdc)
{
	HWND hwnd = WindowFromDC(hdc);
	RECT rect;
	GetClientRect(hwnd, &rect);
	rect.right -= rect.left;
	rect.bottom -= rect.top;

	float scale0 = (float)rect.right / (float)(PRETTY_BORDER_WIDTH * 2 + 256);
	float scale1 = (float)rect.bottom / (float)(PRETTY_BORDER_WIDTH * 2 + 192);

	if (scale1 < scale0) scale0 = scale1;

	//yuck
	//SetStretchBltMode(hdc, HALFTONE);
	//SetBrushOrgEx(hdc, 0, 0, NULL);

	if (scale0 == 2.0f && enable_hqx)
	{
		BITMAPINFO bmi = spec_screen_bmi;
		bmi.bmiHeader.biWidth = SCANLINE_WIDTH * 2;
		bmi.bmiHeader.biHeight = -SCREEN_HEIGHT * 2;
		hq2x_32(output_screen, os2, SCANLINE_WIDTH, SCREEN_HEIGHT);
		StretchDIBits(hdc, 0, 0, (int)((PRETTY_BORDER_WIDTH * 2 + 256) * scale0), (int)((PRETTY_BORDER_WIDTH * 2 + 192) * scale0), 2*(BORDER_WIDTH - PRETTY_BORDER_WIDTH), 2*(BORDER_WIDTH - PRETTY_BORDER_WIDTH + 5), 2*(2 * PRETTY_BORDER_WIDTH + 256), 2*(2 * PRETTY_BORDER_WIDTH + 192), os2, &bmi, DIB_RGB_COLORS, SRCCOPY);
	}
	else if (scale0 == 3.0f && enable_hqx)
	{
		BITMAPINFO bmi = spec_screen_bmi;
		bmi.bmiHeader.biWidth = SCANLINE_WIDTH * 3;
		bmi.bmiHeader.biHeight = -SCREEN_HEIGHT * 3;
		hq3x_32(output_screen, os3, SCANLINE_WIDTH, SCREEN_HEIGHT);
		StretchDIBits(hdc, 0, 0, (int)((PRETTY_BORDER_WIDTH * 2 + 256) * scale0), (int)((PRETTY_BORDER_WIDTH * 2 + 192) * scale0), 3*( BORDER_WIDTH - PRETTY_BORDER_WIDTH), 3*(BORDER_WIDTH - PRETTY_BORDER_WIDTH + 5), 3*(2 * PRETTY_BORDER_WIDTH + 256), 3*(2 * PRETTY_BORDER_WIDTH + 192), os3, &bmi, DIB_RGB_COLORS, SRCCOPY);
	}
	else if (scale0 == 4.0f && enable_hqx)
	{
		BITMAPINFO bmi = spec_screen_bmi;
		bmi.bmiHeader.biWidth = SCANLINE_WIDTH * 4;
		bmi.bmiHeader.biHeight = -SCREEN_HEIGHT * 4;
		hq4x_32(output_screen, os4, SCANLINE_WIDTH, SCREEN_HEIGHT);
		StretchDIBits(hdc, 0, 0, (int)((PRETTY_BORDER_WIDTH * 2 + 256) * scale0), (int)((PRETTY_BORDER_WIDTH * 2 + 192) * scale0), 4*(BORDER_WIDTH - PRETTY_BORDER_WIDTH), 4*(BORDER_WIDTH - PRETTY_BORDER_WIDTH + 5), 4*(2 * PRETTY_BORDER_WIDTH + 256), 4*(2 * PRETTY_BORDER_WIDTH + 192), os4, &bmi, DIB_RGB_COLORS, SRCCOPY);
	}
	else
	{
		StretchDIBits(hdc, 0, 0, (int)((PRETTY_BORDER_WIDTH * 2 + 256) * scale0), (int)((PRETTY_BORDER_WIDTH * 2 + 192) * scale0), BORDER_WIDTH - PRETTY_BORDER_WIDTH, BORDER_WIDTH - PRETTY_BORDER_WIDTH + 5, 2 * PRETTY_BORDER_WIDTH + 256, 2 * PRETTY_BORDER_WIDTH + 192, output_screen, &spec_screen_bmi, DIB_RGB_COLORS, SRCCOPY);
	}
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
void repaint_screen(void)
{
	HDC hdc = GetDC(emu_window);
	paint_screen(hdc);
	ReleaseDC(emu_window, hdc);
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------

//48K
//24T left, 128T screen, 24T right, 48T flyback = 224

//128K (assumed - the borders are a little fatter)
//25T left, 128T screen, 25T right, 50T flyback = 228

void gen_tables();
void ula_init(int Xnum_scanlines, int Xfirst_scanline, int Xscanline_cycle_count)
{
	//gen_tables();
	ula.num_scanlines = Xnum_scanlines;
	ula.first_scanline = Xfirst_scanline;
	ula.scanline_cycle_count = Xscanline_cycle_count;
	ula.frame_cycle_count = ula.num_scanlines * ula.scanline_cycle_count;
	ula.narrow_border_time = (ula.scanline_cycle_count - (256 / 2)) / 4;

	ula_out(0xff);

	ula.screen_src = peeka_fast(16384);
	ula.attr_src = peeka_fast(22528);

	ula.frame_count = 0;
	ula.flashmask = 0;

	ula.last_attribute = 0xff;

	ula_reset_tick();
}

void ula_out(unsigned char out)
{
	ula.port = out;
	ula.border_colour = out & 7;
}

unsigned char ula_in()
{
	return ula.last_attribute;
}

//128k shadow screen paged in/out
void ula_page(void)
{
	ula.screen_src = peeka_fast(16384);
	ula.attr_src = peeka_fast(22528);
}

void ula_reset_tick(void)
{
	ula.cycle = 0;
	ula.screen_dst = output_screen;
}

const unsigned int scanline_to_ZX[];
const unsigned char attributes[];
static int state = 1;
static int pixels = 0;
static void ula_draw()
{
	assert(ula.screen_dst >= output_screen);
	//it's OK for them to be == because it's the flyback on the last scanline
	assert(ula.screen_dst <= output_screen + SCREEN_HEIGHT * SCANLINE_WIDTH);

	ldiv_t beam_position = ldiv(ula.cycle, ula.scanline_cycle_count);

	unsigned int scanline = beam_position.quot;
	unsigned int cycle = beam_position.rem;

	//each ULA cycle is 2 pixels

	//flyback
	if (cycle >= ula.narrow_border_time * 2 + 128)
	{
		//hack (48K need to write 2 more cycles (4 pixels) of border to fill our scanline
		if (cycle == ula.narrow_border_time * 2 + 128 && ula.narrow_border_time == 24)
		{
			unsigned int colour = ula.screen_dst[-1];
			*ula.screen_dst++ = colour;
			*ula.screen_dst++ = colour;
			*ula.screen_dst++ = colour;
			*ula.screen_dst++ = colour;
			pixels += 4;
		}
	}
	else if ((scanline < ula.first_scanline) || //top border
			(scanline >= ula.first_scanline + 192) || //bottom border
			(cycle < ula.narrow_border_time) || //left border
			(cycle >= ula.narrow_border_time + 128))//right border
	{
		*ula.screen_dst++ = zx_colours[ula.border_colour];
		*ula.screen_dst++ = zx_colours[ula.border_colour];
		if (cycle == 0) set_border(scanline, ula.border_colour);
		ula.last_attribute = 0xff;
	}
	else
	{
		//screen
		int src_pixel_x = (cycle - ula.narrow_border_time) * 2;
		int src_pixel_y = scanline - ula.first_scanline;

		//attribute
		int attr_offset = ((src_pixel_y >> 3) * 32) + (src_pixel_x >> 3);
		unsigned char attr = ula.last_attribute = ula.attr_src[attr_offset];

		unsigned char bright = (attr & 0x40) >> 3;
		unsigned int ink = zx_colours[(attr & 7) + bright];
		unsigned int paper = zx_colours[((attr >> 3) & 7) + bright];

		if (ula.flashmask & attr)
		{
			unsigned int t;
			t = ink;
			ink = paper;
			paper = t;
		}

		//pixels
		int screen_offset = scanline_to_ZX[src_pixel_y] + (src_pixel_x >> 3);

		unsigned char bits = ula.screen_src[screen_offset];

		//which bits? 2 bits per clock
		//T state  0   1   2   3
		//bits    76  54  32  10
		int pair = (cycle - ula.narrow_border_time) & 3;
		unsigned char mask = 0x80;
		mask >>= 2 * pair;
		*ula.screen_dst++ = (bits & mask) ? ink : paper;
		mask >>= 1;
		*ula.screen_dst++ = (bits & mask) ? ink : paper;
	}
}

void ula_tick(int tick, int drawing)
{
	while (tick--)
	{
		if (drawing)
			ula_draw();

		//we need to emulate floating bus so that some Elite games don't lock up at start
		//return 0xff during wide borders, 0x00 
		if (!drawing)
			ula.last_attribute = (ula.cycle < (ula.first_scanline  * ula.scanline_cycle_count + ula.narrow_border_time)
								|| ula.cycle > ((ula.first_scanline + 192) * ula.scanline_cycle_count)) ? 0xff : 0x00;

		ula.cycle++;
		if (ula.cycle >= ula.frame_cycle_count)
		{
			ula_reset_tick();
			ula.frame_count++;

			char old_flash = ula.flashmask;
			ula.flashmask = (ula.frame_count << 3) & 0x80;
		
			if (ula.flashmask != old_flash)
				dirty_whole_screen();

			HDC hdc = GetDC(emu_window);
			repaint_screen();
			ReleaseDC(emu_window, hdc);
		}
	}
}

const unsigned int scanline_to_ZX[192] = {
	0x0000, 0x0100, 0x0200, 0x0300,
	0x0400, 0x0500, 0x0600, 0x0700,
	0x0020, 0x0120, 0x0220, 0x0320,
	0x0420, 0x0520, 0x0620, 0x0720,
	0x0040, 0x0140, 0x0240, 0x0340,
	0x0440, 0x0540, 0x0640, 0x0740,
	0x0060, 0x0160, 0x0260, 0x0360,
	0x0460, 0x0560, 0x0660, 0x0760,
	0x0080, 0x0180, 0x0280, 0x0380,
	0x0480, 0x0580, 0x0680, 0x0780,
	0x00A0, 0x01A0, 0x02A0, 0x03A0,
	0x04A0, 0x05A0, 0x06A0, 0x07A0,
	0x00C0, 0x01C0, 0x02C0, 0x03C0,
	0x04C0, 0x05C0, 0x06C0, 0x07C0,
	0x00E0, 0x01E0, 0x02E0, 0x03E0,
	0x04E0, 0x05E0, 0x06E0, 0x07E0,
	0x0800, 0x0900, 0x0A00, 0x0B00,
	0x0C00, 0x0D00, 0x0E00, 0x0F00,
	0x0820, 0x0920, 0x0A20, 0x0B20,
	0x0C20, 0x0D20, 0x0E20, 0x0F20,
	0x0840, 0x0940, 0x0A40, 0x0B40,
	0x0C40, 0x0D40, 0x0E40, 0x0F40,
	0x0860, 0x0960, 0x0A60, 0x0B60,
	0x0C60, 0x0D60, 0x0E60, 0x0F60,
	0x0880, 0x0980, 0x0A80, 0x0B80,
	0x0C80, 0x0D80, 0x0E80, 0x0F80,
	0x08A0, 0x09A0, 0x0AA0, 0x0BA0,
	0x0CA0, 0x0DA0, 0x0EA0, 0x0FA0,
	0x08C0, 0x09C0, 0x0AC0, 0x0BC0,
	0x0CC0, 0x0DC0, 0x0EC0, 0x0FC0,
	0x08E0, 0x09E0, 0x0AE0, 0x0BE0,
	0x0CE0, 0x0DE0, 0x0EE0, 0x0FE0,
	0x1000, 0x1100, 0x1200, 0x1300,
	0x1400, 0x1500, 0x1600, 0x1700,
	0x1020, 0x1120, 0x1220, 0x1320,
	0x1420, 0x1520, 0x1620, 0x1720,
	0x1040, 0x1140, 0x1240, 0x1340,
	0x1440, 0x1540, 0x1640, 0x1740,
	0x1060, 0x1160, 0x1260, 0x1360,
	0x1460, 0x1560, 0x1660, 0x1760,
	0x1080, 0x1180, 0x1280, 0x1380,
	0x1480, 0x1580, 0x1680, 0x1780,
	0x10A0, 0x11A0, 0x12A0, 0x13A0,
	0x14A0, 0x15A0, 0x16A0, 0x17A0,
	0x10C0, 0x11C0, 0x12C0, 0x13C0,
	0x14C0, 0x15C0, 0x16C0, 0x17C0,
	0x10E0, 0x11E0, 0x12E0, 0x13E0,
	0x14E0, 0x15E0, 0x16E0, 0x17E0,
};

void gen_tables()
{
#if 0
	elog("static unsigned char attributes[]={\n");
	//given attr
	for (int attr = 0; attr < 256; attr++)
	{
		unsigned char bright = (attr & 0x40) >> 3;
		unsigned int ink = (attr & 7) + bright;
		unsigned int paper = ((attr >> 3) & 7) + bright;
		unsigned char flash = attr & 0x80;

		if (flash & attr)
		{
			unsigned int t;
			t = ink;
			ink = paper;
			paper = t;
		}

		//possible bits
		//for (int b = 0x80; b >= 0; b>>=2)
		int b = 0x2;
		{
			//for (int i = 0; i < 256; i++)
			for (int i = 0; i < 4; i++)
			{
				//00 paper,paper
				//01 paper,ink
				//10 ink,paper
				//11 ink,ink
				//elog("%d,", (i & b) ? ink : paper);
				//elog("%d,", (i & (b>>1)) ? ink : paper);
				int v0 = (i & b) ? ink : paper;
				int v1 = (i & (b >> 1)) ? ink : paper;
				elog("0x%02X,", v0 | (v1 << 4));
			}
			//elog("\n");
		}
		elog("\n");
	}
	elog("};\n");
#endif
}
