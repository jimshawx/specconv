//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

#ifndef __STATS_H__
#define __STATS_H__

typedef enum
{
	MK_UNKN = 0,
	MK_CODE = 1,
	MK_DATA_RD = 2,
	MK_DATA_WR = 4,
} MK_TYPE;

typedef enum
{
	JT_CALL,
	JT_JUMP,
} JUMP_TYPE;

#endif

#define PC_TRACK_SIZE 512

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void reset_coverage(void);
void pc_trace(unsigned short address);
void do_stats(int type, unsigned char i);
void mark_type(unsigned short address, MK_TYPE type);
void apply_coverage(void);
void mark_jump(unsigned short address, JUMP_TYPE type);
