//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "machine.h"
#include "files.h"
#include "emu.h"
#include "zxgfx.h"
#include "specem.h"
#include "sound.h"
#include "ay8912.h"
#include "coverage.h"
#include "dbgmem.h"
#include "logger.h"
#include "loaders.h"
#include "dasm.h"

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static unsigned char retrieve_tape_bits(int cycles_used);
static void screen_update(void);

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
FASTSCREEN fast_screen = FS_ULA;
MACHINE machine = { 0 };
//extern REG_PACK regs;
extern unsigned int cycles_used;
extern unsigned int tape_cycles;

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void kill_machine(void)
{
	int x;

	for (x = 0; x < 4; x++)
	{
		if (machine.rombanks[x].memory)
		{
			free(machine.rombanks[x].memory);
			machine.rombanks[x].memory=NULL;
		}
	}

	for (x = 0; x < 8; x++)
	{
		if (machine.rambanks[x].memory)
		{
			free(machine.rambanks[x].memory);
			machine.rambanks[x].memory=NULL;
		}
	}

	if (machine.if1.bank.memory)
	{
		free(machine.if1.bank.memory);
		machine.if1.bank.memory = NULL;
	}

	if (machine.mf1.bank.memory)
	{
		free(machine.mf1.bank.memory);
		machine.mf1.bank.memory = NULL;
	}

	if (machine.mf128.bank.memory)
	{
		free(machine.mf128.bank.memory);
		machine.mf128.bank.memory = NULL;
	}

	if (machine.mf3.bank.memory)
	{
		free(machine.mf3.bank.memory);
		machine.mf3.bank.memory = NULL;
	}
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void reset_machine(void)
{
	REG_PACK *sregs;

	machine.tape_playing = 0;
	machine.last_was_ei = 0;

	switch (machine.machine_id)
	{
		case SPEC_16K:
			machine.membank[0] = &machine.rombanks[0];
			machine.membank[1] = &machine.rambanks[0];
			machine.membank[2] = &machine.nullbank;
			machine.membank[3] = &machine.nullbank;
			break;

		case SPEC_48K:
			machine.membank[0] = &machine.rombanks[0];
			machine.membank[1] = &machine.rambanks[0];
			machine.membank[2] = &machine.rambanks[1];
			machine.membank[3] = &machine.rambanks[2];
			break;

		case SPEC_128K:
			machine.membank[0] = &machine.rombanks[0];
			machine.membank[1] = &machine.rambanks[5];
			machine.membank[2] = &machine.rambanks[2];
			machine.membank[3] = &machine.rambanks[0];
			break;

		case SPEC_PLUS2:
			machine.membank[0] = &machine.rombanks[0];
			machine.membank[1] = &machine.rambanks[5];
			machine.membank[2] = &machine.rambanks[2];
			machine.membank[3] = &machine.rambanks[0];
			break;

		case SPEC_PLUS2A:
		case SPEC_PLUS3:
			machine.membank[0] = &machine.rombanks[0];
			machine.membank[1] = &machine.rambanks[5];
			machine.membank[2] = &machine.rambanks[2];
			machine.membank[3] = &machine.rambanks[0];
			break;
		default:
			assert(0);

	}

	sregs = get_regs();
	memset(sregs, 0, sizeof *sregs);
	sregs->ix[0].i = 0xffff;
	sregs->ix[1].i = 0xffff;
	sregs->b.f = FLAGS_ZF;
	sregs->r=0x80;
	machine.cycle=0;

	machine.mf1.ff_nmi_pending = 0;
	machine.mf1.ff_paged_in = 0;

	machine.mf128.ff_nmi_pending = 0;
	machine.mf128.ff_paged_in = 0;

	machine.mf3.ff_nmi_pending = 0;
	machine.mf3.ff_paged_in = 0;

	reset_hotpatches();
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void init_machine(int machine_id)
{
	FILE *f;
	int x;
	char *folder;
	char rompath[_MAX_PATH];

	static const char * const rom_names[4]=
	{
		"..\\resources\\roms\\48.rom",
		"..\\resources\\roms\\128.rom",
		"..\\resources\\roms\\plus2.rom",
		"..\\resources\\roms\\plus3.rom",
	};
	static const char *const if1_rom_names[2] = {
		"..\\resources\\roms\\if1.rom",
		"..\\resources\\roms\\if1v2.rom",
	};
	static const char *const mf1_rom_name = "..\\resources\\roms\\mf1.rom";
	static const char *const mf128_rom_name = "..\\resources\\roms\\mf128.rom";
	static const char *const mf3_rom_name = "..\\resources\\roms\\mf3.rom";

	folder = get_working_folder();
	strcat_s(folder, _MAX_PATH, "\\");

	kill_machine();

	memset(&machine, 0, sizeof machine);

	set_rom_menu(machine_id);

	machine.machine_id = machine_id;

	switch (machine_id)
	{
		case SPEC_16K:
			for (x = 0; x < 1; x++)
			{
				machine.rambanks[x].memory = calloc(0x4000,1);
				machine.rambanks[x].flags = (x==0)?BF_CONTENDED:0;
				machine.rambanks[x].bank_no = x;
			}
			for (x = 1; x < 8; x++)
				machine.rambanks[x].memory = NULL;

			machine.rombanks[0].memory = malloc(0x4000);
			machine.rombanks[0].flags = BF_READONLY;
			sprintf_s(rompath, _MAX_PATH, "%s%s", folder, rom_names[0]);
			fopen_s(&f,rompath, "rb");
			assert(f);
			if (f)
			{
				fread(machine.rombanks[0].memory, 0x4000, 1, f);
				fclose(f);
			}
			machine.rombanks[0].bank_no = 0;

			machine.rombanks[1].memory=NULL;
			machine.rombanks[2].memory=NULL;
			machine.rombanks[3].memory=NULL;

			machine.screen_start = 14336;//224*64 border lines
			machine.first_scanline = 64;
			machine.line_length  = 224;
			machine.frame_length = 69888;
			machine.scanlines    = 312;
			machine.cpu_hz = 3500000;

			machine.nullbank.memory = calloc(0x4000,1);
			machine.nullbank.bank_no = -1;
			machine.nullbank.flags = BF_READONLY;

			break;

		case SPEC_48K:
			for (x = 0; x < 3; x++)
			{
				machine.rambanks[x].memory = calloc(0x4000,1);
				machine.rambanks[x].flags = (x==0)?BF_CONTENDED:0;
				machine.rambanks[x].bank_no = x;
			}
			for (x = 3; x < 8; x++)
				machine.rambanks[x].memory = NULL;

			machine.rombanks[0].memory = malloc(0x4000);
			machine.rombanks[0].flags = BF_READONLY;
			sprintf_s(rompath, _MAX_PATH, "%s%s", folder, rom_names[0]);
			fopen_s(&f,rompath, "rb");
			assert(f);
			if (f)
			{
				fread(machine.rombanks[0].memory, 0x4000, 1, f);
				fclose(f);
			}
			machine.rombanks[0].bank_no = 0;

			machine.rombanks[1].memory=NULL;
			machine.rombanks[2].memory=NULL;
			machine.rombanks[3].memory=NULL;

			machine.screen_start = 14336;//224*64 border lines
			machine.first_scanline = 64;
			machine.line_length  = 224;
			machine.frame_length = 69888;
			machine.scanlines    = 312;
			machine.cpu_hz = 3500000;
			break;

		case SPEC_128K:
			for (x = 0; x < 8; x++)
			{
				machine.rambanks[x].memory = calloc(0x4000,1);
				machine.rambanks[x].flags = (x&1)?BF_CONTENDED:0;//(x>=4)?BF_CONTENDED:0;
				machine.rambanks[x].bank_no = x;
			}

			sprintf_s(rompath, _MAX_PATH, "%s%s", folder, rom_names[1]);
			fopen_s(&f, rompath, "rb");
			assert(f);
			for (x = 0; x < 2; x++)
			{
				machine.rombanks[x].memory = malloc(0x4000);
				if (f) fread(machine.rombanks[x].memory, 0x4000, 1, f);
				machine.rombanks[x].flags = BF_READONLY;
				machine.rombanks[x].bank_no = x;
			}
			if (f) fclose(f);
			machine.rombanks[2].memory=NULL;
			machine.rombanks[3].memory=NULL;

			machine.screen_start = 14364;//228*63 border lines
			machine.first_scanline = 63;
			machine.line_length  = 228;
			machine.frame_length = 70908;
			machine.scanlines    = 311;
			machine.cpu_hz = 3546900;
			break;

		case SPEC_PLUS2:
			for (x = 0; x < 8; x++)
			{
				machine.rambanks[x].memory = calloc(0x4000,1);
				machine.rambanks[x].flags = (x&1)?BF_CONTENDED:0;//(x>=4)?BF_CONTENDED:0;
				machine.rambanks[x].bank_no = x;
			}

			sprintf_s(rompath, _MAX_PATH, "%s%s", folder, rom_names[2]);
			fopen_s(&f,rompath, "rb");
			assert(f);
			for (x = 0; x < 2; x++)
			{
				machine.rombanks[x].memory = malloc(0x4000);
				machine.rombanks[x].flags = BF_READONLY;
				if (f) fread(machine.rombanks[x].memory, 0x4000, 1, f);
				machine.rombanks[x].bank_no = x;
			}
			if (f) fclose(f);
			machine.rombanks[2].memory=NULL;
			machine.rombanks[3].memory=NULL;

			machine.screen_start = 14364;//228*63 border lines
			machine.first_scanline = 63;
			machine.line_length  = 228;                       
			machine.frame_length = 70908;                     
			machine.scanlines    = 311;                       
			machine.cpu_hz = 3546900;
			break;

		case SPEC_PLUS2A:
		case SPEC_PLUS3:
			for (x = 0; x < 8; x++)
			{
				machine.rambanks[x].memory = calloc(0x4000,1);
				machine.rambanks[x].flags = (x&1)?BF_CONTENDED:0;//(x>=4)?BF_CONTENDED:0;
				machine.rambanks[x].bank_no = x;
			}

			sprintf_s(rompath, _MAX_PATH, "%s%s", folder, rom_names[3]);
			fopen_s(&f, rompath, "rb");
			assert(f);
			for (x = 0; x < 4; x++)
			{
				machine.rombanks[x].memory = malloc(0x4000);
				machine.rombanks[x].flags = BF_READONLY;
				if (f) fread(machine.rombanks[x].memory, 0x4000, 1, f);
				machine.rombanks[x].bank_no = x;
			}
			if (f) fclose(f);

			machine.screen_start = 14364;//228*63 border lines
			machine.first_scanline = 63;
			machine.line_length  = 228;                       
			machine.frame_length = 70908;                     
			machine.scanlines    = 311;                       
			machine.cpu_hz = 3546900;
			break;

		default:
			assert(0);
	}

	machine.ear_cycle = machine.cpu_hz/SOUND_RATE;
	machine.ear_timer = 0;

	machine.ay_timer = machine.ay_countdown = 10000;

	machine.line_timer = 0;
	machine.interrupt_timer = 0;
	machine.cycle = 0;

	if (machine.machine_id == SPEC_128K ||
		machine.machine_id == SPEC_PLUS2 ||
		machine.machine_id == SPEC_PLUS2A ||
		machine.machine_id == SPEC_PLUS3)
		machine.has_ay = 1;

	machine.has_if1 = 0;
	if (machine.has_if1)
	{
		machine.if1.bank.bank_no = 0;
		machine.if1.bank.flags = BF_READONLY;
		machine.if1.bank.memory = malloc(0x4000);
		sprintf_s(rompath, _MAX_PATH, "%s%s", folder, if1_rom_names[1]);
		fopen_s(&f, rompath, "rb");
		if (f)
		{
			fread(machine.if1.bank.memory, 0x2000, 1, f);
			memset(machine.if1.bank.memory + 0x2000, 0, 0x2000);
			fclose(f);
		}
		else
		{
			machine.has_if1 = 0;
		}
	}

	machine.has_mf1 = 0;
	if (machine.has_mf1)
	{
		machine.mf1.bank.bank_no = 0;
		machine.mf1.bank.flags = 0;//8K ROM followed by 8K RAM
		machine.mf1.bank.memory = malloc(0x8000);
		sprintf_s(rompath, _MAX_PATH, "%s%s", folder, mf1_rom_name);
		fopen_s(&f, rompath, "rb");
		if (f)
		{
			fread(machine.mf1.bank.memory, 0x2000, 1, f);
			memset(machine.mf1.bank.memory + 0x2000, 0, 0x2000);
			fclose(f);
		}
		else
		{
			machine.has_mf1 = 0;
		}
	}

	machine.has_mf128 = 0;
	if (machine.has_mf128)
	{
		machine.mf128.bank.bank_no = 0;
		machine.mf128.bank.flags = 0;//8K ROM followed by 8K RAM
		machine.mf128.bank.memory = malloc(0x8000);
		sprintf_s(rompath, _MAX_PATH, "%s%s", folder, mf128_rom_name);
		fopen_s(&f, rompath, "rb");
		if (f)
		{
			fread(machine.mf128.bank.memory, 0x2000, 1, f);
			memset(machine.mf128.bank.memory + 0x2000, 0, 0x2000);
			fclose(f);
		}
		else
		{
			machine.has_mf128 = 0;
		}
	}

	machine.has_mf3 = 0;
	if (machine.has_mf3)
	{
		machine.mf3.bank.bank_no = 0;
		machine.mf3.bank.flags = 0;//8K ROM followed by 8K RAM
		machine.mf3.bank.memory = malloc(0x8000);
		sprintf_s(rompath, _MAX_PATH, "%s%s", folder, mf3_rom_name);
		fopen_s(&f, rompath, "rb");
		if (f)
		{
			fread(machine.mf3.bank.memory, 0x2000, 1, f);
			memset(machine.mf3.bank.memory + 0x2000, 0, 0x2000);
			fclose(f);
		}
		else
		{
			machine.has_mf3 = 0;
		}
	}

	machine.has_shadow_roms = machine.has_if1 | machine.has_mf1 | machine.has_mf128 | machine.has_mf3;

	reset_machine();
	ula_init(machine.scanlines, machine.first_scanline, machine.line_length);
}

//---------------------------------------------------------------------------------------
// port I/O
//---------------------------------------------------------------------------------------
extern unsigned char speckeys[8];
extern unsigned char kempston_keys;

unsigned char in(unsigned short p)
{
	unsigned char d;

#pragma message("in not correctly implemented")
#pragma message("in does not have ULA contention")
	d = 0xff;

	//MF3
	if (p == 0x7f3f) elog("7F3F %02xh (%d)\n", d, d);
	if (p == 0x1f3f) elog("1F3F %02Xh (%d)\n", d);

	//other devices have priority over ULA

	//AY register 11xx xxxx xxxx xx0x
//	if ((p&0xC002) == 0xC000)
//	{
//		return;
//	}
	//AY data 10xx xxxx xxxx xx0x
	if ((p&0xC002) == 0x8000)
	{
		switch (machine.machine_id)
		{
			case SPEC_128K:
			case SPEC_PLUS2:
			case SPEC_PLUS2A:
			case SPEC_PLUS3:
				d = AY_read_data();
				return d;
		}
	}

	//FDC Data Port (Read/Write)
	if ((p&0xF002) == 0x3000)
	{
		switch (machine.machine_id)
		{
			case SPEC_PLUS2A:
			case SPEC_PLUS3:
				return 0;
		}
	}

	//IF1 microdrive
	if (machine.has_if1)
	{
		if (p == 231)//0xE7
			return 0;
		if (p == 239)//0xEF
			return 0;
		if (p == 247)//0xF7
			return 0;
	}

	//MF128 (responds to ports 63,191 or 0x3f,0xBF)     //v2 uses 0x9F to avoid clashing with Disciple
	//port xxxx-xxxx-x011-x1xx
	if (machine.has_mf128 && (p & 0x0074) == 0x34)
	{
		page_multiface128(p & 0x80);
		return 0;
	}
	else
	//MF3 (ports are switched from MF128)
	if (machine.has_mf3 && (p & 0x0074) == 0x34)
	{
		page_multiface3(~p & 0x80);
		return 0;
	}
	else
	//MF1
	//https://k1.spdns.de/Vintage/Sinclair/82/Peripherals/Multiface%20I%2C%20128%2C%20and%20%2B3%20(Romantic%20Robot)/MF1/Technical%20Information/
	// port xxxx-xxxx-x001-xx1x (usually 0x1F, 0x9F bit A7 of address indicates page in/out)
	if (machine.has_mf1 && (p & 0x0072) == 0x12)
	{
		page_multiface1(p & 0x80);
		return kempston_keys;
	}
	else if (p == 0x009F)
	{
		elog("Multiface Protection Detected! IN from port %02X!\n", p);
	}

	//multifaces hide Kempston interface, providing their own

	//Kempston joysick
	//port xxxx-xxxx-0001-1111
	if ((p & 0x00FF) == 0x1F)
	{
		//(000FUDLR, active high)
		return kempston_keys;
	}

	//don't know what's supposed to be here, but Cobra/Renegade infinite loops waiting on this port to be <= 0x3f
	//if (p == 0x28ff)
	//	return 0;
	//According to MAME source, these are reading the ULA floating-bus, which is 0xFF during borders and last attribute read by ULA otherwise
	//https://github.com/mamedev/mame/blob/master/src/mame/drivers/spectrum.cpp
	// 0x28ff	Arkanoid, Cobra, Renegade, Short Circuit, Terra Cresta
	// 0x40ff	Sidewize
	if (p == 0x28FF || p == 0x40FF)
 		return ula_in();

	//if (machine.tape_playing)
	//	*d &= ~((machine.ear^1)<<6);
		//*d &= ~(machine.ear<<6);
		//*d &= ~((machine.ear^1)<<4);

	//ULA port xxxx-xxxx-xxxx-xxx0
	if ((p&0x0001)==0)
	{
		d=191;
#ifdef TAPE_ON
		if (!machine.ear) 
			d |= 1<<6;
		else
			d &= ~(1<<6);
#else
		if (machine.tape_playing)
		{
			retrieve_tape_bits(tape_cycles);
			if (!machine.ear) 
				d |= 1<<6;
			else
				d &= ~(1<<6);
		}
		tape_cycles = 0;
#endif

		if (!(p&0x8000))
			d &= speckeys[7];
		if (!(p&0x4000))
			d &= speckeys[5];
		if (!(p&0x2000))
			d &= speckeys[3];
		if (!(p&0x1000))
			d &= speckeys[1];
		if (!(p&0x0800))
			d &= speckeys[0];
		if (!(p&0x0400))
			d &= speckeys[2];
		if (!(p&0x0200))
			d &= speckeys[4];
		if (!(p&0x0100))
			d &= speckeys[6];
	}

	return d;
}

void out(unsigned short p, unsigned char d)
{
#pragma message("out not correctly implemented")
#pragma message("out does not have ULA contention")

	//AY register 11xx xxxx xxxx xx0x
	if ((p&0xC002) == 0xC000)
	{
		switch (machine.machine_id)
		{
			case SPEC_128K:
			case SPEC_PLUS2:
			case SPEC_PLUS2A:
			case SPEC_PLUS3:
				AY_reg(d);
				return;
		}
	}
	
	//AY data 10xx xxxx xxxx xx0x
	if ((p&0xC002) == 0x8000)
	{
		switch (machine.machine_id)
		{
			case SPEC_128K:
			case SPEC_PLUS2:
			case SPEC_PLUS2A:
			case SPEC_PLUS3:
				AY_data(d);
				return;
		}
	}

	//128k memory page 01xx xxxx xxxx xx0x
	if ((p&0xC002) == 0x4000)
	{
		switch (machine.machine_id)
		{
			case SPEC_128K:
			case SPEC_PLUS2:
				if (machine.port_7FFD & 0x20)
					break;

				machine.membank[3] = &machine.rambanks[d&0x7];
				if (machine.has_mf128 && machine.mf128.ff_paged_in)
					machine.mf128.prevbank = &machine.rombanks[!!(d & 0x10)];
				else if (machine.has_mf3 && machine.mf3.ff_paged_in)
					machine.mf3.prevbank = &machine.rombanks[!!(d & 0x10)];
				else
					machine.membank[0] = &machine.rombanks[!!(d&0x10)];
				machine.port_7FFD = d;
				ula_page();
				return;

			case SPEC_PLUS2A:
			case SPEC_PLUS3:
				if (machine.port_7FFD & 0x20)
					break;

				{
				int rom;
				machine.membank[3] = &machine.rambanks[d&0x7];
				rom = !!(d&0x10);
				rom += 2*(!!(machine.port_1FFD&0x4));
				if (machine.has_mf128 && machine.mf128.ff_paged_in)
					machine.mf128.prevbank = &machine.rombanks[!!(d & 0x10)];
				else if (machine.has_mf3 && machine.mf3.ff_paged_in)
					machine.mf3.prevbank = &machine.rombanks[!!(d & 0x10)];
				else
					machine.membank[0] = &machine.rombanks[rom];
				machine.port_7FFD = d;
				ula_page();
				}
				return;
		}
	}

	//+3 memory page 0001 xxxx xxxx xx0x
	if ((p&0xF002) == 0x1000)
	{
		switch (machine.machine_id)
		{
			case SPEC_PLUS2A:
			case SPEC_PLUS3:
				if (machine.port_7FFD & 0x20)
					break;

				machine.port_1FFD = d;
				if (d&1)
				{
					//special mode
					switch ((d>>1)&0x3)
					{
						case 0:
							if (machine.has_mf128 && machine.mf128.ff_paged_in)
								machine.mf128.prevbank = &machine.rambanks[0];
							else if (machine.has_mf3 && machine.mf3.ff_paged_in)
								machine.mf3.prevbank = &machine.rambanks[0];
							else
								machine.membank[0] = &machine.rambanks[0];
							machine.membank[1] = &machine.rambanks[1];
							machine.membank[2] = &machine.rambanks[2];
							machine.membank[3] = &machine.rambanks[3];
							break;
						case 1:
							if (machine.has_mf128 && machine.mf128.ff_paged_in)
								machine.mf128.prevbank = &machine.rambanks[4];
							else if (machine.has_mf3 && machine.mf3.ff_paged_in)
								machine.mf3.prevbank = &machine.rambanks[4];
							else
								machine.membank[0] = &machine.rambanks[4];
							machine.membank[1] = &machine.rambanks[5];
							machine.membank[2] = &machine.rambanks[6];
							machine.membank[3] = &machine.rambanks[7];
							break;
						case 2:
							if (machine.has_mf128 && machine.mf128.ff_paged_in)
								machine.mf128.prevbank = &machine.rambanks[4];
							else if (machine.has_mf3 && machine.mf3.ff_paged_in)
								machine.mf3.prevbank = &machine.rambanks[4];
							else
								machine.membank[0] = &machine.rambanks[4];
							machine.membank[1] = &machine.rambanks[5];
							machine.membank[2] = &machine.rambanks[6];
							machine.membank[3] = &machine.rambanks[3];
							break;
						case 3:
							if (machine.has_mf128 && machine.mf128.ff_paged_in)
								machine.mf128.prevbank = &machine.rambanks[4];
							else if (machine.has_mf3 && machine.mf3.ff_paged_in)
								machine.mf3.prevbank = &machine.rambanks[4];
							else
								machine.membank[0] = &machine.rambanks[4];
							machine.membank[1] = &machine.rambanks[7];
							machine.membank[2] = &machine.rambanks[6];
							machine.membank[3] = &machine.rambanks[3];
							break;
					}
				}
				else
				{
					int rom;
					machine.membank[3] = &machine.rambanks[machine.port_7FFD&0x7];
					rom = !!(machine.port_7FFD&0x10);
					rom += 2*(!!(d&0x4));
					if (machine.has_mf128 && machine.mf128.ff_paged_in)
						machine.mf128.prevbank = &machine.rombanks[rom];
					else if (machine.has_mf3 && machine.mf3.ff_paged_in)
						machine.mf3.prevbank = &machine.rombanks[rom];
					else
						machine.membank[0] = &machine.rombanks[rom];
				}
				ula_page();
				return;
		}
	}

	//+3 uPD765A Disk Controller Status Register (Read)
	if ((p&0xF002) == 0x2000)
	{
		switch (machine.machine_id)
		{
			case SPEC_PLUS2A:
			case SPEC_PLUS3:
				return;
		}
	}

	//FDC Data Port (Read/Write)
	if ((p&0xF002) == 0x3000)
	{
		switch (machine.machine_id)
		{
			case SPEC_PLUS2A:
			case SPEC_PLUS3:
				return;
		}
	}

	//IF1 microdrive
	if (machine.has_if1)
	{
		//https://worldofspectrum.org/faq/reference/48kreference.htm#PortE7
		if (p == 231)//0xE7
			return;
		if (p == 239)//0xEF
			return;
		if (p == 247)//0xF7
			return;
	}

	// port xxxx-xxxx-x011-x1xx (usually ports 63,191 or 0x3F, 0xBF, bit A7 of address indicates page in/out
	if (machine.has_mf128 && (p & 0x0074) == 0x34)
	{
		machine.mf128.ff_nmi_pending = 0;
		return;
	}
	else
	// port xxxx-xxxx-x011-x1xx (usually ports 63,191 or 0x3F, 0xBF, bit A7 of address indicates page out/in
	if (machine.has_mf3 && (p & 0x0074) == 0x34)
	{
		machine.mf3.ff_nmi_pending = 0;
		return;
	}
	else
	// port xxxx-xxxx-x001-xx1x (usually 0x1F, 0x9F bit A7 of address indicates page in/out)
	if (machine.has_mf1 && (p & 0x0072) == 0x12)
	{
		machine.mf1.ff_nmi_pending = 0;
		return;
	}
	else if (p == 0x009F)
	{
		elog("Multiface Protection Detected! OUT to port %02X!\n", p);
		return;
	}

	// SpecDrum
	if (p == 0x00df)
	{
		specdrum_out(d);
		return;
	}

	//ULA port xxxx-xxxx-xxxx-xxx0
	if ((p&0x0001)==0)
	{
		if ((p&0xff) != 254)
			elog("%u %02Xh written to unknown port %u %04Xh from PC %04X\n", (unsigned int)d, (unsigned int)d, (unsigned int)p, (unsigned int)p, get_regs()->pc);

		machine.border = d&0x7;
		machine.ear = (unsigned char)!!(d&0x10);
		machine.mic = (unsigned char)!!(d&0x8);
		ula_out(d);
		return;
	}

	//Ultimate games like writing to 0xFD, possibly thinking it's needed to select the keyboard row
	//The others are 128K ports being written in 48K mode.
	if (p != 0xBFFD && p != 0xFFFD && p != 0x00FD)
		elog("%u %02Xh written to unknown port %u %04Xh from PC %04X\n", (unsigned int)d, (unsigned int)d, (unsigned int)p, (unsigned int)p, get_regs()->pc);
}

//---------------------------------------------------------------------------------------
// force a screen update with latest contents, no matter the screen mode
//---------------------------------------------------------------------------------------
void update_screen(void)
{
	dirty_whole_screen();
	convert_screen();
	repaint_screen();
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void emulate_init(void)
{
	init_parity();
//	emulate_reset();
	create_screen();
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void emulate_stop(void)
{
	kill_screen();
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
extern void switch_out_of_fast_mode(void);
extern void switch_to_fast_mode(void);
void machine_play_tape(int motor)
{
	machine.tape_playing = motor;
	if (motor)
	{
		switch_to_fast_mode();
		start_tape();
	}
	else
	{
		stop_tape();
		switch_out_of_fast_mode();
	}
	tape_cycles = 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
unsigned char retrieve_tape_bits(int cycles)
{
	unsigned char bit;
	bit = get_next_bit(cycles);
	machine.ear = bit;
	machine.mic = bit;
	return (unsigned char)(0xff&~(bit<<6));
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void machine_page_rom()
{
	int n_roms;
	int curr_rom;

	switch (machine.machine_id)
	{
		case SPEC_128K:
		case SPEC_PLUS2:
			n_roms = 2;
			break;
		case SPEC_PLUS2A:
		case SPEC_PLUS3:
			n_roms = 4;
			break;
		default:
			n_roms = 1;
	}

	curr_rom = machine.membank[0]->bank_no;
	curr_rom++;
	curr_rom %= n_roms;
	machine.membank[0] = &machine.rombanks[curr_rom];

	//todo: deal with banks and disassembly
	//disassemble_plain();
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void toggle_fastscreen(void)
{
	fast_screen++;
	if (fast_screen > FS_MAX)
		fast_screen = FS_SCANLINE;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void set_screen_mode(FASTSCREEN mode)
{
	fast_screen = mode;
}
//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
FASTSCREEN get_screen_mode(void)
{
	return fast_screen;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
unsigned int get_first_scanline(void)
{
	return machine.first_scanline;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void load_ram(unsigned char *code)
{
	int x;

	for (x = 0x4000; x < 0x10000; x++)
		poke((unsigned short)x, code[x]);
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void set_ram(unsigned short dest, unsigned char *src, unsigned int length)
{
	while (length--)
		poke_fast(dest++, *src++);
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
unsigned short emulate_start_game(START_REGS *start_regs)
{
//	init_parity();
	REG_PACK *sregs;

	sregs = get_regs();

	sregs->i = start_regs->I;
	sregs->exregs.hl = start_regs->HL_alt;
	sregs->exregs.de = start_regs->DE_alt;
	sregs->exregs.bc = start_regs->BC_alt;
	sregs->exregs.af = start_regs->AF_alt;

	sregs->w.hl = start_regs->HL;
	sregs->w.de = start_regs->DE;
	sregs->w.bc = start_regs->BC;
	sregs->w.af = start_regs->AF;

	sregs->ix[R_IX].i = start_regs->IX;
	sregs->ix[R_IY].i = start_regs->IY;

	setr(start_regs->R);

	sregs->sp = start_regs->SP;

	sregs->im = start_regs->interrupt_mode;

	sregs->iff1 = start_regs->interrupt&1;
	sregs->iff2 = (start_regs->interrupt>>1)&1;

	machine.border = start_regs->border_colour;
	unsigned char d = in(254);
	d = (d & ~7) | machine.border;
	out(256, d);
	set_border_colour(machine.border);

//	retn();
	sregs->pc = start_regs->PC;

	sregs->r7 = start_regs->R7;

	sregs->memptr = 0;

	machine.cycle = 0;

	machine.port_7FFD = start_regs->port_7ffd;
	out(0x7ffd, machine.port_7FFD);

return sregs->pc;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static FILE *logfile = NULL;
static void logout(unsigned short addr, char *l)
{
	if (logfile == NULL)
		fopen_s(&logfile,"inslog.txt", "w");
	if (logfile == NULL)
		return;
	fprintf(logfile, "%u %s\n", (unsigned int)addr, l);
}

static void closelogout(void)
{
	if (logfile != NULL)
	{
		fclose(logfile);
		logfile = NULL;
	}
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------

/*
unsigned short get_cycle_counts()
{
	REG_PACK *sregs;

	sregs = get_regs();
	elog("static const int mnemtime[256]=\n{\n");
	unsigned short pc = sregs->pc;
	for (int c = 0; c < 256; c++)
	{
		sregs->pc = pc;
		sregs->b.f = 0;
		sregs->w.bc = 0xfe;
		poke_fast(pc, c);

		cycles_used = 0;
		unsigned char i = get_imm8();

		execute(i);

		elog("%d ,", cycles_used);
		if (((c+1) % 8) == 0) elog("\n");
	}
	elog("};\n\n");

	elog("// CB prefix\n");
	elog("static const int mnemtimeCB[256]=\n{\n");
	for (int c = 0; c < 256; c++)
	{
		sregs->pc = pc;
		sregs->b.f = 0;
		sregs->w.bc = 0xfe;
		poke_fast(pc, 0xcb);
		poke_fast(pc+1, c);

		cycles_used = 0;
		unsigned char i = get_imm8();

		execute(i);

		elog("%d ,", cycles_used);
		if (((c + 1) % 8) == 0) elog("\n");
	}
	elog("};\n\n");

	elog("// DD/FD prefix (IX/IY instead of HL)\n");
	elog("static const int mnemtimeXX[256]=\n{\n");
	for (int c = 0; c < 256; c++)
	{
		sregs->pc = pc;
		sregs->b.f = 0;
		sregs->w.bc = 0xfe;
		poke_fast(pc, 0xdd);
		poke_fast(pc + 1, c);

		cycles_used = 0;
		unsigned char i = get_imm8();

		execute(i);

		elog("%d ,", cycles_used);
		if (((c + 1) % 8) == 0) elog("\n");
	}
	elog("};\n\n");

	elog("// ED prefix\n");
	elog("static const int mnemtimeED[256]=\n{\n");
	for (int c = 0; c < 256; c++)
	{
		sregs->pc = pc;
		sregs->b.f = 0;
		sregs->w.bc = 0xfe;
		poke_fast(pc, 0xed);
		poke_fast(pc + 1, c);

		cycles_used = 0;
		unsigned char i = get_imm8();

		execute(i);

		elog("%d ,", cycles_used);
		if (((c + 1) % 8) == 0) elog("\n");
	}
	elog("};\n\n");

	elog("// DD CB / FD CB prefix\n");
	elog("static const int mnemtimeXXCB[256]=\n{\n");
	for (int c = 0; c < 256; c++)
	{
		sregs->pc = pc;
		sregs->b.f = 0;
		sregs->w.bc = 0xfe;
		poke_fast(pc, 0xdd);
		poke_fast(pc+1, 0xcb);
		poke_fast(pc + 2, 1);//d
		poke_fast(pc + 3, c);

		cycles_used = 0;
		unsigned char i = get_imm8();

		execute(i);

		elog("%d ,", cycles_used);
		if (((c + 1) % 8) == 0) elog("\n");
	}
	elog("};\n\n");

	exit(0);

	return sregs->pc;
}
*/

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------

static char instruction_log = 0;

unsigned short emulate_next(void)
{
	unsigned char i;
//	int scanline;
	REG_PACK *sregs;

	sregs = get_regs();

	incr();

	cycles_used = 0;

	pc_trace(sregs->pc);

	//instruction_log = (sregs->pc >= 0xc000 && sregs->pc < 0xc0de);

	if (instruction_log)
	{
		DASM_RES dasm;
		DAsmEx(sregs->pc, &dasm);
		elog("%04X %s\n", sregs->pc, dasm.instr);
	}

	if (machine.has_shadow_roms)
	{
		if (machine.has_if1 && (sregs->pc == 8 || sregs->pc == 0x1708))
			page_interface1();
		if (machine.has_mf1 && machine.mf1.ff_nmi_pending && sregs->pc == 0x66)
			page_multiface1(1);
		if (machine.has_mf128 && machine.mf128.ff_nmi_pending && sregs->pc == 0x66)
			page_multiface128(1);
		if (machine.has_mf3 && machine.mf3.ff_nmi_pending && sregs->pc == 0x66)
			page_multiface3(1);
	}

	i = get_imm8();

	do_stats(CS_NORMAL, i);

	execute(i);

	assert(cycles_used);

	//sample the ear bit every cpuhz/soundhz
	machine.ear_timer -= cycles_used;
	if (machine.ear_timer < 0)
	{
		add_click(machine.ear);
		machine.ear_timer += machine.ear_cycle;
	}

#ifdef TAPE_ON
	retrieve_tape_bits(cycles_used);
#else
	//update the tape counter
	if (machine.tape_playing)
	{
		tape_cycles += cycles_used;
	}
	else
	{
		tape_cycles = 0;
	}
#endif

	//3.5MHz = 70000 cycles per 1/50th (ish)
	machine.interrupt_timer -= cycles_used;

	if (machine.nmi)
	{
		machine.nmi = 0;
		if (peek_fast(sregs->pc) == 118) //halt 0x76
			sregs->pc++;
		nmi();
	}
	else if (machine.interrupt_timer < 0 && !machine.last_was_ei && sregs->iff1)
	{
		if (peek_fast(sregs->pc) == 118) //halt 0x76
		{
			sregs->pc++;

			//want to synchronise the screen update with the maskable interrupt
			machine.line_timer = machine.line_length;
			machine.frame_cycle = 0;
			ula_reset_tick();
			cycles_used += 28;//todo: without this, we are not synching with the frame correctly, why is that?
			//todo: ula should be driving this, not the other way around
		}
		interrupt();
		machine.interrupt_timer = machine.frame_length;
	}
	machine.last_was_ei = 0;

	//update the screen
	machine.frame_cycle += cycles_used;
	machine.line_timer -= cycles_used;

	ula_tick(cycles_used, fast_screen == FS_ULA);
	if (machine.line_timer < 0)
		screen_update();

	//update the AY
	if (machine.has_ay)
	{
		machine.ay_timer -= cycles_used;
		if (machine.ay_timer < 0)
		{
			machine.ay_timer += machine.ay_countdown;
			//AY_update(machine.ay_timer);
			AY_update((int)machine.cycle);
		}
	}

	machine.cycle += cycles_used;

	if (instruction_log)
		elog(" %d\n", cycles_used);

return sregs->pc;
}

unsigned int get_cycle(void)
{
	return (unsigned int)machine.cycle;
}

//---------------------------------------------------------------------------------------
// gets called every scanline
//---------------------------------------------------------------------------------------
static void screen_update(void)
{
	//machine.line_timer just ticked so we hit a new line

	//set the timer to go off at the end of the next line
	machine.line_timer += machine.line_length;

	if (fast_screen == FS_OFF || fast_screen == FS_ULA)
		return;
	
	machine.frame_cycle %= machine.frame_length;

	//this is the scanline we just started, want to paint the previous one
	unsigned int scanline;
	scanline = machine.frame_cycle / machine.line_length;

	//grab the border colour at the start of the scanline
	set_border(scanline, machine.border);

	//paint the previous scanline
	if (scanline == 0)
		scanline = machine.scanlines - 1;
	else
		scanline--;

	if (fast_screen == FS_IMMEDIATE)
	{
		//poke() is calling in to write direct to the screen
	}
	else if (fast_screen == FS_SCREEN)
	{
		//convert each frame at the end of the frame
		if (scanline == machine.scanlines - 1)
			convert_screen();
	}
	else if (fast_screen == FS_SCANLINE)
	{
		//convert each scanline at the end of the scanline
		convert_scanline(scanline, machine.border);
	}
}
