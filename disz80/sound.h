//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
//#define SOUND_RATE 11025
//#define SOUND_RATE 22050
//#define SOUND_RATE 44100
#define SOUND_RATE 48000

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void init_sound(void);
void kill_sound(void);
void add_click(unsigned char click);
void send_byte(int a);
void send_ay_words(int a, int b, int c);
void specdrum_out(unsigned char b);
void toggle_sound(void);
int is_sound_on(void);
