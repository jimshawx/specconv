//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
typedef struct __TAPE_ENTRY__
{
	int type;
	unsigned int flags;
	int block_number;
	void *data_ptr;
	unsigned int data_len;
	struct __TAPE_ENTRY__ *prev, *next;
} TAPE_ENTRY;

typedef enum
{
	TS_STOPPED,
	TS_PILOT,
	TS_SYNC1,
	TS_SYNC2,
	TS_DATA,
	TS_TONE,
	TS_MULTITONE,
	TS_END,
} PULSE_STATE;

typedef struct
{
	PULSE_STATE state;// = 0;// ' state 0 - playing pulse
	unsigned char bit;

	int pilot;// = 2168
	int sync1;// = 667
	int sync2;// = 735
	int zero;// = 855
	int one;// = 1710
	int pilot_tone;//        if tzxarray(tzxpointer + 5) = &hff then tzxpulsetonelen = 3220 else tzxpulsetonelen = 8064
	int used_bits;// = 8
	int pause_ms;// = tzxarray(tzxpointer + 1) + (tzxarray(tzxpointer + 2) * 256&)

	int block_len;// = tzxblocklength(blocknum) + tzxoffsets(blocknum)
//	int byte;// = 0
	int no_pilots;
	unsigned char *byte_ptr;
	unsigned char *byte_start;
	int cur_block;// = blocknum
	int data_pos;// = tzxpointer + 5
	int pulses_done;// = 2
	int pulse_count;
	unsigned short *pulse_ptr;
	int bit_counter;
	int bit_limit;
	int countdown;

} TAPE_STATE;

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
enum
{
	TAP_STANDARD=0x10,
	TAP_TURBO=0x11,
	TAP_TONE=0x12,
	TAP_PULSES=0x13,
	TAP_DATA=0x14,
	TAP_DIRECT=0x15,
	TAP_C64=0x16,
	TAP_C64_TURBO=0x17,
	TAP_PAUSE=0x20,
	TAP_GROUP=0x21,
	TAP_GROUPEND=0x22,
	TAP_JUMP=0x23,
	TAP_LOOP=0x24,
	TAP_LOOPEND=0x25,
	TAP_CALL=0x26,
	TAP_RET=0x27,
	TAP_SELECT=0x28,
	TAP_STOPTAPE48K=0x2A,
	TAP_DESCRIPTION=0x30,
	TAP_MESSAGE=0x31,
	TAP_ARCHIVEINFO=0x32,
	TAP_HARDWARE=0x33,
	TAP_EMUINFO=0x34,
	TAP_CUSTOM=0x35,
	TAP_SNAPSHOT=0x36,
};

int load_tape(void);
START_REGS *load(void);
void save(void);
void init_tape(void);
void stop_tape(void);
void start_tape(void);
void ff_tape(void);
void rew_tape(void);
void reset_tape(void);
void free_tape(void);
char *show_tape_info(void);
void set_tape_position(int index);
char *generic_load(char *filter, char *caption, char *defext, char *suggested);
char *generic_save(char *filter, char *caption, char *defext, char *suggested);
void save_sna(char *fname);
void *prepare_snapshot(void);

void load_tape_file(char *file);
START_REGS *load_snapshot_file(char *file);

unsigned char get_next_bit(int cycles);

//#define TAPE_ON

