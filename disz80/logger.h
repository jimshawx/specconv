//-----------------------------------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
#pragma once

typedef enum
{
	LOG_INFO,
	LOG_WARN,
	LOG_ERROR
} LOG_LEVEL;

void elog(const char *fmt, ...);
void eloglevel(LOG_LEVEL level, const char *fmt, ...);
void elograw(const char *txt);
