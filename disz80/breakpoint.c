//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

#include "emu.h"
#include "breakpoint.h"

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static BREAKPOINT breakpoints[100];
static int n_breakpoints;
//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
int get_breakpoints(BREAKPOINT **bp)
{
	*bp = breakpoints;
	return n_breakpoints;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void remove_breakpoint(int index)
{
	if (n_breakpoints == 0)
		return;
	if (index < 0)
		return;
	memmove(breakpoints + index, breakpoints + index + 1, sizeof(BREAKPOINT) * n_breakpoints - index - 1);
	n_breakpoints--;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void remove_all_temp_breakpoints(void)
{
	char dirty;
	do
	{
		dirty = 0;
		for (int i = 0; i < n_breakpoints; i++)
			if (breakpoints[i].flags & BP_TEMP)
			{
				remove_breakpoint(i);
				dirty = 1;
				break;
			}
	} while (dirty);
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
int check_breakpoints(void)
{
	if (!n_breakpoints) return 0;

	int z;
	BREAKPOINT *bp = breakpoints;
	REG_PACK *regs = get_regs();

	for (z = 0; z < n_breakpoints; z++, bp++)
	{
		if (bp->pc == regs->pc && (bp->flags & BP_ACTIVE))
		{
			if (bp->flags & BP_TEMP)
				remove_breakpoint(z);
			return 1;
		}
	}
	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
int toggle_breakpoint(unsigned short pc)
{
	int x;
	BREAKPOINT *bp = breakpoints;
	for (x = 0; x < n_breakpoints; x++, bp++)
	{
		if (bp->pc == pc)
		{
			bp->flags ^= BP_ACTIVE;
			return 1;
		}
	}
	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void set_breakpoint(unsigned short pc)
{
	breakpoints[n_breakpoints].pc = pc;
	breakpoints[n_breakpoints].flags = BP_ACTIVE;
	n_breakpoints++;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void set_temp_breakpoint(unsigned short pc)
{
	breakpoints[n_breakpoints].pc = pc;
	breakpoints[n_breakpoints].flags = BP_ACTIVE | BP_TEMP;
	n_breakpoints++;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
int get_breakpoint_status(unsigned short pc)
{
	for (int i = 0; i < n_breakpoints; i++)
	{
		if (breakpoints[i].pc == pc)
			return breakpoints[i].flags;
	}
	return 0;
}
