//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

enum
{
	AY_AFINE,
	AY_ACOARSE,
	AY_BFINE,
	AY_BCOARSE,
	AY_CFINE,
	AY_CCOARSE,
	AY_NOISEPER,
	AY_ENABLE,
	AY_AVOL,
	AY_BVOL,
	AY_CVOL,
	AY_EFINE,
	AY_ECOARSE,
	AY_ESHAPE,
	AY_PORTA,
	AY_PORTB,
};

enum
{
	CHANNEL_A,
	CHANNEL_B,
	CHANNEL_C,
	CHANNEL_N,
	CHANNEL_E,
};

#define AY_CLOCK 1773400.0f

typedef struct
{
	unsigned char reg[16];
	int period[5];
	int count[5];
	unsigned int vol[5];
	unsigned char envelope[3];
	unsigned char output[5];

	unsigned int update_step;
//	unsigned int last_pos;

	signed char countEnv;
	unsigned char hold,alternate,attack,holding;
	int rng;
	unsigned int volTable[32];
//	unsigned char *buf[3];

} AY_STATE;

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------

void AY_reg(unsigned char reg);
void AY_data(unsigned char data);
unsigned char AY_read_data(void);
void AY_update(int cycles);
void AY_init(void);
void AY_shutdown(void);
