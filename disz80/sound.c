#if 0
//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

#include <windows.h>
#include <dsound.h>
#include <process.h>
#include <string.h>
#include <assert.h>

#include "sound.h"

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
//#define BUFFER_SIZE 1024
#define BUFFER_SIZE 4096
//#define BUFFER_SIZE 8192
//#define BUFFER_SIZE 22050

static unsigned int __stdcall dsnd_update_thread(void *lpParameter);
static void convert_clicks(void *address, int length);
static void reset_clicks(void);

static void init_clicks();
static void kill_clicks();
static void lock_clickbuffer();
static void unlock_clickbuffer();

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static LPDIRECTSOUND8 dsnd8;
static LPDIRECTSOUNDBUFFER dsnd_primary;

static LPDIRECTSOUNDBUFFER8 dsnd_output8;
static LPDIRECTSOUNDNOTIFY8 dsnd_notify8;

static HANDLE events[10];
static uintptr_t dsnd_thread;

static int sound_on = 1;

extern HWND dis_window;

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void init_sound(void)
{
	HRESULT err;
	DSBUFFERDESC bdesc;
	WAVEFORMATEX wavefmt;
	LPDIRECTSOUNDBUFFER dsnd_output;
	DSBPOSITIONNOTIFY notify[2];
	unsigned int thread_id;
	VOID *buffer0;
	DWORD length0;

	HWND hwnd = dis_window;

	init_clicks();

	err = DirectSoundCreate8(NULL, &dsnd8, NULL);
	assert(err == S_OK);

	err = IDirectSound8_SetCooperativeLevel(dsnd8, hwnd, DSSCL_EXCLUSIVE);
	assert(err == S_OK);

	bdesc.dwSize = sizeof(DSBUFFERDESC);
	bdesc.dwFlags = DSBCAPS_PRIMARYBUFFER;
	bdesc.dwBufferBytes = 0;
	bdesc.dwReserved = 0;
	bdesc.lpwfxFormat = NULL;
	bdesc.guid3DAlgorithm = GUID_NULL;

	wavefmt.wFormatTag = WAVE_FORMAT_PCM;
	wavefmt.nChannels = 2;
	wavefmt.nSamplesPerSec = SOUND_RATE;
	wavefmt.wBitsPerSample = 16;
	wavefmt.nBlockAlign = (wavefmt.nChannels * wavefmt.wBitsPerSample) / 8;
	wavefmt.nAvgBytesPerSec = wavefmt.nBlockAlign * wavefmt.nSamplesPerSec;
	wavefmt.cbSize = sizeof(WAVEFORMATEX);
	err = IDirectSound8_CreateSoundBuffer(dsnd8, &bdesc, &dsnd_primary, NULL);
	assert(err == S_OK);

	err = IDirectSoundBuffer_SetFormat(dsnd_primary, &wavefmt);
	assert(err == S_OK);

	err = IDirectSoundBuffer_Play(dsnd_primary, 0, 0, DSBPLAY_LOOPING);
	assert(err == S_OK);

	wavefmt.nChannels = 1;
	wavefmt.wBitsPerSample = 8;
	wavefmt.nBlockAlign = (wavefmt.nChannels * wavefmt.wBitsPerSample) / 8;
	wavefmt.nAvgBytesPerSec = wavefmt.nBlockAlign * wavefmt.nSamplesPerSec;
	bdesc.dwFlags = DSBCAPS_GETCURRENTPOSITION2|DSBCAPS_CTRLPOSITIONNOTIFY|DSBCAPS_LOCSOFTWARE;
	bdesc.dwBufferBytes = BUFFER_SIZE;
	bdesc.lpwfxFormat = &wavefmt;
	err = IDirectSound8_CreateSoundBuffer(dsnd8, &bdesc, &dsnd_output, NULL);
	assert(err == S_OK);

	err = IDirectSoundBuffer_QueryInterface(dsnd_output, &IID_IDirectSoundBuffer8, &dsnd_output8);
	assert(err == S_OK);

	err = IDirectSoundBuffer8_QueryInterface(dsnd_output8, &IID_IDirectSoundNotify8, &dsnd_notify8);
	assert(err == S_OK);

	err = IDirectSoundBuffer_Release(dsnd_output);
	//err=1 (ref count);

	events[0] = CreateEvent(NULL, 0, 0, "shutdown");
	assert(events[0]);
	events[1] = CreateEvent(NULL, 0, 0, "bufferstart");
	assert(events[1]);
	events[2] = CreateEvent(NULL, 0, 0, "buffermiddle");
	assert(events[2]);
	events[3] = CreateEvent(NULL, 0, 0, "emulate");
	assert(events[3]);

	notify[0].dwOffset = 0;
	notify[0].hEventNotify=events[1];
	notify[1].dwOffset = BUFFER_SIZE/2;
	notify[1].hEventNotify=events[2];
	err = IDirectSoundNotify_SetNotificationPositions(dsnd_notify8, 2, notify);
	assert(err == S_OK);

	dsnd_thread = _beginthreadex(NULL, 0, dsnd_update_thread, NULL, 0, &thread_id);
	assert(dsnd_thread);

	err = IDirectSoundBuffer8_Lock(dsnd_output8, 0,0, &buffer0, &length0, NULL, NULL, DSBLOCK_ENTIREBUFFER);
	assert(err == S_OK);

	memset(buffer0, 127, length0);
	err = IDirectSoundBuffer8_Unlock(dsnd_output8, buffer0, length0, NULL, 0);
	assert(err == S_OK);

	err = IDirectSoundBuffer8_Play(dsnd_output8, 0, 0, DSBPLAY_LOOPING);
	assert(err == S_OK);
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void kill_sound(void)
{
	HRESULT err;

	err = IDirectSoundBuffer8_Stop(dsnd_output8);
	assert(err == S_OK);

	err = IDirectSoundBuffer_Stop(dsnd_primary);
	assert(err == S_OK);

	//tell update thread to die
	SetEvent(events[0]);
	WaitForSingleObject((HANDLE)dsnd_thread, INFINITE);

	SetEvent(events[3]);

	err = IDirectSoundNotify_Release(dsnd_notify8);
	assert(err == S_OK);

	err = IDirectSoundBuffer8_Release(dsnd_output8);
	//err=0 (ref count);

	err = IDirectSoundBuffer_Release(dsnd_primary);
	assert(err == S_OK);

	err = IDirectSound8_Release(dsnd8);
	assert(err == S_OK);

	CloseHandle(events[3]);
	CloseHandle(events[2]);
	CloseHandle(events[1]);
	CloseHandle(events[0]);

	kill_clicks();
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void create_buffer()
{
}
//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static unsigned int __stdcall dsnd_update_thread(void *lpParameter)
{
	DWORD flagged;
	VOID *buffer0, *buffer1;
	DWORD length0, length1;
	HRESULT err;
	lpParameter;

	for (;;)
	{
		//wait for an event...
		flagged = WaitForMultipleObjects(3, events, FALSE, INFINITE);
		if (flagged == WAIT_OBJECT_0)
			break;

		buffer0 = buffer1 = NULL;
		length0 = length1 = 0;

		if (flagged == WAIT_OBJECT_0+1)
			err = IDirectSoundBuffer8_Lock(dsnd_output8, BUFFER_SIZE/2, BUFFER_SIZE/2, &buffer0, &length0, &buffer1, &length1, 0);
		else
			err = IDirectSoundBuffer8_Lock(dsnd_output8, 0, BUFFER_SIZE/2, &buffer0, &length0, &buffer1, &length1, 0);
		assert(err == S_OK);

		assert(!buffer1);
		assert(!length1);
		assert(buffer0);
		assert(length0 == BUFFER_SIZE/2);
		convert_clicks(buffer0, length0);
		err = IDirectSoundBuffer8_Unlock(dsnd_output8, buffer0, length0, buffer1, length1);
		assert(err == S_OK);

		SetEvent(events[3]);
	}
	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static unsigned char clicks[BUFFER_SIZE/2];
static unsigned char *click_buffer;

static unsigned char clicks2[BUFFER_SIZE/2];
static unsigned char *click_buffer2;

static HANDLE clickbuffersem;

static void init_clicks()
{
	//clickbuffersem = CreateSemaphore(NULL, 1, 1, NULL);
	//assert(clickbuffersem);
	reset_clicks();
}

static void kill_clicks()
{
	//CloseHandle(clickbuffersem);
}

static volatile LONG spin = 0;
static void lock_clickbuffer()
{
	//DWORD err = WaitForSingleObject(clickbuffersem, INFINITE);
	//assert(err == WAIT_OBJECT_0);
	while(InterlockedCompareExchange(&spin, 1, 0)!=0)
		YieldProcessor();
}

static void unlock_clickbuffer()
{
	//ReleaseSemaphore(clickbuffersem, 1, 0);
	spin = 0;
}

static void reset_clicks()
{
	lock_clickbuffer();

	click_buffer = clicks;
	click_buffer2 = clicks2;
	memset(click_buffer,  128, BUFFER_SIZE/2);
	memset(click_buffer2, 128, BUFFER_SIZE/2);

	assert(click_buffer == clicks);
	assert(click_buffer2 == clicks2);

	unlock_clickbuffer();
}

void add_click(unsigned char click)
{
	if (!sound_on)
		return;

	if (click_buffer >= clicks + BUFFER_SIZE / 2)
	{
		HRESULT err = WaitForSingleObject(events[3], INFINITE);
		assert(err == S_OK);

		lock_clickbuffer();
		click_buffer = clicks;
	}
	else
	{
		lock_clickbuffer();
	}

	assert(click_buffer < clicks + BUFFER_SIZE / 2);
	assert(click_buffer >= clicks);

	if (click)
		*click_buffer++ = 159;//0xff;
	else
		*click_buffer++ = 128;//127;

	unlock_clickbuffer();
}

static void convert_clicks(void *address, int length)
{
	char *dst = (char *)address;
	int x;
	int smp;

	for (x = 0; x < length; x++)
	{
		smp = ((int)clicks[x] + clicks2[x])>>1;
		//smp = clicks[x];
		*dst++ = (char)smp;
	}
	reset_clicks();
}

void send_ay_words(int a, int b, int c)
{
	send_byte(((a + b + c) / 3)>>8);
}

void send_byte(int a)
{
	if (!sound_on)
		return;

	lock_clickbuffer();

	if (click_buffer2 >= clicks2 + BUFFER_SIZE / 2)
	{
		unlock_clickbuffer();
		return;
	}
	*click_buffer2++ = (unsigned char)a;

	unlock_clickbuffer();
}

void specdrum_out(unsigned char v) { v; }

void toggle_sound(void)
{
	sound_on ^= 1;
}

int is_sound_on(void)
{
	return sound_on;
}
//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
#include <stdio.h>
#include <assert.h>
 
void write_little_endian(unsigned int word, int num_bytes, FILE *wav_file)
{
	unsigned buf;
	while(num_bytes>0)
	{
		buf = word & 0xff;
		fwrite(&buf, 1,1, wav_file);
		num_bytes--;
		word >>= 8;
	}
}

void write_wav(char * filename, unsigned long num_samples, short int * data, int s_rate)
{
	FILE* wav_file;
	unsigned int sample_rate;
	unsigned int num_channels;
	unsigned int bytes_per_sample;
	unsigned int byte_rate;
	unsigned long i;	/* counter for samples */
 
	num_channels = 1;   /* monoaural */
	bytes_per_sample = 2;
 
	if (s_rate<=0) sample_rate = SOUND_RATE;
	else sample_rate = (unsigned int) s_rate;
 
	byte_rate = sample_rate*num_channels*bytes_per_sample;
 
	fopen_s(&wav_file, filename, "w");
	assert(wav_file);   /* make sure it opened */
 
	/* write RIFF header */
	fwrite("RIFF", 1, 4, wav_file);
	write_little_endian(36 + bytes_per_sample* num_samples*num_channels, 4, wav_file);
	fwrite("WAVE", 1, 4, wav_file);
 
	/* write fmt  subchunk */
	fwrite("fmt ", 1, 4, wav_file);
	write_little_endian(16, 4, wav_file);   /* SubChunk1Size is 16 */
	write_little_endian(1, 2, wav_file);	/* PCM is format 1 */
	write_little_endian(num_channels, 2, wav_file);
	write_little_endian(sample_rate, 4, wav_file);
	write_little_endian(byte_rate, 4, wav_file);
	write_little_endian(num_channels*bytes_per_sample, 2, wav_file);  /* block align */
	write_little_endian(8*bytes_per_sample, 2, wav_file);  /* bits/sample */
 
	/* write data subchunk */
	fwrite("data", 1, 4, wav_file);
	write_little_endian(bytes_per_sample* num_samples*num_channels, 4, wav_file);
	for (i=0; i< num_samples; i++)
	{
		write_little_endian((unsigned int)(data[i]),bytes_per_sample, wav_file);
	}
 
	fclose(wav_file);
}
#endif
;
