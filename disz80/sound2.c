#if 1
//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

#include <windows.h>
#include <xaudio2.h>

#include <assert.h>

#include "sound.h"

#include <crtdbg.h>
#include "dbgmem.h"
#include "logger.h"

//in samples
#define BUFFER_SIZE 8192

static IXAudio2 *xaudio;
static IXAudio2MasteringVoice *xaudioMasteringVoice;
static IXAudio2SourceVoice *xaudioSpeakerVoice;
static IXAudio2SourceVoice *xaudioAYVoice;
static IXAudio2SourceVoice *xaudioSpecDrumVoice;
static XAUDIO2_BUFFER xaudioSpeakerBuffer = { 0 };
static XAUDIO2_BUFFER xaudioAYBuffer = { 0 };
static XAUDIO2_BUFFER xaudioSpecDrumBuffer = { 0 };
static char sound_on = 1;

static unsigned char clicks[BUFFER_SIZE*2];
static unsigned char *click_buffer = clicks;

static signed short clicks2[BUFFER_SIZE*2];
static signed short *click_buffer2 = clicks2;

static unsigned char specdrum[BUFFER_SIZE*2];
static unsigned char *specdrum_buffer = specdrum;

void __stdcall x_OnVoiceProcessingPassStart(IXAudio2VoiceCallback *pVoiceCallback, UINT32 BytesRequired) { pVoiceCallback; BytesRequired; /*elog("OnVoiceProcessingPassStart\n");*/ }
void __stdcall x_OnVoiceProcessingPassEnd(IXAudio2VoiceCallback *pVoiceCallback) { pVoiceCallback; /*elog("OnVoiceProcessingPassEnd\n");*/ }
void __stdcall x_OnStreamEnd(IXAudio2VoiceCallback *pVoiceCallback) { pVoiceCallback; elog("OnStreamEnd\n"); }
void __stdcall x_OnBufferStart(IXAudio2VoiceCallback *pVoiceCallback, void *pBufferContext) { pVoiceCallback; pBufferContext; /*elog("OnBufferStart\n");*/ }
void __stdcall x_OnBufferEnd(IXAudio2VoiceCallback *pVoiceCallback, void *pBufferContext) { pVoiceCallback; pBufferContext; /*elog("OnBufferEnd\n");*/ }
void __stdcall x_OnLoopEnd(IXAudio2VoiceCallback *pVoiceCallback, void *pBufferContext) { pVoiceCallback; pBufferContext; elog("OnLoopEnd\n"); }
void __stdcall x_OnVoiceError(IXAudio2VoiceCallback *pVoiceCallback, void *pBufferContext, HRESULT Error) { pVoiceCallback; pBufferContext; Error; elog("OnVoiceError\n"); }

static IXAudio2VoiceCallbackVtbl vtbl =
{
	x_OnVoiceProcessingPassStart,
	x_OnVoiceProcessingPassEnd,
	x_OnStreamEnd,
	x_OnBufferStart,
	x_OnBufferEnd,
	x_OnLoopEnd,
	x_OnVoiceError,
};

static IXAudio2VoiceCallback callbacks =
{
	&vtbl
};

void init_sound(void)
{
	HRESULT err;

	err = XAudio2Create(&xaudio, 0, XAUDIO2_DEFAULT_PROCESSOR);
	assert(err == S_OK);

	err = IXAudio2_CreateMasteringVoice(xaudio, &xaudioMasteringVoice, XAUDIO2_DEFAULT_CHANNELS, XAUDIO2_DEFAULT_SAMPLERATE, 0, NULL, NULL, AudioCategory_GameEffects);
	assert(err == S_OK);

	XAUDIO2_DEBUG_CONFIGURATION pdb;
	pdb.TraceMask = XAUDIO2_LOG_ERRORS | XAUDIO2_LOG_WARNINGS | XAUDIO2_LOG_DETAIL | XAUDIO2_LOG_API_CALLS | XAUDIO2_LOG_FUNC_CALLS;
	pdb.BreakMask = 0;
	pdb.LogThreadID = TRUE;
	pdb.LogFileline = TRUE;
	pdb.LogFunctionName = TRUE;
	pdb.LogTiming = TRUE;
	IXAudio2_SetDebugConfiguration(xaudio, &pdb, NULL);

	XAUDIO2_BUFFER *xb;
	WAVEFORMATEX wex = { 0 };

	wex.wFormatTag = WAVE_FORMAT_PCM;
	wex.nChannels = 2;
	wex.nSamplesPerSec = SOUND_RATE;

	//8bit stereo
	wex.wBitsPerSample = 8;
	wex.nBlockAlign = (wex.nChannels * wex.wBitsPerSample) / 8;
	wex.nAvgBytesPerSec = wex.nSamplesPerSec * wex.nBlockAlign;

	xb = &xaudioSpeakerBuffer;
	xb->AudioBytes = BUFFER_SIZE*2;
	xb->pAudioData = (BYTE *)clicks;
	xb->PlayLength = BUFFER_SIZE;

	err = IXAudio2_CreateSourceVoice(xaudio, &xaudioSpeakerVoice, &wex, 0, XAUDIO2_DEFAULT_FREQ_RATIO, &callbacks, NULL, NULL);
	assert(err == S_OK);

	//16bit stereo
	wex.wBitsPerSample = 16;
	wex.nBlockAlign = (wex.nChannels * wex.wBitsPerSample) / 8;
	wex.nAvgBytesPerSec = wex.nSamplesPerSec * wex.nBlockAlign;

	xb = &xaudioAYBuffer;
	xb->AudioBytes = BUFFER_SIZE*sizeof(signed short)*2;
	xb->pAudioData = (BYTE *)clicks2;
	xb->PlayLength = BUFFER_SIZE;

	err = IXAudio2_CreateSourceVoice(xaudio, &xaudioAYVoice, &wex, 0, XAUDIO2_DEFAULT_FREQ_RATIO, NULL, NULL, NULL);
	assert(err == S_OK);

	//8bit stereo
	wex.wBitsPerSample = 8;
	wex.nSamplesPerSec = 20200;//173T States ~ 20231Hz
	wex.nBlockAlign = (wex.nChannels * wex.wBitsPerSample) / 8;
	wex.nAvgBytesPerSec = wex.nSamplesPerSec * wex.nBlockAlign;

	xb = &xaudioSpecDrumBuffer;
	xb->AudioBytes = BUFFER_SIZE * 2;
	xb->pAudioData = (BYTE *)specdrum;
	xb->PlayLength = BUFFER_SIZE;

	err = IXAudio2_CreateSourceVoice(xaudio, &xaudioSpecDrumVoice, &wex, 0, XAUDIO2_DEFAULT_FREQ_RATIO, NULL, NULL, NULL);
	assert(err == S_OK);

	err = IXAudio2SourceVoice_Start(xaudioSpeakerVoice,0, XAUDIO2_COMMIT_NOW);
	assert(err == S_OK);
	err = IXAudio2SourceVoice_Start(xaudioAYVoice,0, XAUDIO2_COMMIT_NOW);
	assert(err == S_OK);
	err = IXAudio2SourceVoice_Start(xaudioSpecDrumVoice, 0, XAUDIO2_COMMIT_NOW);
	assert(err == S_OK);
}

void kill_sound(void)
{
	IXAudio2SourceVoice_DestroyVoice(xaudioSpeakerVoice);
	IXAudio2SourceVoice_DestroyVoice(xaudioAYVoice);
	IXAudio2SourceVoice_DestroyVoice(xaudioSpecDrumVoice);

	IXAudio2MasteringVoice_DestroyVoice(xaudioMasteringVoice);
	
	IXAudio2_Release(xaudio);
}

void add_click(unsigned char click)
{
	static unsigned long long calls = 0;
	static DWORD time = 0;
	if (time == 0) time = timeGetTime();
	calls++;

	HRESULT err;

	if (!sound_on)
		return;

	assert(click_buffer < clicks + BUFFER_SIZE * 2);
	assert(click_buffer >= clicks);

	//8bit unsigned is 0..255 with 128 as midpoint
	const int power = 127;
	if (!click)
	{
		*click_buffer++ = 0x80;
		*click_buffer++ = 0x80;
	}
	else
	{
		*click_buffer++ = (unsigned char)(0x80 + power);
		*click_buffer++ = (unsigned char)(0x80 + power);
	}

	if (click_buffer == clicks + BUFFER_SIZE || click_buffer == clicks + BUFFER_SIZE * 2)
	{
		XAUDIO2_VOICE_STATE xvs;
		IXAudio2SourceVoice_GetState(xaudioSpeakerVoice , &xvs, 0);
		//elog("SP Q: %u S: %llu T: %u C: %llu R: %fsamples/s = %fseconds\n", xvs.BuffersQueued, xvs.SamplesPlayed, timeGetTime() - time, calls, (double)calls * 1000.0 / (double)(timeGetTime() - time), (double)calls * 1000.0 / (double)(timeGetTime() - time) / 44100.0);
	}

	if (click_buffer == clicks + BUFFER_SIZE)
	{
		xaudioSpeakerBuffer.pAudioData = clicks;
		xaudioSpeakerBuffer.PlayLength = BUFFER_SIZE/2;
		err = IXAudio2SourceVoice_SubmitSourceBuffer(xaudioSpeakerVoice, &xaudioSpeakerBuffer, NULL);
		assert(err == S_OK);
		err = IXAudio2SourceVoice_Start(xaudioSpeakerVoice,0, XAUDIO2_COMMIT_NOW);
		assert(err == S_OK);
		
		XAUDIO2_VOICE_STATE xvs;
		IXAudio2SourceVoice_GetState(xaudioSpeakerVoice, &xvs, XAUDIO2_VOICE_NOSAMPLESPLAYED);
		if (xvs.BuffersQueued >= 2)
		{
			//elog("SP waiting...");
			//ULONG t = timeGetTime();
			do
			{
				YieldProcessor();
				IXAudio2SourceVoice_GetState(xaudioSpeakerVoice, &xvs, XAUDIO2_VOICE_NOSAMPLESPLAYED);
			} while (xvs.BuffersQueued >= 2);
			//elog("%u\n", timeGetTime() - t);
		}
	}
	else if (click_buffer == clicks + BUFFER_SIZE * 2)
	{
		xaudioSpeakerBuffer.pAudioData = clicks+BUFFER_SIZE;
		xaudioSpeakerBuffer.PlayLength = BUFFER_SIZE / 2;
		err = IXAudio2SourceVoice_SubmitSourceBuffer(xaudioSpeakerVoice, &xaudioSpeakerBuffer, NULL);
		assert(err == S_OK);
		err = IXAudio2SourceVoice_Start(xaudioSpeakerVoice, 0, XAUDIO2_COMMIT_NOW);
		assert(err == S_OK);

		XAUDIO2_VOICE_STATE xvs;
		IXAudio2SourceVoice_GetState(xaudioSpeakerVoice, &xvs, XAUDIO2_VOICE_NOSAMPLESPLAYED);
		if (xvs.BuffersQueued >= 2)
		{
			//elog("SP waiting...");
			//ULONG t = timeGetTime();
			do
			{
				YieldProcessor();
				IXAudio2SourceVoice_GetState(xaudioSpeakerVoice, &xvs, XAUDIO2_VOICE_NOSAMPLESPLAYED);
			} while (xvs.BuffersQueued >= 2);
			//elog("%u\n", timeGetTime() - t);
		}
		click_buffer = clicks;
	}
}

void send_ay_words(int a, int b, int c)
{
	static unsigned long long calls = 0;
	static DWORD time = 0;
	if (time == 0) time = timeGetTime();
	calls++;

	HRESULT err;

	if (!sound_on)
		return;

	assert(click_buffer2 < clicks2 + BUFFER_SIZE * 2);
	assert(click_buffer2 >= clicks2);

	//ABC
	//*click_buffer2++ = (short)((b + c) / 2);
	//*click_buffer2++ = (short)((b + a) / 2);

	//ACB
	*click_buffer2++ = (short)((c + b) / 2);
	*click_buffer2++ = (short)((c + a) / 2);

	if (click_buffer2 == clicks2 + BUFFER_SIZE || click_buffer2 == clicks2 + BUFFER_SIZE * 2)
	{
		XAUDIO2_VOICE_STATE xvs;
		IXAudio2SourceVoice_GetState(xaudioAYVoice, &xvs, 0);
		//elog("AY Q: %u S: %llu T: %u C: %llu R: %fsamples/s = %fseconds\n", xvs.BuffersQueued, xvs.SamplesPlayed, timeGetTime() - time, calls, (double)calls*1000.0/ (double)(timeGetTime()-time), (double)calls * 1000.0 / (double)(timeGetTime() - time)/44100.0);
	}

	if (click_buffer2 == clicks2 + BUFFER_SIZE)
	{
		xaudioAYBuffer.pAudioData = (BYTE *)clicks2;
		xaudioAYBuffer.PlayLength = BUFFER_SIZE/2;
		err = IXAudio2SourceVoice_SubmitSourceBuffer(xaudioAYVoice, &xaudioAYBuffer, NULL);
		assert(err == S_OK);
		err = IXAudio2SourceVoice_Start(xaudioAYVoice, 0, XAUDIO2_COMMIT_NOW);
		assert(err == S_OK);

		XAUDIO2_VOICE_STATE xvs;
		IXAudio2SourceVoice_GetState(xaudioAYVoice, &xvs, XAUDIO2_VOICE_NOSAMPLESPLAYED);
		if (xvs.BuffersQueued >= 2)
		{
			//elog("AY waiting...");
			//ULONG t = timeGetTime();
			do
			{
				YieldProcessor();
				IXAudio2SourceVoice_GetState(xaudioAYVoice, &xvs, XAUDIO2_VOICE_NOSAMPLESPLAYED);
			} while (xvs.BuffersQueued >= 2);
			//elog("%u\n", timeGetTime() - t);
		}
	}
	else if (click_buffer2 == clicks2 + BUFFER_SIZE * 2)
	{
		xaudioAYBuffer.pAudioData = (BYTE *)(clicks2 + BUFFER_SIZE);
		xaudioAYBuffer.PlayLength = BUFFER_SIZE/2;
		err = IXAudio2SourceVoice_SubmitSourceBuffer(xaudioAYVoice, &xaudioAYBuffer, NULL);
		assert(err == S_OK);
		err = IXAudio2SourceVoice_Start(xaudioAYVoice, 0, XAUDIO2_COMMIT_NOW);
		assert(err == S_OK);

		XAUDIO2_VOICE_STATE xvs;
		IXAudio2SourceVoice_GetState(xaudioAYVoice, &xvs, XAUDIO2_VOICE_NOSAMPLESPLAYED);
		if (xvs.BuffersQueued >= 2)
		{
			//elog("AY waiting...");
			//ULONG t = timeGetTime();
			do
			{
				YieldProcessor();
				IXAudio2SourceVoice_GetState(xaudioAYVoice, &xvs, XAUDIO2_VOICE_NOSAMPLESPLAYED);
			} while (xvs.BuffersQueued >= 2);
			//elog("%u\n", timeGetTime() - t);
		}

		click_buffer2 = clicks2;
	}
}

void specdrum_out(unsigned char value)
{
	static unsigned long long calls = 0;
	static DWORD time = 0;
	if (time == 0) time = timeGetTime();
	calls++;

	HRESULT err;

	if (!sound_on)
		return;

	assert(specdrum_buffer < specdrum + BUFFER_SIZE * 2);
	assert(specdrum_buffer >= specdrum);

	//8bit unsigned is 0..255 with 128 as midpoint
	*specdrum_buffer++ = value;
	*specdrum_buffer++ = value;

	if (specdrum_buffer == specdrum + BUFFER_SIZE || specdrum_buffer == specdrum + BUFFER_SIZE * 2)
	{
		XAUDIO2_VOICE_STATE xvs;
		IXAudio2SourceVoice_GetState(xaudioSpecDrumVoice, &xvs, 0);
		//elog("SpecDrum Q: %u S: %llu T: %u C: %llu R: %fsamples/s = %fseconds\n", xvs.BuffersQueued, xvs.SamplesPlayed, timeGetTime() - time, calls, (double)calls * 1000.0 / (double)(timeGetTime() - time), (double)calls * 1000.0 / (double)(timeGetTime() - time) / 44100.0);
	}

	if (specdrum_buffer == specdrum + BUFFER_SIZE)
	{
		xaudioSpecDrumBuffer.pAudioData = specdrum;
		xaudioSpecDrumBuffer.PlayLength = BUFFER_SIZE / 2;
		err = IXAudio2SourceVoice_SubmitSourceBuffer(xaudioSpecDrumVoice, &xaudioSpecDrumBuffer, NULL);
		assert(err == S_OK);
		err = IXAudio2SourceVoice_Start(xaudioSpecDrumVoice, 0, XAUDIO2_COMMIT_NOW);
		assert(err == S_OK);

		XAUDIO2_VOICE_STATE xvs;
		IXAudio2SourceVoice_GetState(xaudioSpecDrumVoice, &xvs, XAUDIO2_VOICE_NOSAMPLESPLAYED);
		if (xvs.BuffersQueued >= 2)
		{
			//elog("SpecDrum waiting...");
			//ULONG t = timeGetTime();
			do
			{
				YieldProcessor();
				IXAudio2SourceVoice_GetState(xaudioSpecDrumVoice, &xvs, XAUDIO2_VOICE_NOSAMPLESPLAYED);
			} while (xvs.BuffersQueued >= 2);
			//elog("%u\n", timeGetTime() - t);
		}
	}
	else if (specdrum_buffer == specdrum + BUFFER_SIZE * 2)
	{
		xaudioSpecDrumBuffer.pAudioData = specdrum + BUFFER_SIZE;
		xaudioSpecDrumBuffer.PlayLength = BUFFER_SIZE / 2;
		err = IXAudio2SourceVoice_SubmitSourceBuffer(xaudioSpecDrumVoice, &xaudioSpecDrumBuffer, NULL);
		assert(err == S_OK);
		err = IXAudio2SourceVoice_Start(xaudioSpecDrumVoice, 0, XAUDIO2_COMMIT_NOW);
		assert(err == S_OK);

		XAUDIO2_VOICE_STATE xvs;
		IXAudio2SourceVoice_GetState(xaudioSpecDrumVoice, &xvs, XAUDIO2_VOICE_NOSAMPLESPLAYED);
		if (xvs.BuffersQueued >= 2)
		{
			//elog("SP waiting...");
			//ULONG t = timeGetTime();
			do
			{
				YieldProcessor();
				IXAudio2SourceVoice_GetState(xaudioSpecDrumVoice, &xvs, XAUDIO2_VOICE_NOSAMPLESPLAYED);
			} while (xvs.BuffersQueued >= 2);
			//elog("%u\n", timeGetTime() - t);
		}
		specdrum_buffer = specdrum;
	}
}

void toggle_sound(void)
{
	sound_on ^= 1;
}

int is_sound_on(void)
{
	return sound_on;
}
#endif
