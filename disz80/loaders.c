//-----------------------------------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------

//https://faqwiki.zxnet.co.uk/wiki/Loading_routine_%22cores%22

#include <stdlib.h>
#include <assert.h>

#include "loaders.h"
#include "emu.h"
#include "machine.h"
#include "dasm.h"
#include "logger.h"

typedef enum
{
	ACTIVE=1
} LOADERFLAGS;

typedef struct
{
	char *name;
	unsigned char hotpatch_number;
	char (*detection)(unsigned short);
	unsigned short offset_from_in_a;
	void (*speedup)(void);
	LOADERFLAGS flags;
} LOADER;

extern unsigned int tape_cycles;
extern unsigned int cycles_used;
extern MACHINE machine;

static int machine_memcmp(const void *vsrc, unsigned short src1, size_t size)
{
	const unsigned char *src0 = vsrc;
	while (size--)
		if (*src0++ != peek_fast(src1++)) return 1;
	return 0;
}

/*
ROM Loader
Status - OK

LD-SAMPLE	INC B
			RET Z
			LD A,7F
			IN A,(FE)
			RRA
			RET NC
			XOR C
			AND 20
			JR Z,LD-SAMPLE
*/
static void rom_loader_fn(void)
{
	REG_PACK *regs = get_regs();

	unsigned int last_cycles_used = cycles_used;

	do
	{
		regs->b.b++;
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.b == 0)
		{
			regs->b.f = FLAGS_ZF;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;
			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a = 0x7f;
		tape_cycles += 7; cycles_used += 7;

		regs->b.a = in(0x7ffe); //tape_cycles gets reset to 0
		last_cycles_used = cycles_used;
		tape_cycles += 11; cycles_used += 11;

		unsigned char c = regs->b.a & 1;
		regs->b.a = (regs->b.a >> 1) | (regs->b.f << 7);
		tape_cycles += 4; cycles_used += 4;

		if (!c)
		{
			regs->b.f = 0;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;
			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a ^= regs->b.c;
		tape_cycles += 4; cycles_used += 4;

		regs->b.a &= 0x20;
		regs->b.f = 0;
		tape_cycles += 7; cycles_used += 7;

		tape_cycles += 12; cycles_used += 12;
	} while (regs->b.a == 0);
	tape_cycles -= 5; cycles_used -= 5;

	//we're going to add 'cycles_used' to 'tape_cycles' at exit, so we need to adjust
	//to account for the fact we've been incrementing it in expectation of looping again
	tape_cycles -= last_cycles_used;

	regs->b.f = 0;
	regs->pc += 11;
}

static char rom_detect_fn(unsigned short loader_address)
{
	const unsigned char detection[] = { 0x04, 0xC8, 0x3E, 0x7F, 0xDB, 0xFE, 0x1F, 0xD0, 0xA9, 0xE6, 0x20, 0x28, 0xF3 };
	return machine_memcmp(detection, loader_address, sizeof detection) == 0;
}

static LOADER rom_loader =
{
	"ROM Loader", 0,
	rom_detect_fn,
	4,
	rom_loader_fn,
	ACTIVE
};

/*
Speedlock
e.g. Daley Thompson's Decathlon, Head over Heels
Status - some OK, some broken with and without accelerator

LD-SAMPLE	INC B
			RET Z
			LD A,7F
			IN A,(FE)
			RRA
			XOR C
			AND 20
			JR Z,LD-SAMPLE
*/
static void speedlock_loader_fn(void)
{
	REG_PACK *regs = get_regs();

	unsigned int last_cycles_used = cycles_used;

	do
	{
		regs->b.b++;
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.b == 0)
		{
			regs->b.f = FLAGS_ZF;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;
			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a = 0x7f;
		tape_cycles += 7; cycles_used += 7;

		regs->b.a = in(0x7ffe); //tape_cycles gets reset to 0
		last_cycles_used = cycles_used;
		tape_cycles += 11; cycles_used += 11;

		regs->b.a = (regs->b.a >> 1) | (regs->b.f << 7);
		tape_cycles += 4; cycles_used += 4;

		regs->b.a ^= regs->b.c;
		tape_cycles += 4; cycles_used += 4;

		regs->b.a &= 0x20;
		regs->b.f = 0;
		tape_cycles += 7; cycles_used += 7;

		tape_cycles += 12; cycles_used += 12;
	} while (regs->b.a == 0);
	tape_cycles -= 5; cycles_used -= 5;

	//we're going to add 'cycles_used' to 'tape_cycles' at exit, so we need to adjust
	//to account for the fact we've been incrementing it in expectation of looping again
	tape_cycles -= last_cycles_used;

	regs->b.f = 0;
	regs->pc += 10;
}

static char speedlock_detect_fn(unsigned short loader_address)
{
	const unsigned char detection[] = { 0x04, 0xC8, 0x3E, 0x7F, 0xDB, 0xFE, 0x1F, 0xA9, 0xE6, 0x20, 0x28, 0xF4 };
	return machine_memcmp(detection, loader_address, sizeof detection) == 0;
}

static LOADER speedlock_loader =
{
	"Speedlock Loader", 1,
	speedlock_detect_fn, 4,
	speedlock_loader_fn,
	0,//ACTIVE
};

/*
Alkatraz
e.g. 720, Cobra
Status - instant tape loading error. odd, because the variant loader is just fine.

LD-SAMPLE	INC B
			JR NZ,LD-SAMPLE2
			JP <somewhere else>
LD-SAMPLE2	IN A,(FE)
			RRA
			RET Z
			XOR C
			AND 20
			JR Z,LD-SAMPLE
*/
static void alkatraz_loader_fn(void)
{
	REG_PACK *regs = get_regs();

	unsigned int last_cycles_used = cycles_used;

	do
	{
		regs->b.b++;
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.b == 0)
		{
			tape_cycles += 7; cycles_used += 7;
			regs->b.f = FLAGS_ZF;

			//This loader uses self modifying code!
			//To begin with there is a RET here followed by 2 unused bytes and
			//later it is poked with JP nn.
			//PC points just past our hotpatch instruction
			//RET/JP is at PC+1
			unsigned char ins = peek_fast(regs->pc+1);
			if (ins == 0xC9)
			{
				//it's a ret
				regs->pc = peek_fastw(regs->sp);
				regs->sp += 2;
				tape_cycles += 10; cycles_used += 10;
			}
			else if (ins == 0xC3)
			{
				//it's a jp
				regs->pc = peek_fastw(regs->pc + 2);
				tape_cycles += 10; cycles_used += 10;
			}
			else
			{
				assert(0);
			}

			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 12; cycles_used += 12;

		regs->b.a = in(0x7ffe); //tape_cycles gets reset to 0
		last_cycles_used = cycles_used;
		tape_cycles += 11; cycles_used += 11;

		regs->b.a = (regs->b.a >> 1) | (regs->b.f << 7);
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.a == 0)
		{
			regs->b.f = FLAGS_ZF;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;

			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a ^= regs->b.c;
		tape_cycles += 4; cycles_used += 4;

		regs->b.a &= 0x20;
		regs->b.f = 0;
		tape_cycles += 7; cycles_used += 7;

		tape_cycles += 12; cycles_used += 12;
	} while (regs->b.a == 0);
	tape_cycles -= 5; cycles_used -= 5;

	//we're going to add 'cycles_used' to 'tape_cycles' at exit, so we need to adjust
	//to account for the fact we've been incrementing it in expectation of looping again
	tape_cycles -= last_cycles_used;

	regs->b.f = 0;
	regs->pc += 13;
}

static char alkatraz_detect_fn(unsigned short loader_address)
{
	const unsigned char detection[] = { 0xDB, 0xFE, 0x1F, 0xC8, 0xA9, 0xE6, 0x20, 0x28, 0xF1 };
	if (peek_fast(loader_address++) != 0x04) return 0;//INC B
	if (peek_fast(loader_address++) != 0x20) return 0;//JR NZ +3
	if (peek_fast(loader_address++) != 0x03) return 0;
	if (peek_fast(loader_address) != 0xC3 && peek_fast(loader_address) != 0xC9) return 0;//JP/RET
	loader_address += 3;//RET xx xx or JP NN
	return machine_memcmp(detection, loader_address, sizeof detection) == 0;
}

static LOADER alkatraz_loader =
{
	"Alkatraz Loader", 9,
	alkatraz_detect_fn,
	6,
	alkatraz_loader_fn,
	0//ACTIVE
};

/*
"Variant" Alkatraz
e.g Gauntlet III, Outrun Europa, Shadow Dancer
Status - OK

LD-SAMPLE	INC B
			JR NZ,LD-SAMPLE2
			RET
LD-SAMPLE2	IN A,(FE)
			RRA
			RET Z
			XOR C
			AND 20
			JR Z,LD-SAMPLE
*/
static void alkatraz_variant_loader_fn(void)
{
	REG_PACK *regs = get_regs();

	unsigned int last_cycles_used = cycles_used;

	do
	{
		regs->b.b++;
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.b == 0)
		{
			tape_cycles += 7; cycles_used += 7;
			regs->b.f = FLAGS_ZF;

			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 10; cycles_used += 10;

			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 12; cycles_used += 12;

		regs->b.a = in(0x7ffe); //tape_cycles gets reset to 0
		last_cycles_used = cycles_used;
		tape_cycles += 11; cycles_used += 11;

		regs->b.a = (regs->b.a >> 1) | (regs->b.f << 7);
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.a == 0)
		{
			regs->b.f = FLAGS_ZF;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;

			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a ^= regs->b.c;
		tape_cycles += 4; cycles_used += 4;

		regs->b.a &= 0x20;
		regs->b.f = 0;
		tape_cycles += 7; cycles_used += 7;

		tape_cycles += 12; cycles_used += 12;
	} while (regs->b.a == 0);
	tape_cycles -= 5; cycles_used -= 5;

	//we're going to add 'cycles_used' to 'tape_cycles' at exit, so we need to adjust
	//to account for the fact we've been incrementing it in expectation of looping again
	tape_cycles -= last_cycles_used;

	regs->b.f = 0;
	regs->pc += 11;
}

static char alkatraz_variant_detect_fn(unsigned short loader_address)
{
	const unsigned char detection[] = { 0x04, 0x20, 0x01, 0xC9, 0xDB, 0xFE, 0x1F, 0xC8, 0xA9, 0xE6, 0x20, 0x28, 0xF3 };
	return machine_memcmp(detection, loader_address, sizeof detection) == 0;
}

static LOADER alkatraz_variant_loader =
{
	"Variant Alkatraz Loader", 10,
	alkatraz_variant_detect_fn,
	4,
	alkatraz_variant_loader_fn,
	ACTIVE
};

/*
Bleepload (Firebird)
e.g Bubble Bobble, Starglider 2
Status - broken - crashes at end of first block loaded, OK without accelerator

LD-SAMPLE	INC B
			RET Z
			LD A,7F
			IN A,(FE)
			RRA
			NOP
			XOR C
			AND 20
			JR Z,LD-SAMPLE
*/
static void bleepload_loader_fn(void)
{
	REG_PACK *regs = get_regs();

	unsigned int last_cycles_used = cycles_used;

	do
	{
		regs->b.b++;
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.b == 0)
		{
			regs->b.f = FLAGS_ZF;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;
			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a = 0x7f;
		tape_cycles += 7; cycles_used += 7;

		regs->b.a = in(0x7ffe); //tape_cycles gets reset to 0
		last_cycles_used = cycles_used;
		tape_cycles += 11; cycles_used += 11;

		regs->b.a = (regs->b.a >> 1) | (regs->b.f << 7);
		tape_cycles += 4; cycles_used += 4;

		tape_cycles += 4; cycles_used += 4;

		regs->b.a ^= regs->b.c;
		tape_cycles += 4; cycles_used += 4;

		regs->b.a &= 0x20;
		regs->b.f = 0;
		tape_cycles += 7; cycles_used += 7;

		tape_cycles += 12; cycles_used += 12;
	} while (regs->b.a == 0);
	tape_cycles -= 5; cycles_used -= 5;

	//we're going to add 'cycles_used' to 'tape_cycles' at exit, so we need to adjust
	//to account for the fact we've been incrementing it in expectation of looping again
	tape_cycles -= last_cycles_used;

	regs->b.f = 0;
	regs->pc += 11;
}

static char bleepload_detect_fn(unsigned short loader_address)
{
	const unsigned char detection[] = { 0x04, 0xC8, 0x3E, 0x7F, 0xDB, 0xFE, 0x1F, 0x00, 0xA9, 0xE6, 0x20, 0x28, 0xF3 };
	return machine_memcmp(detection, loader_address, sizeof detection) == 0;
}

static LOADER bleepload_loader =
{
	"Bleepload Loader", 2,
	bleepload_detect_fn,
	4,
	bleepload_loader_fn,
	0,//ACTIVE
};

/*
Microsphere
e.g. Contact Sam Cruise, Skool Daze
Status - broken, crashes at end of loader. OK without accelerator.

LD-SAMPLE	INC B
			RET Z
			LD A,7F
			IN A,(FE)
			RRA
			AND A
			XOR C
			AND 20
			JR Z,LD-SAMPLE
*/
static void microsphere_loader_fn(void)
{
	REG_PACK *regs = get_regs();

	unsigned int last_cycles_used = cycles_used;

	do
	{
		regs->b.b++;
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.b == 0)
		{
			regs->b.f = FLAGS_ZF;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;
			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a = 0x7f;
		tape_cycles += 7; cycles_used += 7;

		regs->b.a = in(0x7ffe); //tape_cycles gets reset to 0
		last_cycles_used = cycles_used;
		tape_cycles += 11; cycles_used += 11;

		regs->b.a = (regs->b.a >> 1) | (regs->b.f << 7);
		tape_cycles += 4; cycles_used += 4;

		tape_cycles += 4; cycles_used += 4;

		regs->b.a ^= regs->b.c;
		tape_cycles += 4; cycles_used += 4;

		regs->b.a &= 0x20;
		regs->b.f = 0;
		tape_cycles += 7; cycles_used += 7;

		tape_cycles += 12; cycles_used += 12;
	} while (regs->b.a == 0);
	tape_cycles -= 5; cycles_used -= 5;

	//we're going to add 'cycles_used' to 'tape_cycles' at exit, so we need to adjust
	//to account for the fact we've been incrementing it in expectation of looping again
	tape_cycles -= last_cycles_used;

	regs->b.f = 0;
	regs->pc += 11;
}

static char microsphere_detect_fn(unsigned short loader_address)
{
	const unsigned char detection[] = { 0x04, 0xC8, 0x3E, 0x7F, 0xDB, 0xFE, 0x1F, 0xA7, 0xA9, 0xE6, 0x20, 0x28, 0xF3 };
	return machine_memcmp(detection, loader_address, sizeof detection) == 0;
}

static LOADER microsphere_loader =
{
	"Microsphere Loader", 3,
	microsphere_detect_fn,
	4,
	microsphere_loader_fn,
	0//ACTIVE
};

/*
Paul Owens
e.g. Chase HQ
Status - OK

LD-SAMPLE	INC B
			RET Z
			LD A,7F
			IN A,(FE)
			RRA
			RET Z
			XOR C
			AND 20
			JR Z,LD-SAMPLE
*/
static void paulowens_loader_fn(void)
{
	REG_PACK *regs = get_regs();

	unsigned int last_cycles_used = cycles_used;

	do
	{
		regs->b.b++;
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.b == 0)
		{
			regs->b.f = FLAGS_ZF;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;
			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a = 0x7f;
		tape_cycles += 7; cycles_used += 7;

		regs->b.a = in(0x7ffe); //tape_cycles gets reset to 0
		last_cycles_used = cycles_used;
		tape_cycles += 11; cycles_used += 11;

		regs->b.a = (regs->b.a >> 1) | (regs->b.f << 7);
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.b == 0)
		{
			regs->b.f = 0;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;
			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a ^= regs->b.c;
		tape_cycles += 4; cycles_used += 4;

		regs->b.a &= 0x20;
		regs->b.f = 0;
		tape_cycles += 7; cycles_used += 7;

		tape_cycles += 12; cycles_used += 12;
	} while (regs->b.a == 0);
	tape_cycles -= 5; cycles_used -= 5;

	//we're going to add 'cycles_used' to 'tape_cycles' at exit, so we need to adjust
	//to account for the fact we've been incrementing it in expectation of looping again
	tape_cycles -= last_cycles_used;

	regs->b.f = 0;
	regs->pc += 11;
}

static char paulowens_detect_fn(unsigned short loader_address)
{
	const unsigned char detection[] = { 0x04, 0xC8, 0x3E, 0x7F, 0xDB, 0xFE, 0x1F, 0xC8, 0xA9, 0xE6, 0x20, 0x28, 0xF3 };
	return machine_memcmp(detection, loader_address, sizeof detection) == 0;
}

static LOADER paulowens_loader =
{
	"Paul Owens Loader", 4,
	paulowens_detect_fn,
	4,
	paulowens_loader_fn,
	ACTIVE
};

/*
Dinaload a.k.a Poliload
e.g. Astro Marine Corps, Freddy Hardest en Manhattan Sur
Status - OK

LD-SAMPLE	INC B
			RET Z
			LD A,FF
			IN A,(FE)
			RRA
			RET NC
			XOR C
			AND 20
			JR Z,LD-SAMPLE
*/
static void dinaload_loader_fn(void)
{
	REG_PACK *regs = get_regs();

	unsigned int last_cycles_used = cycles_used;

	do
	{
		regs->b.b++;
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.b == 0)
		{
			regs->b.f = FLAGS_ZF;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;
			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a = 0xff;
		tape_cycles += 7; cycles_used += 7;

		regs->b.a = in(0xfffe); //tape_cycles gets reset to 0
		last_cycles_used = cycles_used;
		tape_cycles += 11; cycles_used += 11;

		unsigned char c = regs->b.a & 1;
		regs->b.a = (regs->b.a >> 1) | (regs->b.f << 7);
		tape_cycles += 4; cycles_used += 4;

		if (!c)
		{
			regs->b.f = 0;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;
			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a ^= regs->b.c;
		tape_cycles += 4; cycles_used += 4;

		regs->b.a &= 0x20;
		regs->b.f = 0;
		tape_cycles += 7; cycles_used += 7;

		tape_cycles += 12; cycles_used += 12;
	} while (regs->b.a == 0);
	tape_cycles -= 5; cycles_used -= 5;

	//we're going to add 'cycles_used' to 'tape_cycles' at exit, so we need to adjust
	//to account for the fact we've been incrementing it in expectation of looping again
	tape_cycles -= last_cycles_used;

	regs->b.f = 0;
	regs->pc += 11;
}

static char dinaload_detect_fn(unsigned short loader_address)
{
	const unsigned char detection[] = { 0x04, 0xC8, 0x3E, 0xFF, 0xDB, 0xFE, 0x1F, 0xD0, 0xA9, 0xE6, 0x20, 0x28, 0xF3 };
	return machine_memcmp(detection, loader_address, sizeof detection) == 0;
}

static LOADER dinaload_loader =
{
	"Dinaload Loader", 5,
	dinaload_detect_fn,
	4,
	dinaload_loader_fn,
	ACTIVE
};

/*
Search Loader
e.g. Blood Brothers, City Slicker
Status - OK

LD-SAMPLE	INC B
			RET Z
			LD A,00
			IN A,(FE)
			XOR C
			AND 40
			RET C
			NOP
			JR Z,LD-SAMPLE
*/
static void search_loader_fn(void)
{
	REG_PACK *regs = get_regs();

	unsigned int last_cycles_used = cycles_used;

	do
	{
		regs->b.b++;
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.b == 0)
		{
			regs->b.f = FLAGS_ZF;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;
			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a = 0;
		tape_cycles += 7; cycles_used += 7;

		regs->b.a = in(0x00fe); //tape_cycles gets reset to 0
		last_cycles_used = cycles_used;
		tape_cycles += 11; cycles_used += 11;

		regs->b.a ^= regs->b.c;
		tape_cycles += 4; cycles_used += 4;

		regs->b.a &= 0x40;
		regs->b.f = 0;
		tape_cycles += 7; cycles_used += 7;

		tape_cycles += 5; cycles_used += 5;

		tape_cycles += 4; cycles_used += 4;

		tape_cycles += 12; cycles_used += 12;
	} while (regs->b.a == 0);
	tape_cycles -= 5; cycles_used -= 5;

	//we're going to add 'cycles_used' to 'tape_cycles' at exit, so we need to adjust
	//to account for the fact we've been incrementing it in expectation of looping again
	tape_cycles -= last_cycles_used;

	regs->b.f = 0;
	regs->pc += 11;
}

static char search_detect_fn(unsigned short loader_address)
{
	const unsigned char detection[] = { 0x04, 0xC8, 0x3E, 0x00, 0xDB, 0xFE, 0xA9, 0xE6, 0x40, 0xD8, 0x00, 0x28, 0xF3 };
	return machine_memcmp(detection, loader_address, sizeof detection) == 0;
}

static LOADER search_loader =
{
	"Search Loader", 6,
	search_detect_fn,
	4,
	search_loader_fn,
	ACTIVE
};

/*
"Variant" Search Loader
e.g. Lotus Esprit Turbo Challenge, Space Crusade
Status - OK

LD-SAMPLE	INC B
			RET Z
			LD A,7F
			IN A,(FE)
			XOR C
			AND 40
			JR Z,LD-SAMPLE
*/
static void search_variant_loader_fn(void)
{
	REG_PACK *regs = get_regs();

	unsigned int last_cycles_used = cycles_used;

	do
	{
		regs->b.b++;
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.b == 0)
		{
			regs->b.f = FLAGS_ZF;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;
			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a = 0x7f;
		tape_cycles += 7; cycles_used += 7;

		regs->b.a = in(0x7ffe); //tape_cycles gets reset to 0
		last_cycles_used = cycles_used;
		tape_cycles += 11; cycles_used += 11;

		regs->b.a ^= regs->b.c;
		tape_cycles += 4; cycles_used += 4;

		regs->b.a &= 0x40;
		regs->b.f = 0;
		tape_cycles += 7; cycles_used += 7;

		tape_cycles += 12; cycles_used += 12;
	} while (regs->b.a == 0);
	tape_cycles -= 5; cycles_used -= 5;

	//we're going to add 'cycles_used' to 'tape_cycles' at exit, so we need to adjust
	//to account for the fact we've been incrementing it in expectation of looping again
	tape_cycles -= last_cycles_used;

	regs->b.f = 0;
	regs->pc += 9;
}

static char search_variant_detect_fn(unsigned short loader_address)
{
	const unsigned char detection[] = { 0x04, 0xC8, 0x3E, 0x7f, 0xDB, 0xFE, 0xA9, 0xE6, 0x40, 0x28, 0xF5 };
	return machine_memcmp(detection, loader_address, sizeof detection) == 0;
}

static LOADER search_variant_loader =
{
	"Variant Search Loader", 7,
	search_variant_detect_fn,
	4,
	search_variant_loader_fn,
	ACTIVE
};

/*
Digital Integration (Dinamic)
e.g. ATF, Tomahawk
Status - broken with and without accelerator

LD-SAMPLE	DEC B
			RET Z
			IN A,(FE)
			XOR C
			AND 40
			JP Z,LD-SAMPLE (yes, JP)
*/
static void digital_integration_loader_fn(void)
{
	REG_PACK *regs = get_regs();

	unsigned int last_cycles_used = cycles_used;

	do
	{
		regs->b.b--;
		tape_cycles += 4; cycles_used += 4;

		if (regs->b.b == 0)
		{
			regs->b.f = FLAGS_ZF;
			regs->pc = peek_fastw(regs->sp);
			regs->sp += 2;
			tape_cycles += 11; cycles_used += 11;
			tape_cycles -= last_cycles_used;
			return;
		}
		tape_cycles += 5; cycles_used += 5;

		regs->b.a = in(0x7ffe); //tape_cycles gets reset to 0
		last_cycles_used = cycles_used;
		tape_cycles += 11; cycles_used += 11;

		regs->b.a ^= regs->b.c;
		tape_cycles += 4; cycles_used += 4;

		regs->b.a &= 0x40;
		regs->b.f = regs->b.a ? 0 : FLAGS_ZF;
		tape_cycles += 7; cycles_used += 7;

		tape_cycles += 10; cycles_used += 10;
	} while (regs->b.a == 0);

	//we're going to add 'cycles_used' to 'tape_cycles' at exit, so we need to adjust
	//to account for the fact we've been incrementing it in expectation of looping again
	tape_cycles -= last_cycles_used;

	//regs->b.f = 0;
	regs->pc += 8;
}

static char digital_integration_detect_fn(unsigned short loader_address)
{
	const unsigned char detection[] = { 0x05, 0xC8, 0xDB, 0xFE, 0xA9, 0xE6, 0x40, 0xCA };//, 0x78, 0xff } last 2 bytes differ based on location
	return machine_memcmp(detection, loader_address, sizeof detection) == 0;
}

static LOADER digital_integration_loader =
{
	"Digital Integration Loader", 8,
	digital_integration_detect_fn,
	2,
	digital_integration_loader_fn,
	0,//ACTIVE
};

LOADER *loaders[] =
{
	&rom_loader,
	&speedlock_loader,
	&bleepload_loader,
	&microsphere_loader,
	&paulowens_loader,
	&dinaload_loader,
	&search_loader,
	&search_variant_loader,
	&digital_integration_loader,
	&alkatraz_loader,
	&alkatraz_variant_loader
};

int n_failed_patches = 0;
unsigned short failed_patches[100];
int loaders_enabled = 0;
void detect_loader(unsigned short pc)
{
	if (!loaders_enabled)
		return;

	for (int i = 0; i < n_failed_patches; i++)
		if (failed_patches[i] == pc)
			return;

	LOADER **loader_ptr = loaders;
	for (int i = 0; i < sizeof loaders / sizeof * loaders; i++)
	{
		LOADER *loader = *loader_ptr++;
		unsigned short loader_address = pc - loader->offset_from_in_a;
		if ((loader->flags & ACTIVE) && loader->detection(loader_address))
		{
			poke_fast(loader_address, 0xED);
			poke_fast(loader_address + 1, loader->hotpatch_number);
			elog("[PATCH] patched '%s' at %04X (%d) - hotpatch # %u\n", loader->name, loader_address, loader_address, loader->hotpatch_number);
			return;
		}
	}

	failed_patches[n_failed_patches++] = pc;

	elog("[PATCH] failed to patch a loader at %04X (%d)\n", pc, pc);
	DAsmBlock(pc-4, 13);
}

void run_hotpatch(unsigned char i)
{
	loaders[i]->speedup();
}

void reset_hotpatches(void)
{
	n_failed_patches = 0;
}
