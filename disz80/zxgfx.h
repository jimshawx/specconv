//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
void create_screen(void);
void kill_screen(void);
void convert_scanline(unsigned int line, unsigned char border);
void convert_screen(void);
void dirty_screen(unsigned short address, unsigned char value);
void set_border(int scanline, unsigned char value);
void set_border_colour(unsigned char value);
void repaint_screen(void);
void dirty_whole_screen(void);
void gfx_init(void);
void gfx_shutdown(void);

#define GFX_SCREEN 1
#define GFX_SPRITE 2
#define GFX_CHAR   4
#define GFX_ATTR   8

#define PRETTY_BORDER_WIDTH 30
#define BORDER_WIDTH 50
#define SCANLINE_WIDTH (2*BORDER_WIDTH+256)
#define SCREEN_HEIGHT 312

void ula_init(int num_scanlines, int first_scanline, int scanline_cycle_count);
void ula_reset_tick(void);
void ula_tick(int tick, int drawing);
void ula_out(unsigned char out);
unsigned char ula_in();
void ula_page(void);
