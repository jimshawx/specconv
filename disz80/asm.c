//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <stdio.h>
#include <math.h>

#include "logger.h"
#include "dasm.h"
#include "daos.h"
#include "dbgmem.h"

#define MAX_BITS 256
typedef struct
{
	int n_bits;
	union
	{
		char *bits[MAX_BITS];
		struct
		{
			char *ins;
			union
			{
				struct
				{
					char *dst;
					char *src;
					char *extra;
				};
				struct
				{
					char *cond;
					char *target;
				};
			};
		};
	};
} DECODER;

typedef struct
{
	unsigned short address;
	char *label;
} ASM_LABEL;

typedef struct
{
	unsigned short address;//location within 'program' of where to poke the fixup
	unsigned short i8offset;//jr/djnz offsetting from this address
	unsigned short pc;//the pc of the start of the instruction
	char size;//8 or 16 (bit)
	int lineno;//the source line number
	char *evalfixup;//the expression to parse
	int value;//the calculated value of the expression 
} ASM_FIXUP;

#define MAX_LABELS 65536
#define MAX_FIXUPS 65636

typedef struct
{
	//which line number are we working on? (starts at 0)
	int lineno;

	//current input line
	char *src;

	//decoder
	DECODER decode;

	//current address to assemble to
	unsigned short pc;

	char have_seen_org;

	//start address
	unsigned short org;

	//error detail
	int error;
	int position;

	//the assembled instruction
	unsigned char asm[256];
	unsigned short n_bytes;

	//the full payload
	unsigned char program[65536];
	int n_program;

	//labels encountered
	ASM_LABEL labels[MAX_LABELS];
	int n_labels;
	
	//fixups encountered
	ASM_FIXUP fixups[MAX_FIXUPS];
	int n_fixups;

} ASTATE;

static char is_A(char *reg)
{
	if (reg == NULL) return 0;
	return (reg[0] == 'a' || reg[0] == 'A') && reg[1] == '\0';
}

static char is_F(char *reg)
{
	if (reg == NULL) return 0;
	return (reg[0] == 'f'|| reg[0] == 'F') && reg[1] == '\0';
}

static unsigned char R8(char *reg)
{
	if (reg == NULL) return 0;
	if (reg[0] == 'b' || reg[0] == 'B') return 0;
	if (reg[0] == 'c' || reg[0] == 'C') return 1;
	if (reg[0] == 'd' || reg[0] == 'D') return 2;
	if (reg[0] == 'e' || reg[0] == 'E') return 3;
	if (reg[0] == 'h' || reg[0] == 'H') return 4;
	if (reg[0] == 'l' || reg[0] == 'L') return 5;
	if (reg[0] == 'a' || reg[0] == 'A') return 7;

	if (reg[0] == 'i' && reg[1] == 'x' && reg[2] == 'h') return 4;
	if (reg[0] == 'i' && reg[1] == 'x' && reg[2] == 'l') return 5;
	if (reg[0] == 'I' && reg[1] == 'X' && reg[2] == 'H') return 4;
	if (reg[0] == 'I' && reg[1] == 'X' && reg[2] == 'L') return 5;

	if (reg[0] == 'i' && reg[1] == 'y' && reg[2] == 'h') return 4;
	if (reg[0] == 'i' && reg[1] == 'y' && reg[2] == 'l') return 5;
	if (reg[0] == 'I' && reg[1] == 'Y' && reg[2] == 'H') return 4;
	if (reg[0] == 'I' && reg[1] == 'Y' && reg[2] == 'L') return 5;

	assert(0);
	return 0;
}

static char is_R(char *reg)
{
	if (reg == NULL) return 0;
	return (reg[0] == 'r' || reg[0] == 'R') && reg[1] == '\0';
}

static char is_I(char *reg)
{
	if (reg == NULL) return 0;
	return (reg[0] == 'i' || reg[0] == 'I') && reg[1] == '\0';
}

static char is_R8(char *reg)
{
	if (reg == NULL) return 0;
	return (((reg[0] >= 'a' && reg[0] <= 'e') || reg[0] == 'h' || reg[0] == 'l') ||
			((reg[0] >= 'A' && reg[0] <= 'E') || reg[0] == 'H' || reg[0] == 'L')) && reg[1] == '\0';
}

static char is_IXL(char *reg)
{
	if (reg == NULL) return 0;
	return ((reg[0] == 'i' && reg[1] == 'x' && reg[2] == 'l') ||
			(reg[0] == 'I' && reg[1] == 'X' && reg[2] == 'L')) && reg[3] == '\0';
}

static char is_IXH(char *reg)
{
	if (reg == NULL) return 0;
	return ((reg[0] == 'i' && reg[1] == 'x' && reg[2] == 'h')||
			(reg[0] == 'I' && reg[1] == 'X' && reg[2] == 'H')) && reg[3] == '\0';
}

static char is_IYL(char *reg)
{
	if (reg == NULL) return 0;
	return ((reg[0] == 'i' && reg[1] == 'y' && reg[2] == 'l')|| 
			(reg[0] == 'I' && reg[1] == 'Y' && reg[2] == 'L')) && reg[3] == '\0';
}

static char is_IYH(char *reg)
{
	if (reg == NULL) return 0;
	return ((reg[0] == 'i' && reg[1] == 'y' && reg[2] == 'h')||
			(reg[0] == 'I' && reg[1] == 'Y' && reg[2] == 'H')) && reg[3] == '\0';
}

static char is_R16sp(char *reg)
{
	if (reg == NULL) return 0;
	return  (reg[0] == 'b' && reg[1] == 'c' && reg[2] == '\0') ||
			(reg[0] == 'd' && reg[1] == 'e' && reg[2] == '\0') ||
			(reg[0] == 'h' && reg[1] == 'l' && reg[2] == '\0') ||
			(reg[0] == 's' && reg[1] == 'p' && reg[2] == '\0') ||
			(reg[0] == 'B' && reg[1] == 'C' && reg[2] == '\0') ||
			(reg[0] == 'D' && reg[1] == 'E' && reg[2] == '\0') ||
			(reg[0] == 'H' && reg[1] == 'L' && reg[2] == '\0') ||
			(reg[0] == 'S' && reg[1] == 'P' && reg[2] == '\0');
}

static char is_R16af(char *reg)
{
	if (reg == NULL) return 0;
	return  (reg[0] == 'b' && reg[1] == 'c' && reg[2] == '\0') ||
			(reg[0] == 'd' && reg[1] == 'e' && reg[2] == '\0') ||
			(reg[0] == 'h' && reg[1] == 'l' && reg[2] == '\0') ||
			(reg[0] == 'a' && reg[1] == 'f' && reg[2] == '\0') ||
			(reg[0] == 'B' && reg[1] == 'C' && reg[2] == '\0') ||
			(reg[0] == 'D' && reg[1] == 'E' && reg[2] == '\0') ||
			(reg[0] == 'H' && reg[1] == 'L' && reg[2] == '\0') ||
			(reg[0] == 'A' && reg[1] == 'F' && reg[2] == '\0');
}

static char is_R16ix(char *reg)
{
	if (reg == NULL) return 0;
	return  (reg[0] == 'b' && reg[1] == 'c' && reg[2] == '\0') ||
			(reg[0] == 'd' && reg[1] == 'e' && reg[2] == '\0') ||
			(reg[0] == 'i' && reg[1] == 'x' && reg[2] == '\0') ||
			(reg[0] == 's' && reg[1] == 'p' && reg[2] == '\0') ||
			(reg[0] == 'B' && reg[1] == 'C' && reg[2] == '\0') ||
			(reg[0] == 'D' && reg[1] == 'E' && reg[2] == '\0') ||
			(reg[0] == 'I' && reg[1] == 'X' && reg[2] == '\0') ||
			(reg[0] == 'S' && reg[1] == 'P' && reg[2] == '\0');
}

static char is_R16iy(char *reg)
{
	if (reg == NULL) return 0;
	return  (reg[0] == 'b' && reg[1] == 'c' && reg[2] == '\0') ||
			(reg[0] == 'd' && reg[1] == 'e' && reg[2] == '\0') ||
			(reg[0] == 'i' && reg[1] == 'y' && reg[2] == '\0') ||
			(reg[0] == 's' && reg[1] == 'p' && reg[2] == '\0') ||
			(reg[0] == 'B' && reg[1] == 'C' && reg[2] == '\0') ||
			(reg[0] == 'D' && reg[1] == 'E' && reg[2] == '\0') ||
			(reg[0] == 'I' && reg[1] == 'Y' && reg[2] == '\0') ||
			(reg[0] == 'S' && reg[1] == 'P' && reg[2] == '\0');
}

static unsigned char R16(char *reg)
{
	if (reg[0] == 'b' && reg[1] == 'c') return 0;
	if (reg[0] == 'd' && reg[1] == 'e') return 1;
	if (reg[0] == 'h' && reg[1] == 'l') return 2;
	if (reg[0] == 'i' && reg[1] == 'x') return 2;
	if (reg[0] == 'i' && reg[1] == 'y') return 2;
	if (reg[0] == 's' && reg[1] == 'p') return 3;
	if (reg[0] == 'a' && reg[1] == 'f') return 3;
	if (reg[0] == 'B' && reg[1] == 'C') return 0;
	if (reg[0] == 'D' && reg[1] == 'E') return 1;
	if (reg[0] == 'H' && reg[1] == 'L') return 2;
	if (reg[0] == 'I' && reg[1] == 'X') return 2;
	if (reg[0] == 'I' && reg[1] == 'Y') return 2;
	if (reg[0] == 'S' && reg[1] == 'P') return 3;
	if (reg[0] == 'A' && reg[1] == 'F') return 3;
	assert(0);
	return 0;
}

static char is_DE(char *reg)
{
	if (reg == NULL) return 0;
	return ((reg[0] == 'd' && reg[1] == 'e') || (reg[0] == 'D' && reg[1] == 'E')) && reg[2] == '\0';
}

static char is_HL(char *reg)
{
	if (reg == NULL) return 0;
	return ((reg[0] == 'h' && reg[1] == 'l') || (reg[0] == 'H' && reg[1] == 'L')) && reg[2] == '\0';
}

static char is_SP(char *reg)
{
	if (reg == NULL) return 0;
	return ((reg[0] == 's' && reg[1] == 'p') || (reg[0] == 'S' && reg[1] == 'P')) && reg[2] == '\0';
}

static char is_IX(char *reg)
{
	if (reg == NULL) return 0;
	return ((reg[0] == 'i' && reg[1] == 'x') || (reg[0] == 'I' && reg[1] == 'X')) && reg[2] == '\0';
}

static char is_IY(char *reg)
{
	if (reg == NULL) return 0;
	return ((reg[0] == 'i' && reg[1] == 'y') || (reg[0] == 'I' && reg[1] == 'Y')) && reg[2] == '\0';
}

static char is_AF(char *reg)
{
	if (reg == NULL) return 0;
	return ((reg[0] == 'a' && reg[1] == 'f') || (reg[0] == 'A' && reg[1] == 'F')) && reg[2] == '\0';
}

static char is_AFalt(char *reg)
{
	if (reg == NULL) return 0;
	return ((reg[0] == 'a' && reg[1] == 'f') || (reg[0] == 'A' && reg[1] == 'F')) && reg[2] == '\'' && reg[3] == '\0';
}

static char is_expr(char *mem)
{
	//if it starts/ends with a () then that's indirection
	size_t len = strlen(mem);
	if (mem[0] == '(' && mem[len-1]==')') return 0;

	//it it's a register name, that's not an expression
	if (is_R8(mem) || is_R16sp(mem) || is_IX(mem) || is_IY(mem) ||
		is_AF(mem) || is_AFalt(mem) || is_I(mem) || is_R(mem) || is_F(mem) ||
		is_IXL(mem) || is_IXH(mem) || is_IYL(mem) || is_IYH(mem))
		return 0;

	//otherwise any other freeform text is an expression
	return 1;
}

static void add_fixup(ASTATE *s, char *expr, char size)
{
	if (s == NULL)
	{
		assert(0);
		return;
	}

	memset(&s->fixups[s->n_fixups], 0, sizeof(ASM_FIXUP));
	s->fixups[s->n_fixups].address = (unsigned short)(s->n_program + s->n_bytes);
	s->fixups[s->n_fixups].evalfixup = _strdup(expr);
	s->fixups[s->n_fixups].size = size;
	s->fixups[s->n_fixups].lineno = s->lineno;
	s->fixups[s->n_fixups].pc = s->pc;
	s->n_fixups++;
}

static char is_numerici8(char *mem)
{
	size_t len = strlen(mem);
	char *end = NULL;
	if (mem[len - 1] == 'h' || mem[len - 1] == 'H')
	{
		int val = strtol(mem, &end, 16);
		if (val < -128 || val > 255) return 0;
		if (end != NULL && (*end == 'h' || *end == 'H')) return 1;
	}
	else if (mem[0] == '#' || mem[0] == '$')
	{
		int val = strtol(mem+1, &end, 16);
		if (val < -128 || val > 255) return 0;
		if (end != NULL && *end == '\0') return 1;
	}
	else
	{
		int val = strtol(mem, &end, 10);
		if (val < -128 || val > 255) return 0;
		if (end != NULL && *end == '\0') return 1;
	}
	return 0;
}

static char is_numerici16(char *mem);
static char is_imm8(char *mem)
{
	if (mem == NULL) return 0;
	if (is_numerici8(mem)) return 1;
	if (is_numerici16(mem)) return 0;
	if (is_expr(mem))
		return 1;
	return 0;
}

static unsigned char imm8(ASTATE *s, char *mem)
{
	size_t len = strlen(mem);
	char *end;
	unsigned char i8;
	if (mem[len - 1] == 'h' || mem[len - 1] == 'H')
		i8 = (unsigned char)strtol(mem, &end, 16);
	else if (mem[0] == '$' || mem[0] == '#')
		i8 = (unsigned char)strtol(mem+1, &end, 16);
	else
		i8 = (unsigned char)strtol(mem, &end, 10);
	if (*end == '\0' || *end == 'h' || *end == 'H')
		return i8;
	
	add_fixup(s, mem, 8);
	return 0xff;
}

static unsigned char imm8R(ASTATE *s, char *mem)
{
	int n_fixups = s->n_fixups;
	unsigned char i8 = imm8(s,mem);
	if (s->n_fixups != n_fixups)
		s->fixups[n_fixups].i8offset = s->pc + s->n_bytes + 1;
	return i8;
}

static char is_numerici16(char *mem)
{
	if (mem == NULL) return 0;
	size_t len = strlen(mem);
	char *end = NULL;
	if (mem[len - 1] == 'h' || mem[len - 1] == 'H')
	{
		int val = strtol(mem, &end, 16);
		if (val < -32768 || val > 65535) return 0;
		if (end != NULL && (*end == 'h' || *end == 'H')) return 1;
	}
	else if (mem[0] == '#' || mem[0] == '$')
	{
		int val = strtol(mem + 1, &end, 16);
		if (val < -32768 || val > 65535) return 0;
		if (end != NULL && *end == '\0') return 1;
	}
	else
	{
		int val = strtol(mem, &end, 10);
		if (val < -32768 || val > 65535) return 0;
		if (end != NULL && *end == '\0') return 1;
	}
	return 0;
}

static char is_imm16(char *mem)
{
	if (is_numerici16(mem)) return 1;
	if (is_expr(mem))
		return 1;
	return 0;
}

static unsigned short imm16(ASTATE *s, char *mem)
{
	size_t len = strlen(mem);
	char *end;
	unsigned short i16;
	if (mem[len - 1] == 'h' || mem[len - 1] == 'H')
		i16 = (unsigned short)strtol(mem, &end, 16);
	else if (mem[0] == '$' || mem[0] == '#')
		i16 = (unsigned short)strtol(mem+1, &end, 16);
	else
		i16 = (unsigned short)strtol(mem, &end, 10);
	if (*end == '\0' || *end == 'h' || *end == 'H')
		return i16;

	add_fixup(s, mem, 16);
	return 0xffff;
}

static char is_refHL(char *ref)
{
	if (ref == NULL) return 0;
	return _stricmp(ref, "(hl)") == 0;
}

static char is_refBC(char *ref)
{
	if (ref == NULL) return 0;
	return _stricmp(ref, "(bc)") == 0;
}

static char is_refDE(char *ref)
{
	if (ref == NULL) return 0;
	return _stricmp(ref, "(de)") == 0;
}

static char is_refSP(char *ref)
{
	if (ref == NULL) return 0;
	return _stricmp(ref, "(sp)") == 0;
}

static char is_refIX(char *ref)
{
	if (ref == NULL) return 0;
	return _stricmp(ref, "(ix)") == 0;
}

static char is_refIY(char *ref)
{
	if (ref == NULL) return 0;
	return _stricmp(ref, "(iy)") == 0;
}

static char is_refC(char *ref)
{
	if (ref == NULL) return 0;
	return _stricmp(ref, "(c)") == 0;
}

static char is_refIXd(char *ref)
{
	if (ref == NULL) return 0;
	size_t len = strlen(ref);
	if (_strnicmp(ref, "(ix", 3) == 0 && ref[len - 1] == ')')
	{
		char tmp[10] = { 0 };
		strncpy_s(tmp, 10, ref + 3, len - 4);
		return is_imm8(tmp);
	}
	return 0;
}

static char is_refIYd(char *ref)
{
	if (ref == NULL) return 0;
	size_t len = strlen(ref);
	if (_strnicmp(ref, "(iy", 3) == 0 && ref[len - 1] == ')')
	{
		char tmp[10] = { 0 };
		strncpy_s(tmp, 10, ref + 3, len - 4);
		return is_imm8(tmp);
	}
	return 0;
}

static char immd8(ASTATE *s, char *ref)
{
	size_t len = strlen(ref);
	char tmp[10] = { 0 };
	strncpy_s(tmp, 10, ref + 3, len - 4);
	return imm8(s, tmp);
}

static char is_refi8(char *ref)
{
	if (ref == NULL) return 0;
	size_t len = strlen(ref);
	if (ref[0] == '(' && ref[len - 1] == ')')
	{
		char tmp[10] = { 0 };
		strncpy_s(tmp, 10, ref + 1, len - 2);
		return is_imm8(tmp);
	}
	return 0;
}

static unsigned char refi8(ASTATE *s, char *ref)
{
	size_t len = strlen(ref);
	char tmp[10] = { 0 };
	strncpy_s(tmp, 10, ref + 1, len - 2);
	return imm8(s, tmp);
}

static char is_refi16(char *ref)
{
	if (ref == NULL) return 0;
	size_t len = strlen(ref);
	if (ref[0] == '(' && ref[len - 1] == ')')
	{
		char tmp[10] = { 0 };
		strncpy_s(tmp, 10, ref + 1, len - 2);
		return is_imm16(tmp);
	}
	return 0;
}

static unsigned short refi16(ASTATE *s, char *ref)
{
	size_t len = strlen(ref);
	char tmp[10] = { 0 };
	strncpy_s(tmp, 10, ref + 1, len - 2);
	return imm16(s, tmp);
}

static char is_callCC(char *cc)
{
	if (cc == NULL) return 0;
	return (cc[0] == 'n' && cc[1] == 'z' && cc[2] == '\0') ||
		(cc[0] == 'z' && cc[1] == '\0') ||
		(cc[0] == 'n' && cc[1] == 'c' && cc[2] == '\0') ||
		(cc[0] == 'c' && cc[1] == '\0') ||
		(cc[0] == 'p' && cc[1] == 'o' && cc[2] == '\0') ||
		(cc[0] == 'p' && cc[1] == 'e' && cc[2] == '\0') ||
		(cc[0] == 'p' && cc[1] == '\0') ||
		(cc[0] == 'm' && cc[1] == '\0') ||	
		(cc[0] == 'N' && cc[1] == 'Z' && cc[2] == '\0') ||
		(cc[0] == 'Z' && cc[1] == '\0') ||
		(cc[0] == 'N' && cc[1] == 'C' && cc[2] == '\0') ||
		(cc[0] == 'C' && cc[1] == '\0') ||
		(cc[0] == 'P' && cc[1] == 'O' && cc[2] == '\0') ||
		(cc[0] == 'P' && cc[1] == 'E' && cc[2] == '\0') ||
		(cc[0] == 'P' && cc[1] == '\0') ||
		(cc[0] == 'M' && cc[1] == '\0');
}

static unsigned char callCC(char *cc)
{
	if (cc[0] == 'n' && cc[1] == 'z') return 0;
	if (cc[0] == 'z') return 1;
	if (cc[0] == 'n' && cc[1] == 'c') return 2;
	if (cc[0] == 'c') return 3;
	if (cc[0] == 'p' && cc[1] == 'o') return 4;
	if (cc[0] == 'p' && cc[1] == 'e') return 5;
	if (cc[0] == 'p') return 6;
	if (cc[0] == 'm') return 7;

	if (cc[0] == 'N' && cc[1] == 'Z') return 0;
	if (cc[0] == 'Z') return 1;
	if (cc[0] == 'N' && cc[1] == 'C') return 2;
	if (cc[0] == 'C') return 3;
	if (cc[0] == 'P' && cc[1] == 'O') return 4;
	if (cc[0] == 'P' && cc[1] == 'E') return 5;
	if (cc[0] == 'P') return 6;
	if (cc[0] == 'M') return 7;

	assert(0);
	return 0;
}

static char is_jrCC(char *cc)
{
	if (cc == NULL) return 0;
	return (cc[0] == 'n' && cc[1] == 'z' && cc[2] == '\0') ||
		(cc[0] == 'z' && cc[1] == '\0') ||
		(cc[0] == 'n' && cc[1] == 'c' && cc[2] == '\0') ||
		(cc[0] == 'c' && cc[1] == '\0') ||
		(cc[0] == 'N' && cc[1] == 'Z' && cc[2] == '\0') ||
		(cc[0] == 'Z' && cc[1] == '\0') ||
		(cc[0] == 'N' && cc[1] == 'C' && cc[2] == '\0') ||
		(cc[0] == 'C' && cc[1] == '\0');
}

static unsigned char jrCC(char *cc)
{
	if (cc[0] == 'n' && cc[1] == 'z') return 0;
	if (cc[0] == 'z') return 1;
	if (cc[0] == 'n' && cc[1] == 'c') return 2;
	if (cc[0] == 'c') return 3;

	if (cc[0] == 'N' && cc[1] == 'Z') return 0;
	if (cc[0] == 'Z') return 1;
	if (cc[0] == 'N' && cc[1] == 'C') return 2;
	if (cc[0] == 'C') return 3;

	assert(0);
	return 0;
}

static char is_rstN(char *rst)
{
	if (rst == NULL) return 0;
	if (!is_numerici8(rst)) return 0;

	unsigned char i8 = imm8(NULL, rst);
	return (i8 == 0x00 ||
		i8 == 0x08 ||
		i8 == 0x10 ||
		i8 == 0x18 ||
		i8 == 0x20 ||
		i8 == 0x28 ||
		i8 == 0x30 ||
		i8 == 0x38);
}

static unsigned char rstN(char *rst)
{
	unsigned char i8 = imm8(NULL, rst);
	if (i8 == 0x00) return 0;
	if (i8 == 0x08) return 1;
	if (i8 == 0x10) return 2;
	if (i8 == 0x18) return 3;
	if (i8 == 0x20) return 4;
	if (i8 == 0x28) return 5;
	if (i8 == 0x30) return 6;
	if (i8 == 0x38) return 7;
	assert(0);
	return 0;
}

static char is_bitN(char *bit)
{
	if (bit == NULL) return 0;
	return bit[0] >= '0' && bit[0] <= '7' && bit[1] == '\0';
}

static char bitN(char *bit)
{
	return bit[0] - '0';
}

static int escape_to_char(char c)
{
	if (c == '0') return 0;
	if (c == 'a') return 7;
	if (c == 'b') return 8;
	if (c == 't') return 9;
	if (c == 'n') return 10;
	if (c == 'v') return 11;
	if (c == 'f') return 12;
	if (c == 'r') return 13;
	if (c == 'e') return 27;
	if (c == '\'') return '\'';
	if (c == '\\') return '\\';
	if (c == '"') return '"';
	return -1;
}

#define EMIT(x) s->asm[s->n_bytes++] = x
#define EMIT16(x) { unsigned short t = x; EMIT(t&0xff); EMIT(t>>8); }

#pragma warning(push)
#pragma warning(disable: 4100)

static void nop(ASTATE *s) { EMIT(0x00); }
static void ld(ASTATE *s)
{
	// 8-Bit Load Group
	if (is_R8(s->decode.dst) && is_R8(s->decode.src))
	{
		EMIT(0x40 + (R8(s->decode.dst) << 3) + R8(s->decode.src));
	}
	else if (is_R8(s->decode.dst) && (is_IXL(s->decode.src) || is_IXH(s->decode.src)))
	{
		EMIT(0xDD); EMIT(0x40 + (R8(s->decode.dst) << 3) + R8(s->decode.src));
	}
	else if (is_R8(s->decode.dst) && (is_IYL(s->decode.src) || is_IYH(s->decode.src)))
	{
		EMIT(0xFD); EMIT(0x40 + (R8(s->decode.dst) << 3) + R8(s->decode.src));
	}
	else if ((is_IXL(s->decode.dst) || is_IXH(s->decode.dst)) && is_R8(s->decode.src))
	{
		EMIT(0xDD); EMIT(0x40 + (R8(s->decode.dst) << 3) + R8(s->decode.src));
	}
	else if ((is_IYL(s->decode.dst) || is_IYH(s->decode.dst)) && is_R8(s->decode.src))
	{
		EMIT(0xFD); EMIT(0x40 + (R8(s->decode.dst) << 3) + R8(s->decode.src));
	}
	else if ((is_IXL(s->decode.dst) || is_IXH(s->decode.dst)) && (is_IXL(s->decode.src) || is_IXH(s->decode.src)))
	{
		EMIT(0xDD); EMIT(0x40 + (R8(s->decode.dst) << 3) + R8(s->decode.src));
	}
	else if ((is_IYL(s->decode.dst) || is_IYH(s->decode.dst)) && (is_IYL(s->decode.src) || is_IYH(s->decode.src)))
	{
		EMIT(0xFD); EMIT(0x40 + (R8(s->decode.dst) << 3) + R8(s->decode.src));
	}
	else if (is_R8(s->decode.dst) && is_imm8(s->decode.src))
	{
		EMIT(0x06 + (R8(s->decode.dst) << 3)); EMIT(imm8(s, s->decode.src));
	}
	else if ((is_IXL(s->decode.dst) || is_IXH(s->decode.dst)) && is_imm8(s->decode.src))
	{
		EMIT(0xDD); EMIT(0x06 + (R8(s->decode.dst) << 3)); EMIT(imm8(s, s->decode.src));
	}
	else if ((is_IYL(s->decode.dst) || is_IYH(s->decode.dst)) && is_imm8(s->decode.src))
	{
		EMIT(0xFD); EMIT(0x06 + (R8(s->decode.dst) << 3)); EMIT(imm8(s, s->decode.src));
	}
	else if (is_R8(s->decode.dst) && is_refHL(s->decode.src))
	{
		EMIT(0x46 + (R8(s->decode.dst) << 3));
	}
	else if (is_R8(s->decode.dst) && is_refIXd(s->decode.src))
	{
		EMIT(0xDD); EMIT(0x46+(R8(s->decode.dst)<<3)); EMIT(immd8(s, s->decode.src));
	}
	else if (is_R8(s->decode.dst) && is_refIYd(s->decode.src))
	{
		EMIT(0xFD); EMIT(0x46 + (R8(s->decode.dst) << 3)); EMIT(immd8(s, s->decode.src));
	}
	else if (is_refHL(s->decode.dst) && is_R8(s->decode.src))
	{
		EMIT(0x70 + R8(s->decode.src));
	}
	else if (is_refIXd(s->decode.dst) && is_R8(s->decode.src))
	{
		EMIT(0xDD); EMIT(0x70 + R8(s->decode.src)); EMIT(immd8(s, s->decode.dst));
	}
	else if (is_refIYd(s->decode.dst) && is_R8(s->decode.src))
	{
		EMIT(0xFD); EMIT(0x70 + R8(s->decode.src)); EMIT(immd8(s, s->decode.dst));
	}
	else if (is_refHL(s->decode.dst) && is_imm8(s->decode.src))
	{
		EMIT(0x36); EMIT(imm8(s, s->decode.src));
	}
	else if (is_refIXd(s->decode.dst) && is_imm8(s->decode.src))
	{
		EMIT(0xDD); EMIT(0x36); EMIT(immd8(s, s->decode.dst)); EMIT(imm8(s, s->decode.src));
	}
	else if (is_refIYd(s->decode.dst) && is_imm8(s->decode.src))
	{
		EMIT(0xFD); EMIT(0x36); EMIT(immd8(s, s->decode.dst)); EMIT(imm8(s, s->decode.src));
	}
	else if (is_A(s->decode.dst) && is_refBC(s->decode.src))
	{
		EMIT(0x0A);
	}
	else if (is_A(s->decode.dst) && is_refDE(s->decode.src))
	{
		EMIT(0x1A);
	}
	else if (is_A(s->decode.dst) && is_refi16(s->decode.src))
	{
		EMIT(0x3A); EMIT16(refi16(s, s->decode.src));
	}
	else if (is_refBC(s->decode.dst) && is_A(s->decode.src))
	{
		EMIT(0x02);
	}
	else if (is_refDE(s->decode.dst) && is_A(s->decode.src))
	{
		EMIT(0x12);
	}
	else if (is_refi16(s->decode.dst) && is_A(s->decode.src))
	{
		EMIT(0x32); EMIT16(refi16(s, s->decode.dst));
	}
	else if (is_A(s->decode.dst) && is_I(s->decode.src))
	{
		EMIT(0xED); EMIT(0x57);
	}
	else if (is_A(s->decode.dst) && is_R(s->decode.src))
	{
		EMIT(0xED); EMIT(0x5F);
	}
	else if (is_I(s->decode.dst) && is_A(s->decode.src))
	{
		EMIT(0xED); EMIT(0x47);
	}
	else if (is_R(s->decode.dst) && is_A(s->decode.src))
	{
		EMIT(0xED); EMIT(0x4F);
	}

	// 16-Bit Load Group
	else if (is_R16sp(s->decode.dst) && is_imm16(s->decode.src))
	{
		EMIT(0x01 + (R16(s->decode.dst)<<4)); EMIT16(imm16(s, s->decode.src));
	}
	else if (is_IX(s->decode.dst) && is_imm16(s->decode.src))
	{
		EMIT(0xDD); EMIT(0x21); EMIT16(imm16(s, s->decode.src));
	}
	else if (is_IY(s->decode.dst) && is_imm16(s->decode.src))
	{
		EMIT(0xFD); EMIT(0x21); EMIT16(imm16(s, s->decode.src));
	}
	else if (is_HL(s->decode.dst) && is_refi16(s->decode.src))
	{
		EMIT(0x2A); EMIT16(refi16(s, s->decode.src)); // also ED 6B n n
	}
	else if (is_R16sp(s->decode.dst) && is_refi16(s->decode.src))
	{
		EMIT(0xED); EMIT(0x4B + (R16(s->decode.dst) << 4)); EMIT16(refi16(s, s->decode.src));
	}
	else if (is_IX(s->decode.dst) && is_refi16(s->decode.src))
	{
		EMIT(0xDD); EMIT(0x2A); EMIT16(refi16(s, s->decode.src));
	}
	else if (is_IY(s->decode.dst) && is_refi16(s->decode.src))
	{
		EMIT(0xFD); EMIT(0x2A); EMIT16(refi16(s, s->decode.src));
	}
	else if (is_refi16(s->decode.dst) && is_HL(s->decode.src))
	{
		EMIT(0x22); EMIT16(refi16(s, s->decode.dst)); // also ED 63 n n 
	}
	else if (is_refi16(s->decode.dst) && is_R16sp(s->decode.src))
	{
		EMIT(0xED); EMIT(0x43 + (R16(s->decode.src) << 4)); EMIT16(refi16(s, s->decode.dst))
	}
	else if (is_refi16(s->decode.dst) && is_IX(s->decode.src))
	{
		EMIT(0xDD); EMIT(0x22); EMIT16(refi16(s, s->decode.dst));
	}
	else if (is_refi16(s->decode.dst) && is_IY(s->decode.src))
	{
		EMIT(0xFD);  EMIT(0x22); EMIT16(refi16(s, s->decode.dst));
	}
	else if (is_SP(s->decode.dst) && is_HL(s->decode.src))
	{
		EMIT(0xF9);
	}
	else if (is_SP(s->decode.dst) && is_IX(s->decode.src))
	{
		EMIT(0xDD); EMIT(0xF9);
	}
	else if (is_SP(s->decode.dst) && is_IY(s->decode.src))
	{
		EMIT(0xFD); EMIT(0xF9);
	}
	else
	{
		s->error = 1;
	}
}
static char incdec(ASTATE *s, unsigned char op)
{
	if (is_R8(s->decode.dst))
	{
		EMIT(op + (R8(s->decode.dst) << 3));
	}
	else if (is_IXL(s->decode.dst) || is_IXH(s->decode.dst))
	{
		EMIT(0xDD); EMIT(op + (R8(s->decode.dst) << 3));
	}
	else if (is_IYL(s->decode.dst) || is_IYH(s->decode.dst))
	{
		EMIT(0xFD); EMIT(op + (R8(s->decode.dst) << 3));
	}
	else if (is_refHL(s->decode.dst))
	{
		EMIT(0x30 + op);
	}
	else if (is_refIXd(s->decode.dst))
	{
		EMIT(0xDD); EMIT(0x30 + op); EMIT(immd8(s, s->decode.dst));
	}
	else if (is_refIYd(s->decode.dst))
	{
		EMIT(0xFD); EMIT(0x30 + op); EMIT(immd8(s, s->decode.dst));
	}
	else
	{
		return 0;
	}
	return 1;
}
static void inc(ASTATE *s)
{
	if (!incdec(s, 0x04))
	{
		if (is_R16sp(s->decode.dst))
		{
			EMIT(0x03 + (R16(s->decode.dst)<<4));
		}
		else if (is_IX(s->decode.dst))
		{
			EMIT(0xDD); EMIT(0x23);
		}
		else if (is_IY(s->decode.dst))
		{
			EMIT(0xFD); EMIT(0x23);
		}
		else
		{
			s->error = 1;
		}
	}
}
static void decode(ASTATE *s)
{
	if (!incdec(s, 0x05))
	{
		if (is_R16sp(s->decode.dst))
		{
			EMIT(0x0B + (R16(s->decode.dst)<<4));
		}
		else if (is_IX(s->decode.dst))
		{
			EMIT(0xDD); EMIT(0x2B);
		}
		else if (is_IY(s->decode.dst))
		{
			EMIT(0xFD); EMIT(0x2B);
		}
		else
		{
			s->error = 1;
		}
	}
}
static void rlca(ASTATE *s) { EMIT(0x7); }
static void ex(ASTATE *s)
{
	if (is_DE(s->decode.dst) && is_HL(s->decode.src))
	{
		EMIT(0xEB);
	}
	else if (is_AF(s->decode.dst) && is_AFalt(s->decode.src))
	{
		EMIT(0x08);
	}
	else if (is_refSP(s->decode.dst) && is_HL(s->decode.src))
	{
		EMIT(0xE3);
	}
	else if (is_refSP(s->decode.dst) && is_IX(s->decode.src))
	{
		EMIT(0xDD); EMIT(0xE3);
	}
	else if (is_refSP(s->decode.dst) && is_IY(s->decode.src))
	{
		EMIT(0xFD); EMIT(0xE3);
	}
	else
	{
		s->error = 1;
	}
}
static void exx(ASTATE *s) { EMIT(0xD9); }
static void rrca(ASTATE *s) { EMIT(0x0F); }
static void djnz(ASTATE *s)
{
	if (is_imm8(s->decode.dst))
	{
		EMIT(0x10); EMIT(imm8R(s, s->decode.dst));
	}
	else
	{
		s->error = 1;
	}
}
static void rla(ASTATE *s) { EMIT(0x17); }
static void rra(ASTATE *s) { EMIT(0x1F); }
static void jr(ASTATE *s)
{
	if (is_jrCC(s->decode.cond) && is_imm8(s->decode.target))
	{
		EMIT(0x20 + (jrCC(s->decode.cond) << 3)); EMIT(imm8R(s, s->decode.target));
	}
	else if (is_imm8(s->decode.cond))
	{
		EMIT(0x18); EMIT(imm8R(s, s->decode.cond));
	}
	else
	{
		s->error = 1;
	}
}
static void daa(ASTATE *s) { EMIT(0x27); }
static void cpl(ASTATE *s) { EMIT(0x2F); }
static void scf(ASTATE *s) { EMIT(0x37); }
static void ccf(ASTATE *s) { EMIT(0x3F); }
static void halt(ASTATE *s) { EMIT(0x76); }
static char alu8(ASTATE *s, unsigned char op)
{
	//remove explicit A dst
	if (is_A(s->decode.dst) && s->decode.src && (is_R8(s->decode.src) || is_imm8(s->decode.src) || is_refHL(s->decode.src) || is_refIXd(s->decode.src) || is_refIYd(s->decode.src)))
		s->decode.dst = s->decode.src;

	if (is_R8(s->decode.dst))
	{
		EMIT(0x80 + op + R8(s->decode.dst));
	}
	else if (is_refHL(s->decode.dst))
	{
		EMIT(0x86 + op);
	}
	else if (is_IXL(s->decode.dst) || is_IXH(s->decode.dst))
	{
		EMIT(0xDD); EMIT(0x80+op + R8(s->decode.dst));
	}
	else if (is_IYL(s->decode.dst) || is_IYH(s->decode.dst))
	{
		EMIT(0xFD); EMIT(0x80 + op + R8(s->decode.dst));
	}
	else if (is_imm8(s->decode.dst))
	{
		EMIT(0xC6 + op); EMIT(imm8(s, s->decode.dst));
	}
	else if (is_refIXd(s->decode.dst))
	{
		EMIT(0xDD); EMIT(0x86 + op); EMIT(immd8(s, s->decode.dst));
	}
	else if (is_refIYd(s->decode.dst))
	{
		EMIT(0xFD); EMIT(0x86 + op); EMIT(immd8(s, s->decode.dst));
	}
	else
	{
		return 0;
	}
	return 1;
}
static void add(ASTATE *s)
{
	if (!alu8(s, 0x0 << 3))
	{
		// 16-Bit Arithmetic Group
		if (is_HL(s->decode.dst) && is_R16sp(s->decode.src))
		{
			EMIT(0x09 + (R16(s->decode.src)<<4));
		}
		else if (is_IX(s->decode.dst) && is_R16ix(s->decode.src))
		{
			EMIT(0xDD); EMIT(0x09 + (R16(s->decode.src) << 4));
		}
		else if (is_IY(s->decode.dst) && is_R16iy(s->decode.src))
		{
			EMIT(0xFD); EMIT(0x09 + (R16(s->decode.src) << 4));
		}
		else
		{
			s->error = 1;
		}
	}
}
static void adc(ASTATE *s)
{
	if (!alu8(s, 0x1 << 3))
	{
		// 16-Bit Arithmetic Group
		if (is_HL(s->decode.dst) && is_R16sp(s->decode.src))
		{
			EMIT(0xED); EMIT(0x4A + (R16(s->decode.src) << 4));
		}
		else
		{
			s->error = 1;
		}
	}
}
static void sub(ASTATE *s)
{
	if (!alu8(s, 0x2 << 3))
		s->error = 1;
}
static void sbc(ASTATE *s)
{
	if (!alu8(s, 0x3 << 3))
	{
		// 16-Bit Arithmetic Group
		if (is_HL(s->decode.dst) && is_R16sp(s->decode.src))
		{
			EMIT(0xED); EMIT(0x42 + (R16(s->decode.src) << 4));
		}
		else
		{
			s->error = 1;
		}
	}
}
static void and(ASTATE *s)
{
	if (!alu8(s, 0x4 << 3))
		s->error = 1;
}
static void xor(ASTATE *s)
{
	if (!alu8(s, 0x5 << 3))
		s->error = 1;
}
static void or(ASTATE * s)
{
	if (!alu8(s, 0x6 << 3))
		s->error = 1;
}
static void cp(ASTATE *s)
{
	if (!alu8(s, 0x7 << 3))
		s->error = 1;
}
static void ret(ASTATE *s)
{
	if (is_callCC(s->decode.cond))
	{
		EMIT(0xC0 + (callCC(s->decode.cond)<<3));
	}
	else
	{
		EMIT(0xC9);
	}
}
static void pop(ASTATE *s)
{
	if (is_R16af(s->decode.dst))
	{
		EMIT(0xC1 + (R16(s->decode.dst) << 4));
	}
	else if (is_IX(s->decode.dst))
	{
		EMIT(0xDD); EMIT(0xE1);
	}
	else if (is_IY(s->decode.dst))
	{
		EMIT(0xFD); EMIT(0xE1);
	}
	else
	{
		s->error = 1;
	}
}
static void jp(ASTATE *s)
{
	if (is_callCC(s->decode.cond) && is_imm16(s->decode.target))
	{
		EMIT(0xC2 + (callCC(s->decode.cond) << 3)); EMIT16(imm16(s, s->decode.target));
	}
	else if (is_imm16(s->decode.cond))
	{
		EMIT(0xC3); EMIT16(imm16(s, s->decode.cond));
	}
	else if (is_refHL(s->decode.cond))
	{
		EMIT(0xE9);
	}
	else if (is_refIX(s->decode.cond))
	{
		EMIT(0xDD);
		EMIT(0xE9);
	}
	else if (is_refIY(s->decode.cond))
	{
		EMIT(0xFD);
		EMIT(0xE9);
	}
	else
	{
		s->error = 1;
	}
}
static void call(ASTATE *s)
{
	if (is_callCC(s->decode.cond) && is_imm16(s->decode.target))
	{
		EMIT(0xC4 + (callCC(s->decode.cond) << 3)); EMIT16(imm16(s, s->decode.target));
	}
	else if (is_imm16(s->decode.cond))
	{
		EMIT(0xCD); EMIT16(imm16(s, s->decode.cond));
	}
	else
	{
		s->error = 1;
	}
}
static void push(ASTATE *s)
{
	if (is_R16af(s->decode.dst))
	{
		EMIT(0xC5 + (R16(s->decode.dst) << 4));
	}
	else if (is_IX(s->decode.dst))
	{
		EMIT(0xDD); EMIT(0xE5);
	}
	else if (is_IY(s->decode.dst))
	{
		EMIT(0xFD); EMIT(0xE5);
	}
	else
	{
		s->error = 1;
	}
}
static void rst(ASTATE *s)
{
	if (is_rstN(s->decode.dst))
	{
		EMIT(0xC7 + (rstN(s->decode.dst)<<3));
	}
	else
	{
		s->error = 1;
	}
}

static void in(ASTATE *s){
	
	if (is_A(s->decode.dst) && is_refi8(s->decode.src))
	{
		EMIT(0xDB); EMIT(refi8(s, s->decode.src));
	}
	else if (is_R8(s->decode.dst) && is_refC(s->decode.src))
	{
		EMIT(0xED); EMIT(0x40 + (R8(s->decode.dst) << 3));
	}
	else if (is_F(s->decode.dst) && is_refC(s->decode.src))
	{
		EMIT(0xED); EMIT(0x70);
	}
	else
	{
		s->error = 1;
	}
}

static void out(ASTATE *s)
{
	if (is_refi8(s->decode.dst) && is_A(s->decode.src))
	{
		EMIT(0xD3); EMIT(refi8(s, s->decode.dst));
	}
	else if (is_refC(s->decode.dst) && is_R8(s->decode.src))
	{
		EMIT(0xED); EMIT(0x41+(R8(s->decode.src)<<3));
	}
	else if (is_refC(s->decode.dst) && is_imm8(s->decode.src) && imm8(s, s->decode.src) == 0)
	{
		EMIT(0xED); EMIT(0x71);
	}
	else
	{
		s->error = 1;
	}
}
static void di(ASTATE *s) { EMIT(0xF3); }
static void ei(ASTATE *s) { EMIT(0xFB); }
static void alushift(ASTATE *s, unsigned char op)
{
	if (is_R8(s->decode.dst))
	{
		EMIT(0xCB); EMIT(0x00 + op + R8(s->decode.dst));
	}
	else if (is_refHL(s->decode.dst))
	{
		EMIT(0xCB); EMIT(0x06 + op);
	}
	else if (is_refIXd(s->decode.dst) && is_R8(s->decode.src))
	{
		EMIT(0xDD); EMIT(0xCB); EMIT(immd8(s, s->decode.dst)); EMIT(op+R8(s->decode.src));
	}
	else if (is_refIYd(s->decode.dst) && is_R8(s->decode.src))
	{
		EMIT(0xFD); EMIT(0xCB); EMIT(immd8(s, s->decode.dst)); EMIT(op+R8(s->decode.src));
	}
	else if (is_refIXd(s->decode.dst))
	{
		EMIT(0xDD); EMIT(0xCB); EMIT(immd8(s, s->decode.dst)); EMIT(0x06 + op);
	}
	else if (is_refIYd(s->decode.dst))
	{
		EMIT(0xFD); EMIT(0xCB); EMIT(immd8(s, s->decode.dst)); EMIT(0x06 + op);
	}
	else
	{
		s->error = 1;
	}
}
static void rlc(ASTATE *s) { alushift(s, 0 << 3); }
static void rl(ASTATE *s) { alushift(s, 2 << 3); }
static void rrc(ASTATE *s){ alushift(s, 1 << 3); }
static void rr(ASTATE *s){ alushift(s, 3 << 3); }
static void sla(ASTATE *s){ alushift(s, 4 << 3); }
static void sll(ASTATE *s) { alushift(s, 6 << 3); }
static void sra(ASTATE *s){ alushift(s, 5 << 3); }
static void srl(ASTATE *s){ alushift(s, 7 << 3); }
static void bit(ASTATE *s){

	if (is_bitN(s->decode.dst))
	{
		if (is_R8(s->decode.src))
		{
			EMIT(0xCB); EMIT(0x40 + (bitN(s->decode.dst) << 3) + R8(s->decode.src));
		}
		else if (is_refHL(s->decode.src))
		{
			EMIT(0xCB); EMIT(0x46 + (bitN(s->decode.dst) << 3));
		}
		//NB bit n,(ix+d),r doesn't exist
		else if (is_refIXd(s->decode.src))
		{
			EMIT(0xDD); EMIT(0xCB); EMIT(immd8(s, s->decode.src)); EMIT(0x46 + (bitN(s->decode.dst) << 3));
		}
		else if (is_refIYd(s->decode.src))
		{
			EMIT(0xFD); EMIT(0xCB); EMIT(immd8(s, s->decode.src)); EMIT(0x46 + (bitN(s->decode.dst) << 3));
		}
		else
		{
			s->error = 1;
		}
	}
	else
	{
		s->error = 1;
	}
}
static void resset(ASTATE *s, unsigned char op)
{
	if (is_bitN(s->decode.dst))
	{
		if (is_R8(s->decode.src))
		{
			EMIT(0xCB); EMIT(op + (bitN(s->decode.dst) << 3) + R8(s->decode.src));
		}
		else if (is_refHL(s->decode.src))
		{
			EMIT(0xCB); EMIT(op + (bitN(s->decode.dst) << 3) + 6);
		}
		else if (is_refIXd(s->decode.src) && is_R8(s->decode.extra))
		{
			EMIT(0xDD); EMIT(0xCB); EMIT(immd8(s, s->decode.src)); EMIT(op + (bitN(s->decode.dst) << 3) + R8(s->decode.extra));
		}
		else if (is_refIYd(s->decode.src) && is_R8(s->decode.extra))
		{
			EMIT(0xFD); EMIT(0xCB); EMIT(immd8(s, s->decode.src)); EMIT(op + (bitN(s->decode.dst) << 3) + R8(s->decode.extra));
		}
		else if (is_refIXd(s->decode.src))
		{
			EMIT(0xDD); EMIT(0xCB); EMIT(immd8(s, s->decode.src)); EMIT(op + (bitN(s->decode.dst) << 3) + 0x06);
		}
		else if (is_refIYd(s->decode.src))
		{
			EMIT(0xFD); EMIT(0xCB); EMIT(immd8(s, s->decode.src)); EMIT(op + (bitN(s->decode.dst) << 3) + 0x06);
		}
		else
		{
			s->error = 1;
		}
	}
	else
	{
		s->error = 1;
	}
}
static void res(ASTATE *s) { resset(s, 0x2 << 6); }
static void set(ASTATE *s) { resset(s, 0x3 << 6); }
static void neg(ASTATE *s) { EMIT(0xED); EMIT(0x44); }
static void reti(ASTATE *s) { EMIT(0xED); EMIT(0x4D); }
static void retn(ASTATE *s) { EMIT(0xED); EMIT(0x45); }
static void im(ASTATE *s)
{
	if (is_imm8(s->decode.dst) && imm8(s, s->decode.dst)<3)
	{
		EMIT(0xED);
		unsigned char i8 = imm8(s, s->decode.dst);
		if (i8 == 0) EMIT(0x46);
		else if (i8 == 1) EMIT(0x56);
		else if (i8 == 2) EMIT(0x5E);
	}
	else
	{
		s->error = 1;
	}
}
static void ldi(ASTATE *s){ EMIT(0xED); EMIT(0xA0); }
static void cpi(ASTATE *s){ EMIT(0xED); EMIT(0xA1); }
static void ini(ASTATE *s) { EMIT(0xED); EMIT(0xA2); }
static void outi(ASTATE *s){ EMIT(0xED); EMIT(0xA3); }
static void ldd(ASTATE *s){ EMIT(0xED); EMIT(0xA8); }
static void cpd(ASTATE *s){ EMIT(0xED); EMIT(0xA9); }
static void ind(ASTATE *s){ EMIT(0xED); EMIT(0xAA); }
static void outd(ASTATE *s){ EMIT(0xED); EMIT(0xAB); }
static void ldir(ASTATE *s){ EMIT(0xED); EMIT(0xB0); }
static void cpir(ASTATE *s){ EMIT(0xED); EMIT(0xB1); }
static void inir(ASTATE *s){ EMIT(0xED); EMIT(0xB2); }
static void otir(ASTATE *s){ EMIT(0xED); EMIT(0xB3); }
static void lddr(ASTATE *s){ EMIT(0xED); EMIT(0xB8); }
static void cpdr(ASTATE *s){ EMIT(0xED); EMIT(0xB9); }
static void indr(ASTATE *s){ EMIT(0xED); EMIT(0xBA); }
static void otdr(ASTATE *s){ EMIT(0xED); EMIT(0xBB); }
static void rld(ASTATE *s) { EMIT(0xED); EMIT(0x6F); }
static void rrd(ASTATE *s) { EMIT(0xED); EMIT(0x67); }
static void defb(ASTATE *s)
{
	if (s->decode.n_bits <= 1)
	{
		s->error = 1;
		return;
	}
	for (int i = 1; i < s->decode.n_bits; i++)
	{
		char *expr = s->decode.bits[i];
		if (is_imm8(expr))
		{
			EMIT(imm8(s, expr));
		}
		else
		{
			s->error = 1;
			break;
		}
	}
}
static void defw(ASTATE *s)
{
	if (s->decode.n_bits <= 1)
	{
		s->error = 1;
		return;
	}
	for (int i = 1; i < s->decode.n_bits; i++)
	{
		char *expr = s->decode.bits[i];
		if (is_imm16(expr))
		{
			EMIT16(imm16(s, expr));
		}
		else
		{
			s->error = 1;
			break;
		}
	}
}
static void defm(ASTATE *s)
{
	if (s->decode.n_bits <= 1)
	{
		s->error = 1;
		return;
	}
	for (int i = 1; i < s->decode.n_bits; i++)
	{
		char *txt = s->decode.bits[i];
		size_t len = strlen(txt);
		if (len < 2 || txt[0] != '"' || txt[len - 1] != '"') { s->error = 1; return; }
		txt++;
		while (*txt != '"')
		{
			int t = *txt++;
			if (t == '\\')
			{
				t = escape_to_char(*txt++);
				if (t == -1) { s->error = 1; return; }
			}
			EMIT((unsigned char)t);
		}
	}
}

static void org(ASTATE *s)
{

	if (is_imm16(s->decode.dst))
	{
		if (s->have_seen_org)
		{
			unsigned short newpc = imm16(s, s->decode.dst);
			if (newpc >= s->pc)
			{
				//pad with NOPs
				while (s->pc++ < newpc)
					EMIT(0x00);
			}
			else
			{
				//new pc is less than current pc!
				//s->error = 1;
				elog("new ORG specified, but it's %d bytes behind current pc (old: %04X new: %04X)\n", s->pc-newpc, s->pc, newpc);
				//this is an error! but temporarily, just step back
				s->n_program -= s->pc - newpc;
				s->pc = newpc;
			}
		}
		else
		{
			s->have_seen_org = 1;
			s->org = s->pc = imm16(s, s->decode.dst);
		}
	}
	else
	{
		s->error = 1;
	}
}

typedef struct
{
	const char *op;
	void (*fn)(ASTATE *);
} PARSER;

//todo: reorder these so common instructions are first.
static const PARSER parsers[] =
{
	{"nop",nop},
	{"ld",ld},
	{"inc",inc},
	{"dec",decode},
	{"rlca",rlca},
	{"rld", rld},
	{"rrd", rrd},
	{"ex",ex},
	{"exx",exx},
	{"rrca",rrca},
	{"djnz",djnz},
	{"rla",rla},
	{"rra",rra},
	{"jr",jr},
	{"daa",daa},
	{"cpl",cpl},
	{"scf",scf},
	{"ccf",ccf},
	{"halt", halt},
	{"add",add},
	{"and",and},
	{"adc",adc},
	{"sub",sub},
	{"sbc",sbc},
	{"xor", xor},
	{"or", or},
	{"cp",cp},
	{"ret",ret},
	{"pop",pop},
	{"jp",jp},
	{"call",call},
	{"push",push },
	{"rst",rst},
	{"in",in},
	{"out",out},
	{"di",di},
	{"ei",ei},
	{"rlc",rlc},
	{"rrc",rrc},
	{"rl",rl},
	{"rr",rr},
	{"sla",sla},
	{"sra",sra},
	{"sll",sll},
	{"srl",srl},
	{"bit",bit},
	{"res",res},
	{"set",set},
	{"neg",neg},
	{"reti",reti},
	{"retn",retn},
	{"im",im},
	{"ldi",ldi},
	{"cpi",cpi},
	{"ini",ini},
	{"outi",outi},
	{"ldd",ldd},
	{"cpd",cpd},
	{"ind",ind},
	{"outd",outd},
	{"ldir",ldir},
	{"cpir",cpir},
	{"inir",inir},
	{"otir",otir},
	{"lddr",lddr},
	{"cpdr",cpdr},
	{"indr",indr},
	{"otdr",otdr},
	{"defb", defb},
	{"defw", defw},
	{"defm", defm},
	{"org", org},
	{NULL,NULL}
};

static char *find_comment(char *line)
{
	char *c = line;
	int in_literal = 0;//1 double quotes, 2 single quotes
	while (*c != '\0')
	{
		if (*c == '\'' && in_literal == 0)
		{
			if (c - line <= 1 || !((c[-2] == 'A' && c[-1] == 'F') || (c[-2] == 'a' && c[-1] == 'f')))
				in_literal = 2;
		}
		else if (*c == '\'' && in_literal == 2) in_literal = 0;

		if (*c == '"' && in_literal == 0) in_literal = 1;
		else if (*c == '"' && in_literal == 1) in_literal = 0;

		if (*c == '\\' && in_literal) { c++; if (*c == '\0') return NULL; }

		if (*c == ';' && !in_literal)
			return c;
		if (*c == '/' && *(c + 1) == '/') return c;

		c++;
	}
	return NULL;
}

static char *trim(char *line)
{
	if (line != NULL)
	{
		char *c = line + strlen(line) - 1;
		while (c > line && isspace((unsigned char)*c))
			c--;
		*(c+1) = '\0';
		c = line;
		while (*c != '\0' && isspace((unsigned char)*c))
			c++;
		return c;
	}
	return line;
}

static char is_delimiter(char c, const char *delimiters)
{
	while (*delimiters)
		if (c == *delimiters++) return 1;
	return 0;
}

static char *smart_strtok_s_internal(char *str, const char *delimiters, char **context)
{
	if (str == NULL) str = *context;
	if (*str == '\0') { return NULL; }
	char *c = str;
	int in_literal = 0;//1 double quotes, 2 single quotes
	while (*c != '\0' && is_delimiter(*c, delimiters)) c++, str++;
	if (*str == '\0') return NULL;
	while (*c != '\0')
	{
		if (*c == '\'' && in_literal == 0)
		{
			if (c - str <= 1 || !((c[-2] == 'A' && c[-1] == 'F') || (c[-2] == 'a' && c[-1] == 'f')))
				in_literal = 2;
		}
		else if (*c == '\'' && in_literal == 2) in_literal = 0;

		if (*c == '"' && in_literal == 0) in_literal = 1;
		else if (*c == '"' && in_literal == 1) in_literal = 0;

		if (*c == '\\' && in_literal) { c++; if (*c == '\0') { *context = c; return str; } }

		if (is_delimiter(*c, delimiters) && !in_literal)
		{
			*c++ = '\0';
			while (*c != '\0' && is_delimiter(*c, delimiters)) c++;
			*context = c;
			return str;
		}

		c++;
	}
	*context = c;
	return str;
}

char *smart_strtok_s(char *str, const char *delimiters, char **context)
{
	//char *s = strtok_s(str, delimiters, context);
	//elog("%s || %s\n", s, *context);
	//return s;
	//works the same as strtok_s except that it won't delimit within a string or character constant
	char *s = smart_strtok_s_internal(str, delimiters, context);
	//elog("%s || %s\n", s, *context);
	return s;
}

static int asm(char *line, ASTATE *status)
{
	status->n_bytes = 0;

	//nothing to see here
	if (line[0] == ';' || line[0] == '\0' || line[0] == '\r' || line[0] == '\n')
		return 0;

	//copy other the useful bit of the line
	char ins[100];
	char *c = trim(find_comment(line));
	if (c == NULL)
	{
		if (strlen(line) >= 100)
		{
			status->error = 2;
			return -1;
		}

		strcpy_s(ins, 100, line);
	}
	else
	{
		if (c - line >= 100)
		{
			status->error = 2;
			return -1;
		}

		strncpy_s(ins, 100, line, c - line);
		ins[c - line] = '\0';
	}
	status->src = line;

	char *ctx=NULL;

	memset(&status->decode, 0, sizeof status->decode);

	const char *sep = " ,;\t\r\n";

	char *bits = trim(smart_strtok_s(ins, sep, &ctx));
	if (bits == NULL)
		return 0;

	//first character on line is an assembler directive
	if (ins[0] == '#')
		return 0;

	//first character on line is a letter. make it a label, unless it's org.
	if (isalpha((unsigned char)ins[0]) && _strnicmp(ins, "ORG", 3) != 0)
	{
		status->labels[status->n_labels].address = status->pc;
		status->labels[status->n_labels].label = _strdup(bits);
		status->n_labels++;
		bits = trim(smart_strtok_s(NULL, sep, &ctx));
	}

	if (bits == NULL)
		return 0;

	while (bits != NULL && status->decode.n_bits < MAX_BITS)
	{
		sep = ",;\r\n";
		status->decode.bits[status->decode.n_bits++] = bits;
		bits = trim(smart_strtok_s(NULL,sep, &ctx));
	}

	const PARSER *p = parsers;
	while (p->fn != NULL)
	{
		if (_stricmp(p->op, status->decode.ins) == 0)
		{
			p->fn(status);
			break;
		}
		p++;
	}
	if (p->fn == NULL)
	{
		elog("error, unknown op %s : %s\n", ins, status->src);
		return -1;
	}
	else if (status->error)
	{
		elog("############# error %d at column %d: %s\n", status->error, status->position, status->src);
		return -1;
	}

	status->pc += status->n_bytes;

	return 0;
}

static int run_fixups(ASTATE *s);
static void dump_bytes(ASTATE *s);

void assemble(char *originalsrc)
{
	const char *sep = "\r\n";
	char *context = NULL;
	char *src = NULL;
	ASTATE *status = NULL;

	src = _strdup(originalsrc);
	if (src == NULL) goto xit;

	status = calloc(1, sizeof * status);
	if (status == NULL) goto xit;

	char *ins = strtok_s(src, sep, &context);
	
	while (ins != NULL)
	{
		status->lineno++;//no good. strtok removes blank lines

		asm(ins, status);
		if (status->error) break;

		//maintain the entire assembly
		memcpy(status->program + status->n_program, status->asm, status->n_bytes);
		status->n_program += status->n_bytes;
		
		ins = strtok_s(NULL, sep, &context);
	}

	if (status->error)
	{
		elog("error %d at line %d\n", status->error, status->lineno+1);
		goto xit;
	}

	//dump_bytes(status);

	if (run_fixups(status) != 0)
		goto xit;

	//dump_bytes(status);

xit:
	//dump_bytes(status);
	//{
	//	FILE *f;
	//	fopen_s(&f, "rom48.bin", "wb");
	//	if (f != NULL)
	//	{
	//		fwrite(status->program, status->n_program, 1, f);
	//		fclose(f);
	//	}
	//}

	if (status != NULL)
	{
#pragma warning(push)
#pragma warning(disable: 6001)
		for (int i = 0; i < status->n_labels; i++)
			free(status->labels[i].label);
		for (int i = 0; i < status->n_fixups; i++)
			free(status->fixups[i].evalfixup);
#pragma warning(pop)
		free(status);
	}

	if (src != NULL) free(src);
}

static void dump_bytes(ASTATE *status)
{
	DASM_RES res = { 0 };
	unsigned char *p = status->program;
	unsigned short pc = status->org;
	int line = 2;
	elog("1  \torg %04X\n", pc);
	while (p < status->program + status->n_program)
	{
		int len = DAsmExMem(p, pc, &res);
		elog("%-3d ", line++);
		elog("%04X ", pc);
		elog("\t%-20s", res.instr);
		for (int i = 0; i < len; i++)
			elog("%02X ", res.bytes[i]);
		for (int i = len; i < 4; i++)
			elog("   ");
		for (int i = 0; i < len; i++)
			elog("%c", (res.bytes[i]>=32&&res.bytes[i]<=127) ? res.bytes[i]:'.');
		elog("\n");
		p += len;
		pc += (unsigned short)len;
	}
}

typedef struct
{
	char *expr;
	ASM_LABEL *labels;
	int n_labels;
	int error;
	char *errorpos;
	unsigned short pc;
} EXPR_STATE;

#define UNARY_MINUS '}'
#define UNARY_PLUS '{'

static void whitespace(EXPR_STATE *state)
{
	while (isspace((unsigned char)*state->expr))
		state->expr++;
}

static char search_label(EXPR_STATE *state, char *name, size_t len)
{
	for (int i = 0; i < state->n_labels; i++)
	{
		if (strncmp(name, state->labels[i].label, len) == 0 && strlen(state->labels[i].label) <= len)
		{
			//elog(">1 matching expr %s (%d) with label %s\n", name, len, state->labels[i].label);
			return 1;
		}
	}
	return 0;
}

//letter followed by letters, numbers, _, -
//maximum munch to deal with labels that have '-' in them
static char is_label(EXPR_STATE *state, char *s, size_t *len)
{
	if (s == NULL) return 0;
	if (s[0] == '\0') return 0;
	if (!isalpha((unsigned char)*s)) return 0;
	char label[100];
	char *m;
	strcpy_s(label, 100, s);
	do
	{
		s = label;
		char *start = s++;
		while (isalnum((unsigned char)*s) || *s == '_' || *s == '-') s++;
		*len = s - start;
		//if we find it, then it's a label
		if (search_label(state, label, *len)) return 1;
		//else, if there's a '-' in the name, try again up to the '-'
		m = strrchr(label, '-');
		if (m) *m = '\0';
	} while (m);
	return 0;
}

static int labelv(EXPR_STATE *state, size_t len)
{
	for (int i = 0; i < state->n_labels; i++)
	{
		if (strncmp(state->expr, state->labels[i].label, len) == 0 && strlen(state->labels[i].label) <= len)
		{
			//elog(">2 matching expr %s (%d) with label %s\n", state->expr, len, state->labels[i].label);
			return state->labels[i].address;
		}
	}
	state->error = 1;
	return 0;
}

static char is_binary_numeric(char *mem, size_t *len)
{
	//% followed by 8 or 16 0/1s
	if (mem[0] != '%') return 0;
	for (int i = 0; i < 8; i++)
		if (mem[i + 1] != '0' && mem[i + 1] != '1') return 0;
	*len = 9;
	if (mem[9] == '1' || mem[9] == '0')
	{
		for (int i = 0; i < 8; i++)
			if (mem[i + 1+8] != '0' && mem[i + 1+8] != '1') return 0;
		*len = 16;
	}
	return 1;
}

static char is_numeric(char *mem, size_t *len)
{
	if (mem == NULL) return 0;
	char *end = NULL;
	int val;
	val = strtol(mem, &end, 16);
	if (end != NULL && *end == 'h')
	{
		*len = end - mem + 1;
		return 1;
	}
	val = strtol(mem, &end, 10);
	if (end != NULL && end != mem)
	{
		*len = end - mem;
		return 1;
	}
	val = strtol(mem + 1, &end, 16);
	if (mem[0] == '$' && !isxdigit(mem[1])) return 0;
	if ((mem[0] == '$' || mem[0] == '#') && end != NULL && end != mem + 1)
	{
		*len = end - mem;
		return 1;
	}
	if (mem[0] == '\'' && mem[1] > 0 && mem[2] == '\'')
	{
		*len = 3;
		return 1;
	}
	if (mem[0] == '\'' && mem[1] == '\\' && mem[2] > 0 && mem[3] == '\'')
	{
		*len = 4;
		return 1;
	}
	return 0;
}

static int number(EXPR_STATE *s, char *mem, size_t len)
{
	char *end;
	int number;
	if (mem[len - 1] == 'h')
		{ number = strtol(mem, &end, 16); len--; }
	else if (mem[0] == '#' || mem[0] == '$')
		number = strtol(mem+1, &end, 16);
	else if (mem[0] == '%')
		number = strtol(mem + 1, &end, 2);
	else if (len == 3 && mem[0] == '\'' && mem[2] == '\'') return mem[1];
	else if (len == 4 && mem[0] == '\'' && mem[1] == '\\' && mem[3] == '\'')
	{
		int c = escape_to_char(mem[2]);
		if (c != -1) return c;
		s->error = 9;
		s->errorpos = mem+1;
		return 0;
	}
	else
		number = strtol(mem, &end, 10);
	assert(end - mem == (ptrdiff_t)len);
	return number;
}

//to add an operator:
//fix is_operator()
//fix operator()
//fix priority()
//fix apply_op()
//fix operation_type()
static char is_operator(char *mem, size_t *len)
{
	if (mem == 0) return 0;
	if (*mem == '+' || *mem == '-' || *mem == '*' || *mem == '/' || *mem == '^' || *mem == '|' || *mem == '&' || *mem == '%')
	{
		*len = 1;
		return 1;
	}
	else if ((mem[0] == '<' && mem[1] == '<') || (mem[0] == '>' && mem[1] == '>'))
	{
		*len = 2;
		return 1;
	}
	return 0;
}

static char operator(char *mem)
{
	if (*mem == '+' || *mem == '-' || *mem == '*' || *mem == '/' || *mem == '^' || *mem == '|' || *mem == '&' || *mem == '%')
		return *mem;
	else if (mem[0] == '<' && mem[1] == '<')
		return '<';
	else if (mem[0] == '>' && mem[1] == '>')
		return '>';
	assert(0);
	return 0;
}

static char is_unary_operator(char *mem, size_t *len)
{
	if (*mem == '~' || *mem == '-' || *mem == '+')
	{
		*len = 1;
		return 1;
	}
	return 0;
}

static char unary_operator(char *mem)
{
	if (*mem == '-') return UNARY_MINUS;
	if (*mem == '+') return UNARY_PLUS;
	return *mem;
}

static char operator_type(char op)
{
	static char ops[] = "+-*/^|&%><";
	static char uops[] = { '~', UNARY_MINUS, UNARY_PLUS };
	for (int i = 0; i < sizeof ops; i++)
		if (op == ops[i]) return 2;
	for (int i = 0; i < sizeof uops; i++)
		if (op == uops[i]) return 1;
	assert(0);
	return -1;
}

typedef struct
{
	char opstack[100];
	int n_ops;

	struct
	{
		char is_op;
		char op;
		int value;
	} output[100];

	int n_outputs;
} RPN;

static RPN rpn;
static void reset_rpn() { rpn.n_ops = rpn.n_outputs = 0; }
static void push_op(char op) { rpn.opstack[rpn.n_ops++] = op; }
static char pop_op(void) { return rpn.opstack[--rpn.n_ops]; }
static char peek_op(void){ return rpn.opstack[rpn.n_ops - 1]; }
static char has_op(void) { return rpn.n_ops > 0; }
static char is_right(char op) { return op == '^' || op == '~' || op == UNARY_MINUS; }
static char priority(char op)
{
	if (op == '|' || op == '&') return 0;
	if (op == '-' || op == '+') return 1;
	if (op == '*' || op == '/' || op == '%') return 2;
	if (op == '^') return 3;
	if (op == '~' || op == UNARY_MINUS || op == UNARY_PLUS) return 4;
	assert(0);
	return 0;
}

static char is_less_than_op_top(char op) { return is_right(op) ? (priority(op) < priority(peek_op())) : (priority(op) <= priority(peek_op())); }

static void output_op(char op)
{
	rpn.output[rpn.n_outputs].is_op = 1;
	rpn.output[rpn.n_outputs].op = op;
	rpn.n_outputs++;
}

static void output_value(int value)
{
	rpn.output[rpn.n_outputs].is_op = 0;
	rpn.output[rpn.n_outputs].value = value;
	rpn.n_outputs++;
}

static int apply_op(EXPR_STATE *state, char op, int v0, int v1)
{
	if (op == '+') return v0 + v1;
	if (op == '-') return v0 - v1;
	if (op == '*') return v0 * v1;
	if (op == '/')
	{
		if (v1 == 0)
		{
			state->error = 8;
			return 0;
		}
		return v0 / v1;
	}
	if (op == '%')
	{
		if (v1 == 0)
		{
			state->error = 8;
			return 0;
		}
		return v0 % v1;
	}
	if (op == '^') return (int)pow((double)v0, (double)v1);
	if (op == '|') return v0 | v1;
	if (op == '&') return v0 & v1;
	if (op == '<') return v0 << v1;
	if (op == '>') return v0 >> v1;
	assert(0);
	return 0;
}

static int apply_unary_op(EXPR_STATE *state, char op, int v)
{
	if (op == '~') return ~v;
	if (op == UNARY_MINUS) return -v;
	if (op == UNARY_PLUS) return v;
	assert(0);
	return 0;
}

static void dump_rpn(void)
{
	for (int i = 0; i < rpn.n_outputs; i++)
	{
		if (rpn.output[i].is_op)
			elog("%c ", rpn.output[i].op);
		else
			elog("%04X ", rpn.output[i].value);
	}
	elog("\n");
}

int trace_rpn = 0;
static int evalrpn(EXPR_STATE *state)
{
	if (trace_rpn) dump_rpn();

	if (rpn.n_outputs == 0)
	{
		state->error = 7;
		return 0;
	}

	while (rpn.n_outputs > 1)
	{
		char no_op = 1;
		for (int i = 0; i < rpn.n_outputs; i++)
		{
			if (rpn.output[i].is_op)
			{
				switch (operator_type(rpn.output[i].op))
				{
					case 1:
						if (i < 1) { state->error = 7; return 0; }
						rpn.output[i - 1].value = apply_unary_op(state, rpn.output[i].op, rpn.output[i - 1].value);
						rpn.output[i - 1].is_op = 0;
						memmove(rpn.output + i, rpn.output + i + 1, sizeof rpn.output[0] * (rpn.n_outputs - i));
						rpn.n_outputs -= 1;
						break;

					case 2:
						if (i < 2) { state->error = 7; return 0; }
						rpn.output[i - 2].value = apply_op(state, rpn.output[i].op, rpn.output[i - 2].value, rpn.output[i - 1].value);
						rpn.output[i - 2].is_op = 0;
						memmove(rpn.output + i - 1, rpn.output + i + 1, sizeof rpn.output[0] * (rpn.n_outputs - i - 1));
						rpn.n_outputs -= 2;
						break;
					default:
						state->error = 7;
						return 0;
				}
				no_op = 0;
				break;
			}
		}
		if (no_op)
		{
			state->error = 7;
			return 0;
		}
		if (trace_rpn) dump_rpn();
	};
	if (trace_rpn) elog("=%d\n", rpn.output[0].value);
	return rpn.output[0].value;
}

static int process_expr(EXPR_STATE *state)
{
	reset_rpn();

	size_t len;
	size_t len2;
	int last_symbol_type = 5;

	if (trace_rpn) elog("processing expression: %s\n", state->expr);

	for (;;)
	{
		whitespace(state);

		if (is_label(state, state->expr, &len))
		{
			output_value(labelv(state, len));
			if (state->error)
			{
				state->errorpos = state->expr;
				return 0;
			}
			state->expr += len;
			last_symbol_type = 1;
		}
		else if (is_binary_numeric(state->expr, &len))
		{
			output_value(number(NULL, state->expr, len));
			state->expr += len;
			last_symbol_type = 4;
		}
		else if (is_unary_operator(state->expr, &len) && !is_operator(state->expr, &len2))
		{
			push_op(unary_operator(state->expr));
			state->expr += len;
			last_symbol_type = 2;
		}
		else if (is_operator(state->expr, &len))
		{
			if (is_unary_operator(state->expr, &len2) && (last_symbol_type == 3 || last_symbol_type == 5))
			{
				push_op(unary_operator(state->expr));
				state->expr += len2;
				last_symbol_type = 2;
			}
			else
			{
				char this_op = operator(state->expr);
				while (has_op() && peek_op() != '(' && is_less_than_op_top(this_op))
					output_op(pop_op());
				push_op(this_op);
				state->expr += len;
				last_symbol_type = 3;
			}
		}
		else if (is_numeric(state->expr, &len))
		{
			output_value(number(NULL, state->expr, len));
			state->expr += len;
			last_symbol_type = 4;
		}
		else if (*state->expr == '$')
		{
			output_value(state->pc);
			state->expr++;
			last_symbol_type = 4;
		}
		else if (*state->expr == '(')
		{
			push_op('(');
			state->expr++;
			last_symbol_type = 5;
		}
		else if (*state->expr == ')')
		{
			while (has_op() && peek_op() != '(')
				output_op(pop_op());
			if (!has_op())
			{
				state->error = 5;
				state->errorpos = state->expr;
				return 0;
			}
			pop_op();

			state->expr++;
			last_symbol_type = 6;
		}
		else if (*state->expr == '\0')
		{
			while (has_op())
			{
				char op = pop_op();
				if (op == '(' || op == ')')
				{
					state->error = 6;
					return 0;
				}
				output_op(op);
			}
			return evalrpn(state);
		}
		else
		{
			state->error = 4;
			state->errorpos = state->expr;
			return 0;
		}
	}
}

int expr_eval(char *exp, unsigned short pc, int *value, DB_RES *labels)
{
	EXPR_STATE state = { 0 };
	state.expr = exp;
	state.pc = pc;

	if (labels != NULL)
	{
		ZX_LABEL *zx = labels->rows;
		state.labels = malloc(labels->count * sizeof state.labels[0]);
		for (int i = 0; i < labels->count; i++, zx++)
		{
			state.labels[state.n_labels].address = zx->address;
			state.labels[state.n_labels].label = zx->name;
			state.n_labels++;
		}
	}

	*value = process_expr(&state);

	free(state.labels);

	if (state.error)
		return 0;
	return -1;
}

static void evalfixup(ASM_FIXUP *fixup, EXPR_STATE *state)
{
	state->expr = fixup->evalfixup;
	int value = process_expr(state);

	if (fixup->size == 8)
	{
		value -= fixup->i8offset;
		fixup->value = value;

		if (value < -128 || value > 255)
			state->error = 3;
	}
	else if (fixup->size == 16)
	{
		fixup->value = value;
		if (value < -32768 || value > 65535)
			state->error = 3;
	}
}

static int run_fixups(ASTATE *s)
{
	//todo: should add database labels

	//strip the ':' off any labels
	for (int i = 0; i < s->n_labels; i++)
	{
		size_t len = strlen(s->labels[i].label);
		if (s->labels[i].label[len - 1] == ':')
			s->labels[i].label[len - 1] = '\0';
	}

	EXPR_STATE state = { 0 };

	state.labels = s->labels;
	state.n_labels = s->n_labels;

	//evaluate the fixups
	for (int i = 0; i < s->n_fixups; i++)
	{
		state.pc = s->fixups[i].pc;
		evalfixup(&s->fixups[i], &state);

		if (state.error != 0)
		{
			if (state.error == 1) elog("unknown label at character %d", state.errorpos - s->fixups[i].evalfixup + 1);
			else if (state.error == 2) elog("unexpected EOL");
			else if (state.error == 3) elog("expression out of range for immediate %d - %04X (%d) - ", s->fixups[i].size, s->fixups[i].value, s->fixups[i].value);
			else if (state.error == 4) elog("unexpected symbol at character %d", state.errorpos - s->fixups[i].evalfixup + 1);
			else if (state.error == 5) elog("unmatched close bracket at character %d", state.errorpos - s->fixups[i].evalfixup + 1);
			else if (state.error == 6) elog("mismatched parenthesis");
			else if (state.error == 7) elog("invalid expression");
			else if (state.error == 8) elog("division by zero");
			else if (state.error == 9) elog("invalid control character at character %d", state.errorpos - s->fixups[i].evalfixup + 1);
			else elog("error");
			elog(" at line %d : expression %s\n", s->fixups[i].lineno, s->fixups[i].evalfixup);
			//return -1;
			s->n_fixups = i - 1;
		}
	}

	//apply the fixups
	for (int i = 0; i < s->n_fixups; i++)
	{
		if (s->fixups[i].size == 8)
		{
			s->program[s->fixups[i].address] = (unsigned char)s->fixups[i].value;
		}
		else if (s->fixups[i].size == 16)
		{
			s->program[s->fixups[i].address] = (unsigned char)s->fixups[i].value;
			s->program[s->fixups[i].address+1] = (unsigned char)(s->fixups[i].value>>8);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------------------
// Test Cases
//---------------------------------------------------------------------------------------
static void asm1(char *ins2)
{
	ASTATE *status = calloc(1, sizeof * status);
	if (status == NULL) return;

	char *ins = _strdup(ins2);
	if (ins == NULL) return;

	int err = asm(ins, status);
	if (err != 0)
		return;

	for (int i = 0; i < status->n_bytes; i++)
		elog("%02X ", status->asm[i]);
	for (int i = status->n_bytes; i < 4; i++)
		elog("   ");
	DASM_RES res = { 0 };
	DAsmExMem(status->asm, 0, &res);
	elog("%-20s", res.instr);
	elog("\n");
	free(status);
	free(ins);
}

extern const char *const mnemonics[256];
extern const char *const mnemonicsCB[256];
extern const char *const mnemonicsED[256];
// DD/FD prefix (IX/IY instead of HL)
extern const char *const mnemonicsXX[256];
// DD CB / FD CB prefix
extern const char *const mnemonicsXXCB[256];

static void num1(char *dst, unsigned char num)
{
	char tmp[3] = { 0 };
	sprintf_s(tmp, 3, "%02X", num);
	dst[0] = tmp[0];
	dst[1] = tmp[1];
}

static void num1dec(char *dst, unsigned char num)
{
	char tmp[4] = { 0 };
	sprintf_s(tmp, 4, "%3d", num);
	dst[0] = tmp[0];
	dst[1] = tmp[1];
	dst[2] = tmp[2];
}

static void num2(char *dst, unsigned short num)
{
	char tmp[5] = { 0 };
	sprintf_s(tmp, 5, "%04X", num);
	dst[0] = tmp[0];
	dst[1] = tmp[1];
	dst[2] = tmp[2];
	dst[3] = tmp[3];
}

static char xy = 'x';

static char *pad(const char *m)
{
// % - IX or IY
// \1 - immediate byte
// \2 - immediate byte offset
// \3 - immediate word (load)
// \4 - immediate word (load relative)
// \5 - immediate word (call/jump)
// \6 - immediate byte offset from IX/IY
	static char mnem[100];
	mnem[0] = '\t';
	strcpy_s(mnem+1, 99, m);
	
	char *pc = strchr(mnem, '%');
	char *star = strchr(mnem, '\1');
	char *at = strchr(mnem, '\2');
	char *hash = strchr(mnem, '\3');
	char *and = strchr(mnem, '\5');
	char *caret = strchr(mnem, '\6');
	char *pling = strchr(mnem, '\4');

	if (pc)
		*pc = xy;
	pc = strchr(mnem, '%');
	if (pc)
		*pc = xy;

	if (star)
	{
		if (rand() & 1)
		{
			memmove(star + 2, star, strlen(mnem));
			num1(star, (unsigned char)rand());
			*(star + 2) = 'h';
		}
		else
		{
			memmove(star + 2, star, strlen(mnem));
			num1dec(star, (unsigned char)rand());
		}
	}
	if (at)
	{
		memmove(at + 3, at, strlen(mnem));
		*at = (rand() & 1) ? '+' : '-';
		num1(at + 1, (unsigned char)(rand() % 127));
		*(at + 3) = 'h';
	}
	if (hash)
	{
		memmove(hash + 4, hash, strlen(mnem));
		num2(hash, (unsigned short)rand());
		*(hash + 4) = 'h';
	}
	if (and)
	{
		memmove(and + 4, and, strlen(mnem));
		num2(and, (unsigned short)rand());
		*(and + 4) = 'h';
	}
	if (caret)
	{
		memmove(caret + 3, caret, strlen(mnem));
		*caret = (rand() & 1) ? '+' : '-';
		num1(caret + 1, (unsigned char)(rand() % 127));
		*(caret + 3) = 'h';
	}
	if (pling)
	{
		memmove(pling + 4, pling, strlen(mnem));
		num2(pling, (unsigned short)rand());
		*(pling + 4) = 'h';
	}

	//make uppercase
	for (unsigned int i = 0; i < strlen(mnem); i++)
		mnem[i] = (char)toupper(mnem[i]);

	return mnem;
}

static char *sample1 =
"	ORG FEFEh\r\n"\
"	; no need to di, Z80 does this automatically\r\n"\
"	push af ; save carry flag\r\n"\
"	push bc ; save bc register\r\n"\
"\r\n"\
";set border to blue\r\n"\
"\r\n"\
"	ld a,01h; blue border\r\n"\
"	out (FEh),a; change color\r\n"\
"\r\n"\
";and wait a multiple of 48 + 176 = 224 T - States[2]\r\n"\
"\r\n"\
"	ld bc,6408h\r\n"\
"wait: nop ; 4T\r\n"\
"	nop ; 4T\r\n"\
"	nop ; 4T\r\n"\
"	dec bc ; 6T\r\n"\
"	jp nc, wait ; 10T\r\n"\
"	; 28T for each loop, so 8 iterations gets you down one scan line\r\n"\
"\r\n"\
";then change the border color to black\r\n"\
"\r\n"\
"	ld a,00h; black border\r\n"\
"		out (FEh),a; change color\r\n"\
"\r\n"\
"; restore the registers andreturn from the interrupt handler\r\n"\
"\r\n"\
"	pop bc; restore bc\r\n"\
"	pop af; restore af\r\n"\
"	ei; re - enable interrupts\r\n"\
"	reti; return\r\n";

static char *sample2 =
"        ORG     0FF5Ah\r\n"\
"\r\n"\
"        DI\r\n"\
"        LD      HL,0FFFFh\r\n"\
"        LD      (HL),18h\r\n"\
"        LD      A,3Bh\r\n"\
"        LD      I,A\r\n"\
"        IM      2\r\n"\
"        EI\r\n"\
"        RET\r\n"\
"\r\n"\
"LFF68:  DI\r\n"\
"        LD      A,3Fh\r\n"\
"        LD      I,A\r\n"\
"        IM      1\r\n"\
"        EI\r\n"\
"        RET\r\n"\
"\r\n"\
"LFF71:  NOP\r\n"\
"LFF72:  JR      LFF74\r\n"\
"\r\n"\
"LFF74:  NOP\r\n"\
"LFF75:  NOP\r\n"\
"        NOP\r\n"\
"        NOP\r\n"\
"        NOP\r\n"\
"LFF79:  PUSH    BC\r\n"\
"        PUSH    DE\r\n"\
"        PUSH    HL\r\n"\
"        PUSH    AF\r\n"\
"        LD      (LFF75),SP\r\n"\
"        LD      SP,0FF79h\r\n"\
"        LD      BC,0218h\r\n"\
"LFF87:  DEC     BC\r\n"\
"        LD      A,B\r\n"\
"        OR      C\r\n"\
"        JR      NZ,LFF87\r\n"\
"        LD      A,(LFF71)\r\n"\
"        ADD     A,A\r\n"\
"        ADD     A,A\r\n"\
"        ADD     A,A\r\n"\
"        JP      Z,LFFA0\r\n"\
"LFF95:  LD      B,0Fh\r\n"\
"LFF97:  DJNZ    LFF97\r\n"\
"        NOP\r\n"\
"        NOP\r\n"\
"        RET     Z\r\n"\
"        DEC     A\r\n"\
"        JP      NZ,LFF95\r\n"\
"LFFA0:  LD      L,A\r\n"\
"        LD      A,(LFF71)\r\n"\
"        LD      H,A\r\n"\
"        SRL     H\r\n"\
"        RR      L\r\n"\
"        SRL     H\r\n"\
"        RR      L\r\n"\
"        SRL     H\r\n"\
"        RR      L\r\n"\
"        LD      DE,580Ch\r\n"\
"        ADD     HL,DE\r\n"\
"        EX      DE,HL\r\n"\
"        LD      HL,(LFF72+1)\r\n"\
"        LD      A,(LFF72)\r\n"\
"LFFBC:  LD      BC,0040h\r\n"\
"LFFBF:  PUSH    DE\r\n"\
"        LDI\r\n"\
"        LDI\r\n"\
"        LDI\r\n"\
"        LDI\r\n"\
"        LDI\r\n"\
"        LDI\r\n"\
"        LDI\r\n"\
"        LDI\r\n"\
"        POP     DE\r\n"\
"        JP      PO,LFFDB\r\n"\
"        JR      LFFD6\r\n"\
"\r\n"\
"LFFD6:  NOP\r\n"\
"        AND     0FFh\r\n"\
"        JR      LFFBF\r\n"\
"\r\n"\
"LFFDB:  EX      DE,HL\r\n"\
"        LD      C,20h\r\n"\
"        ADD     HL,BC\r\n"\
"        EX      DE,HL\r\n"\
"        DEC     A\r\n"\
"        JP      NZ,LFFBC\r\n"\
"        LD      SP,(LFF75)\r\n"\
"        POP     AF\r\n"\
"        POP     HL\r\n"\
"        POP     DE\r\n"\
"        POP     BC\r\n"\
"        EI\r\n"\
"        JP      0038h\r\n"\
"\r\n"\
"LFFF0:  NOP\r\n"\
"        NOP\r\n"\
"        NOP\r\n"\
"        NOP\r\n"\
"        DI\r\n"\
"        JP      LFF79\r\n";

static char *sample3 =
"	ORG 1000h\r\n"\
"	LD A,$123A\r\n"\
"L04D0:  EX      AF,AF'          ; save flag\r\n"\
"	DEFM \"1 2 3\"\r\n"\
"	NOP\r\n"\
"	NOP\r\n"\
"	NOP\r\n"\
"	NOP\r\n"\
"L0000-5: LD IXH,4\r\n"
"L0000:\r\n"\
"	LD A,(L0000)\r\n"\
"L0000-1:\r\n"
"	LD A,(L0000-1)\r\n"\
"	LD A,(L0000)\r\n"\
"	LD A,(L0000-1-1)\r\n"\
"	LD BC,$\r\n"\
"	LD BC,$-L0000\r\n"\
"	LD BC,-(L0000-$)\r\n"\
"	LD A,$123A\r\n"\
"	LD A,#567B\r\n"\
"	DEFM \"1; 2\t 3\"\r\n"\
"	DEFM \"1; 2\t 3\";comment\r\n"\
"	DEFM \"1 \\\" 2 3\";comment\r\n"\
"	DEFB $-L0000-1;comment\r\n"\
"	DEFB $-L0000,5,$,'3','\t',5--5;comment\r\n"\
"";

static void test_sample(void)
{
	//assemble(sample1);
	//assemble(sample2);
	elog("%s", sample3);
	assemble(sample3);
}

static void test_rom(void)
{
	FILE *f;
	char *mem;
	size_t len;
	fopen_s(&f, "../resources/tests/rom48k.asm", "rb");
	if (f == NULL) return;

	fseek(f, 0, SEEK_END);
	len = ftell(f);
	fseek(f, 0, SEEK_SET);
	mem = malloc(len+1);
	if (mem == NULL) return;
	fread(mem, len, 1, f);
	fclose(f);
	mem[len] = '\0';
	assemble(mem);
	free(mem);

	unsigned char *originalrom = malloc(16384);
	fopen_s(&f, "../resources/roms/48.rom", "rb");
	if (f != NULL && originalrom != NULL)
	{
		fread(originalrom, 16384, 1, f);
		fclose(f);
	}
	unsigned char *testrom = malloc(16384);
	fopen_s(&f, "rom48.bin", "rb");
	if (f != NULL && testrom != NULL)
	{
		fread(testrom, 16384, 1, f);
		fclose(f);
	}

	if (testrom && originalrom)
	{
		int match = 1;
		for (int x = 0; x < 16384; x++)
		{
			if (testrom[x] != originalrom[x])
			{
				//elog("ROMS differ at offset %04X %d\n", x, x);
				
				int k = x;
				//int j = x;
				//while (testrom[j] != originalrom[j] && j < 16384) j++;
				//for (int k = max(x - 1, 0); k < min(j + 1, 16384); k++)
				//for (int k = x; k < j; k++)
					elog("%04X: %02X %02X\n", k, testrom[k], originalrom[k]);
				match = 0;
				//break;
			}
		}
		if (match)
			elog("ROMS match!!\n");
	}
	if (originalrom) free(originalrom);
	if (testrom) free(testrom);
}

static struct
{
	char *expr;
	int value;
} test_expressions[] =
{
	{"+$40", 64},
	{"-$40", -64},
	{"5 - -5", 10},
	{"80h >> 4", 0x8},
	{"~80h", 0xffffff7f},
	{"5 - -(2+3)", 10},
	{"5 - -(2++3)", 10},
	{"5 - -(-2+-3)", 0},
	{"4h<<1",0x8},
	{"4<<1",0x8},
	{"10/2+3",8},
	{"1|2|8",11},
	{"89h&10h",0},
	{"89h&8h",8},
	{"1000%9",1},
	{"5+1000%9",6 },
	{"1000%(7+2)",1},
	{ "$89&$8",8 },
	{ "#89&#8",8 },
	{ "#89&$8",8 },
	{ "$89&#8",8 },
	{"'\t'",9},
	{"'\\t'",9},
	{ "'m'", 'm'},
	{"$-c9c9h",0},
	{"-c9c9h+$+1",1},
};

static void test_expr(void)
{
	EXPR_STATE state;
	for (int i = 0; i < sizeof test_expressions / sizeof test_expressions[0]; i++)
	{
		memset(&state, 0, sizeof state);
		state.expr = test_expressions[i].expr;
		state.pc = 0xC9C9;
		elog("%s expected: %d, ", test_expressions[i].expr, test_expressions[i].value);
		int value = process_expr(&state);
		if (state.error)
			elog("got: error %d\n", state.error);
		else
			elog("got: %d\n", value);
	}
}

int asm_tests_enabled = 0;
void test_asm(void)
{
	if (!asm_tests_enabled)
		return;

	if (!(asm_tests_enabled & 1)) goto test2;

	asm1(" ld (0x1244h),ix");
	asm1("\tld a,(hl)");
	asm1(" ld a,(hl)");
	asm1("\t \tld a,(hl);this is a comment");

test2:
	if (!(asm_tests_enabled & 2)) goto test4;

	elog("[TEST] Unextended\n");
	for (int i = 0; i < 256; i++)
	{
		char *mnem = pad(mnemonics[i]);
		elog("%-20s", mnem);
		asm1(mnem);
	}

	elog("[TEST] CB\n");
	for (int i = 0; i < 256; i++)
	{
		char *mnem = pad(mnemonicsCB[i]);
		elog("%-20s", mnem);
		asm1(mnem);
	}

	elog("[TEST] ED\n");
	for (int i = 0; i < 256; i++)
	{
		char *mnem = pad(mnemonicsED[i]);
		elog("%-20s", mnem);
		asm1(mnem);
	}

	xy = 'x';
	elog("[TEST] DD\n");
	for (int i = 0; i < 256; i++)
	{
		char *mnem = pad(mnemonicsXX[i]);
		elog("%-20s", mnem);
		asm1(mnem);
	}

	xy = 'y';
	elog("[TEST] FD\n");
	for (int i = 0; i < 256; i++)
	{
		char *mnem = pad(mnemonicsXX[i]);
		elog("%-20s", mnem);
		asm1(mnem);
	}

	xy = 'x';
	elog("[TEST] DD CB\n");
	for (int i = 0; i < 256; i++)
	{
		char *mnem = pad(mnemonicsXXCB[i]);
		elog("%-20s", mnem);
		asm1(mnem);
	}

	xy = 'y';
	elog("[TEST] FD CB\n");
	for (int i = 0; i < 256; i++)
	{
		char *mnem = pad(mnemonicsXXCB[i]);
		elog("%-20s", mnem);
		asm1(mnem);
	}

test4:
	if (!(asm_tests_enabled & 4)) goto test5;
	
	test_sample();

test5:
	if (!(asm_tests_enabled & 8)) goto test6;

	test_expr();

test6:
	if (!(asm_tests_enabled & 16)) goto end;

	test_rom();

end:
	return;
}
