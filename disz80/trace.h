//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

typedef enum
{
	BT_FALLTHROUGH = 0,
	BT_JP = 1,
	BT_JR = 2,
	BT_CALL = 3
} BRANCH_TYPE;

typedef struct BRANCH_NODE
{
	unsigned short start;	//start of this block
	unsigned short ret;		//address of the ending ret instruction
	unsigned int end;		//one past the end of the block so (start,end] is the block

	unsigned short from;	//address of the branch/jump instruction
	unsigned short to;		//target of the branch/jump

	struct BRANCH_NODE *taken;
	struct BRANCH_NODE *nottaken;

	BRANCH_TYPE branchtype;	//type af branch taken to get here
	char visited;

	void *node;
	void *edit;

	struct BRANCH_NODE *parent;

} BRANCH_NODE;


typedef struct
{
	BRANCH_NODE *nodes[65536];
	int node_idx;
} PC_TRACE;

PC_TRACE *start_pc_trace(unsigned short pc);
void free_trace(PC_TRACE *pctrace);
void graph_trace(PC_TRACE *pctrace);

