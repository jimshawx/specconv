//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

typedef enum
{
	MRU_SNA,
	MRU_TAP,
	MRU_DB
} MRU_TYPE;

void get_settings(void);
int update_setting(char *name, int valueint, char *valuestring);
int get_setting_int(char *name, int dflt);
char *get_setting_string(char *name, char *dflt);
int save_mru(MRU_TYPE type, char *name);
char **search_mru(MRU_TYPE type);