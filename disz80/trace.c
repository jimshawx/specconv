//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#include "logger.h"
#include "daos.h"
#include "disview.h"
#include "emu.h"
#include "dasm.h"
#include "trace.h"
#include "coverage.h"
#include "labels.h"
#include "graph.h"
#include "dbgmem.h"

static int nc(const void *s0, const void *s1)
{
	BRANCH_NODE *bs0 = *(BRANCH_NODE **)s0;
	BRANCH_NODE *bs1 = *(BRANCH_NODE **)s1;
	return (int)bs0->start - (int)bs1->start;
}

int dump_nodes_ok = 1;
static void dump_nodes(PC_TRACE *pctrace)
{
	DASM_RES res;

	if (!dump_nodes_ok)
		return;

	static BRANCH_NODE *sorter[65536];
	for (int i = 0; i < pctrace->node_idx; i++)
		sorter[i] = pctrace->nodes[i];
	qsort(sorter, pctrace->node_idx, sizeof *sorter, nc);

	elog("nodes\n");
	for (int i = 0; i < pctrace->node_idx; i++)
	{
		BRANCH_NODE *n = sorter[i];
		elog("--- %s %04X->%04X\n", n->branchtype == BT_CALL?"SUB":"", n->start, n->end);

		BRANCH_NODE *parent = n->parent;
		elog("%04X<-", n->start);
		while (parent != NULL)
		{
			elog("%04X<-", parent->start);
			parent = parent->parent;
		}
		elog("\n");

		unsigned int pc = n->start;
		do
		{
			int size = DAsmEx((unsigned short)pc, &res);
			//elog("%04X %-20s %s\n", pc, pc_to_label(pc, 0, 0), res.instr);
			elog("%04X %s\n", pc, res.instr);
			pc += size;
		} while (pc < n->end);
		if (n->taken) elog("-> %04X \n", n->taken->start);
		if (n->nottaken)elog("fall through -> %04X\n", n->nottaken->start);
	}
	elog("\n");
}

static BRANCH_NODE *new_node(int start, BRANCH_TYPE branchtype, PC_TRACE *pctrace, BRANCH_NODE *from)
{
	if (start > 65535 || start < 0)
		return NULL;

	//if we end up at the ROM START routine, stop tracing
	if (start == 0)
		return NULL;

	//don't bother tracing into the ROM
	if (start < 16384)
		return NULL;

	for (int i = 0; i < pctrace->node_idx; i++)
	{
		if (start == pctrace->nodes[i]->start)
		{
			pctrace->nodes[i]->visited = 1;
			if (branchtype > pctrace->nodes[i]->branchtype)
				pctrace->nodes[i]->branchtype = branchtype;

			return pctrace->nodes[i];
		}
	}

	BRANCH_NODE *c = calloc(1, sizeof * c);
	assert(c);
	if (c == NULL) return NULL;
	c->start = (unsigned short)start;
	c->branchtype = branchtype;
	c->parent = from;
	pctrace->nodes[pctrace->node_idx++] = c;
	return c;
}

static void trace(BRANCH_NODE *curr, int depth, PC_TRACE *pctrace);
static void fix_overlaps(PC_TRACE *pctrace);
static void create_labels_and_types(PC_TRACE *pctrace);
void free_trace(PC_TRACE *pctrace);

PC_TRACE *start_pc_trace(unsigned short pc)
{
	PC_TRACE *pctrace = calloc(1, sizeof * pctrace);
	if (pctrace == NULL) return NULL;

	elog("tracing...\n");

	trace(new_node(pc, BT_FALLTHROUGH, pctrace, NULL), 0, pctrace);

	elog("found %d blocks...fixing overlaps...", pctrace->node_idx);

	fix_overlaps(pctrace);

	dump_nodes(pctrace);

	create_labels_and_types(pctrace);

	elog("done!\n");

	return pctrace;
}

static void fix_overlaps(PC_TRACE *pctrace)
{
	//some of the blocks will overlap, where we have jumped into an existing block but not at the top.
	//split those blocks and link everything up
	//elog("\n");

	static BRANCH_NODE *sorter[65536];
	for (int i = 0; i < pctrace->node_idx; i++)
		sorter[i] = pctrace->nodes[i];
	qsort(sorter, pctrace->node_idx, sizeof *sorter, nc);

	BRANCH_NODE *curr, *over;
	char dirty;
	do
	{
		dirty = 0;
		for (int i = 0; i < pctrace->node_idx; i++)
		{
			curr = sorter[i];

			for (int j = i + 1; j < pctrace->node_idx; j++)
			{
				over = sorter[j];

				//early out (only works if sorted)
				if (curr->end < curr->start) break;

				if (over->start >= curr->start && over->start < curr->end)
				{
					//the blocks overlap
					//elog("%u->%u overlaps with %u->%u\n", curr->start, curr->end, over->start, over->end);

					//shrink curr to only go as far as the start of overlapping block
					curr->end = over->start;

					//clear out its jumps/calls/ret/end
					//the taken/nottaken branches from overlapping block will be the same
					curr->ret = 0;//doesn't finish with a ret/jump/call
					curr->from = 0;
					curr->to = 0;

					//point it at the overlapping block on the nottaken path
					curr->nottaken = over;
					curr->taken = NULL;

					//elog("resolved %u->%u , %u->%u\n\n", curr->start, curr->end, over->start, over->end);

					//restart the process
					//dirty = 1;
					//break;
				}
			}
			//if (dirty) break;
		}
	} while (dirty);
}

static void create_labels_and_types(PC_TRACE *pctrace)
{
	elog("saving %d...", pctrace->node_idx);

	//now the nodes contain all the code
	//with those marked as branchtype == CALL as subroutines
	bulk_insert_begin();

	for (int i = 0; i < pctrace->node_idx; i++)
	{
		ZX_TYPE zxt = { 0 };
		zxt.type = CODE;
		zxt.address = pctrace->nodes[i]->start;
		zxt.size = pctrace->nodes[i]->end - pctrace->nodes[i]->start;
		zxt.source = SR_PCTRACE;
		if (zxt.address >= 16384)
			make_code_with_size(zxt.source, zxt.address, zxt.size);

		if (pctrace->nodes[i]->branchtype != BT_FALLTHROUGH)
		{
			char name[100];
			if (pctrace->nodes[i]->branchtype == BT_CALL)
				format_label(name, 100, pctrace->nodes[i]->start, LBL_SUB);
			else
				format_label(name, 100, pctrace->nodes[i]->start, LBL_JUMP);

			ZX_LABEL zxl = { 0 };
			zxl.address = pctrace->nodes[i]->start;
			zxl.name = name;
			zxl.source = SR_PCTRACE;
			zxl.type = label_type_from_branch_type(pctrace->nodes[i]->branchtype);
			if (zxl.address >= 16384)
				make_label(zxl.name, zxl.source, zxl.address, zxl.type);
		}
	}

	bulk_insert_end();
}

void free_trace(PC_TRACE *pctrace)
{
	while (--pctrace->node_idx >= 0)
	{
		free(pctrace->nodes[pctrace->node_idx]);
		pctrace->nodes[pctrace->node_idx] = NULL;
	}
	free(pctrace);
}

static void trace(BRANCH_NODE *curr, int depth, PC_TRACE *pctrace)
{
	if (curr == NULL) return;
	if (curr->visited) return;
	if (depth > 200) { elog("[TRACE] too deep!\n"); curr->end = curr->start + 1; return; }

	static char disasm[256];
	M_TYPE type;//extended code
	int target;//target of jump/call

	int pc = curr->start;

	int nop_count = 0;
	do
	{
		unsigned char ins = peek_fast((unsigned short)pc);

		// returns - if we hit a return, that's the end of the block

		if ((ins == 0xC9) || // ret C9 (201)
			(ins == 0xED && peek_fast((unsigned short)(pc + 1)) == 0x4D) || //reti ED 4D (237 77)
			(ins == 0xED && peek_fast((unsigned short)(pc + 1)) == 0x45))   //retn ED 45 (237 69)
		{
			curr->ret = (unsigned short)pc;
			curr->end = pc + 1 + (ins == 0xED);
			return;
		}

		// dead ends - if we hit a jump where the address is computed, that's a dead end

		if ((ins == 0xE9) || // jp (hl) E9 (223)
			((ins == 0xDD || ins == 0xFD) && peek_fast((unsigned short)(pc + 1)) == 0xE9))// jp (ix) DD E9 (221 223) or jp (iy) FD E9 (237 223),
		{
			curr->end = pc + 1 + (ins == 0xDD || ins == 0xFD);
			return;
		}

		// dead ends - if we hit an rst NN
		if (ins == 0xC7 || ins == 0xCF || ins == 0xD7 || ins == 0xDF ||
			ins == 0xE7 || ins == 0xEF || ins == 0xF7 || ins == 0xFF)
		{
			curr->end = pc + 1;
			return;
		}

		// dead ends - more than 10 nops in a row (probably space for self-modifying code).
		if (ins == 0x00)
		{
			nop_count++;
			if (nop_count == 10)
			{
				curr->end = pc + 1;
				return;
			}
		}
		else
		{
			nop_count = 0;
		}

		int size = DAsm(disasm, (unsigned short)pc, &type, &target);

		pc += size;

		switch (type)
		{
			case M_CDST: //jump abs
				curr->from = (unsigned short)(pc - size);
				curr->to = (unsigned short)target;
				curr->end = pc;

				curr->taken = new_node(target, BT_JP, pctrace, curr);
				trace(curr->taken, depth+1, pctrace);

				//if it's an unconditional jump (jp NN, C3 (195)) don't follow the drop through case
				if (peek_fast((unsigned short)(pc - size)) != 0xC3)
				{
					curr->nottaken = new_node(pc, BT_FALLTHROUGH, pctrace, curr);
					trace(curr->nottaken, depth+1, pctrace);
				}

				return;

			case M_CDSTR: //jump rel
				//care, because pc+target can wrap over the end of memory
				curr->from = (unsigned short)(pc - size);
				curr->to = (unsigned short)target;
				curr->end = pc;

				//jumped past the end of memory?
				if (target < 65536)
				{
					curr->taken = new_node(target, BT_JR, pctrace, curr);
					trace(curr->taken, depth + 1, pctrace);
				}

				//if it's an unconditional jump (jr NN, 18 (24)) don't follow the drop through case
				if (peek_fast((unsigned short)(pc - size)) != 0x18)
				{
					curr->nottaken = new_node(pc, BT_FALLTHROUGH, pctrace, curr);
					trace(curr->nottaken, depth + 1, pctrace);
				}

				return;

			case M_CDSTS: //call abs
				curr->from = (unsigned short)(pc - size);
				curr->to = (unsigned short)target;
				curr->end = pc;

				curr->taken = new_node(target, BT_CALL, pctrace, curr);
				trace(curr->taken, depth+1, pctrace);

				curr->nottaken = new_node(pc, BT_FALLTHROUGH, pctrace, curr);
				trace(curr->nottaken, depth+1, pctrace);

				return;
		}

	} while (pc < 65536);

	//wrapped around the end of memory
	curr->end = 65536;

	//pc should not drop off the end of RAM...
}

void graph_trace(PC_TRACE *pctrace)
{
	graph_nodes(pctrace->nodes, pctrace->node_idx);
}
