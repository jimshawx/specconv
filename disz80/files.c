//--------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//--------------------------------------------------------------------------------------
#include <windows.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "machine.h"
#include "files.h"
#include "emu.h"
#include "dbgmem.h"
#include "logger.h"
#include "settings.h"

#include "kubazip/zip.h"

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
static char *load_zip(char *fname, int type);

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
extern HWND dis_window;

static char lastfilename[_MAX_PATH];
static char tape_filename[_MAX_PATH];

static START_REGS start_regs;

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
#pragma pack(push,1)
typedef struct
{
	unsigned char I;
	unsigned short HL_alt;
	unsigned short DE_alt;
	unsigned short BC_alt;
	unsigned short AF_alt;
	unsigned short HL;
	unsigned short DE;
	unsigned short BC;
	unsigned short IY;
	unsigned short IX;
	unsigned char interrupt;
	unsigned char R;
	unsigned short AF;
	unsigned short SP;
	unsigned char interrupt_mode;
	unsigned char border_colour;
} SNA_HEADER;
#pragma pack(pop)

void *prepare_snapshot(void)
{
	SNA_HEADER s;

	unsigned char *snapshot = malloc(49152+sizeof s);
	if (!snapshot)
		return NULL;
	
	REG_PACK *regs = get_regs();

	//push pc on to the stack
	regs->sp -= 2;
	unsigned short savestack = peek_fastw(regs->sp);
	poke_fastw(regs->sp, regs->pc);

	s.I = regs->i;
	s.HL_alt = regs->exregs.hl;
	s.DE_alt = regs->exregs.de;
	s.BC_alt = regs->exregs.bc;
	s.AF_alt = regs->exregs.af;
	s.HL = regs->w.hl;
	s.DE = regs->w.de;
	s.BC = regs->w.bc;
	s.IY = regs->ix[1].i;
	s.IX = regs->ix[0].i;
	s.interrupt = regs->iff2 << 2;
	s.R = regs->r;
	s.AF = regs->w.af;
	s.SP = regs->sp;
	s.interrupt_mode = regs->im;
	s.border_colour = 0;

	memcpy(snapshot, &s, sizeof s);

	for (int i = 16384; i < 65536; i++)
		snapshot[sizeof s+i-16384] = peek_fast((unsigned short)i);

	//put the old value back on the stack
	poke_fastw(regs->sp, savestack);
	regs->sp += 2;

	return snapshot;
}

void save_sna(char *fname)
{
	void *s = prepare_snapshot();
	if (!s)
		return;

	FILE *f;
	fopen_s(&f, fname, "wb");
	if (!f)
	{
		free(s);
		return;
	}
	fwrite(s, sizeof(SNA_HEADER)+49152, 1, f);
	fclose(f);
	free(s);
}

void load_sna(char *fname)
{
	FILE *f;
	SNA_HEADER header;
	static unsigned char tmp[49152];
	long l;

	fopen_s(&f,fname, "rb");
	if (!f)
		return;

	fseek(f, 0, SEEK_END);
	l = ftell(f);
	fseek(f, 0, SEEK_SET);
	assert(l==49179);//handle 128k snaps later

	fread(&header, sizeof(SNA_HEADER), 1, f);
	fread(tmp, 49152, 1, f);
	fclose(f);
	reset_machine();
	set_ram(0x4000, tmp, 49152);
	update_screen();

	memset(&start_regs, 0, sizeof(START_REGS));
	start_regs.I = header.I;
	start_regs.HL_alt = header.HL_alt;
	start_regs.DE_alt = header.DE_alt;
	start_regs.BC_alt = header.BC_alt;
	start_regs.AF_alt = header.AF_alt;
	start_regs.HL = header.HL;
	start_regs.DE = header.DE;
	start_regs.BC = header.BC;
	start_regs.IY = header.IY;
	start_regs.IX = header.IX;
	start_regs.interrupt = header.interrupt;
	start_regs.R = header.R;
	start_regs.AF = header.AF;
	start_regs.SP = header.SP;
	start_regs.interrupt_mode = header.interrupt_mode;
	start_regs.border_colour = header.border_colour;

	//retn()
	start_regs.PC = peek_fastw(start_regs.SP);
	start_regs.SP += 2;
	start_regs.interrupt = (start_regs.interrupt&0xfe)|(start_regs.interrupt>>1);

return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
static TAPE_ENTRY *curr_tape=NULL;
static TAPE_ENTRY *curr_entry;

unsigned char *tap_file=NULL;
static TAPE_STATE tape;

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
#pragma pack(push,1)
typedef struct
{
	char zxtape[7];
	unsigned char hex_0x1A;
	unsigned char major;
	unsigned char minor;
} TZX_HEADER;

typedef struct
{
	unsigned short pause_ms;
//	unsigned short data_len;
	unsigned char  data[1];
} STANDARD_DATA;

typedef struct
{
	unsigned short pilot;
	unsigned short sync1;
	unsigned short sync2;
	unsigned short zero;
	unsigned short one;
	unsigned short pilot_tone;
	unsigned char  used_bits;
	unsigned short pause_ms;
	unsigned char  data_len[3];
} TURBO_DATA;

typedef struct
{
	unsigned short zero;
	unsigned short one;
	unsigned char  used_bits;
	unsigned short pause_ms;
	unsigned char  data_len[3];
} PURE_DATA;

typedef struct
{
	unsigned short tstates;
	unsigned short pause_ms;
	unsigned char  used_bits;
	unsigned char  data_len[3];
} DIRECT_DATA;

typedef struct
{
	unsigned short emu_flags;
	unsigned char  refresh_delay;
	unsigned short interrupt_frequency;
	unsigned char  reserved[3];
} EMULATION_DATA;

#pragma pack(pop)

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
long calc_length(unsigned char *d)
{
return (long)d[0]+256*(long)d[1]+65536*(long)d[2];
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
static TAPE_ENTRY *new_block()
{
	return calloc(sizeof(TAPE_ENTRY), 1);
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
static void add_tape_block(TAPE_ENTRY *t)
{
	TAPE_ENTRY *h;

	h = curr_tape;
	if (!h)
	{
		curr_tape = t;
		return;
	}

	while (h->next) h = h->next;

	h->next = t;
	t->prev = h;

return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void free_tape()
{
	TAPE_ENTRY *h, *p;

	tape.state = TS_STOPPED;

	h = curr_tape;
	while (h)
	{
		p = h;
		h = h->next;
		if (p->flags)
			free(p->data_ptr);
		free(p);
	}
	curr_tape = NULL;

	if (tap_file)
	{
		free(tap_file);
		tap_file = NULL;
	}

return;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
int tzx_block_size(void *data, int block_id)
{
	unsigned char  *c;
	unsigned short *s;
	unsigned int   *i;
	
	switch (block_id)
	{
		case 0x10:  //Standard Speed Data Block
			s = (unsigned short *)data;
			s++;
			return (int)*s+4;

		case 0x11:  //Turbo Loading Data Block 
			{
			TURBO_DATA *t = (TURBO_DATA *)data;
			return calc_length(t->data_len)+sizeof(TURBO_DATA);
			}

		case 0x12:  //Pure Tone 
			return 4;

		case 0x13:  //Sequence of Pulses of Different Lengths 
			c = (unsigned char *)data;
			return (int)*c*sizeof(short)+1;

		case 0x14:  //Pure Data Block 
			{
			PURE_DATA *t = (PURE_DATA *)data;
			return calc_length(t->data_len)+sizeof(PURE_DATA);
			}

		case 0x15:  //Direct Recording 
			{
			DIRECT_DATA *t = (DIRECT_DATA *)data;
			return calc_length(t->data_len)+sizeof(DIRECT_DATA);
			}

		case 0x16:  //C64 ROM Type Data Block 
			i = (unsigned int *)data;
			return *i+4;

		case 0x17:  //C64 Turbo Tape Data Block 
			i = (unsigned int *)data;
			return *i+4;

		case 0x20:  //Pause (Silence) or `Stop the Tape' Command 
			return 2;

		case 0x21:  //Group Start 
			c = (unsigned char *)data;
			return (int)*c+1;

		case 0x22:  //Group End 
			return 0;

		case 0x23:  //Jump To Block 
			return 2;

		case 0x24:  //Loop Start 
			return 2;

		case 0x25:  //Loop End 
			return 0;

		case 0x26:  //Call Sequence 
			s = (unsigned short *)data;
			return (int)*s*sizeof(short)+2;

		case 0x27:  //Return From Sequence 
			return 0;

		case 0x28:  //Select Block 
			i = (unsigned int *)data;
			return *i+4;

		case 0x2A:  //Stop Tape if in 48K Mode 
			i = (unsigned int *)data;
			return *i+4;

		case 0x30:  //Text Description 
			c = (unsigned char *)data;
			return (int)*c+1;

		case 0x31:  //Message Block 
			c = (unsigned char *)data;
			c++;
			return 2*sizeof(unsigned char)+(int)*c;

		case 0x32:  //Archive Info 
			s = (unsigned short *)data;
			return *s+2;

		case 0x33:  //Hardware Type 
			c = (unsigned char *)data;
			return (int)*c*3 + 1;

		case 0x34:  //Emulation Info 
			return sizeof(EMULATION_DATA);

		case 0x35:  //Custom Info Block 
			c = (unsigned char *)data;
			c += 10;
			i = (unsigned int *)c;
			return *i + 10;

		case 0x40:  //Snapshot Block 
			c = (unsigned char *)data;
			c++;
			return calc_length(c)+4;

		case 0x5A:  //(90 dec, ASCII Letter `Z') 
			return sizeof(TZX_HEADER);

		default:
			i = (unsigned int *)data;
			return *i+4;

	}
}


//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
char *show_tape_info(void)
{
	const int tsize = 128000;
	char *tinfo=malloc(tsize);
	char *tptr=tinfo;

	TAPE_ENTRY *view_entry;

	tinfo[0] = '\0';

	if (!curr_tape)
		return tinfo;

	view_entry = curr_tape;

	while (view_entry)
	{
		if (!view_entry)
			return tinfo;

		switch (view_entry->type)
		{
			case 0x10:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X %04X Standard Speed Data Block\n", view_entry->block_number, view_entry->type, view_entry->data_len);
				break;

			case 0x11:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X %04X Turbo Loading Data Block\n", view_entry->block_number, view_entry->type, view_entry->data_len);
				break;

			case 0x12:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X %04X Pure Tone Block\n", view_entry->block_number, view_entry->type, view_entry->data_len);
				break;

			case 0x13:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X %04X Sequence of Pulses\n", view_entry->block_number, view_entry->type, view_entry->data_len);
				break;

			case 0x14:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X %04X Pure Data Block\n", view_entry->block_number, view_entry->type, view_entry->data_len);
				break;

			case 0x15:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X %04X Direct Recording Block\n", view_entry->block_number, view_entry->type, view_entry->data_len);
				break;

			case 0x16:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X %04X C64 Data Block\n", view_entry->block_number, view_entry->type, view_entry->data_len);
				break;

			case 0x17:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X %04X C64 Turbo Data Block\n", view_entry->block_number, view_entry->type, view_entry->data_len);
				break;

			case 0x20:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Pause\n", view_entry->block_number, view_entry->type);
				break;

			case 0x21:
				{
				unsigned char *g = (unsigned char *)view_entry->data_ptr;
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Group Start \"%.*s\"\n", view_entry->block_number, view_entry->type, (int)*g, g+1);
				}
				break;

			case 0x22:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Group End\n", view_entry->block_number, view_entry->type);
				break;

			case 0x23:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Jump-To Block\n", view_entry->block_number, view_entry->type);
				break;

			case 0x24:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Loop Start Block\n", view_entry->block_number, view_entry->type);
				break;

			case 0x25:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Loop End Block\n", view_entry->block_number, view_entry->type);
				break;

			case 0x26:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Call Sequence Block\n", view_entry->block_number, view_entry->type);
				break;

			case 0x27:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Call-Return Block\n", view_entry->block_number, view_entry->type);
				break;

			case 0x28:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Select Block\n", view_entry->block_number, view_entry->type);
				break;

			case 0x2A:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Stop-if-48K Block\n", view_entry->block_number, view_entry->type);
				break;

			case 0x30:
				{
				unsigned char *g = (unsigned char *)view_entry->data_ptr;
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Descripton %.*s\n", view_entry->block_number, view_entry->type, (int)*g, g+1);
				}
				break;

			case 0x31:
				{
				unsigned char *g = (unsigned char *)view_entry->data_ptr;
				g++;
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Message %.*s\n", view_entry->block_number, view_entry->type, (int)*g, g+1);
				}
				break;

			case 0x32:
				{
				unsigned char *g = (unsigned char *)view_entry->data_ptr;
				unsigned char n;
				g += 2;
				n = *g++;
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Archive Info\n", view_entry->block_number, view_entry->type);
				while (n--)
				{
					unsigned char id;
					unsigned char len;
					const char * const ids[10]=
					{
						"Full Title: ",
						"Software House/Publisher:",
						"Author(s):",
						"Year of Publication:",
						"Language:",
						"Game/Utility Type:",
						"Price:",
						"Protection Scheme/Loader:",
						"Origin:",
						"Comments:",
					};
					id = *g++;
					len = *g++;
					if (id > 9) id = 9;
					tptr += sprintf_s(tptr, tinfo-tptr+tsize, "\t%s %.*s\n", ids[id], (int)len, g);
					g += len;
				}
				}
				break;


			case 0x33:
				{
				unsigned char *g = (unsigned char *)view_entry->data_ptr;
				unsigned char n;
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Hardware Type\n", view_entry->block_number, view_entry->type);
				n = *g++;
				while (n--)
				{
					unsigned char type;
					unsigned char id;
					unsigned char value;

					static const char * const types[17]=
					{
						"Computers",
						"External Storage",
						"ROM/RAM Add-ons",
						"Sound Devices",
						"Joysticks",
						"Mice",
						"Controllers",
						"Serial IO",
						"Parallel IO",
						"Printers",
						"Modems",
						"Digitisers",
						"Network Adaptors",
						"Keyboards",
						"AD/DA Convertors",
						"EPROM Programmers",
						"Unknown",
					};

					static const char * const sub_types[17][30]=
					{
						{
						"ZX Spectrum 16k",                  
						"ZX Spectrum 48k, Plus",            
						"ZX Spectrum 48k ISSUE 1",          
						"ZX Spectrum 128k (Sinclair)",      
						"ZX Spectrum 128k +2 (Grey case)",  
						"ZX Spectrum 128k +2A, +3",         
						"Timex Sinclair TC-2048",           
						"Timex Sinclair TS-2068",           
						"Pentagon 128",                     
						"Sam Coupe",                        
						"Didaktik M",                       
						"Didaktik Gama",                    
						"ZX-81 with  1k RAM",               
						"ZX-81 with 16k RAM or more",       
						"ZX Spectrum 128k, Spanish version",
						"ZX Spectrum, Arabic version",      
						"TK 90-X",                          
						"TK 95",                            
						"Byte",                             
						"Elwro",                            
						"ZS Scorpion",                      
						"Amstrad CPC 464",                  
						"Amstrad CPC 664",                  
						"Amstrad CPC 6128",                 
						"Amstrad CPC 464+",                 
						"Amstrad CPC 6128+",                
						"Jupiter ACE",                      
						"Enterprise",                       
						"Commodore 64",                     
						"Commodore 128",                    
						},
						{
						"Microdrive",                 
						"Opus Discovery",             
						"Disciple",                   
						"Plus-D",                     
						"Rotronics Wafadrive",        
						"TR-DOS (BetaDisk)",          
						"Byte Drive",                 
						"Watsford",                   
						"FIZ",                        
						"Radofin",                    
						"Didaktik disk drives",       
						"BS-DOS (MB-02)",             
						"ZX Spectrum +3 disk drive",  
						"JLO (Oliger) disk interface",
						"FDD3000",                    
						"Zebra disk drive",           
						"Ramex Millenia",             
						"Larken",
						},
						{
						"Sam Ram",                
						"Multiface",              
						"Multiface 128k",         
						"Multiface +3",           
						"MultiPrint",             
						"MB-02 ROM/RAM expansion",
						},
						{
						"Classic AY hardware (compatible with 128k ZXs)",
						"Fuller Box AY sound hardware",                  
						"Currah microSpeech",                            
						"SpecDrum",                                      
						"AY ACB stereo (A+C=left, B+C=right); Melodik",  
						"AY ABC stereo (A+B=left, B+C=right)",           
						},
						{
						"Kempston",                
						"Cursor, Protek, AGF",     
						"Sinclair 2 Left  (12345)",
						"Sinclair 1 Right (67890)",
						"Fuller",                  
						},
						{
						"AMX mouse",     
						"Kempston mouse",
						},
						{
						"Trickstick",           
						"ZX Light Gun",         
						"Zebra Graphics Tablet",
						},
						{
						"ZX Interface 1",  
						"ZX Spectrum 128k",
						},
						{
						"Kempston S",                            
						"Kempston E",                            
						"ZX Spectrum +3",                        
						"Tasman",                                
						"DK'Tronics",                            
						"Hilderbay",                             
						"INES Printerface",                      
						"ZX LPrint Interface 3",                 
						"MultiPrint",                            
						"Opus Discovery",                        
						"Standard 8255 chip with ports 31,63,95",
						},
						{
						"ZX Printer, Alphacom 32 & compatibles",
						"Generic Printer",                      
						"EPSON Compatible",                     
						},
						{
						"VTX 5000",                             
						"T/S 2050 or Westridge 2050",           
						},
						{
						"RD Digital Tracer",                    
						"DK'Tronics Light Pen",                 
						"British MicroGraph Pad",               
						},
						{
						"ZX Interface 1",                       
						},
						{
						"Keypad for ZX Spectrum 128k",          
						},
						{
						"Harley Systems ADC 8.2",               
						"Blackboard Electronics",               
						},
						{
						"Orme Electronics",                     
						}
					};

					static const char * const values[4]=
					{
						"The tape RUNS on this machine or with this hardware, but may or may not use the hardware or special features of the machine.",
						"The tape USES the hardware or special features of the machine, such as extra memory or a sound chip.",
						"The tape RUNS but it DOESN'T use the hardware or special features of the machine.",
						"The tape DOESN'T RUN on this machine or with this hardware.",
					};

					type = *g++;  if (type>16) type=16;
					id = *g++;    if (id>29) id = 29;
					value = *g++; if (value>3) value = 3;

					tptr += sprintf_s(tptr, tinfo-tptr+tsize, "\t%s %s %s\n", types[type], sub_types[type][id], values[value]);
				}
				}
				break;

			case 0x34:  //Emulation Info 
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Emulation Info Block\n", view_entry->block_number, view_entry->type);
				break;

			case 0x35:  //Custom Info Block 
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Custom Info Block\n", view_entry->block_number, view_entry->type);
				break;

			case 0x40:  //Snapshot Block 
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Snapshot Block\n", view_entry->block_number, view_entry->type);
				break;

			case 0x5A:  //(90 dec, ASCII Letter `Z') 
				{
				TZX_HEADER *tzh = (TZX_HEADER *)view_entry->data_ptr;
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X TZX Header\n", view_entry->block_number, view_entry->type);
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "\t%.7s\n", tzh->zxtape);
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "\tVersion %d.%d\n", tzh->major, tzh->minor);
				}
				break;

			default:
				tptr += sprintf_s(tptr, tinfo-tptr+tsize, "%3d %02X Unknown Block\n", view_entry->block_number, view_entry->type);
				break;

		}
		*tptr++ = '\0';
		view_entry = view_entry->next;
	}
	*tptr++ = '\0';
	return tinfo;
}

//--------------------------------------------------------------------------------------
//
// TZX Format
// https://k1.spdns.de/Develop/Projects/zasm/TZX%20format.html
//
//--------------------------------------------------------------------------------------
void load_tzx(char *fname)
{
	FILE *f;
	unsigned char block_id;
	unsigned char *curr_ptr;
	long length;
	TAPE_ENTRY *block;
	int block_len;
	int curr_block_no = 0;

	fopen_s(&f, fname, "rb");
	if (!f)
		return;

	fseek(f, 0, SEEK_END);
	length = ftell(f);
	fseek(f, 0, SEEK_SET);
	tap_file = malloc(length);
	fread(tap_file, length, 1, f);
	fclose(f);

	curr_ptr = tap_file;

	do
	{
		block_id = *curr_ptr++;

		block = new_block();
		block->type = block_id;
		block->block_number = curr_block_no++;

		block_len = tzx_block_size(curr_ptr, block_id);

		block->data_len = (unsigned int)block_len;

		if (block_id == 0x5A)
		{
			block->data_ptr = curr_ptr-1;
			block_len--;
		}
		else
			block->data_ptr = curr_ptr;

		add_tape_block(block);

		curr_ptr += block_len;
	} while (curr_ptr - tap_file < length);

	curr_entry = curr_tape;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
/*
#pragma pack(push,1)
typedef struct
{
	unsigned short block_size;
	unsigned char data_flag;
	unsigned char type;
	char progname[10];
	unsigned short data_block_len;
	unsigned short parameter1;
	unsigned short parameter2;
	unsigned char header_checksum;
	unsigned short content_block_len;
	unsigned char flag;
	unsigned char content_checksum;
} TAP_HEADER;
#pragma pack(pop)
*/

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void load_tap(char *fname)
{
	FILE *f;
	long length;
	TAPE_ENTRY *block;
	unsigned short data_len, block_len;
	unsigned char *curr_ptr;
	STANDARD_DATA *sd;
	int curr_block_no = 0;

	fopen_s(&f,fname, "rb");
	if (!f)
		return;

	fseek(f, 0, SEEK_END);
	length = ftell(f);
	fseek(f, 0, SEEK_SET);

	tap_file = malloc(length);
	if (!tap_file)
		return;
	fread(tap_file, length,1,f);
	fclose(f);

	curr_ptr = (unsigned char *)tap_file;

	//convert a TAP data block into a TZX standard speed data block
	do
	{
		data_len = *(unsigned short *)curr_ptr;
		curr_ptr += 2;
		block = new_block();
		block->type = 0x10;
		block->block_number = curr_block_no++;
		block_len = sizeof(STANDARD_DATA)+data_len-sizeof(unsigned char);
		block->data_len = block_len;
		block->data_ptr = malloc(block_len+2);
		assert(block->data_ptr!=NULL);
		block->flags = 1;
		sd = (STANDARD_DATA *)block->data_ptr;
		memcpy(sd->data, curr_ptr-2, data_len+2);
//		sd->data_len = data_len;
		sd->pause_ms = 1000;

		add_tape_block(block);
		curr_ptr += data_len;

	} while (curr_ptr - tap_file < length);

	curr_entry = curr_tape;

	assert(_CrtCheckMemory());

return;
}

static char is_known_ext(const char *fname, int type)
{
	char *ext = strrchr(fname, '.');
	if (!ext) return 0;

	ext++;

	if (type == 0 && _stricmp(ext, "tzx") == 0) return 1;
	if (type == 0 && _stricmp(ext, "tap") == 0) return 1;
	if (type == 1 && _stricmp(ext, "sna") == 0) return 1;
	if (type == 1 && _stricmp(ext, "z80") == 0) return 1;
	if (type == 1 && _stricmp(ext, "scr") == 0) return 1;
	if (type == 1 && _stricmp(ext, "sp") == 0) return 1;

	return 0;
}

static char *load_zip(char *fname, int type)
{
	struct zip_t *zip = zip_open(fname, 0, 'r');
	static char tmpfile[L_tmpnam+5];
	errno_t rv = -1;

	int index = 0;
	while (zip_entry_openbyindex(zip, index++) == 0)
	{
		const char *entryname = zip_entry_name(zip);
		if (!zip_entry_isdir(zip) && is_known_ext(entryname, type))
		{
			char *dot = strrchr(entryname, '.');
			rv = tmpnam_s(tmpfile, L_tmpnam+5);
			if (rv == 0 && dot)
			{
				strcat_s(tmpfile, L_tmpnam + 5, dot);
				zip_entry_fread(zip, tmpfile);
			}
			zip_entry_close(zip);
			break;
		}

		zip_entry_close(zip);
	}

	zip_close(zip);

	return rv==0?tmpfile:NULL;
}

static int tzx_loop_start;
static int tzx_loop_count;
static int tzx_call_start;

void next_tape_block(void)
{
	unsigned char  *c;
	unsigned short *s;
//	unsigned int   *i;

	if (!curr_entry)
	{
		tape.state = TS_STOPPED;
		machine_play_tape(0);
	}
	tape.countdown = 0;
	while (curr_entry)
	{
		switch (curr_entry->type)
		{
			case 0x10:  //Standard Speed Data Block
				{
				STANDARD_DATA *data = (STANDARD_DATA *)curr_entry->data_ptr;
				unsigned short *tap_data = (unsigned short *)&data->data;

				tape.state = TS_PILOT;
				tape.pilot = 2168;
				tape.sync1 = 667;
				tape.sync2 = 735;
				tape.zero = 855;
				tape.one = 1710;
				tape.pilot_tone = 8064;
				tape.used_bits = 8;
				tape.pause_ms = data->pause_ms;

				tape.no_pilots = 0;
				tape.data_pos = 0;
				tape.pulses_done = 2;

				tape.countdown = tape.pilot;

				tape.block_len = *tap_data++;
				tape.byte_ptr = tape.byte_start = (unsigned char *)tap_data;
				}
				
				//go do it!
				curr_entry = curr_entry->next;
				return;

			case 0x11:  //Turbo Loading Data Block 
				{
				TURBO_DATA *data = (TURBO_DATA *)curr_entry->data_ptr;
				unsigned char *tap_data = (unsigned char *)(data+1);

				tape.state = TS_PILOT;
				tape.pilot		= data->pilot;
				tape.sync1		= data->sync1;
				tape.sync2		= data->sync2;
				tape.zero		= data->zero;
				tape.one		= data->one;
				tape.pilot_tone	= data->pilot_tone;
				tape.used_bits	= data->used_bits;
				tape.pause_ms	= data->pause_ms;

				tape.no_pilots = 0;
				tape.data_pos = 0;
				tape.pulses_done = 2;

				tape.countdown = tape.pilot;

				tape.block_len = curr_entry->data_len - sizeof(TURBO_DATA);
				tape.byte_ptr = tape.byte_start = (unsigned char *)tap_data;
				}

				//go do it!
				curr_entry = curr_entry->next;
				return;

			case 0x12:  //Pure Tone 
				{
				s = (unsigned short *)curr_entry->data_ptr;
				tape.state = TS_TONE;
				tape.no_pilots = 1;
				tape.pilot = s[0];
				tape.pilot_tone  = s[1];
				tape.countdown = tape.pilot;
				}
				
				//go do it!
				curr_entry = curr_entry->next;
				return;

			case 0x13:  //Sequence of Pulses of Different Lengths 
				{
				c = (unsigned char *)curr_entry->data_ptr;
				tape.pulse_count = *c++;
				s = (unsigned short *)c;
				tape.state = TS_MULTITONE;
				tape.no_pilots = 1;
				tape.pilot = *s++;
				tape.countdown = tape.pilot;
				tape.pulses_done = 1;//0;
				tape.pulse_ptr = s;
				}

				//go do it!
				curr_entry = curr_entry->next;
				return;

			case 0x14:  //Pure Data Block 
				{
				PURE_DATA *data = (PURE_DATA *)curr_entry->data_ptr;
				unsigned char *tap_data = (unsigned char *)(data+1);

				tape.state		= TS_DATA;
				tape.zero		= data->zero;
				tape.one		= data->one;
				tape.used_bits	= data->used_bits;
				tape.pause_ms	= data->pause_ms;

				tape.no_pilots = 0;
				tape.data_pos = 0;
				tape.pulses_done = 2;
				tape.bit_counter = 0x80;
				tape.bit_limit = 1;

				tape.block_len = curr_entry->data_len - sizeof(PURE_DATA);
				tape.byte_ptr   = tape.byte_start = (unsigned char *)tap_data;

				tape.countdown = (*tape.byte_ptr & 0x80) ? tape.one : tape.zero;
				}
				
				//go do it!
				curr_entry = curr_entry->next;
				return;

			case 0x15:  //Direct Recording 
				{
				assert(0);
				//DIRECT_DATA data;
				//long data_len;
				//fread(&data, sizeof(DIRECT_DATA), 1, f);
				//data_len = calc_length(data.data_len);
				//fseek(f, data_len, SEEK_CUR);
				}

				//go do it!
				curr_entry = curr_entry->next;
				return;

			case 0x16:  //C64 ROM Type Data Block 
				assert(0);
				//fread(&length, sizeof(long), 1, f);
				//fseek(f, length, SEEK_CUR);
				break;

			case 0x17:  //C64 Turbo Tape Data Block 
				assert(0);
				//fread(&length, sizeof(long), 1, f);
				//fseek(f, length, SEEK_CUR);
				break;

			case 0x20:  //Pause (Silence) or `Stop the Tape' Command 
				tape.state = TS_END;
				tape.pause_ms = *(unsigned short *)curr_entry->data_ptr;

				//go do it!
				curr_entry = curr_entry->next;
				return;

			case 0x21:  //Group Start
				break;

			case 0x22:  //Group End
				break;

			case 0x23:  //Jump To Block 
				{
				short rel_jump;
				rel_jump = *(unsigned short *)curr_entry->data_ptr;
				assert(rel_jump);
				if (rel_jump < 0)
				{
					while (rel_jump++)
						curr_entry = curr_entry->prev;
				}
				else
				{
					while (rel_jump--)
						curr_entry = curr_entry->next;
				}
				}
				break;

			case 0x24:  //Loop Start 
				tzx_loop_start = curr_entry->block_number;
				tzx_loop_count = *(unsigned short *)curr_entry->data_ptr;
				assert(tzx_loop_count);
				break;

			case 0x25:  //Loop End 
				tzx_loop_count--;
				if (tzx_loop_count)
				{
					while (curr_entry->block_number != tzx_loop_start)
						curr_entry = curr_entry->prev;
					//rely on curr_entry = curr_entry->next to skip over loop start
				}
				break;

			case 0x26:  //Call Sequence 
				assert(0);
				{
				short no_calls;
				short *calls;
				short call;

				calls = (short *)curr_entry->data_ptr;
				tzx_call_start = curr_entry->block_number;
				no_calls = *calls++;

				while (no_calls--)
				{
					call = *calls++;
					if (call < curr_entry->block_number)
					{
						while (call++)
							curr_entry = curr_entry->prev;
					}
					else
					{
						while (call--)
							curr_entry = curr_entry->prev;
					}
				}
				}
				break;

			case 0x27:  //Return From Sequence 
				assert(0);
				break;

			case 0x28:  //Select Block 
				{
				assert(0);
				//unsigned short block_size;
				//fread(&block_size, sizeof(short), 1, f);
				//fseek(f, (long)block_size, SEEK_CUR);
				}
				break;

			case 0x2A:  //Stop Tape if in 48K Mode 
				//assert(0);
				//fread(&length, sizeof(long), 1, f);
				//fseek(f, length, SEEK_CUR);
				break;

			case 0x30:  //Text Description 
				{
				//assert(0);
				//unsigned char name_len;
				//char name[256];
				//fread(&name_len, 1,1, f);
				//fread(name, name_len, 1, f);
				}
				break;

			case 0x31:  //Message Block 
				{
				//assert(0);
				//unsigned char mess_s;
				//unsigned char mess_len;
				//char mess[256];
				//fread(&mess_s, 1,1, f);
				//fread(&mess_len, 1,1, f);
				//fread(mess, mess_len, 1, f);
				}
				break;

			case 0x32:  //Archive Info 
				{
				//assert(0);
				//unsigned short block_size;
				//unsigned char n_strings;
				//
				//unsigned char text_id;
				//unsigned char text_len;
				//unsigned char text[256];
				//
				//fread(&block_size, sizeof(short), 1, f);
				//fread(&n_strings, 1, 1, f);
				//while (n_strings--)
				//{
				//	fread(&text_id, 1,1,f);
				//	fread(&text_len, 1,1,f);
				//	fread(text, text_len,1,f);
				//}
				}
				break;

			case 0x33:  //Hardware Type 
				{
				//assert(0);
				//unsigned char n_machines;
				//
				//unsigned char hw_type;
				//unsigned char hw_id;
				//unsigned char hw_value;
				//
				//fread(&n_machines, 1,1, f);
				//while (n_machines--)
				//{
				//	fread(&hw_type, 1,1, f);
				//	fread(&hw_id, 1,1, f);
				//	fread(&hw_value, 1,1, f);
				//
				//}
				}
				break;

			case 0x34:  //Emulation Info 
				{
				//assert(0);
				//EMULATION_DATA data;
				//fread(&data, sizeof(EMULATION_DATA), 1, f);
				}
				break;

			case 0x35:  //Custom Info Block 
				{
				//assert(0);
				//char id_string[10];
				//fread(id_string, 10, 1, f);
				//fread(&length, sizeof(long), 1, f);
				//fseek(f, length, SEEK_CUR);
				}
				break;

			case 0x40:  //Snapshot Block 
				{
				//assert(0);
				//unsigned char snapshot_type;
				//unsigned char snapshot_length[3];
				//long data_len;
				//
				//fread(&snapshot_type, 1,1, f);
				//fread(snapshot_length, 3,1, f);
				//data_len = calc_length(snapshot_length);
				//fseek(f, data_len, SEEK_CUR);
				}
				break;

			case 0x5A:  //(90 dec, ASCII Letter `Z') 
				//fread(&header, sizeof(TZX_HEADER)-1, 1, f);
				break;

			default:
				//fread(&length, sizeof(long), 1, f);
				//fseek(f, length, SEEK_CUR);
				break;

		}
		curr_entry = curr_entry->next;
	}

	tape.state = TS_STOPPED;
	machine_play_tape(0);
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
unsigned short swabw(unsigned short r)
{
//	return (r>>8)|(r<<8);
	return r;
}

unsigned short swabw2(unsigned short r)
{
	return (r>>8)|(r<<8);
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
#pragma pack(push, 1)
typedef struct
{
	char sp_id[2];
	unsigned short program_len;
	unsigned short program_loc;
	unsigned short BC, DE, HL, AF;
	unsigned short IX,IY;
	unsigned short BC_alt, DE_alt, HL_alt, AF_alt;
	unsigned char R, I;
	unsigned short SP, PC;
	unsigned short reserved1;
	unsigned char border;
	unsigned char reserved2;
	unsigned short status_word;
} SP_HEADER;
#pragma pack(pop)

void load_sp(char *fname)
{
	SP_HEADER sp;
	FILE *f;
	static unsigned char tmp[49152];

	fopen_s(&f,fname, "rb");
	if (!f)
		return;
	fread(&sp, sizeof(SP_HEADER), 1, f);
	assert(sp.program_len <= 0xC000);
	fread(tmp, 49152, 1, f);
	fclose(f);

	reset_machine();
	set_ram(sp.program_loc, tmp, sp.program_len);
	update_screen();

	memset(&start_regs, 0, sizeof(START_REGS));
	start_regs.I = sp.I;
	start_regs.HL_alt = sp.HL_alt;
	start_regs.DE_alt = sp.DE_alt;
	start_regs.BC_alt = sp.BC_alt;
	start_regs.AF_alt = sp.AF_alt;
	start_regs.HL = sp.HL;
	start_regs.DE = sp.DE;
	start_regs.BC = sp.BC;
	start_regs.IY = sp.IY;
	start_regs.IX = sp.IX;
	start_regs.interrupt = (unsigned char)((sp.status_word&1)|((sp.status_word&4)>>1));
	start_regs.R = sp.R;
	start_regs.AF = sp.AF;
	start_regs.SP = sp.SP;
	start_regs.interrupt_mode = (sp.status_word&8)?0:(sp.status_word&2)?2:1;
	start_regs.border_colour = sp.border;
	start_regs.PC = sp.PC;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
#pragma pack(push,1)
typedef struct
{
	unsigned char A;
	unsigned char F;
	unsigned short BC;
	unsigned short HL;
	unsigned short PC;
	unsigned short SP;
	unsigned char I;
	unsigned char R;
	unsigned char flags;
	unsigned short DE;
	unsigned short BC_alt;
	unsigned short DE_alt;
	unsigned short HL_alt;
	unsigned char A_alt;
	unsigned char F_alt;
	unsigned short IY;
	unsigned short IX;
	unsigned char iff1;
	unsigned char iff2;
	unsigned char flags2;
} Z80_HEADER;

typedef struct
{
	unsigned short extra_header_size;
	unsigned short PC;
	unsigned char hardware_mode;
	unsigned char samram_7FFD;
	unsigned char if1_paged;
	unsigned char emulation;
	unsigned char reg_FFFD;
	unsigned char ay_regs[16];
} Z80_V201_HEADER;

typedef struct
{
	unsigned short t_states_low;
	unsigned char  t_states_high;
	unsigned char spectator_flag;
	unsigned char samram_7FFD;
	unsigned char mgt_paged;
	unsigned char mf_paged;
	unsigned char rom_page00_ram;
	unsigned char rom_page01_ram;
	unsigned char keyboard_map[10];
	unsigned char ascii_kb_map[10];
	unsigned char disciple_inhibit_button;
	unsigned char disciple_inhibit_flag;
} Z80_V300_HEADER;

#pragma pack(pop)

void load_z80(char *fname)
{
	FILE *f;
	Z80_HEADER v1;
	Z80_V201_HEADER v2;
	Z80_V300_HEADER v3;
	int version;

	version = 1;

	fopen_s(&f,fname, "rb");
	if (!f)
		return;
	fread(&v1, sizeof(Z80_HEADER), 1, f);

	if (v1.flags==255) v1.flags = 1;

	memset(&start_regs, 0, sizeof(START_REGS));
	start_regs.I =		v1.I;
	start_regs.HL_alt = swabw(v1.HL_alt);
	start_regs.DE_alt = swabw(v1.DE_alt);
	start_regs.BC_alt = swabw(v1.BC_alt);
	start_regs.AF_alt = ((unsigned short)v1.A_alt<<8)|(v1.F_alt);
	start_regs.HL =		swabw(v1.HL);
	start_regs.DE =		swabw(v1.DE);
	start_regs.BC =		swabw(v1.BC);
	start_regs.IY =		swabw(v1.IY);
	start_regs.IX =		swabw(v1.IX);
	start_regs.interrupt =		(v1.iff1&1)|((v1.iff2&1)<<1);
	start_regs.R =		v1.R&0x7f;
	start_regs.R7 =		(v1.flags&1)*0x80;
	start_regs.AF = ((unsigned short)v1.A<<8)|(v1.F);
	start_regs.SP =		swabw(v1.SP);
	start_regs.interrupt_mode = v1.flags2&3;
	start_regs.border_colour =	(v1.flags>>1)&7;
	start_regs.PC = swabw(v1.PC);

#pragma message("startup regs from Z80 can contain more!")

	if (v1.PC==0)
	{
		//assert((v1.flags&(1<<5))==0);//deal with compression later

		fread(&v2, sizeof(Z80_V201_HEADER), 1, f);
		version++;
		assert(v2.extra_header_size == 23 || v2.extra_header_size == 54);

		start_regs.port_7ffd = v2.samram_7FFD;

		if (v2.extra_header_size > sizeof(Z80_V201_HEADER))
		{
			version++;
			fread(&v3, sizeof(Z80_V300_HEADER), 1, f);
		}

		start_regs.PC = swabw(v2.PC);

		if (v2.hardware_mode == 0 || v2.hardware_mode == 1)
			init_machine(SPEC_48K);
		else
			init_machine(SPEC_128K);

		for(;;)
		{
			unsigned short len;
			unsigned char  page;
			unsigned short dest=0;
			static unsigned char tmp[0x4000];
			static unsigned char tmp2[0x4000];

			fread(&len, sizeof(short), 1, f);
			if (feof(f)) break;
			fread(&page, sizeof(char), 1, f);
			if (feof(f)) break;

			assert(page>=0 && page <= 11);

			//normal banks are 5,2,0 (0x4000,0x8000,0xC000)

			if (v2.hardware_mode == 0 || v2.hardware_mode == 1)
			{
				switch (page)
				{
					case 4:
						dest = 0x8000;
						break;
					case 5:
						dest = 0xC000;
						break;
					case 8:
						dest = 0x4000;
						break;
					default:
						assert(0);
				}
			}
			else
			{
				switch (page)
				{
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
					case 10:
						page -= 3;
						page_ram(page);
						dest = 0xC000;
						break;
					default:
						assert(0);
				}
			}

			if (len == 0xffff)
				fread(tmp, 0x4000, 1, f);
			else
			{
				/*
				https://worldofspectrum.org/faq/reference/z80format.htm.
				The compression method is very simple:
					it replaces repetitions of at least five equal bytes by a four-byte code ED ED xx yy, which stands for "byte yy repeated xx times".
					Only sequences of length at least 5 are coded.
					The exception is sequences consisting of ED's; if they are encountered, even two ED's are encoded into ED ED 02 ED.
					Finally, every byte directly following a single ED is not taken into a block, for example ED 6*00 is not encoded into ED ED ED 06 00 but into ED 00 ED ED 05 00.
					The block is terminated by an end marker, 00 ED ED 00.
				*/

				//decompress
				int i = 0;
				int c;

				fread(tmp2, len, 1, f);
				c = 0;
				do
				{
					if (tmp2[c] == 0xED && tmp2[c+1] == 0xED)
					{
						c += 2;
						while (tmp2[c]--)
							tmp[i++] = tmp2[c+1];
						c += 2;
					}
					else
					{
						tmp[i++]=tmp2[c++];
					}
				} while (i < 16384);
			}

			set_ram(dest, tmp, 0x4000);
		}

		//page in the correct ram
		out(0x7FFD, v2.samram_7FFD);

		update_screen();
	}
	else
	{
		static unsigned char tmp[49152];

		init_machine(SPEC_48K);

		if (v1.flags&(1<<5))
		{
			//decompress
			static char tmp2[49152];
			int i = 0;
			int c;

			fread(tmp2, 49152, 1, f);
			c = 0;
			do
			{
				if (tmp2[c] == 0 && tmp2[c+1] == 0xED && tmp2[c+2] == 0xED && tmp2[c+3] == 0)
					break;
				if (tmp2[c] == 0xED && tmp2[c+1] == 0xED)
				{
					c += 2;
					while (tmp2[c]--)
						tmp[i++] = tmp2[c+1];
					c += 2;
				}
				else
				{
					tmp[i++]=tmp2[c++];
				}
			} while (i < 49152);
		}
		else
		{
			//it's just 48k of data
			fread(tmp, 49152, 1, f);
		}
		fclose(f);

		reset_machine();
		set_ram(0x4000, tmp, 0xC000);
		update_screen();
	}
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void load_zx(char *fname)
{
	fname;
	assert(0);
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
void load_scr(char *fname)
{
	FILE *f;
	unsigned char tmp[6912];
	fopen_s(&f,fname, "rb");
	if (f)
	{
		fread(tmp, 6912, 1, f);
		fclose(f);
	}
	set_ram(0x4000, tmp, 6912);
	update_screen();
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
static int loadopts(char *fname, int type, int insidezip)
{
	if (!fname) return 0;

	char *p = strrchr(fname, '.');

	if (!p) return 0;

	//safety
	memset(&start_regs, 0, sizeof(START_REGS));

	if (!_stricmp(p, ".sna") && type == 1)
		load_sna(fname);
	else if (!_stricmp(p, ".z80") && type == 1)
		load_z80(fname);
	else if (!_stricmp(p, ".tzx") && type == 0)
		load_tzx(fname);
	else if (!_stricmp(p, ".tap") && type == 0)
		load_tap(fname);
	else if (!_stricmp(p, ".scr") && type == 1)
	{
		load_scr(fname);
		return 1;
	}
	else if (!_stricmp(p, ".sp") && type == 1)
		load_sp(fname);
	else if (!_stricmp(p, ".zx") && type == 1)
		load_sp(fname);
	if (!_stricmp(p, ".zip"))
	{
		char *n = load_zip(fname, type);
		int rv = 0;
		if (n)
		{
			save_mru(type == 1 ? MRU_SNA : MRU_TAP, fname);
			rv = loadopts(n, type, TRUE);
			_unlink(n);
		}
		return rv;
	}
	assert(_CrtCheckMemory());
	if (!insidezip)
		save_mru(type == 1 ? MRU_SNA : MRU_TAP, fname);

	return 0;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
START_REGS *load(void)
{
	OPENFILENAME ofn;
	char filename[_MAX_PATH];
	BOOL res;

	filename[0] = '\0';
	ofn.lStructSize = sizeof(OPENFILENAME); 
	ofn.hwndOwner = dis_window; 
	ofn.hInstance = NULL;
	ofn.lpstrFilter = "Snapshot Files\0*.SNA;*.Z80;*.SP;*.SCR;*.ZIP\0"; 
	ofn.lpstrCustomFilter = NULL; 
	ofn.nMaxCustFilter = 0; 
	ofn.nFilterIndex = 0; 
	ofn.lpstrFile = filename; 
	ofn.nMaxFile = _MAX_PATH; 
	ofn.lpstrFileTitle = NULL; 
	ofn.nMaxFileTitle = 0; 
	ofn.lpstrInitialDir = NULL; 
	ofn.lpstrTitle = "Select Snapshot File"; 
	ofn.Flags = OFN_FILEMUSTEXIST|OFN_EXPLORER; 
	ofn.nFileOffset=0; 
	ofn.nFileExtension=0; 
	ofn.lpstrDefExt="sna"; 
	ofn.lCustData=0; 
	ofn.lpfnHook=NULL; 
	ofn.lpTemplateName=""; 

	res = GetOpenFileName(&ofn);
	if (res)
	{

		strcpy_s(lastfilename, sizeof lastfilename, filename);
		if (loadopts(filename, 1, FALSE))
			return NULL;
		return &start_regs;
	}
	else
	{
		lastfilename[0]='\0';
	}
	return NULL;
}

//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
int load_tape(void)
{
	OPENFILENAME ofn;
	char filename[_MAX_PATH];
	BOOL res;

	filename[0] = '\0';
	ofn.lStructSize = sizeof(OPENFILENAME); 
	ofn.hwndOwner = dis_window; 
	ofn.hInstance = NULL;
	ofn.lpstrFilter = "Tape Files\0*.TZX;*.TAP;*.ZIP\0"; 
	ofn.lpstrCustomFilter = NULL; 
	ofn.nMaxCustFilter = 0; 
	ofn.nFilterIndex = 0; 
	ofn.lpstrFile = filename; 
	ofn.nMaxFile = _MAX_PATH; 
	ofn.lpstrFileTitle = NULL; 
	ofn.nMaxFileTitle = 0; 
	ofn.lpstrInitialDir = NULL; 
	ofn.lpstrTitle = "Select Tape File"; 
	ofn.Flags = OFN_FILEMUSTEXIST|OFN_EXPLORER; 
	ofn.nFileOffset=0; 
	ofn.nFileExtension=0; 
	ofn.lpstrDefExt="tzx"; 
	ofn.lCustData=0; 
	ofn.lpfnHook=NULL; 
	ofn.lpTemplateName=""; 

	res = GetOpenFileName(&ofn);
	if (res)
	{

		free_tape();
		strcpy_s(tape_filename, sizeof tape_filename, filename);
		strcpy_s(lastfilename, sizeof lastfilename, filename);
		loadopts(tape_filename, 0, FALSE);
		return 1;
	}
	else
	{
		lastfilename[0]='\0';
	}
return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
START_REGS *load_snapshot_file(char *file)
{
	loadopts(file, 1, FALSE);
	return &start_regs;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void load_tape_file(char *file)
{
	free_tape();
	loadopts(file, 0, FALSE);
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void save(void)
{
	OPENFILENAME ofn;
	char filename[_MAX_PATH];
	BOOL res;

	filename[0] = '\0';
	ofn.lStructSize = sizeof(OPENFILENAME); 
	ofn.hwndOwner = dis_window; 
	ofn.hInstance = NULL;
	ofn.lpstrFilter = "Snapshot Files\0*.SNA\0All Files\0*.*\0"; 
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0; 
	ofn.nFilterIndex = 0; 
	ofn.lpstrFile = filename; 
	ofn.nMaxFile = _MAX_PATH; 
	ofn.lpstrFileTitle = NULL; 
	ofn.nMaxFileTitle = 0; 
	ofn.lpstrInitialDir = NULL; 
	ofn.lpstrTitle = "Select Snapshot File"; 
	ofn.Flags = OFN_FILEMUSTEXIST|OFN_EXPLORER;
	ofn.nFileOffset=0; 
	ofn.nFileExtension=0; 
	ofn.lpstrDefExt="sna";
	ofn.lCustData=0; 
	ofn.lpfnHook=NULL; 
	ofn.lpTemplateName=""; 

	res = GetSaveFileName(&ofn);
	if (res)
	{
		save_sna(filename);
	}

	return;
}

int universalT;
void start_tape(void)
{
	tape.bit = 0;
	universalT = 0;
	next_tape_block();
}

void stop_tape(void)
{
	tape.state = TS_STOPPED;
}

void init_tape(void)
{
	tape.state = TS_STOPPED;
}

void ff_tape(void)
{
	if (curr_entry && curr_entry->next)
		curr_entry = curr_entry->next;
}

void rew_tape(void)
{
	if (curr_entry && curr_entry->prev)
		curr_entry = curr_entry->prev;
}

void reset_tape(void)
{
	tape.state = TS_STOPPED;
	curr_entry = curr_tape;
}

void set_tape_position(int index)
{
	reset_tape();
	while (index-- && curr_entry != NULL && curr_entry->next != NULL)
		curr_entry = curr_entry->next;
}

unsigned char get_next_bitX(int elapsed);
unsigned char get_next_bit(int elapsed)
{
	get_next_bitX(elapsed);
	return tape.bit;
/*
	if (tape.state != TS_STOPPED)
		universalT += elapsed;
	
	//if (tape.state != TS_STOPPED)
	//{
	//	char tmp[100];
	//	sprintf_s(tmp, 100, "%d ", elapsed);
	//	OutputDebugString(tmp);
	//}
	
	static unsigned char last_bit = -1;
	if (last_bit != tape.bit && tape.state != TS_STOPPED)
	{
		static int lastT = 0;
		char tmp[100];
		sprintf_s(tmp, 100, "%d %d %d %d\n", tape.state, (int)tape.bit, universalT, universalT - lastT);
		OutputDebugString(tmp);
		last_bit = tape.bit;
		lastT = universalT;
	}

	return tape.bit;
*/
}

unsigned char get_next_bitX(int elapsed)
{
	while (elapsed > 0)
	{
		if (elapsed < tape.countdown)
		{
			tape.countdown -= elapsed;
			break;
		}
		else
		{
			elapsed -= tape.countdown;
			switch (tape.state)
			{
				case TS_STOPPED:
					tape.countdown = 0;
					elapsed=0;
					break;

				case TS_PILOT:// playing pilot tone.
					tape.bit ^= 1;
					if (tape.no_pilots < tape.pilot_tone) // byte holds number of pilots
					{
						tape.countdown = tape.pilot;
						tape.no_pilots++;
					}
					else
					{
						tape.no_pilots = 0;
						tape.state = TS_SYNC1; // set to sync1 pilot output
						tape.countdown = tape.sync1;
					}
					break;

				case TS_SYNC1:// sync 1
					tape.bit ^= 1;
					tape.state = TS_SYNC2; // set to sync2 pilot output
					tape.countdown = tape.sync2;
					break;

				case TS_SYNC2:// sync 2
					tape.bit ^= 1;
					tape.state = TS_DATA; // set to data byte(s) output
					if (*tape.byte_ptr & 0x80) // set next pilot length
						tape.countdown = tape.one;
					else
						tape.countdown = tape.zero;

					tape.pulses_done = 2; // *2* edges per data bit, one on, one off
					tape.bit_counter = 0x80; // start with the full byte
					tape.bit_limit = 1;
					break;

				case TS_DATA:// // data bytes out
					tape.bit ^= 1;
					tape.pulses_done--;
					if (!tape.pulses_done) // done both pulses for this bit?
					{
						if (tape.bit_counter > tape.bit_limit) // done all the bits for this byte?
						{
							tape.bit_counter>>=1;
							tape.pulses_done = 2;
							if (*tape.byte_ptr & tape.bit_counter)
								tape.countdown = tape.one;
							else
								tape.countdown = tape.zero;
						}
						else // all bits done, setup for next byte
						{
							tape.byte_ptr++;
							if (tape.byte_ptr < tape.byte_start+tape.block_len) // last byte?
							{
								if (tape.byte_ptr == tape.byte_start+tape.block_len - 1)
									tape.bit_limit = 1<<(8 - tape.used_bits); // if so, set up the last bits used
								else
									tape.bit_limit = 1; // else use full 8 bits

								tape.bit_counter = 0x80;
								tape.pulses_done = 2;
								if (*tape.byte_ptr & 0x80)
									tape.countdown = tape.one;
								else
									tape.countdown = tape.zero;
							}
							else
							{
								if (tape.pause_ms > 0)
								{
									tape.countdown = tape.pause_ms * 3500;//3.5MHz
									tape.state = TS_END; // set to pause_ms output
								}
								else
								{
									next_tape_block();
								}
							}
						}
					}
					else // not done both pilots, flip the ear bit next time
					{
						if (*tape.byte_ptr & tape.bit_counter)
							tape.countdown = tape.one;
						else
							tape.countdown = tape.zero;
					}
					break;

				case TS_TONE:
					tape.bit ^= 1;
					if (tape.no_pilots < tape.pilot_tone)
					{
						tape.countdown = tape.pilot;
						tape.no_pilots++;
					}
					else
					{
						next_tape_block();
					}
					break;

				case TS_MULTITONE:
					tape.bit ^= 1;
					if (tape.pulses_done < tape.pulse_count)
					{
						tape.countdown = *tape.pulse_ptr++;
						tape.pulses_done++;
					}
					else
					{
						next_tape_block();
					}
					break;

				case TS_END:// end pause_ms output
					next_tape_block();
					break;

				default:
					next_tape_block();
			}
		}
	}

	return tape.bit;
}

static void dmp(int elapsed)
{
	static char tmp[10000];
	char *tptr = tmp;
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "\n[TAPE]\n");
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "elapsed %d\n", elapsed);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "countdown %d\n", tape.countdown);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "pilot %d\n", tape.pilot);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "sync1 %d\n", tape.sync1);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "sync2 %d\n", tape.sync2);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "zero %d\n", tape.zero);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "one %d\n", tape.one);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "pilot_tone %d\n", tape.pilot_tone);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "pause_ms %d\n", tape.pause_ms);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "block_len %d\n", tape.block_len);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "cur_block %d\n", tape.cur_block);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "data_pos %d\n", tape.data_pos);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "pulses_done %d\n", tape.pulses_done);
	tptr += sprintf_s(tptr, tmp - tptr + 10000, "pulse_count %d\n", tape.pulse_count);
	elog(tmp);
}


//--------------------------------------------------------------------------------------
//
//--------------------------------------------------------------------------------------
static char selectedfile[_MAX_PATH];

//filter, eg. "Tape Files\0*.TZX;*.TAP\0";
char *generic_load(char *filter, char *caption, char *defext, char *suggested_file)
{
	OPENFILENAME ofn;
	BOOL res;

	if (suggested_file)
		strcpy_s(selectedfile, _MAX_PATH, suggested_file);
	else
		selectedfile[0] = '\0';

	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = dis_window;
	ofn.hInstance = NULL;
	ofn.lpstrFilter = filter;
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = 0;
	ofn.lpstrFile = selectedfile;
	ofn.nMaxFile = _MAX_PATH;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrTitle = caption;
	ofn.Flags = OFN_FILEMUSTEXIST | OFN_EXPLORER;
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = defext;
	ofn.lCustData = 0;
	ofn.lpfnHook = NULL;
	ofn.lpTemplateName = "";

	res = GetOpenFileName(&ofn);
	if (res)
		return selectedfile;

	selectedfile[0] = '\0';
	return NULL;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
char *generic_save(char *filter, char *caption, char *defext, char *suggested_file)
{
	OPENFILENAME ofn;
	BOOL res;

	if (suggested_file)
		strcpy_s(selectedfile, _MAX_PATH, suggested_file);
	else
		selectedfile[0] = '\0';

	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = dis_window;
	ofn.hInstance = NULL;
	ofn.lpstrFilter = filter;
	ofn.lpstrCustomFilter = NULL;
	ofn.nMaxCustFilter = 0;
	ofn.nFilterIndex = 0;
	ofn.lpstrFile = selectedfile;
	ofn.nMaxFile = _MAX_PATH;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrTitle = caption;
	ofn.Flags = OFN_EXPLORER;
	ofn.nFileOffset = 0;
	ofn.nFileExtension = 0;
	ofn.lpstrDefExt = defext;
	ofn.lCustData = 0;
	ofn.lpfnHook = NULL;
	ofn.lpTemplateName = "";

	res = GetSaveFileName(&ofn);
	if (res)
		return selectedfile;

	selectedfile[0] = '\0';
	return NULL;
}
