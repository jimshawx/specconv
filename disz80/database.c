//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "sqlite/sqlite3.h"

#include "daos.h"
#include "database.h"
#include "logger.h"
#include "dbgmem.h"

static int init_and_upgrade_schema(sqlite3 *db);

static sqlite3 *database;

static void db_logger(void *data, int err, const char *msg)
{
	data;
	eloglevel(LOG_ERROR, "[DB] (%d) %s\n", err, msg);
}

static void sql_logger(void *data, sqlite3 *db, const char *sql, int logtype)
{
	data; db;
	if (logtype == 0) elog("[DB] opened %s @%p", sql, db);
	if (logtype == 1) elog("[DB] %s\n", sql);
	if (logtype == 2) elog("[DB] closed @%p", db);
}

sqlite3 *db_init(void)
{
	int err;

	sqlite3_config(SQLITE_CONFIG_LOG, db_logger, NULL);
	sqlite3_config(SQLITE_CONFIG_SQLLOG, sql_logger, NULL);

	err = sqlite3_open("databases/disassembly.db", &database);
	if (err != SQLITE_OK)
		return NULL;

	int fk_status = -1;
	err = sqlite3_db_config(database, SQLITE_DBCONFIG_ENABLE_FKEY, 1, &fk_status);
	if (err != SQLITE_OK)
	{
		sqlite3_close(database);
		database = NULL;
		return NULL;
	}
	elog("[DB] %s\n", sqlite3_libversion());

	err = init_and_upgrade_schema(database);
	if (err)
	{
		sqlite3_close(database);
		database = NULL;
		return NULL;
	}

	return database;
}

void db_shutdown(void)
{
	if (database)
		sqlite3_close(database);
	database = NULL;
}

/*
int db_callback(void *args, int num_columns, char **column_text_ptr, char **column_name_ptr)
{
	args;
	//if (args) elog("%s\n", args);
	while (num_columns--)
	{
		elog("%s %s\n", *column_name_ptr++, *column_text_ptr++);
	}
	//success
	return 0;
}
*/

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------

int begin(sqlite3 *db)
{
	return sqlite3_exec(db, "begin transaction;", NULL, NULL, NULL);
}

int rollback(sqlite3 *db)
{
	return sqlite3_exec(db, "rollback;", NULL, NULL, NULL);
}

int commit(sqlite3 *db)
{
	return sqlite3_exec(db, "commit;", NULL, NULL, NULL);
}

static int exec(sqlite3 *db, char *sql)
{
	return sqlite3_exec(db, sql, NULL, NULL, NULL);
}

static int execstatement(sqlite3 *db, sqlite3_stmt *res)
{
	int err = sqlite3_step(res);
	sqlite3_finalize(res);
	if (err != SQLITE_ROW && err != SQLITE_DONE)
	{
		eloglevel(LOG_ERROR, "[DB] %s\n", sqlite3_errmsg(db));
		return -1;
	}
	return 0;
}

static int immediate(sqlite3 *db, const char *const sql)
{
	int err;
	sqlite3_stmt *res;

	err = sqlite3_prepare_v2(db, sql, -1, &res, NULL);
	if (err != SQLITE_OK)
		return -1;

	int value = -1;
	err = sqlite3_step(res);

	if (err == SQLITE_ROW)
		value = sqlite3_column_int(res, 0);

	sqlite3_finalize(res);

	return value;
}

static int immediatestatement(sqlite3_stmt *res)
{
	int err;

	int value = -1;
	err = sqlite3_step(res);

	if (err == SQLITE_ROW)
		value = sqlite3_column_int(res, 0);

	sqlite3_finalize(res);

	return value;
}

static int exists(sqlite3 *db, char *sql, int value)
{
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db, sql, -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, value);
	int count = immediatestatement(stmt);
	return count > 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------

#define MAX_ROW_COUNT 1000000
#define MAX_STRING_COUNT 100000

DB_RES *db_new(size_t rowsize)
{
	DB_RES *rv = calloc(1, sizeof *rv);
	assert(rv);
	rv->rowsize = rowsize;
	rv->rows = malloc(MAX_ROW_COUNT * rv->rowsize);
	rv->strings = malloc(MAX_STRING_COUNT * sizeof(char *));
	return rv;
}

void db_free(DB_RES *res)
{
	while (--res->n_strings >= 0)
		free(res->strings[res->n_strings]);
	free(res->strings);
	free(res->rows);
	free(res);
}

void *db_rowptr(DB_RES *res)
{
	void *rv = (void *)((char *)res->rows + res->count * res->rowsize);
	res->count++;
	assert(res->count < MAX_ROW_COUNT);
	return rv;
}

char *db_strdup(DB_RES *res, const unsigned char *str)
{
	if (str == NULL)
		return NULL;

	size_t len = strlen((const char *)str)+1;
	char *b = malloc(len);
	assert(b);
	strcpy_s(b, len, (const char *)str);
	res->strings[res->n_strings++] = b;
	assert(res->n_strings < MAX_STRING_COUNT);
	return b;
}

DB_RES *db_result(DB_RES *res)
{
#pragma warning(push)
#pragma warning(disable: 6308)
	res->strings = realloc(res->strings, sizeof(char *) * res->n_strings);
	res->rows = realloc(res->rows, res->rowsize * res->count);
#pragma warning(pop)
	return res;
}

#define SEARCH_PREAMBLE(type)\
DB_RES *res = db_new(sizeof(type));\
sqlite3_stmt *stmt;

#define SEARCH_EXECUTE(mapper)\
while (sqlite3_step(stmt) == SQLITE_ROW)\
	mapper(res, stmt);\
sqlite3_finalize(stmt);\
return db_result(res);

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------

typedef struct
{
	int version;
	ZXTIME time;
	char *description;
} ZX_SCHEMA;

const char SCHEMA_create[] = "create table zxschema (version integer primary key not null, time real not null, description text); insert into zxschema values (0, julianday('now'), 'initial schema');";
void SCHEMA_map_in(DB_RES *res, sqlite3_stmt *stmt)
{
	ZX_SCHEMA *zx = (ZX_SCHEMA *)db_rowptr(res);
	zx->version = sqlite3_column_int(stmt, 0);
	zx->time = sqlite3_column_double(stmt, 1);
	zx->description = db_strdup(res, sqlite3_column_text(stmt, 2));
}

void SCHEMA_map_out(ZX_SCHEMA *zx, sqlite3_stmt *stmt)
{
	sqlite3_bind_int(stmt, 1, zx->version);
	sqlite3_bind_text(stmt, 2, zx->description, -1, NULL);
}

int save_ZXSCHEMA(sqlite3 *db, ZX_SCHEMA *zx)
{
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db, "insert into zxschema (version, time, description) values (?,julianday('now'),?);", -1, &stmt, NULL);
	SCHEMA_map_out(zx, stmt);
	int err = sqlite3_step(stmt);
	if (err != SQLITE_DONE) eloglevel(LOG_ERROR, "[DB] ZXSCHEMA %s\n", sqlite3_errmsg(db));
	sqlite3_finalize(stmt);
	return err != SQLITE_DONE;
}
//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------

const char DB_create[] = "create table zxdb (id integer primary key autoincrement, time real not null, name text not null);";

void DB_map_in(DB_RES *res, sqlite3_stmt *stmt)
{
	ZX_DB *zx = (ZX_DB *)db_rowptr(res);
	zx->id = sqlite3_column_int(stmt, 0);
	zx->time = sqlite3_column_double(stmt, 1);
	zx->name = db_strdup(res, sqlite3_column_text(stmt, 2));
}

void DB_map_out(ZX_DB *zx, sqlite3_stmt *stmt)
{
	sqlite3_bind_text(stmt, 1, zx->name, -1, NULL);
}

int save_ZXDB(sqlite3 *db, ZX_DB *zx)
{
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db, "insert into zxdb (time, name) values (julianday('now'),?);", -1, &stmt, NULL);
	DB_map_out(zx, stmt);
	int err = sqlite3_step(stmt);
	if (err != SQLITE_DONE) eloglevel(LOG_ERROR, "[DB] ZXDB %s\n", sqlite3_errmsg(db));
	sqlite3_finalize(stmt);
	return (err == SQLITE_DONE) ? (int)sqlite3_last_insert_rowid(db) : -1;
}

DB_RES *search_ZXDB(sqlite3 *db)
{
	SEARCH_PREAMBLE(ZX_DB);
	sqlite3_prepare_v2(db, "select id,time,name from zxdb order by name;", -1, &stmt, NULL);
	SEARCH_EXECUTE(DB_map_in);
}

int get_ZXDB_id_by_name(sqlite3 *db, char *name)
{
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db, "select id from zxdb where name = ?;", -1, &stmt, NULL);
	sqlite3_bind_text(stmt, 1, name, -1, NULL);
	return immediatestatement(stmt);
}

DB_RES *get_ZXDB_by_dbid(sqlite3 *db, int dbid)
{
	SEARCH_PREAMBLE(ZX_DB);
	sqlite3_prepare_v2(db, "select id,time,name from zxdb where id = ?;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, dbid);
	SEARCH_EXECUTE(DB_map_in);
}

int delete_ZXDB(sqlite3 *db, int dbid)
{
	sqlite3_stmt *stmt;

	begin(db);

	sqlite3_prepare_v2(db, "delete from zxlabel where dbid = ?;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, dbid);
	if (execstatement(db, stmt) == -1)
	{
		rollback(db);
		return -1;
	}
	sqlite3_prepare_v2(db, "delete from zxtype where dbid = ?;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, dbid);
	if (execstatement(db, stmt) == -1)
	{
		rollback(db);
		return -1;
	}

	sqlite3_prepare_v2(db, "delete from zxdb where id = ?;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, dbid);
	if (execstatement(db, stmt) == -1)
	{
		rollback(db);
		return -1;
	}

	commit(db);

	return 0;
}

//---------------------------------------------------------------------------------------

const char LABEL_create[] = "create table zxlabel (id integer primary key autoincrement, time real not null, address integer not null, name text not null, dbid integer not null, foreign key (dbid) references zxdb(id));";

void LABEL_map_in(DB_RES *res, sqlite3_stmt *stmt)
{
	ZX_LABEL *zx = (ZX_LABEL *)db_rowptr(res);
	zx->id = sqlite3_column_int(stmt, 0);
	zx->time = sqlite3_column_double(stmt, 1);
	zx->address = (unsigned short)sqlite3_column_int(stmt, 2);
	zx->name = db_strdup(res, sqlite3_column_text(stmt, 3));
	zx->dbid = sqlite3_column_int(stmt, 4);
	zx->source = (ZX_SOURCE)sqlite3_column_int(stmt, 5);
	zx->type = (ZX_LABELTYPE)sqlite3_column_int(stmt, 6);
}

void LABEL_map_out(ZX_LABEL *zx, sqlite3_stmt *stmt)
{
	sqlite3_bind_int(stmt, 1, zx->address);
	sqlite3_bind_text(stmt, 2, zx->name, -1, NULL);
	sqlite3_bind_int(stmt, 3, zx->dbid);
	sqlite3_bind_int(stmt, 4, zx->source);
	sqlite3_bind_int(stmt, 5, zx->type);
}

int save_ZXLABEL(sqlite3 *db, ZX_LABEL *zx)
{
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db, "insert into zxlabel (time, address, name, dbid, source, type) values (julianday('now'), ?,?,?,?,?);", -1, &stmt, NULL);
	LABEL_map_out(zx, stmt);
	int err = sqlite3_step(stmt);
	if (err != SQLITE_DONE) eloglevel(LOG_ERROR, "[DB] ZXLABEL %s\n", sqlite3_errmsg(db));
	sqlite3_finalize(stmt);
	return (err == SQLITE_DONE) ? (int)sqlite3_last_insert_rowid(db) : -1;
}
void save_ZXLABEL_batch(sqlite3 *db, ZX_LABEL *labels, int count)
{
	begin(db);
	while (count--)
		save_ZXLABEL(db, labels++);
	commit(db);
}

DB_RES *search_ZXLABEL(sqlite3 *db)
{
	SEARCH_PREAMBLE(ZX_LABEL);
	sqlite3_prepare_v2(db, "select id,time,address,name,dbid,source,type from zxlabel order by time;", -1, &stmt, NULL);
	SEARCH_EXECUTE(LABEL_map_in);
}

DB_RES *search_ZXLABEL_by_name(sqlite3 *db, int dbid, char *name)
{
	SEARCH_PREAMBLE(ZX_LABEL);
	sqlite3_prepare_v2(db, "select id,time,address,name,dbid,source,type from zxlabel where dbid = ? and name = ? order by time;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, dbid);
	sqlite3_bind_text(stmt, 2, name, -1, NULL);
	SEARCH_EXECUTE(LABEL_map_in);
}

DB_RES *search_ZXLABEL_by_address(sqlite3 *db, int dbid, unsigned int address)
{
	SEARCH_PREAMBLE(ZX_LABEL);
	sqlite3_prepare_v2(db, "select id,time,address,name,dbid,source,type from zxlabel where dbid = ? and address = ? order by time;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, dbid);
	sqlite3_bind_int(stmt, 2, address);
	SEARCH_EXECUTE(LABEL_map_in);
}

DB_RES *search_ZXLABEL_by_dbid(sqlite3 *db, int dbid)
{
	SEARCH_PREAMBLE(ZX_LABEL);
	sqlite3_prepare_v2(db, "select id,time,address,name,dbid,source,type from zxlabel where dbid = ? order by address;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, dbid);
	SEARCH_EXECUTE(LABEL_map_in);
}

int delete_ZXLABEL(sqlite3 *db, int id)
{
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db, "delete from zxlabel where id = ?;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, id);
	return execstatement(db, stmt);
}

int update_ZXLABEL(sqlite3 *db, ZX_LABEL *zx)
{
	int err;
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db, "update zxlabel set (time, address, name, dbid, source, type) = (julianday('now'),?,?,?,?,?) where id = ?;", -1, &stmt, NULL);
	LABEL_map_out(zx, stmt);
	sqlite3_bind_int(stmt, 6, zx->id);
	err = sqlite3_step(stmt);
	if (err != SQLITE_DONE)
		eloglevel(LOG_ERROR, "[DB] %s\n", sqlite3_errmsg(db));
	sqlite3_finalize(stmt);
	return (err == SQLITE_DONE) ? zx->id : -1;
}
//---------------------------------------------------------------------------------------

const char TYPE_create[] = "create table zxtype (id integer primary key autoincrement, time real not null, type integer not null, address integer not null, size integer not null, dbid integer not null, foreign key (dbid) references zxdb(id));";

void TYPE_map_in(DB_RES *res, sqlite3_stmt *stmt)
{
	ZX_TYPE *zx = (ZX_TYPE *)db_rowptr(res);
	zx->id = sqlite3_column_int(stmt, 0);
	zx->time = sqlite3_column_double(stmt, 1);
	zx->type = (ZX_DATA_TYPE)sqlite3_column_int(stmt, 2);
	zx->address = (unsigned short)sqlite3_column_int(stmt, 3);
	zx->size = (unsigned int)sqlite3_column_int(stmt, 4);
	zx->dbid = sqlite3_column_int(stmt, 5);
	zx->source = (ZX_SOURCE)sqlite3_column_int(stmt, 6);
	zx->structid = sqlite3_column_int(stmt, 7);
	zx->gfxtype = (ZX_GFX_TYPE)sqlite3_column_int(stmt, 8);
	zx->gfxwidth = (short)sqlite3_column_int(stmt, 9);
	zx->gfxheight = (short)sqlite3_column_int(stmt, 10);
}

void TYPE_map_out(ZX_TYPE *zx, sqlite3_stmt *stmt)
{
	sqlite3_bind_int(stmt, 1, zx->type);
	sqlite3_bind_int(stmt, 2, zx->address);
	sqlite3_bind_int(stmt, 3, zx->size);
	sqlite3_bind_int(stmt, 4, zx->dbid);
	sqlite3_bind_int(stmt, 5, zx->source);
	sqlite3_bind_int(stmt, 6, zx->structid);
	sqlite3_bind_int(stmt, 7, zx->gfxtype);
	sqlite3_bind_int(stmt, 8, zx->gfxwidth);
	sqlite3_bind_int(stmt, 9, zx->gfxheight);
}

int save_ZXTYPE(sqlite3 *db, ZX_TYPE *zx)
{
	int err;
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db, "insert into zxtype (time, type, address, size, dbid, source, structid, gfxtype, gfxwidth, gfxheight) values (julianday('now'),?,?,?,?,?,?,?,?,?);", -1, &stmt, NULL);
	TYPE_map_out(zx, stmt);
	err = sqlite3_step(stmt);
	if (err != SQLITE_DONE)
		eloglevel(LOG_ERROR, "[DB] %s\n", sqlite3_errmsg(db));
	sqlite3_finalize(stmt);
	return (err == SQLITE_DONE) ? (int)sqlite3_last_insert_rowid(db) : -1;
}

void save_ZXTYPE_batch(sqlite3 *db, ZX_TYPE *types, int count)
{
	begin(db);
	while (count--)
		save_ZXTYPE(db, types++);
	commit(db);
}

DB_RES *search_ZXTYPE(sqlite3 *db)
{
	SEARCH_PREAMBLE(ZX_TYPE);
	sqlite3_prepare_v2(db, "select id,time,type,address,size,dbid,source,structid, gfxtype, gfxwidth, gfxheight from zxtype order by time;", -1, &stmt, NULL);
	SEARCH_EXECUTE(TYPE_map_in);
}

DB_RES *search_ZXTYPE_by_dbid(sqlite3 *db, int dbid)
{
	SEARCH_PREAMBLE(ZX_TYPE);
	sqlite3_prepare_v2(db, "select id,time,type,address,size,dbid,source,structid, gfxtype, gfxwidth, gfxheight from zxtype where dbid = ? order by address;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, dbid);
	SEARCH_EXECUTE(TYPE_map_in);
}

DB_RES *search_ZXTYPE_neighbours(sqlite3 *db, int dbid, unsigned int address, unsigned int size)
{
	SEARCH_PREAMBLE(ZX_TYPE);
	sqlite3_prepare_v2(db, "with p as (select id,time,type,address,size,dbid,source,structid, gfxtype, gfxwidth, gfxheight from zxtype where dbid = ? and address < ? and address >= ? - size order by address desc limit 1),"\
		"q as (select id, time, type, address, size, dbid, source, structid, gfxtype, gfxwidth, gfxheight from zxtype where dbid = ? and address = ? limit 1),"\
		"r as (select id, time, type, address, size, dbid, source, structid, gfxtype, gfxwidth, gfxheight from zxtype where dbid = ? and address > ? and address <= ? order by address) "\
		"select * from p union select * from q union select * from r order by address"\
		";", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, dbid);
	sqlite3_bind_int(stmt, 2, address);
	sqlite3_bind_int(stmt, 3, address);
	sqlite3_bind_int(stmt, 4, dbid);
	sqlite3_bind_int(stmt, 5, address);
	sqlite3_bind_int(stmt, 6, dbid);
	sqlite3_bind_int(stmt, 7, address);
	sqlite3_bind_int(stmt, 8, address+size);
	SEARCH_EXECUTE(TYPE_map_in);
}

DB_RES *search_ZXTYPE_by_address(sqlite3 *db, int dbid, unsigned int address)
{
	SEARCH_PREAMBLE(ZX_TYPE);
	sqlite3_prepare_v2(db, "select * from zxtype where dbid = ? and address = ?;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, dbid);
	sqlite3_bind_int(stmt, 2, address);
	SEARCH_EXECUTE(TYPE_map_in);
}

DB_RES *search_ZXTYPE_at_address(sqlite3 *db, int dbid, unsigned int address)
{
	SEARCH_PREAMBLE(ZX_TYPE);
	sqlite3_prepare_v2(db, "select * from zxtype where dbid = ? and address <= ? and address + size > ?;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, dbid);
	sqlite3_bind_int(stmt, 2, address);
	sqlite3_bind_int(stmt, 3, address);
	SEARCH_EXECUTE(TYPE_map_in);
}
DB_RES *search_ZXTYPE_by_type(sqlite3 *db, int dbid, ZX_DATA_TYPE type)
{
	SEARCH_PREAMBLE(ZX_TYPE);
	sqlite3_prepare_v2(db, "select * from zxtype where dbid = ? and type = ? order by address, time;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, dbid);
	sqlite3_bind_int(stmt, 2, type);
	SEARCH_EXECUTE(TYPE_map_in);
}

int delete_ZXTYPE(sqlite3 *db, int id)
{
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db, "delete from zxtype where id = ?;", -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, id);
	return execstatement(db, stmt);
}

int update_ZXTYPE(sqlite3 *db, ZX_TYPE *zx)
{
	int err;
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db, "update zxtype set (time, type, address, size, dbid, source, structid, gfxtype, gfxwidth, gfxheight) = (julianday('now'),?,?,?,?,?,?,?,?,?) where id = ?;", -1, &stmt, NULL);
	TYPE_map_out(zx, stmt);
	sqlite3_bind_int(stmt, 10, zx->id);
	err = sqlite3_step(stmt);
	if (err != SQLITE_DONE)
		eloglevel(LOG_ERROR, "[DB] %s\n", sqlite3_errmsg(db));
	sqlite3_finalize(stmt);
	return (err == SQLITE_DONE) ? zx->id : -1;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void SETTING_map_in(DB_RES *res, sqlite3_stmt *stmt)
{
	ZX_SETTING *zx = (ZX_SETTING *)db_rowptr(res);
	zx->id = sqlite3_column_int(stmt, 0);
	zx->name = db_strdup(res, sqlite3_column_text(stmt, 1));
	zx->valueint = sqlite3_column_int(stmt, 2);
	zx->valuestring = db_strdup(res, sqlite3_column_text(stmt, 3));
	zx->time = sqlite3_column_double(stmt, 4);
}

void SETTING_map_out(ZX_SETTING *zx, sqlite3_stmt *stmt)
{
	sqlite3_bind_text(stmt, 1, zx->name, -1, NULL);
	sqlite3_bind_int(stmt, 2, zx->valueint);
	sqlite3_bind_text(stmt, 3, zx->valuestring, -1, NULL);
}

DB_RES *search_ZXSETTING(sqlite3 *db)
{
	SEARCH_PREAMBLE(ZX_SETTING);
	sqlite3_prepare_v2(db, "select id,name,valueint,valuestring,time from zxsetting order by name;", -1, &stmt, NULL);
	SEARCH_EXECUTE(SETTING_map_in);
}

DB_RES *search_ZXSETTING_by_name(sqlite3 *db, char *name)
{
	SEARCH_PREAMBLE(ZX_SETTING);
	sqlite3_prepare_v2(db, "select id,name,valueint,valuestring,time from zxsetting where name = ? order by name;", -1, &stmt, NULL);
	sqlite3_bind_text(stmt, 1, name, -1, NULL);
	SEARCH_EXECUTE(SETTING_map_in);
}

int save_or_update_ZXSETTING(sqlite3 *db, ZX_SETTING *zx)
{
	sqlite3_stmt *stmt;

	if (exists(db, "select count(*) from zxsetting where id = ?;", zx->id))
	{
		sqlite3_prepare_v2(db, "update zxsetting set (time,name,valueint,valuestring) = (julianday('now'),?,?,?) where id = ?;", -1, &stmt, NULL);
		sqlite3_bind_int(stmt, 4, zx->id);
		sqlite3_set_last_insert_rowid(db, zx->id);
	}
	else
	{
		sqlite3_prepare_v2(db, "insert into zxsetting (time,name,valueint,valuestring) values (julianday('now'),?,?,?);", -1, &stmt, NULL);
	}

	SETTING_map_out(zx, stmt);

	if (execstatement(db, stmt) != 0)
		return -1;

	return (int)sqlite3_last_insert_rowid(db);
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
typedef struct
{
	int version;
	const char *const description;
	const char *const updatesql;
} SCHEMA_UPGRADE;

const SCHEMA_UPGRADE schema_upgrades[] =
{
	{1,"add indexes",	"create index zxdb_id on zxdb (id);"\
						"create index zxlabel_id on zxlabel (id);"\
						"create index zxlabel_dbid on zxlabel (dbid);"\
						"create index zxlabel_time on zxlabel (time asc);"\
						"create index zxtype_id on zxtype (id); "\
						"create index zxtype_dbid on zxtype (dbid);"\
						"create index zxtype_time on zxtype (time asc);"},
	{ 2,"db name unique",	"create unique index zxdb_name_unique on zxdb (name);" },
	{ 3,"settings",		"create table zxsetting (id integer primary key autoincrement, name text not null, valueint int, valuestring text);"\
						"create index zxsetting_id on zxsetting (id);"\
						"create index zxsetting_name on zxsetting (name);" },
	{ 4,"settings2",	"drop index zxsetting_name;"\
						"create unique index zxsetting_name on zxsetting (name);" },
	{ 5,"source",		"alter table zxtype add column source int;"\
						"update zxtype set source = 0;"\
						"alter table zxtype add column structid int;"\
						"alter table zxlabel add column source int;"\
						"update zxlabel set source = 0;"},
	{ 6, "gfxtype",		"alter table zxtype add column gfxtype int;"\
						"alter table zxtype add column gfxwidth int;"\
						"alter table zxtype add column gfxheight int;" },
	{ 7, "indexes",		"create index zxlabel_address on zxlabel (address);"\
						"create index zxlabel_name on zxlabel (name);"\
						"create index zxtype_address on zxtype (address);"},
	{ 8, "settingtime",	"alter table zxsetting add column time real not null default 0;"},
	{ 9, "label type",	"alter table zxlabel add column type not null default 0;"},
};

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
typedef struct
{
	const char *const name;
	const char *const createsql;
} ZX_TABLE;

const ZX_TABLE schema_creators[] =
{
	{"zxschema", SCHEMA_create},
	{"zxdb", DB_create},
	{"zxlabel", LABEL_create},
	{"zxtype", TYPE_create}
};

static int init_schema(sqlite3 *db)
{
	int err;

	//initial state is all or nothing

	sqlite3_stmt *res;
	err = sqlite3_prepare_v2(db, "select count(*) from sqlite_master where type = 'table' and name = ?;", -1, &res, NULL);
	sqlite3_bind_text(res, 1, schema_creators[0].name, -1, NULL);
	int table_exists = immediatestatement(res) > 0;
	if (table_exists)
		return 0;

	err = begin(db);
	if (err != SQLITE_OK)
		return -1;

	for (int i = 0; i < sizeof schema_creators / sizeof * schema_creators; i++)
	{
		char *errtxt = NULL;
		err = sqlite3_exec(db, schema_creators[i].createsql, NULL, NULL, &errtxt);
		if (err != SQLITE_OK)
		{
			eloglevel(LOG_ERROR, "[DB] bootstrap %d %s\n", i, errtxt);
			sqlite3_free(errtxt);

			rollback(db);
			return -1;
		}
	}

	err = commit(db);

	elog("[DB] bootstrap successful!\n");
	
	return err != SQLITE_OK;
}

static int upgrade_schema(sqlite3 *db)
{
	int err;
	int version = immediate(db, "select max(version) from zxschema;");
	if (version == -1)
		return -1;

	for (int i = 0; i < sizeof schema_upgrades / sizeof * schema_upgrades; i++)
	{
		if (schema_upgrades[i].version <= version)
			continue;

		//each upgrade is all or nothing

		err = begin(db);
		if (err != SQLITE_OK)
			return -1;

		char *errtxt = NULL;
		err = sqlite3_exec(db, schema_upgrades[i].updatesql, NULL, NULL, &errtxt);
		if (err != SQLITE_OK)
		{
			eloglevel(LOG_ERROR, "[DB] upgrade %d %s", schema_upgrades[i].version, errtxt);
			sqlite3_free(errtxt);
			rollback(db);
			return -1;
		}

		ZX_SCHEMA schema;
		schema.version = schema_upgrades[i].version;
		schema.description = (char *)schema_upgrades[i].description;
		if (save_ZXSCHEMA(db, &schema))
		{
			rollback(db);
			return -1;
		}

		err = commit(db);
		if (err != SQLITE_OK)
			return -1;

		elog("[DB] upgrade %d (\"%s\") applied successfully!\n", schema_upgrades[i].version, schema_upgrades[i].description);
	}
	return 0;
}

static int init_and_upgrade_schema(sqlite3 *db)
{
	if (init_schema(db) == -1)
		return -1;

	if (upgrade_schema(db) == -1)
		return -1;

	return 0;
}
