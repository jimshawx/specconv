#include <windows.h>
#include <string.h>
#include "logger.h"

static struct
{
	LARGE_INTEGER time;
	char name[100];
} timestamps[100];
static int n_timestamps;
void time_stamp(char *name) { strcpy_s(timestamps[n_timestamps].name, 100, name); QueryPerformanceCounter(&timestamps[n_timestamps++].time); }
void start_timer(void) { n_timestamps = 0; time_stamp("start"); }
void show_timers(void)
{
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);
	for (int i = 0; i < n_timestamps - 1; i++)
	{
		long long dt = timestamps[i + 1].time.QuadPart - timestamps[i].time.QuadPart;
		elog("%-9s %11.9f\n", timestamps[i + 1].name, (double)dt / (double)freq.QuadPart);
	}
}
