//----------------------------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//----------------------------------------------------------------------------------------------------------

#define _WIN32_DCOM
#define _WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <richedit.h>
#include <process.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "resource.h"

#include "specem.h"
#include "machine.h"
#include "files.h"
#include "emu.h"
#include "zxgfx.h"
#include "sound.h"
#include "ay8912.h"
#include "basic.h"
#include "coverage.h"
#include "trace.h"
#include "daos.h"
#include "dbgmem.h"
#include "disview.h"
#include "logger.h"
#include "settings.h"
#include "gfxview.h"
#include "labels.h"
#include "dasm.h"
#include "breakpoint.h"
#include "asm.h"
#include "graph.h"

HWND dis_window;
HWND coverage_window;
HWND dis_dialog;
HWND disctl_dialog;
HWND stats_dialog;
HWND mem_dialog;
HWND reg_dialog;
HWND zxgfx_dialog;
HWND status_window;
HWND emu_window;
HWND stack_dialog;
HWND fpu_dialog;
HWND basic_dialog;
HWND sysvar_dialog;
HWND pctrace_dialog;
HWND tapeinfo_dialog;
HWND console_dialog;
HWND pokes_dialog;
HMENU main_menu;
HMENU popup_menu;
HCURSOR hourglass, old_cursor;
HINSTANCE dis_instance;
int quit;
HBRUSH background;

static int auto_update = 0;
int border = 20;
int emulating = 0;

char specconv_class[] = "specconvCLASS";
char emu_class[] = "specemuCLASS";
char coverage_class[] = "speccovCLASS";

void update_display(void);
void update_aux_display(void);
static void update_status_bar(void);

extern void paint_screen(HDC hdc);

void update_memory_view(void);
void update_regs_view(void);
void update_stack_view(void);
void update_fpstack_view(void);
void update_coverage_view(void);
void update_stats_view(void);
void update_pctrace_view(void);
void update_basic_view(void);
void update_sysvars_view(void);
void update_gfx_view(void);
void update_tapeinfo_view(void);
void update_console_view(void);
void update_pokes_view(void);

static struct
{
	char shown;
	int menuitemid;
	void (*update)(void);
} dialog_status[DIALOG_ID_MAX+1]=
{
	{0,ID_VIEW_MEMORY, update_memory_view},
	{0,ID_VIEW_REGISTERS, update_regs_view},
	{0,ID_VIEW_STACK, update_stack_view},
	{0,ID_VIEW_FPU, update_fpstack_view},
	{0,ID_VIEW_COVERAGE, update_coverage_view},
	{0,ID_VIEW_STATS, update_stats_view},
	{0,ID_VIEW_PCTRACE, update_pctrace_view},
	{0,ID_VIEW_BASIC, update_basic_view},
	{0,ID_VIEW_SYSVARS, update_sysvars_view},
	{0,ID_VIEW_GFX, update_gfx_view},
	{0,ID_VIEW_TAPEINFO, update_tapeinfo_view},
	{0,ID_VIEW_CONSOLE, update_console_view},
	{0,ID_VIEW_POKES, update_pokes_view},
};

void set_rom_menu(int machine_no)
{
	MENUITEMINFO mii;

	mii.cbSize = sizeof(MENUITEMINFO);
	mii.fMask = MIIM_STATE;

	mii.fState = machine_no==SPEC_16K?MFS_CHECKED:MFS_UNCHECKED;
	SetMenuItemInfo(main_menu, ID_OPTIONS_16K, FALSE, &mii);

	mii.fState = machine_no==SPEC_48K?MFS_CHECKED:MFS_UNCHECKED;
	SetMenuItemInfo(main_menu, ID_OPTIONS_48K, FALSE, &mii);

	mii.fState = machine_no==SPEC_128K?MFS_CHECKED:MFS_UNCHECKED;
	SetMenuItemInfo(main_menu, ID_OPTIONS_128K, FALSE, &mii);

	mii.fState = machine_no==SPEC_PLUS2?MFS_CHECKED:MFS_UNCHECKED;
	SetMenuItemInfo(main_menu, ID_OPTIONS_PLUS2, FALSE, &mii);

	mii.fState = machine_no==SPEC_PLUS2A?MFS_CHECKED:MFS_UNCHECKED;
	SetMenuItemInfo(main_menu, ID_OPTIONS_PLUS2A, FALSE, &mii);

	mii.fState = machine_no==SPEC_PLUS3?MFS_CHECKED:MFS_UNCHECKED;
	SetMenuItemInfo(main_menu, ID_OPTIONS_PLUS3, FALSE, &mii);

	mii.fState = machine_no==-1?MFS_CHECKED:MFS_UNCHECKED;
	SetMenuItemInfo(main_menu, ID_OPTIONS_NONE, FALSE, &mii);
}

#define MRU_MENU_DBX 0x8000
#define MRU_MENU_TAP (MRU_MENU_DBX+0x20)
#define MRU_MENU_SNA (MRU_MENU_DBX+0x40)

static void set_mru_menu(void)
{
	char **mru;
	int i;
	HMENU menu;

	menu = GetSubMenu(main_menu, 1);

	for (i = 0; i < 0x20; i++)
		DeleteMenu(menu, MRU_MENU_DBX + i, MF_BYCOMMAND);

	mru = search_mru(MRU_DB);
	i = 0;
	AppendMenu(menu, MF_SEPARATOR, MRU_MENU_DBX + i++, NULL);
	while (*mru != NULL)
		AppendMenu(menu, MF_STRING, MRU_MENU_DBX + i++, *mru++);

	menu = GetSubMenu(main_menu, 0);

	for (i = 0; i < 0x20; i++)
		DeleteMenu(menu, MRU_MENU_SNA + i, MF_BYCOMMAND);

	for (i = 0; i < 0x20; i++)
		DeleteMenu(menu, MRU_MENU_TAP + i, MF_BYCOMMAND);

	mru = search_mru(MRU_SNA);
	i = 0;
	AppendMenu(menu, MF_SEPARATOR, MRU_MENU_SNA + i++, NULL);
	while (*mru != NULL)
		AppendMenu(menu, MF_STRING, MRU_MENU_SNA + i++, *mru++);

	mru = search_mru(MRU_TAP);
	i = 0;
	AppendMenu(menu, MF_SEPARATOR, MRU_MENU_TAP + i++, NULL);
	while (*mru != NULL)
		AppendMenu(menu, MF_STRING, MRU_MENU_TAP + i++, *mru++);
}

void set_dialog_status(DIALOG_ID id, int toggleset)
{
	MENUITEMINFO mii;

	mii.cbSize = sizeof(MENUITEMINFO);
	mii.fMask = MIIM_STATE;

	//if toggleset is -1, toggle the status
	//else if it's 0,1 set the status

	if (toggleset == -1)
		dialog_status[id].shown ^= 1;
	else
		dialog_status[id].shown = toggleset?1:0;

	mii.fState = dialog_status[id].shown ? MFS_CHECKED : MFS_UNCHECKED;

	SetMenuItemInfo(main_menu, dialog_status[id].menuitemid, FALSE, &mii);
}

int get_dialog_status(DIALOG_ID id)
{
	return dialog_status[id].shown;
}

void toggle_dialog(HWND dlg, DIALOG_ID id, int toggleset)
{
	set_dialog_status(id, toggleset);
	ShowWindow(dlg, get_dialog_status(id) ? SW_SHOW : SW_HIDE);
	update_display();
}

void set_wait_cursor()
{
	old_cursor = SetCursor(hourglass);
}

void unset_wait_cursor()
{
	SetCursor(old_cursor);
}

int get_current_line()
{
	int start = 0, end = 0;
	SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_GETSEL, (WPARAM)&start, (LPARAM)&end);
	return (int)SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_LINEFROMCHAR, start, 0);
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void go(void)
{
	update_display();
	remove_all_temp_breakpoints();
	emulating = EMU_RUN;
	SetFocus(emu_window);
}

static void stop(void)
{
	if (emulating)
	{
		emulating = EMU_STOP;
		update_display();
		disview_skip_to_pc();
		SetFocus(dis_window);
	}
}

static void stepover(void)
{
//want to use stepinto when next instruction is
//a dead end, ie. ret, reti, retn, jp NN, jp (hl), jp (ix), jp (iy)
	unsigned short pc;
	pc = get_regs()->pc;
	unsigned char i0 = peek_fast(pc);
	if (i0 == 0xC9 || i0 == 0xE9 || i0 == 0xC3)//ret, jp (hl), jp NN
	{
		emulating = EMU_STEPINTO;
		return;
	}
	else if (i0 == 0xDD || i0 == 0xED || i0 == 0xFD)
	{
		unsigned char i1 = peek_fast((unsigned short)(pc + 1));
		if ((i0 == 0xDD && i1 == 0xE9)//jp (ix)
			|| (i0 == 0xFD && i1 == 0xE9)//jp (iy)
			|| (i0 == 0xED && i1 == 0x4D)//reti
			|| (i0 == 0xED && i1 == 0x45))//retn
		{
			emulating = EMU_STEPINTO;
			return;
		}
	}

	remove_all_temp_breakpoints();

	pc = find_next_pc();
	set_temp_breakpoint(pc);
	emulating = EMU_STEPOVER;
	SetFocus(emu_window);
}

static void stepinto(void)
{
	remove_all_temp_breakpoints();
	emulating = EMU_STEPINTO;
	SetFocus(emu_window);
}

static unsigned short stepout_sp = 0;
static void stepout(void)
{
	remove_all_temp_breakpoints();
	REG_PACK *r = get_regs();
	stepout_sp = r->sp;
	emulating = EMU_STEPOUT;
	SetFocus(emu_window);
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static unsigned char keyboard[256];
//static unsigned char keyring[256];
//static int keyread=0;
//static int keywrite=0;

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
unsigned char kempston_keys = 0;
unsigned char speckeys[8]={0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};
//1-5  6-0  Q-T  Y-P  A-G  H-Enter  CAPS-V B-SPACE

static const unsigned char keys[8][5]=
{
	{'1','2','3','4','5'},			{'0','9','8','7','6'},				//{'6','7','8','9','0'},
	{'Q','W','E','R','T'},			{'P','O','I','U','Y'},				//{'Y','U','I','O','P'}.
	{'A','S','D','F','G'},			{VK_RETURN,'L','K','J','H'},		//{'H','J','K','L',VK_RETURN},
	{VK_SHIFT,'Z','X','C','V'},		{VK_SPACE,VK_CONTROL,'M','N','B'},	//{'B','N','M',VK_RSHIFT,VK_SPACE}
};

void flush_keyboard(void)
{
	memset(keyboard, 0, sizeof keyboard);
	memset(speckeys, 0xff, sizeof speckeys);
	kempston_keys = 0;
}

void process_kbd_message(unsigned int vkey, int keydown)
{
	int key;

	key = vkey & 0xff;

	if (keydown)
	{
		keyboard[key]=1;
//		keyring[keywrite++]=key;
//		keywrite&=0xff;
	}
	else
	{
		keyboard[key]=0;
	}

	//process into speccy keys
	{
	int m, k;
	unsigned char c;
	for (m = 0; m < 8; m++)
	{
		c=0xff;
		for (k = 0; k < 5; k++)
		{
			if (keyboard[keys[m][k]])
				c &= ~(1<<k);
		}
		speckeys[m]=c;
	}
	}

	//process into kempston keys
	//(000FUDLR, active high)
	kempston_keys = keyboard[VK_RIGHT]|(keyboard[VK_LEFT]<<1)|
					(keyboard[VK_DOWN]<<2)|(keyboard[VK_UP]<<3)|
					(keyboard[VK_SPACE]<<4);

	//numeric pad
	if (keyboard[VK_NUMPAD1]) speckeys[0] &= ~(1<<0);
	if (keyboard[VK_NUMPAD2]) speckeys[0] &= ~(1<<1);
	if (keyboard[VK_NUMPAD3]) speckeys[0] &= ~(1<<2);
	if (keyboard[VK_NUMPAD4]) speckeys[0] &= ~(1<<3);
	if (keyboard[VK_NUMPAD5]) speckeys[0] &= ~(1<<4);
	if (keyboard[VK_NUMPAD6]) speckeys[1] &= ~(1<<4);
	if (keyboard[VK_NUMPAD7]) speckeys[1] &= ~(1<<3);
	if (keyboard[VK_NUMPAD8]) speckeys[1] &= ~(1<<2);
	if (keyboard[VK_NUMPAD9]) speckeys[1] &= ~(1<<1);
	if (keyboard[VK_NUMPAD0]) speckeys[1] &= ~(1<<0);

	//do some specials
	if (keyboard[VK_BACK])//delete
	{
		speckeys[1] &= ~1;
		speckeys[6] &= ~1;
	}

	if (keyboard[VK_SHIFT] && keyboard[VK_OEM_7])// "
	{
		speckeys[3] &= ~(1<<0);//P
		speckeys[7] &= ~(1<<1);//sym shift
		speckeys[6] |=  (1<<0);//no caps shift
	}
	else if (keyboard[VK_OEM_7])// '
	{
		speckeys[1] &= ~(1 << 3);//7
		speckeys[7] &= ~(1 << 1);//sym shift
	}

	if (keyboard[VK_SHIFT] && keyboard[VK_OEM_1])//colon
	{
		speckeys[6] &= ~(1 << 1);//Z
		speckeys[7] &= ~(1 << 1);//sym shift
		speckeys[6] |=  (1<<0);//no caps shift
	}
	else if (keyboard[VK_OEM_1])//semicolon
	{
		speckeys[3] &= ~(1 << 1);//O
		speckeys[7] &= ~(1 << 1);//sym shift
	}
	
	if (keyboard[VK_SHIFT] && keyboard[VK_OEM_COMMA])// <
	{
		speckeys[2] &= ~(1 << 3);//R
		speckeys[7] &= ~(1 << 1);//sym shift
		speckeys[6] |= (1 << 0);//no caps shift
	}
	else if (keyboard[VK_OEM_COMMA])//comma
	{
		speckeys[7] &= ~(1<<3);//N
		speckeys[7] &= ~(1<<1);//sym shift
	}

	if (keyboard[VK_SHIFT] && keyboard[VK_OEM_PERIOD])// >
	{
		speckeys[2] &= ~(1 << 4);//T
		speckeys[7] &= ~(1 << 1);//sym shift
		speckeys[6] |= (1 << 0);//no caps shift
	}
	else if (keyboard[VK_OEM_PERIOD])//full stop
	{
		speckeys[7] &= ~(1<<2);//M
		speckeys[7] &= ~(1<<1);//sym shift
	}

	if (keyboard[VK_SHIFT] && keyboard[VK_OEM_MINUS])// _
	{
		speckeys[1] &= ~(1 << 0);//0
		speckeys[7] &= ~(1 << 1);//sym shift
		speckeys[6] |= (1 << 0);//no caps shift
	}
	else if (keyboard[VK_OEM_MINUS])// -
	{
		speckeys[5] &= ~(1 << 3);//J
		speckeys[7] &= ~(1 << 1);//sym shift
	}

	if (keyboard[VK_SHIFT] && keyboard[VK_OEM_PLUS])// +
	{
		speckeys[5] &= ~(1 << 2);//K
		speckeys[7] &= ~(1 << 1);//sym shift
		speckeys[6] |= (1 << 0);//no caps shift
	}
	else if (keyboard[VK_OEM_PLUS])// =
	{
		speckeys[5] &= ~(1 << 1);//L
		speckeys[7] &= ~(1 << 1);//sym shift
	}

	if (keyboard[VK_SHIFT] && keyboard[VK_OEM_2])// ?
	{
		speckeys[6] &= ~(1 << 3);//C
		speckeys[7] &= ~(1 << 1);//sym shift
		speckeys[6] |= (1 << 0);//no caps shift
	}
	else if (keyboard[VK_OEM_2])// /
	{
		speckeys[6] &= ~(1 << 4);//V
		speckeys[7] &= ~(1 << 1);//sym shift
	}

	//if (keyboard[VK_SHIFT] && keyboard[VK_OEM_4])// {
	//{
	//	speckeys[1] &= ~(1 << 2);//8
	//	speckeys[7] &= ~(1 << 1);//sym shift
	//	speckeys[6] |= (1 << 0);//no caps shift
	//}

	//if (keyboard[VK_SHIFT] && keyboard[VK_OEM_6])// }
	//{
	//	speckeys[1] &= ~(1 << 1);//9
	//	speckeys[7] &= ~(1 << 1);//sym shift
	//	speckeys[6] |= (1 << 0);//no caps shift
	//}

	//arrows
	if (keyboard[VK_LEFT])
	{
		speckeys[6] &= ~1;
		speckeys[0] &= ~(1<<4);
	}
	if (keyboard[VK_RIGHT])
	{
		speckeys[6] &= ~1;
		speckeys[1] &= ~(1<<2);
	}
	if (keyboard[VK_UP])
	{
		speckeys[6] &= ~1;
		speckeys[1] &= ~(1<<3);
	}
	if (keyboard[VK_DOWN])
	{
		speckeys[6] &= ~1;
		speckeys[1] &= ~(1<<4);
	}
return;
}

void set_emu_size_scale(HWND hwnd, int scale)
{
	PostMessage(hwnd, WM_SIZE, 0, ((PRETTY_BORDER_WIDTH * 2 + 256) * scale) | (((PRETTY_BORDER_WIDTH * 2 + 192) * scale) << 16));

	MENUITEMINFO mii = { 0 };
	mii.cbSize = sizeof mii;
	mii.fMask = MIIM_STATE;
	int items[] = { ID_DISPLAY_X1 , ID_DISPLAY_X2 , ID_DISPLAY_X3 , ID_DISPLAY_X4 };
	for (int j = 0; j < 4; j++)
	{
		mii.fState = (j == (scale-1)) ? MFS_CHECKED : MFS_UNCHECKED;
		SetMenuItemInfo(main_menu, items[j], FALSE, &mii);
	}
	update_setting("screenscale", scale, NULL);
}

LRESULT CALLBACK EmuWindowProc(
  HWND hwnd,      // handle to window
  UINT uMsg,      // message identifier
  WPARAM wParam,  // first message parameter
  LPARAM lParam   // second message parameter
)
{
	switch (uMsg)
	{
		case WM_ERASEBKGND:
			return 1;

		case WM_PAINT:
			{
			PAINTSTRUCT paint={0};
			paint.hdc = (HDC)wParam;
			if (GetUpdateRect(hwnd, &paint.rcPaint,TRUE))
			{
				BeginPaint(hwnd, &paint);
				paint_screen(paint.hdc);
				EndPaint(hwnd, &paint);
			}
			}
			break;

		case WM_KEYDOWN:
			if ((wParam & 0xff) == VK_F1)
				set_emu_size_scale(hwnd, 1);
			if ((wParam & 0xff) == VK_F2)
				set_emu_size_scale(hwnd, 2);
			if ((wParam & 0xff) == VK_F3)
				set_emu_size_scale(hwnd, 3);
			if ((wParam & 0xff) == VK_F4)
				set_emu_size_scale(hwnd, 4);

			if ((wParam & 0xff) == VK_F5)
				multiface_nmi();

			if ((wParam & 0xff) == VK_F7)
				setr(0xb8);
			if ((wParam & 0xff) == VK_F8)
			{
				char *txt = basic_sample();
				if (txt) SetDlgItemText(basic_dialog, IDC_BASICEDIT, txt);
			}
			process_kbd_message((unsigned int)wParam, 1);
			break;

		case WM_KEYUP:
			process_kbd_message((unsigned int)wParam, 0);
			break;

		case WM_ACTIVATE:
			flush_keyboard();
			break;

		case WM_SIZE:
			{
			RECT r;
			r.left = r.top = 0;
			r.right = GET_X_LPARAM(lParam);
			r.bottom = GET_Y_LPARAM(lParam);
			float scale = 1.0f/ ((PRETTY_BORDER_WIDTH * 2 + 256.0f) / (PRETTY_BORDER_WIDTH * 2 + 192.0f));
			r.bottom = (int)(r.right * scale);

			int ex_style = WS_EX_TOOLWINDOW;
			int style = WS_CAPTION|WS_VISIBLE|WS_SIZEBOX;
			AdjustWindowRectEx(&r, style, FALSE, ex_style);

			SetWindowPos(hwnd, NULL, 0,0, r.right-r.left, r.bottom-r.top, SWP_NOMOVE| SWP_NOREDRAW| SWP_NOREPOSITION| SWP_NOZORDER);
			}
			break;

		case WM_CLOSE:
			break;

		default:
			return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}
return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK CoverageWindowProc(
  HWND hwndDlg,      // handle to window
  UINT uMsg,      // message identifier
  WPARAM wParam,  // first message parameter
  LPARAM lParam   // second message parameter
)
{
	static HWND tooltip;
	switch (uMsg)
	{
		case WM_INITDIALOG:
			{
			tooltip = CreateWindowEx(WS_EX_TOPMOST, TOOLTIPS_CLASS, NULL, WS_POPUP, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, hwndDlg, NULL, dis_instance, 0);
			TOOLINFO ti = { 0 };
			ti.cbSize = sizeof ti;
			ti.hwnd = hwndDlg;
			ti.uFlags = TTF_SUBCLASS | TTF_IDISHWND | TTF_ABSOLUTE;
			ti.uId = (UINT_PTR)hwndDlg;
			ti.lpszText = "0";
			SendMessage(tooltip, TTM_ADDTOOL, 0, (LPARAM)&ti);

			SendMessage(tooltip, TTM_SETDELAYTIME, TTDT_AUTOPOP, -1);
			SendMessage(tooltip, TTM_SETDELAYTIME, TTDT_INITIAL, 0);
			SendMessage(tooltip, TTM_SETDELAYTIME, TTDT_RESHOW, 0);
			}
			break;

		case WM_ERASEBKGND:
			return 1;

		case WM_MOUSEMOVE:
			{
			POINT pt;
			pt.x = GET_X_LPARAM(lParam);
			pt.y = GET_Y_LPARAM(lParam);

			RECT rect;
			GetClientRect(hwndDlg, &rect);

			int xs = (int)((double)pt.x * 256.0 / (double)rect.right);
			int ys = (int)((double)pt.y * 256.0 / (double)rect.bottom);
			unsigned int address = xs + (ys << 8);

			char tmp[100];
			sprintf_s(tmp, 100, "%04X %d", address, address);

			TOOLINFO ti = { 0 };
			ti.cbSize = sizeof ti;
			ti.hwnd = hwndDlg;
			ti.uFlags = TTF_SUBCLASS | TTF_IDISHWND | TTF_ABSOLUTE;
			ti.uId = (UINT_PTR)hwndDlg;
			ti.lpszText = tmp;

			if (pt.x < 0 || pt.y < 0 || pt.x > rect.right || pt.y > rect.bottom)
				break;

			ClientToScreen(hwndDlg, &pt);
			SendMessage(tooltip, TTM_TRACKPOSITION, 0, (pt.y<<16)|pt.x);
			SendMessage(tooltip, TTM_UPDATETIPTEXT, 0, (LPARAM)&ti);
			}
			break;

		case WM_SIZE:
			update_coverage_view();
			break;

		case WM_CLOSE:
			set_dialog_status(DLG_COV, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			break;

		default:
			return DefWindowProc(hwndDlg, uMsg, wParam, lParam);
	}
	return 0;
}

void set_database_caption(char *fname)
{
	char caption[MAX_DB_NAME_LEN+50];
	if (fname == NULL)
		sprintf_s(caption, MAX_DB_NAME_LEN+50, "Speccy Decompiler - (no database)");
	else
		sprintf_s(caption, MAX_DB_NAME_LEN+50, "Speccy Decompiler - (database: %s)", fname);

	SetWindowText(dis_window, caption);
}

void open_db_file(char *fname)
{
	if (open_db(fname) == -1)
	{
		MessageBox(dis_window, "There was a problem opening the database.", "Error", MB_OK | MB_ICONERROR);
		set_database_caption(NULL);
	}
	else
	{
		set_database_caption(fname);
		save_mru(MRU_DB, fname);
		set_mru_menu();
	}
	disview_database_changed();
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK DatabaseDialogProc(
	HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
)
{
	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			SendDlgItemMessage(hwndDlg, IDC_DBLIST, LB_RESETCONTENT, 0, 0);

			DB_RES *dbs = search_databases();
			ZX_DB *db = (ZX_DB *)dbs->rows;
			for (int i = 0; i < dbs->count; i++, db++)
				SendDlgItemMessage(hwndDlg, IDC_DBLIST, LB_ADDSTRING, 0, (LPARAM)db->name);
			free_result(dbs);
		}
		return 0;

		case WM_COMMAND:
			//button message
			switch (LOWORD(wParam))
			{
				case IDC_DBLIST:
					if (HIWORD(wParam) == LBN_DBLCLK)
					{
						char dbname[MAX_DB_NAME_LEN];
						int sel = (int)SendDlgItemMessage(hwndDlg, IDC_DBLIST, LB_GETCURSEL, 0, 0);
						SendDlgItemMessage(hwndDlg, IDC_DBLIST, LB_GETTEXT, sel, (LPARAM)dbname);
						open_db_file(dbname);
						EndDialog(hwndDlg, 0);
					}
					break;

				case IDC_OPENDB:
				{
					char dbname[MAX_DB_NAME_LEN];
					int sel = (int)SendDlgItemMessage(hwndDlg, IDC_DBLIST, LB_GETCURSEL, 0, 0);
					SendDlgItemMessage(hwndDlg, IDC_DBLIST, LB_GETTEXT, sel, (LPARAM)dbname);
					open_db_file(dbname);
					EndDialog(hwndDlg, 0);
					return 1;
				}
				break;
				case IDC_MERGEDB:
				{
					char dbname[MAX_DB_NAME_LEN];
					int sel = (int)SendDlgItemMessage(hwndDlg, IDC_DBLIST, LB_GETCURSEL, 0, 0);
					SendDlgItemMessage(hwndDlg, IDC_DBLIST, LB_GETTEXT, sel, (LPARAM)dbname);
					merge_db(dbname);
					disview_database_changed();
					EndDialog(hwndDlg, 0);
					return 1;
				}
				break;
				case IDC_DELETEDB:
				{
					char dbname[MAX_DB_NAME_LEN];
					int sel = (int)SendDlgItemMessage(hwndDlg, IDC_DBLIST, LB_GETCURSEL, 0, 0);
					SendDlgItemMessage(hwndDlg, IDC_DBLIST, LB_GETTEXT, sel, (LPARAM)dbname);
					int err = MessageBox(dis_dialog, "WARNING - This operation cannot be undone.\r\nAll data will be removed.", "Confirm Database Removal", MB_OKCANCEL | MB_ICONWARNING | MB_DEFBUTTON2);
					if (err == IDOK)
					{
						err = delete_db(dbname);
						if (err == 0)
							MessageBox(dis_dialog, "The database was removed successfully.", "Success", MB_OK | MB_ICONINFORMATION);
						else
							MessageBox(dis_dialog, "There was an error removing the database.", "Failure", MB_OK | MB_ICONERROR);
					}
					EndDialog(hwndDlg, 0);
					return 1;
				}
				break;
				case IDC_CANCELDB:
				{
					EndDialog(hwndDlg, 0);
					return 1;
				}
				break;
			}
			break;

		case WM_CLOSE:
			EndDialog(hwndDlg, 0);
			return 0;
	}
	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK DatabaseCreateDialogProc(
	HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
)
{
	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			SetFocus(GetDlgItem(hwndDlg, IDC_DATABASENAME));
		}
		return 0;

		case WM_COMMAND:
			//button message
			switch (LOWORD(wParam))
			{
				case IDC_CREATEDB:
				{
					char dbname[MAX_DB_NAME_LEN];
					SendDlgItemMessage(hwndDlg, IDC_DATABASENAME, WM_GETTEXT, MAX_DB_NAME_LEN, (LPARAM)dbname);
					if (create_db(dbname) != -1)
					{
						set_wait_cursor();
						gen_rom_labels();
						set_database_caption(dbname);
						save_mru(MRU_DB, dbname);
						disview_database_changed();
						unset_wait_cursor();
					}
					else
					{
						MessageBox(dis_dialog, "A database with that name already exists", "Error", MB_OK|MB_ICONERROR);
					}
					EndDialog(hwndDlg, 0);
					return 1;
				}
				break;

				case IDC_CANCELCREATEDB:
				{
					EndDialog(hwndDlg, 0);
					return 1;
				}
				break;
			}
			break;

		case WM_CLOSE:
			EndDialog(hwndDlg, 0);
			return 0;
	}
	return 0;
}

#define MAX_LABEL_NAME_LEN 32
//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK LabelDialogProc(
	HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
)
{
	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			SetFocus(GetDlgItem(hwndDlg, IDC_LABELEDIT));
		}
		return 0;

		case WM_COMMAND:
			//button message
			switch (LOWORD(wParam))
			{
				case IDC_CREATELABEL:
				{
					int line = get_current_line();
					unsigned short address = get_pc_at_line(line);

					char labelname[MAX_LABEL_NAME_LEN];
					SendDlgItemMessage(hwndDlg, IDC_LABELEDIT, WM_GETTEXT, MAX_LABEL_NAME_LEN, (LPARAM)labelname);

					make_label(labelname, SR_MANUAL, address, LT_SUB);

					update_display();
					disview_get_labels();

					EndDialog(hwndDlg, 0);
					return 1;
				}
				break;

				case IDC_CANCELCREATELABEL:
				{
					EndDialog(hwndDlg, 0);
					return 1;
				}
				break;
			}
			break;

		case WM_CLOSE:
			EndDialog(hwndDlg, 0);
			return 0;
	}
	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK AddressDialogProc(
	HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
)
{
	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			SetFocus(GetDlgItem(hwndDlg, IDC_ADDRESSEDIT));
		}
		return 0;

		case WM_COMMAND:
			//button message
			switch (LOWORD(wParam))
			{
				case IDC_GOTOADDRESS:
				{
					char labelname[MAX_LABEL_NAME_LEN];
					labelname[0] = '\0';
					SendDlgItemMessage(hwndDlg, IDC_ADDRESSEDIT, WM_GETTEXT, MAX_LABEL_NAME_LEN, (LPARAM)labelname);

					unsigned short pc = (unsigned short)strtoul(labelname, NULL, 16);
					ZX_LABEL zx;
					if (get_label_by_name(labelname, &zx))
						pc = zx.address;
					disview_skip_to_address(pc);

					EndDialog(hwndDlg, 0);
					return 1;
				}
				break;

				case IDC_CANCELADDRESS:
				{
					EndDialog(hwndDlg, 0);
					return 1;
				}
				break;
			}
			break;

		case WM_CLOSE:
			EndDialog(hwndDlg, 0);
			return 0;
	}
	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK TapeInfoDialogProc(
	HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
)
{
	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			SendDlgItemMessage(hwndDlg, IDC_TAPEINFOLIST, LB_RESETCONTENT, 0, 0);
		}
		return 0;

		case WM_COMMAND:
			//button message
			switch (LOWORD(wParam))
			{
				case IDC_TAPEINFOLIST:
					if (HIWORD(wParam) == LBN_DBLCLK)
					{
						int sel = (int)SendDlgItemMessage(hwndDlg, IDC_TAPEINFOLIST, LB_GETCURSEL, 0, 0);
						set_tape_position(sel);
					}
					break;
			}
			break;

		case WM_SIZING:
		{
			RECT edit;
			RECT parent;
			GetWindowRect(GetDlgItem(hwndDlg, IDC_TAPEINFOLIST), &edit);
			parent = *(RECT *)lParam;

			edit.right = parent.right - 18;
			edit.bottom = parent.bottom - 20;

			SetWindowPos(GetDlgItem(hwndDlg, IDC_TAPEINFOLIST), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
		}
		return 0;

		case WM_CLOSE:
			set_dialog_status(DLG_TAPEINFO, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			return 0;
	}
	return 0;
}
//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void make_current_line_data(void)
{
	int line = get_current_line();
	unsigned short address = get_pc_at_line(line);

	ZX_TYPE type;
	if (get_type_at_address(address, &type))
	{
		if (type.type == CODE)
			make_data(SR_MANUAL, address, DAsmEx(address, NULL));
		else if (type.type == DEFW)
			make_data(SR_MANUAL, address, 2);
		else if (type.type != DEFB)
			make_data(SR_MANUAL, address, 1);
		else
			make_dataw(SR_MANUAL, address, 2);
	}
	else
	{
		make_dataw(SR_MANUAL, address, 2);
	}
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void make_current_line_code(void)
{
	int line = get_current_line();
	unsigned short address = get_pc_at_line(line);
	make_code(SR_MANUAL, address);
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK WindowProc(
  HWND hwnd,      // handle to window
  UINT uMsg,      // message identifier
  WPARAM wParam,  // first message parameter
  LPARAM lParam   // second message parameter
)
{
	switch (uMsg)
	{
		case WM_DESTROY:
			PostQuitMessage(0);
			quit = 1;
			break;

		case WM_PAINT:
			{
			PAINTSTRUCT paint={0};
			paint.hdc = (HDC)wParam;
			if (GetUpdateRect(hwnd, &paint.rcPaint,TRUE))
			{
				BeginPaint(hwnd, &paint);
				EndPaint(hwnd, &paint);
			}
			}
			break;

		case WM_COMMAND:
			if (HIWORD(wParam) == 0)
			{
				//menu message
				switch (LOWORD(wParam))
				{
					//file menu
					case ID_FILEMENU_LOAD:
					{
						START_REGS *start_regs;
						set_wait_cursor();
						start_regs = load();
						if (start_regs)
						{
							set_mru_menu();
								//reset_machine();
							//load_ram(code);
							emulate_start_game(start_regs);

							//disassemble();
							//disassemble_plain();
							disassemble_advanced();
						}
						unset_wait_cursor();
					}
					break;
					case ID_FILEMENU_LOADTAPE:
						set_wait_cursor();
						load_tape();
						set_mru_menu();
						unset_wait_cursor();
						break;

					case ID_FILEMENU_SAVE:
						save();
						break;
					case ID_FILEMENU_EXIT:
						quit = 1;
						break;

					//options menu
					case ID_OPTIONS_16K:
						init_machine(SPEC_16K);
						go();
						break;
					case ID_OPTIONS_48K:
						init_machine(SPEC_48K);
						go();
						break;
					case ID_OPTIONS_128K:
						init_machine(SPEC_128K);
						go();
						break;
					case ID_OPTIONS_PLUS2:
						init_machine(SPEC_PLUS2);
						go();
						break;
					case ID_OPTIONS_PLUS2A:
						init_machine(SPEC_PLUS2A);
						go();
						break;
					case ID_OPTIONS_PLUS3:
						init_machine(SPEC_PLUS3);
						go();
						break;
					case ID_OPTIONS_NONE:
						init_machine(SPEC_48K);
						go();
						break;

					//view menu
					case ID_VIEW_BASIC:
						toggle_dialog(basic_dialog, DLG_BASIC, ~GetMenuState(main_menu, ID_VIEW_BASIC, 0) & MF_CHECKED);
						break;
					case ID_VIEW_COVERAGE:
						toggle_dialog(coverage_window, DLG_COV, ~GetMenuState(main_menu, ID_VIEW_COVERAGE, 0) & MF_CHECKED);
						break;
					case ID_VIEW_FPU:
						toggle_dialog(fpu_dialog, DLG_FPU, ~GetMenuState(main_menu, ID_VIEW_FPU, 0) & MF_CHECKED);
						break;
					case ID_VIEW_GFX:
						toggle_dialog(zxgfx_dialog, DLG_ZXGFX, ~GetMenuState(main_menu, ID_VIEW_GFX, 0) & MF_CHECKED);
						break;
					case ID_VIEW_STATS:
						toggle_dialog(stats_dialog, DLG_STATS, ~GetMenuState(main_menu, ID_VIEW_STATS, 0) & MF_CHECKED);
						break;
					case ID_VIEW_MEMORY:
						toggle_dialog(mem_dialog, DLG_MEM, ~GetMenuState(main_menu, ID_VIEW_MEMORY, 0) & MF_CHECKED);
						break;
					case ID_VIEW_PCTRACE:
						toggle_dialog(pctrace_dialog, DLG_PCT, ~GetMenuState(main_menu, ID_VIEW_PCTRACE, 0) & MF_CHECKED);
						break;
					case ID_VIEW_REGISTERS:
						toggle_dialog(reg_dialog, DLG_REG, ~GetMenuState(main_menu, ID_VIEW_REGISTERS, 0) & MF_CHECKED);
						break;
					case ID_VIEW_STACK:
						toggle_dialog(stack_dialog, DLG_STK, ~GetMenuState(main_menu, ID_VIEW_STACK, 0) & MF_CHECKED);
						break;
					case ID_VIEW_SYSVARS:
						toggle_dialog(sysvar_dialog, DLG_SYSVAR, ~GetMenuState(main_menu, ID_VIEW_SYSVARS, 0) & MF_CHECKED);
						break;
					case ID_VIEW_TAPEINFO:
						toggle_dialog(tapeinfo_dialog, DLG_TAPEINFO, ~GetMenuState(main_menu, ID_VIEW_TAPEINFO, 0) & MF_CHECKED);
						break;
					case ID_VIEW_CONSOLE:
						toggle_dialog(console_dialog, DLG_CONSOLE, ~GetMenuState(main_menu, ID_VIEW_CONSOLE, 0) & MF_CHECKED);
						break;
					case ID_VIEW_POKES:
						toggle_dialog(pokes_dialog, DLG_POKES, ~GetMenuState(main_menu, ID_VIEW_POKES, 0) & MF_CHECKED);
						break;

					case ID_EMULATE_GO:
						update_display();
						emulating = EMU_RUN;
						SetFocus(emu_window);
						break;

					//help menu
					case ID_HELP_ABOUT:
						MessageBoxW(NULL, L"Speccy Emulator and Decompiler.\n"
							L"\xA9 Copyright 2020 James Shaw. All Rights Reserved.\n"
							L"\n"
							L"Binaries built from the following sources are included in this project:\n\n"
							L"\tcJSON https://github.com/DaveGamble/cJSON \n\t\xA9 Copyright 2009-2017 Dave Gamble and cJSON contributors.\n\tMIT License.\n\n"
							L"\tSQLite https://www.sqlite.org \n\tPublic Domain.\n\n"
							L"\tKubazip https://github.com/kuba--/zip \n\tPublic Domain.\n\n"
							L"\tGraphviz https://graphviz.org/ \n\tCommon Public License V1.0.\n\n"
							L"\n"
							L"Many, many thanks to the people who have contributed to these projects.\n"
							L"\n"
							L"Binaries of ZX Spectrum ROMs are included.\n\n"
							L"Amstrad have kindly given their permission for the redistribution of their copyrighted material but retain that copyright.",
							L"About", MB_OK);
						break;

					//database menu
					case ID_DATABASE_OPEN:
						DialogBox(dis_instance, (LPCTSTR)IDD_DATABASE, hwnd, (DLGPROC)DatabaseDialogProc);
						break;
					case ID_DATABASE_CLOSE:
						close_db();
						set_database_caption(NULL);
						disview_database_changed();
						break;
					case ID_DATABASE_NEW:
						DialogBox(dis_instance, (LPCTSTR)IDD_DATABASE_CREATE, hwnd, (DLGPROC)DatabaseCreateDialogProc);
						break;
					case ID_DATABASE_EXPORTASJSON:
					{
						char *fname = generic_save("JSON Files\0*.json\0", "Select JSON File to write", "json", get_db_name());
						if (fname)
						{
							if (export_db(fname))
								MessageBox(hwnd, "There was a problem writing the JSON.", "Error", MB_OK | MB_ICONERROR);
							else
								MessageBox(hwnd, "JSON export complete.", "Success", MB_OK | MB_ICONINFORMATION);
						}
					}
					break;
					case ID_DATABASE_IMPORTASJSON:
					{
						char *fname = generic_load("JSON Files\0*.json\0", "Select JSON File", "json", NULL);
						if (fname)
						{
							if (import_db(fname))
							{
								MessageBox(hwnd, "There was a problem reading the JSON.", "Error", MB_OK | MB_ICONERROR);
							}
							else
							{
								MessageBox(hwnd, "JSON import complete.", "Success", MB_OK | MB_ICONINFORMATION);
								set_database_caption(get_db_name());
								disview_database_changed();
							}
						}
					}
					break;
					case ID_DATABASE_APPLYLIBRARY:
						apply_library_labels();
						reload_database();
						break;

				//display menu
					case ID_DISPLAY_X1:
						set_emu_size_scale(emu_window, 1);
						break;
					case ID_DISPLAY_X2:
						set_emu_size_scale(emu_window, 2);
						break;
					case ID_DISPLAY_X3:
						set_emu_size_scale(emu_window, 3);
						break;
					case ID_DISPLAY_X4:
						set_emu_size_scale(emu_window, 4);
						break;

					//popup menu
					case ID_POPUP_BREAKPOINT:
					{
						int line = get_current_line();

						if (toggle_breakpoint(get_pc_at_line(line)) == 0)
							set_breakpoint(get_pc_at_line(line));
					}
					update_display();
					break;

					case ID_POPUP_SETPC:
					{
						int start = 0, end = 0;
						int line;
						SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_GETSEL, (WPARAM)&start, (LPARAM)&end);
						line = (int)SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_LINEFROMCHAR, start, 0);
						{
							REG_PACK *regs = get_regs();
							regs->pc = get_pc_at_line(line);
						}
					}
					break;

					case ID_POPUP_NAME:
						DialogBox(dis_instance, (LPCTSTR)IDD_LABELNAME, hwnd, (DLGPROC)LabelDialogProc);
						break;

					case ID_POPUP_DATA:
						make_current_line_data();
						update_display();
						break;

					case ID_POPUP_CODE:
						make_current_line_code();
						update_display();
						break;

					case ID_POPUP_UNDEFINE:
						break;

					default:
					{
						int item = LOWORD(wParam);
						if (item >= MRU_MENU_DBX && item <= MRU_MENU_DBX + 0x20)
						{
							MENUITEMINFO mii = { 0 }; mii.cbSize = sizeof mii;
							char itemtext[1000];
							mii.dwTypeData = itemtext;
							mii.fMask = MIIM_STRING;
							mii.cch = 1000;
							GetMenuItemInfo(GetSubMenu(main_menu, 1), item, FALSE, &mii);
							open_db_file(itemtext);
						}
						if (item >= MRU_MENU_TAP && item <= MRU_MENU_TAP + 0x20)
						{
							MENUITEMINFO mii = { 0 }; mii.cbSize = sizeof mii;
							char itemtext[1000];
							mii.dwTypeData = itemtext;
							mii.fMask = MIIM_STRING;
							mii.cch = 1000;
							GetMenuItemInfo(GetSubMenu(main_menu, 0), item, FALSE, &mii);
							load_tape_file(itemtext);
						}
						if (item >= MRU_MENU_SNA && item <= MRU_MENU_SNA + 0x20)
						{
							MENUITEMINFO mii = { 0 }; mii.cbSize = sizeof mii;
							char itemtext[1000];
							mii.dwTypeData = itemtext;
							mii.fMask = MIIM_STRING;
							mii.cch = 1000;
							GetMenuItemInfo(GetSubMenu(main_menu, 0), item, FALSE, &mii);
							emulate_start_game(load_snapshot_file(itemtext));
							disassemble_advanced();
						}
					}
				}
			}
			break;

		case WM_TIMER:
			if (auto_update)
				update_aux_display();
			break;

		case WM_SIZING:
			SendMessage(dis_dialog, WM_SIZING, wParam, lParam);
			SendMessage(disctl_dialog, WM_SIZING, wParam, lParam);

			RECT rect;
			GetWindowRect(dis_window, &rect);
			MoveWindow(status_window, 0, rect.bottom, 0, 0, TRUE);
			break;

		default:
			return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}
	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK RegDialogProc(
  HWND hwndDlg,  // handle to dialog box
  UINT uMsg,     // message
  WPARAM wParam, // first message parameter
  LPARAM lParam  // second message parameter
  )
{

	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
			{
			CHARFORMAT charfmt;

			charfmt.cbSize = sizeof(CHARFORMAT);
			charfmt.dwMask = CFM_CHARSET|CFM_FACE|CFM_SIZE;
			charfmt.dwEffects = 0;
			charfmt.yHeight = 10*20;
			charfmt.yOffset = 0;
			charfmt.crTextColor = 0;
			charfmt.bCharSet = ANSI_CHARSET;
			charfmt.bPitchAndFamily = 0;
			strcpy_s(charfmt.szFaceName, LF_FACESIZE, UI_FONT_NAME);

			SendDlgItemMessage(hwndDlg, IDC_REGISTERS, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);

			SendDlgItemMessage(hwndDlg, IDC_REGISTERS, WM_SETTEXT, 0, (LPARAM)"");
			}
			return 0;

		case WM_SIZING:
		{
			RECT edit;
			RECT parent;
			GetWindowRect(GetDlgItem(hwndDlg, IDC_REGISTERS), &edit);
			parent = *(RECT *)lParam;

			edit.right = parent.right - 18;
			edit.bottom = parent.bottom - 20;

			SetWindowPos(GetDlgItem(hwndDlg, IDC_REGISTERS), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
		}
		return 0;

		case WM_CLOSE:
			set_dialog_status(DLG_REG, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			return 0;
	}
return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK MemDialogProc(
  HWND hwndDlg,  // handle to dialog box
  UINT uMsg,     // message
  WPARAM wParam, // first message parameter
  LPARAM lParam  // second message parameter
  )
{
	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
			{
			CHARFORMAT charfmt;

			charfmt.cbSize = sizeof(CHARFORMAT);
			charfmt.dwMask = CFM_CHARSET|CFM_FACE|CFM_SIZE;
			charfmt.dwEffects = 0;
			charfmt.yHeight = 8*20;
			charfmt.yOffset = 0;
			charfmt.crTextColor = 0;
			charfmt.bCharSet = ANSI_CHARSET;
			charfmt.bPitchAndFamily = 0;
			strcpy_s(charfmt.szFaceName, LF_FACESIZE, UI_FONT_NAME);

			SendDlgItemMessage(hwndDlg, IDC_MEMORY, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);

			SendDlgItemMessage(hwndDlg, IDC_MEMORY, WM_SETTEXT, 0, (LPARAM)"");
			}
			return 0;

		case WM_SIZING:
		{
			RECT edit;
			RECT parent;
			GetWindowRect(GetDlgItem(hwndDlg, IDC_MEMORY), &edit);
			parent = *(RECT *)lParam;

			edit.right = parent.right - 18;
			edit.bottom = parent.bottom - 20;

			SetWindowPos(GetDlgItem(hwndDlg, IDC_MEMORY), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
		}
		return 0;

		case WM_CLOSE:
			set_dialog_status(DLG_MEM, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			return 0;
	}
return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK StatsDialogProc(
  HWND hwndDlg,  // handle to dialog box
  UINT uMsg,     // message
  WPARAM wParam, // first message parameter
  LPARAM lParam  // second message parameter
  )
{

	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
			{
			CHARFORMAT charfmt;

			charfmt.cbSize = sizeof(CHARFORMAT);
			charfmt.dwMask = CFM_CHARSET|CFM_FACE|CFM_SIZE;
			charfmt.dwEffects = 0;
			charfmt.yHeight = 8*20;
			charfmt.yOffset = 0;
			charfmt.crTextColor = 0;
			charfmt.bCharSet = ANSI_CHARSET;
			charfmt.bPitchAndFamily = 0;
			strcpy_s(charfmt.szFaceName, LF_FACESIZE, UI_FONT_NAME);

			SendDlgItemMessage(hwndDlg, IDC_STATS, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);

			SendDlgItemMessage(hwndDlg, IDC_STATS, WM_SETTEXT, 0, (LPARAM)"");
			}
			return 0;

		case WM_SIZING:
			{
			RECT edit;
			RECT parent;
			GetWindowRect(GetDlgItem(hwndDlg, IDC_STATS), &edit);
			parent = *(RECT *)lParam;

			edit.right = parent.right - 18;
			edit.bottom = parent.bottom - 20;

			SetWindowPos(GetDlgItem(hwndDlg, IDC_STATS), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
			}
			return 0;

		case WM_CLOSE:
			set_dialog_status(DLG_STATS, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			return 0;
	}
return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK TraceDialogProc(
  HWND hwndDlg,  // handle to dialog box
  UINT uMsg,     // message
  WPARAM wParam, // first message parameter
  LPARAM lParam  // second message parameter
  )
{

	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
			{
			CHARFORMAT charfmt;

			charfmt.cbSize = sizeof(CHARFORMAT);
			charfmt.dwMask = CFM_CHARSET|CFM_FACE|CFM_SIZE;
			charfmt.dwEffects = 0;
			charfmt.yHeight = 8*20;
			charfmt.yOffset = 0;
			charfmt.crTextColor = 0;
			charfmt.bCharSet = ANSI_CHARSET;
			charfmt.bPitchAndFamily = 0;
			strcpy_s(charfmt.szFaceName, LF_FACESIZE, UI_FONT_NAME);

			SendDlgItemMessage(hwndDlg, IDC_STATS, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);

			SendDlgItemMessage(hwndDlg, IDC_PCTRACE, LB_RESETCONTENT, 0, 0);
			}
			return 0;

		case WM_SIZING:
		{
			RECT edit;
			RECT parent;
			GetWindowRect(GetDlgItem(hwndDlg, IDC_PCTRACE), &edit);
			parent = *(RECT *)lParam;

			edit.right = parent.right - 18;
			edit.bottom = parent.bottom - 20;

			SetWindowPos(GetDlgItem(hwndDlg, IDC_PCTRACE), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
		}
		return 0;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDC_PCTRACE:
				{
					if (HIWORD(wParam) == LBN_DBLCLK)
					{
						char label[100];
						int sel = (int)SendDlgItemMessage(hwndDlg, IDC_PCTRACE, LB_GETCURSEL, 0, 0);
						SendDlgItemMessage(hwndDlg, IDC_PCTRACE, LB_GETTEXT, sel, (LPARAM)label);
						label[4] = '\0';
						unsigned short pc = (unsigned short)strtoul(label, NULL, 16);
						disview_skip_to_address(pc);
					}
				}
			}
			break;

		case WM_CLOSE:
			set_dialog_status(DLG_PCT, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			return 0;
	}
return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
typedef struct
{
	unsigned char lastvalue;
	unsigned char interesting;
} POKE_REC;

static char pokemode = 0;
static char has_search;
static POKE_REC pokes[65536];
static void update_interesting(void)
{
	SendDlgItemMessage(pokes_dialog, IDC_POKELIST, LB_RESETCONTENT, 0, 0);
	
	if (!has_search) return;

	for (int i = 0; i < 65536; i++)
	{
		if (pokes[i].interesting)
		{
			char msg[1000];
			msg[0] = '\0';
			if (pokemode == 0)
				sprintf_s(msg, 1000, "B %04X %d (%d)", i, peek_fast((unsigned short)i), pokes[i].lastvalue);
			else if (pokemode == 1 && i != 65535)
				sprintf_s(msg, 1000, "W %04X %d (%d)", i, peek_fastw((unsigned short)i), pokes[i].lastvalue + ((unsigned short)pokes[i+1].lastvalue<<8));
			SendDlgItemMessage(pokes_dialog, IDC_POKELIST, LB_ADDSTRING, 0, (LPARAM)msg);
		}
	}
}

static void poke_reset(void)
{
	has_search = 0;
	for (int i = 0; i < 65536; i++)
	{
		pokes[i].lastvalue = peek_fast((unsigned short)i);
		pokes[i].interesting = 1;
	}
	update_interesting();
}
static void poke_searchb(unsigned char srch)
{
	for (int i = 0; i < 65536; i++)
	{
		unsigned char b = peek_fast((unsigned short)i);
		if (b != srch)
			pokes[i].interesting = 0;
		pokes[i].lastvalue = b;
	}
	has_search = 1;
	update_interesting();
}
static void poke_searchw(unsigned short srch)
{
	for (int i = 0; i < 65535; i++)
	{
		unsigned short b = peek_fastw((unsigned short)i);
		if (b != srch)
			pokes[i].interesting = 0;
		pokes[i].lastvalue = (unsigned char)b;
		pokes[i + 1].lastvalue = (unsigned char)(b >> 8);
	}
	has_search = 1;
	update_interesting();
}
static void poke_incb(void)
{
	for (int i = 0; i < 65536; i++)
	{
		unsigned char b = peek_fast((unsigned short)i);
		if (b <= pokes[i].lastvalue)
			pokes[i].interesting = 0;
		pokes[i].lastvalue = b;
	}
	has_search = 1;
	update_interesting();
}
static void poke_incw(void)
{
	for (int i = 0; i < 65535; i++)
	{
		unsigned short b = peek_fastw((unsigned short)i);
		if (b <= pokes[i].lastvalue + ((unsigned short)pokes[i+1].lastvalue<<8))
			pokes[i].interesting = 0;
		pokes[i].lastvalue = (unsigned char)b;
		pokes[i + 1].lastvalue = (unsigned char)(b >> 8);
	}
	has_search = 1;
	update_interesting();
}
static void poke_decb(void)
{
	for (int i = 0; i < 65536; i++)
	{
		unsigned char b = peek_fast((unsigned short)i);
		if (b >= pokes[i].lastvalue)
			pokes[i].interesting = 0;
		pokes[i].lastvalue = b;
	}
	has_search = 1;
	update_interesting();
}
static void poke_decw(void)
{
	for (int i = 0; i < 65535; i++)
	{
		unsigned short b = peek_fastw((unsigned short)i);
		if (b >= pokes[i].lastvalue + ((unsigned short)pokes[i + 1].lastvalue << 8))
			pokes[i].interesting = 0;
		pokes[i].lastvalue = (unsigned char)b;
		pokes[i + 1].lastvalue = (unsigned char)(b >> 8);
	}
	has_search = 1;
	update_interesting();
}

LRESULT CALLBACK PokesDialogProc(
	HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
)
{
	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			CHARFORMAT charfmt;

			charfmt.cbSize = sizeof(CHARFORMAT);
			charfmt.dwMask = CFM_CHARSET | CFM_FACE | CFM_SIZE;
			charfmt.dwEffects = 0;
			charfmt.yHeight = 8 * 20;
			charfmt.yOffset = 0;
			charfmt.crTextColor = 0;
			charfmt.bCharSet = ANSI_CHARSET;
			charfmt.bPitchAndFamily = 0;
			strcpy_s(charfmt.szFaceName, LF_FACESIZE, UI_FONT_NAME);

			SendDlgItemMessage(hwndDlg, IDC_POKELIST, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);

			SendDlgItemMessage(hwndDlg, IDC_POKEBYTE, BM_SETCHECK, pokemode == 0 ? BST_CHECKED : BST_UNCHECKED, 0);
			SendDlgItemMessage(hwndDlg, IDC_POKEWORD, BM_SETCHECK, pokemode == 1 ? BST_CHECKED : BST_UNCHECKED, 0);

			SetDlgItemInt(hwndDlg, IDC_POKEVALUE, 0, FALSE);

			poke_reset();
			update_interesting();
		}
		return 0;

		case WM_SIZING:
		{
			RECT edit;
			RECT parent;
			GetWindowRect(GetDlgItem(hwndDlg, IDC_POKELIST), &edit);
			parent = *(RECT *)lParam;

			edit.right = parent.right - 18;
			edit.bottom = parent.bottom - 20;

			SetWindowPos(GetDlgItem(hwndDlg, IDC_POKELIST), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
		}
		return 0;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDC_POKEBYTE:
					pokemode = 0;
					SendDlgItemMessage(hwndDlg, IDC_POKEBYTE, BM_SETCHECK, pokemode == 0 ? BST_CHECKED : BST_UNCHECKED, 0);
					SendDlgItemMessage(hwndDlg, IDC_POKEWORD, BM_SETCHECK, pokemode == 1 ? BST_CHECKED : BST_UNCHECKED, 0);
					break;
				case IDC_POKEWORD:
					pokemode = 1;
					SendDlgItemMessage(hwndDlg, IDC_POKEBYTE, BM_SETCHECK, pokemode == 0 ? BST_CHECKED : BST_UNCHECKED, 0);
					SendDlgItemMessage(hwndDlg, IDC_POKEWORD, BM_SETCHECK, pokemode == 1 ? BST_CHECKED : BST_UNCHECKED, 0);
					break;
				case IDC_POKERESET:
					poke_reset();
					break;
				case IDC_POKEINC:
					if (pokemode == 0) poke_incb(); else poke_incw();
					break;
				case IDC_POKEDEC:
					if (pokemode == 0) poke_decb(); else poke_decw();
					break;
				case IDC_POKESEARCH:
				{
					int s = (int)GetDlgItemInt(hwndDlg, IDC_POKEVALUE, NULL, FALSE);
					int t = s;
					if (pokemode == 0)
						t = min(max(s, 0), 255);
					else
						t = min(max(s, 0), 65535);
					if (t != s)
						SetDlgItemInt(hwndDlg, IDC_POKEVALUE, t, FALSE);
					if (pokemode == 0) poke_searchb((unsigned char)t);
					else poke_searchw((unsigned short)t);
					break;
				}
			}
			break;

		case WM_CLOSE:
			set_dialog_status(DLG_STATS, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			return 0;
	}
	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK StackDialogProc(
  HWND hwndDlg,  // handle to dialog box
  UINT uMsg,     // message
  WPARAM wParam, // first message parameter
  LPARAM lParam  // second message parameter
  )
{
	
	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
			{
			CHARFORMAT charfmt;

			charfmt.cbSize = sizeof(CHARFORMAT);
			charfmt.dwMask = CFM_CHARSET|CFM_FACE|CFM_SIZE;
			charfmt.dwEffects = 0;
			charfmt.yHeight = 8*20;
			charfmt.yOffset = 0;
			charfmt.crTextColor = 0;
			charfmt.bCharSet = ANSI_CHARSET;
			charfmt.bPitchAndFamily = 0;
			strcpy_s(charfmt.szFaceName, LF_FACESIZE, UI_FONT_NAME);

			SendDlgItemMessage(hwndDlg, IDC_STACK, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);

			SendDlgItemMessage(hwndDlg, IDC_STACK, WM_SETTEXT, 0, (LPARAM)"");
			}
			return 0;

		case WM_SIZING:
		{
			RECT edit;
			RECT parent;
			GetWindowRect(GetDlgItem(hwndDlg, IDC_STACK), &edit);
			parent = *(RECT *)lParam;

			edit.right = parent.right - 18;
			edit.bottom = parent.bottom - 20;

			SetWindowPos(GetDlgItem(hwndDlg, IDC_STACK), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
		}
		return 0;

		case WM_CLOSE:
			set_dialog_status(DLG_STK, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			return 0;
	}
return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK FPStackDialogProc(
  HWND hwndDlg,  // handle to dialog box
  UINT uMsg,     // message
  WPARAM wParam, // first message parameter
  LPARAM lParam  // second message parameter
  )
{
	
	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
			{
			CHARFORMAT charfmt;

			charfmt.cbSize = sizeof(CHARFORMAT);
			charfmt.dwMask = CFM_CHARSET|CFM_FACE|CFM_SIZE;
			charfmt.dwEffects = 0;
			charfmt.yHeight = 8*20;
			charfmt.yOffset = 0;
			charfmt.crTextColor = 0;
			charfmt.bCharSet = ANSI_CHARSET;
			charfmt.bPitchAndFamily = 0;
			strcpy_s(charfmt.szFaceName, LF_FACESIZE, UI_FONT_NAME);

			SendDlgItemMessage(hwndDlg, IDC_FPSTACK, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);

			SendDlgItemMessage(hwndDlg, IDC_FPSTACK, WM_SETTEXT, 0, (LPARAM)"");
			}
			return 0;

		case WM_SIZING:
		{
			RECT edit;
			RECT parent;
			GetWindowRect(GetDlgItem(hwndDlg, IDC_FPSTACK), &edit);
			parent = *(RECT *)lParam;

			edit.right = parent.right - 18;
			edit.bottom = parent.bottom - 20;

			SetWindowPos(GetDlgItem(hwndDlg, IDC_FPSTACK), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
		}
		return 0;

		case WM_CLOSE:
			set_dialog_status(DLG_FPU, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			return 0;
	}
return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK BASICDialogProc(
	HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
)
{

	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			CHARFORMAT charfmt;

			charfmt.cbSize = sizeof(CHARFORMAT);
			charfmt.dwMask = CFM_CHARSET | CFM_FACE | CFM_SIZE;
			charfmt.dwEffects = 0;
			charfmt.yHeight = 8 * 20;
			charfmt.yOffset = 0;
			charfmt.crTextColor = 0;
			charfmt.bCharSet = ANSI_CHARSET;
			charfmt.bPitchAndFamily = 0;
			strcpy_s(charfmt.szFaceName, LF_FACESIZE, UI_FONT_NAME);

			SendDlgItemMessage(hwndDlg, IDC_BASICEDIT, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);

			SendDlgItemMessage(hwndDlg, IDC_BASICEDIT, WM_SETTEXT, 0, (LPARAM)"");
		}
		return 0;

		case WM_SIZING:
			{
			RECT edit;
			RECT parent;
			GetWindowRect(GetDlgItem(hwndDlg, IDC_BASICEDIT), &edit);
			parent = *(RECT *)lParam;

			edit.right = parent.right - 18;
			edit.bottom = parent.bottom - 20;

			SetWindowPos(GetDlgItem(hwndDlg, IDC_BASICEDIT), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
			}
			return 0;

		case WM_CLOSE:
			set_dialog_status(DLG_BASIC, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			return 0;

		case WM_COMMAND:
			//button message
			switch (LOWORD(wParam))
			{
				case IDC_RUN_BASIC:
					{
						SetDlgItemText(hwndDlg, IDC_BASIC_ERROR, "");

						char *txt = malloc(1000000);
						GetDlgItemText(hwndDlg, IDC_BASICEDIT, txt, 1000000);
						char *err = tokenise(txt);
						free(txt);

						if (err != NULL)
							SetDlgItemText(hwndDlg, IDC_BASIC_ERROR, err);

						SetFocus(emu_window);
					}
					break;
			}
			break;
	}
	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
typedef enum
{
	COMMAND,
	ASSEMBLY,
} CONSOLE_MODE;

typedef struct
{
	CONSOLE_MODE mode;
	unsigned short pc;
	unsigned short mem;
} CONSOLE_STATE;

static CONSOLE_STATE console_state = { COMMAND,0,0 };

typedef struct
{
	char cmd[1000];
	char *tokens[100];
	int n_tokens;
} PARMS;

//---------------------------------------------------------------------------------------
//
//int number(char *num)
//{
//	if (num[0] == '$')
//		return strtol(num + 1, NULL, 16);
//
//	size_t s = strlen(num);
//	if (s == 0) return 0;
//	
//	s--;
//	if (num[s] == 'd' || num[s] == 'D')
//	{
//		num[s] = '\0';
//		return strtol(num, NULL, 10);
//	}
//
//	if (num[s] == 'h' || num[s] == 'H')
//		num[s] = '\0';
//	
//	return strtol(num, NULL, 16);
//}

int number(char *num)
{
	int value;
	if (expr_eval(num, 0, &value, get_db_labels()))
		return value;
	return 0;
}

//---------------------------------------------------------------------------------------
static void console_out_start(void)
{
	HWND hwndDlg = GetDlgItem(console_dialog, IDC_CONSOLEEDIT);
	GETTEXTLENGTHEX gtlex = { GTL_DEFAULT, CP_ACP };
	CHARRANGE range;
	range.cpMin = (LONG)SendMessage(hwndDlg, EM_GETTEXTLENGTHEX, (WPARAM)&gtlex, 0);
	range.cpMax = range.cpMin;
	SendMessage(hwndDlg, EM_EXSETSEL, 0, (LPARAM)&range);
}

static void console_out_end(void) { }

static void console_out_bulk(char *txt)
{
	HWND hwndDlg = GetDlgItem(console_dialog, IDC_CONSOLEEDIT);
	SendMessage(hwndDlg, EM_REPLACESEL, 0, (LPARAM)txt);
}

static void console_out(char *fmt, ...)
{
	char *txt = malloc(max(strlen(fmt)+1, 1000));

	va_list v;
	va_start(v, fmt);
	vsprintf_s(txt, 1000, fmt, v);
	va_end(v);

	console_out_start();
	console_out_bulk(txt);
	console_out_end();

	free(txt);
}
//---------------------------------------------------------------------------------------

static void con_define_byte(PARMS *p)
{
	if (p->n_tokens < 2) return;
	unsigned short address;
	unsigned int size = 1;
	address = (unsigned short)number(p->tokens[1]);
	if (p->n_tokens > 2)
		size = number(p->tokens[2]);
	console_out("defb %04Xh,%04Xh\n", address, size);
	make_data(SR_MANUAL, address, size);
}

static void con_define_word(PARMS *p)
{
	if (p->n_tokens < 2) return;
	unsigned short address;
	unsigned int size = 2;
	address = (unsigned short)number(p->tokens[1]);
	if (p->n_tokens > 2)
	size = number(p->tokens[2]);
	console_out("defw %04Xh,%04Xh\n", address, size);
	make_dataw(SR_MANUAL, address, size);
}

static void con_define_code(PARMS *p)
{
	if (p->n_tokens < 2) return;
	unsigned short address;
	unsigned int size;
	address = (unsigned short)number(p->tokens[1]);
	if (p->n_tokens > 2)
	{
		size = number(p->tokens[2]);
		console_out("defc %04Xh,%04Xh\n", address, size);
		make_code_with_size(SR_MANUAL, address, size);
	}
	else
	{
		console_out("defc %04Xh,%04Xh\n", address);
		make_code(SR_MANUAL, address);
	}
}

static void con_label(PARMS *p)
{
	if (p->n_tokens < 3) return;
	unsigned short address;
	address = (unsigned short)number(p->tokens[1]);
	console_out("label %04Xh,%s\n", address, p->tokens[2]);
	make_label(p->tokens[2], SR_MANUAL, address, LT_SUB);
}

static void con_run(PARMS *p) { p; console_out("run\n");  go(); }
static void con_out(PARMS *p) { p; console_out("out\n"); stepout(); }
static void con_step(PARMS *p) { p; console_out("over\n"); stepover(); }
static void con_stepin(PARMS *p) { p; console_out("into\n"); stepinto(); }
static void con_stop(PARMS *p) { p; console_out("break\n"); stop(); }
static void con_close(PARMS *p) { p; console_out("quit\n"); SendMessage(console_dialog, WM_CLOSE, 0,0); }
static void con_breakpoint(PARMS *p)
{
	if (p->n_tokens < 2) return;
	unsigned int address = number(p->tokens[1]);
	console_out("bpt %04Xh\n", address);
	set_breakpoint((unsigned short)address);
}
static void con_assemble(PARMS *p) { p; console_out("asm mode\n"); console_state.mode = ASSEMBLY; }
static void con_disassemble(PARMS *p)
{
	if (p->n_tokens >= 2)
	{
		unsigned int pc = number(p->tokens[1]);
		console_out("dis %04Xh\n", pc);
		console_state.pc = (unsigned short)pc;
	}
	console_out_start();
	for (int x = 0; x < 20; x++)
	{
		DASM_RES res;
		int size = DAsmEx(console_state.pc, &res);
		char ins[100], *t = ins;
		t += sprintf_s(t, ins - t + 100, "%04X ", console_state.pc);
		for (int i = 0; i < size; i++)
			t += sprintf_s(t, ins - t + 100, "%02X ", res.bytes[i]);
		for (int i = size; i < 4; i++)
			t += sprintf_s(t, ins - t + 100, "   ");
		t += sprintf_s(t, ins - t + 100, "%s", res.instr);
		t += sprintf_s(t, ins - t + 100, "\r\n");
		console_out_bulk(ins);
		console_state.pc += (unsigned short)size;
	}
	console_out_end();
}

static void con_peek(PARMS *p)
{
	if (p->n_tokens < 2) return;
	unsigned int address = number(p->tokens[1]);
	console_out("peek %04Xh\n", address);
	console_out("%02Xh", peek_fast((unsigned short)address));
}

static void con_peekw(PARMS *p)
{
	if (p->n_tokens < 2) return;
	unsigned int address = number(p->tokens[1]);
	console_out("peekw %04Xh\n", address);
	console_out("%04Xh", peek_fastw((unsigned short)address));
}

static void con_poke(PARMS *p)
{
	if (p->n_tokens < 3) return;
	unsigned short address = (unsigned short)number(p->tokens[1]);
	unsigned char value = (unsigned char)number(p->tokens[2]);
	console_out("poke %04Xh,%02Xh\n", address, value);
	poke_fast(address, value);
}

static void con_pokew(PARMS *p)
{
	if (p->n_tokens < 3) return;
	unsigned short address = (unsigned short)number(p->tokens[1]);
	unsigned short value = (unsigned short)number(p->tokens[2]);
	console_out("pokew %04Xh,%04Xh\n", address, value);
	poke_fastw(address, value);
}

static void con_memory(PARMS *p)
{
	char memory[1000];
	char *out = memory;
	const unsigned short lines = 2;

	if (p->n_tokens >= 2) console_state.mem = (unsigned short)number(p->tokens[1]);
	for (int y = console_state.mem; y < console_state.mem + 16*lines; y += 16)
	{
		out += sprintf_s(out, 100, "%04X  ", y);
		for (int i = 0; i < 8; i++)
			out += sprintf_s(out, 100, "%02X ", peek_fast((unsigned short)(y + i)));
		*out++ = ' ';
		for (int i = 0; i < 8; i++)
			out += sprintf_s(out, 100, "%02X ", peek_fast((unsigned short)(y + 8 + i)));
		*out++ = ' ';
		for (int i = 0; i < 16; i++)
		{
			unsigned char c = peek_fast((unsigned short)(y + i));
			*out++ = (c < 32 || c > 127) ? '.' : c;
		}
		*out++ = '\r';
		*out++ = '\n';
	}
	*out++ = '\0';
	console_state.mem += 16*lines;
	console_out(memory);
}

static void con_reset(PARMS *p)
{
	p;
	reset_machine();
}

typedef struct
{
	char *cmd;
	char *longcmd;
	void (*fn)(PARMS *p);
	char *usage;
} CONSOLE_CMD;

static void con_help(PARMS *);

static CONSOLE_CMD commands[] =
{
	{"db", "defb", con_define_byte, "define byte: db address [,size]"},
	{"dw", "defw", con_define_word, "define word: dw address [,size]"},
	{"c", "defc", con_define_code, "define code: c address [,size]"},
	{"l", "label", con_label, "define a label: l address,name"},
	{"r", "run", con_run, "go"},
	{"o", "out", con_out, "step out"},
	{"n", "over", con_step, "step over"},
	{"s", "into", con_stepin, "step into"},
	{"q", "quit", con_close, "close console"},
	{"x", "break", con_stop, "break"},
	{"m", "mem", con_memory, "dump memory: m address"},
	{"b", "bpt", con_breakpoint, "define breakpoint: b address"},
	{"a", "asm", con_assemble, "enter assembly mode: a address"},
	{"d", "dasm", con_disassemble, "disassembly: d [address]"},
	{"pw", "pokew", con_pokew, "poke word: pw address,value"},
	{"p", "poke", con_poke, "poke byte: p address,value"},
	{"kw", "peekw", con_peekw, "peek word: kw address"},
	{"k", "poke", con_peek, "peek byte: k address"},
	{"rx", "reset", con_reset, "reset machine: rx"},
	{"?", "help", con_help, "help"},
	{NULL, NULL}
};

static void con_help(PARMS *p)
{
	p;
	CONSOLE_CMD *c = commands;

	console_out_start();
	while (c->cmd)
	{
		char help[1000];
		sprintf_s(help, 1000, "%-2s - %s\r\n", c->cmd, c->usage);
		console_out_bulk(help);
		c++;
	}
	console_out_bulk("anything else is treated as an expression and evaluated\r\n");
	console_out_end();
}

//---------------------------------------------------------------------------------------
typedef struct
{
	char command[1000];
} HISTORY;
#define MAX_HISTORIES 64
static HISTORY history[MAX_HISTORIES];
static unsigned int history_write = 0;
static unsigned int history_read = 0;

static void init_history(void) { memset(history, 0, sizeof history); history_write = 0; history_read = 0; }

static void dump_history(void)
{
	elog("H\n");
	for (int i = 0; i < MAX_HISTORIES; i++)
		elog("%2d %s (%d)\n", i, history[i].command, strlen(history[i].command));
	elog("count %d, read %d\n", history_write, history_read);
}

static void add_history(char *cmd)
{
	if (cmd != NULL)
	{
		for (unsigned int i = 0; i < strlen(cmd); i++)
			elog("%02X ", cmd[i]);
		elog("\n");
		int s = (int)strlen(cmd) - 1;
		while (s>=0 && isspace((unsigned char)cmd[s])) s--;
		cmd[s+1] = '\0';
		if (strlen(cmd) > 0)
		{
			strcpy_s(history[history_write].command, 1000, cmd);
			history_read = history_write;
			history_write++;
			history_write %= MAX_HISTORIES;
		}
		dump_history();
	}
}

static char *history_up()
{
	do
	{
		history_read = (history_read - 1) % MAX_HISTORIES;
		if (history[history_read].command[0] != '\0')
		{
			char *cmd = history[history_read].command;
			elog("Xread %d\n", history_read);
			return cmd;
		}
	} while (history_read != history_write);

	history_read = (history_read - 1) % MAX_HISTORIES;
	elog("Yread %d\n", history_read);
	if (history[history_read].command[0] != '\0')
		return history[history_read].command;
	return NULL;
}

static char *history_down()
{
	do
	{
		history_read = (history_read + 1) % MAX_HISTORIES;
		if (history[history_read].command[0] != '\0')
		{
			char *cmd = history[history_read].command;
			elog("xread %d\n", history_read);
			return cmd;
		}
		else
		{

		}
	} while (history_read != history_write);
	
	//history_read = (history_read - 1) % MAX_HISTORIES;
	elog("yread %d\n", history_read);
	if (history[history_read].command[0] != '\0')
		return history[history_read].command;
	return NULL;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void exec_console_command(char *cmdx)
{
	PARMS p;

	strcpy_s(p.cmd, 1000, cmdx);
	
	add_history(cmdx);

	const char *sep = ",\t\r\n ";
	char *context = NULL;
	char *token = smart_strtok_s(p.cmd, sep, &context);
	p.n_tokens = 0;
	while (token)
	{
		sep = ",\t\r\n";
		p.tokens[p.n_tokens++] = token;
		token = smart_strtok_s(NULL, sep, &context);
	}
	p.tokens[p.n_tokens] = NULL;

	if (p.n_tokens)
	{
		char found = 0;
		CONSOLE_CMD *command = commands;
		while (command->cmd != NULL)
		{
			if (_stricmp(command->cmd, p.tokens[0]) == 0 || _stricmp(command->longcmd, p.tokens[0]) == 0)
			{
				command->fn(&p);
				update_display();
				found = 1;
				break;
			}
			command++;
		}
		if (!found)
		{
			int value;
			if (expr_eval(cmdx, 0, &value, get_db_labels()))
				console_out("%d %04X\r\n", value, value);
			else
				console_out("unknown command\r\n");
		}
		SetFocus(console_dialog);
	}
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static WNDPROC old_console_richedit_proc;
//---------------------------------------------------------------------------------------
#define CONSOLE_PROMPT "> "

LRESULT CALLBACK ConsoleRichEditDialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT result;

	switch (uMsg)
	{
		case WM_INITDIALOG:
			
			break;

		case WM_ERASEBKGND:
			return 1;

		case WM_KEYDOWN:
		{
			char editing = SendMessage(hwndDlg, EM_LINEFROMCHAR, (WPARAM)-1, 0) == SendMessage(hwndDlg, EM_GETLINECOUNT, 0, 0) - 1;
			
			//pressed a key, but not on the editing (last) line? move the cursor to the end.
			if (!editing || wParam == VK_RETURN)
			{
				GETTEXTLENGTHEX gtlex = { GTL_DEFAULT, CP_ACP };
				CHARRANGE range;
				range.cpMin = (LONG)SendMessage(hwndDlg, EM_GETTEXTLENGTHEX, (WPARAM)&gtlex, 0);
				range.cpMax = range.cpMin;
				SendMessage(hwndDlg, EM_EXSETSEL, 0, (LPARAM)&range);
			}

			switch (wParam)
			{
				case VK_BACK:
				case VK_DELETE:
					if (editing)
					{
						CHARRANGE cr;
						SendMessage(hwndDlg, EM_EXGETSEL, 0, (LPARAM)&cr);
						int lineidx = (int)SendMessage(hwndDlg, EM_LINEINDEX, (WPARAM)-1, 0);
						if (cr.cpMax - lineidx <= (int)strlen(CONSOLE_PROMPT)) return 0;
					}
					break;

				case VK_UP:
					{
						char *cmd = history_up();
						if (cmd != NULL)
						{
							GETTEXTLENGTHEX gtlex = { GTL_DEFAULT, CP_ACP };
							CHARRANGE range;
							range.cpMin = (LONG)SendMessage(hwndDlg, EM_LINEINDEX, (WPARAM)-1, 0) + (LONG)strlen(CONSOLE_PROMPT);
							range.cpMax = (LONG)SendMessage(hwndDlg, EM_GETTEXTLENGTHEX, (WPARAM)&gtlex, 0);
							SendMessage(hwndDlg, EM_EXSETSEL, 0, (LPARAM)&range);
							SendMessage(hwndDlg, EM_REPLACESEL, TRUE, (LPARAM)cmd);
						}
					}
					return 0;

				case VK_DOWN:
					{
						char *cmd = history_down();
						if (cmd != NULL)
						{
							GETTEXTLENGTHEX gtlex = { GTL_DEFAULT, CP_ACP };
							CHARRANGE range;
							range.cpMin = (LONG)SendMessage(hwndDlg, EM_LINEINDEX, (WPARAM)-1, 0) + (LONG)strlen(CONSOLE_PROMPT);
							range.cpMax = (LONG)SendMessage(hwndDlg, EM_GETTEXTLENGTHEX, (WPARAM)&gtlex, 0);
							SendMessage(hwndDlg, EM_EXSETSEL, 0, (LPARAM)&range);
							SendMessage(hwndDlg, EM_REPLACESEL, TRUE, (LPARAM)cmd);
						}
					}
					return 0;
			}
		}
		break;
	}

	result = CallWindowProc(old_console_richedit_proc, hwndDlg, uMsg, wParam, lParam);

	switch (uMsg)
	{
		case WM_KEYDOWN:
		{
			switch (wParam)
			{
				case VK_RETURN:
					{
						char command[1000];
						*(WORD *)command = 1000;

						int line = (int)SendMessage(hwndDlg, EM_LINEFROMCHAR, (WPARAM)-1, 0) - 1;
						int size = (int)SendMessage(hwndDlg, EM_GETLINE, line, (LPARAM)command);
						command[size] = '\0';
						if (strlen(command) >= strlen(CONSOLE_PROMPT))
							exec_console_command(command + strlen(CONSOLE_PROMPT));

						SendMessage(hwndDlg, EM_REPLACESEL, 0, (LPARAM)CONSOLE_PROMPT);
					}
					break;
			}
		}
	}

	return result;
}

LRESULT CALLBACK ConsoleDialogProc(
	HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
)
{
	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
			{
				CHARFORMAT charfmt;

				charfmt.cbSize = sizeof(CHARFORMAT);
				charfmt.dwMask = CFM_CHARSET | CFM_FACE | CFM_SIZE | CFM_COLOR;
				charfmt.dwEffects = 0;
				charfmt.yHeight = 8 * 20;
				charfmt.yOffset = 0;
				charfmt.crTextColor = RGB(0, 255, 0);
				charfmt.bCharSet = ANSI_CHARSET;
				charfmt.bPitchAndFamily = 0;
				strcpy_s(charfmt.szFaceName, LF_FACESIZE, UI_FONT_NAME);

				SendDlgItemMessage(hwndDlg, IDC_CONSOLEEDIT, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);
				SendDlgItemMessage(hwndDlg, IDC_CONSOLEEDIT, EM_SETBKGNDCOLOR, 0, (LPARAM)RGB(0, 0, 0));

				SendDlgItemMessage(hwndDlg, IDC_CONSOLEEDIT, WM_SETTEXT, 0, (LPARAM)CONSOLE_PROMPT);
				init_history();
			}
			break;
		case WM_CTLCOLORDLG:
			return (LRESULT)GetStockObject(BLACK_BRUSH);

		case WM_SIZING:
			{
				RECT edit;
				RECT parent;
				GetWindowRect(GetDlgItem(hwndDlg, IDC_CONSOLEEDIT), &edit);
				parent = *(RECT *)lParam;

				edit.right = parent.right - 18;
				edit.bottom = parent.bottom - 20;

				SetWindowPos(GetDlgItem(hwndDlg, IDC_CONSOLEEDIT), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
			}
			break;

		case WM_CLOSE:
			set_dialog_status(DLG_CONSOLE, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			break;
	}
	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK SYSVARDialogProc(
	HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
)
{

	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			CHARFORMAT charfmt;

			charfmt.cbSize = sizeof(CHARFORMAT);
			charfmt.dwMask = CFM_CHARSET | CFM_FACE | CFM_SIZE;
			charfmt.dwEffects = 0;
			charfmt.yHeight = 8 * 20;
			charfmt.yOffset = 0;
			charfmt.crTextColor = 0;
			charfmt.bCharSet = ANSI_CHARSET;
			charfmt.bPitchAndFamily = 0;
			strcpy_s(charfmt.szFaceName, LF_FACESIZE, UI_FONT_NAME);

			SendDlgItemMessage(hwndDlg, IDC_SYSVAREDIT, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);

			SendDlgItemMessage(hwndDlg, IDC_SYSVAREDIT, WM_SETTEXT, 0, (LPARAM)"");
		}
		return 0;

		case WM_SIZING:
		{
			RECT edit;
			RECT parent;
			GetWindowRect(GetDlgItem(hwndDlg, IDC_SYSVAREDIT), &edit);
			parent = *(RECT *)lParam;

			edit.right = parent.right - 18;
			edit.bottom = parent.bottom - 20;

			SetWindowPos(GetDlgItem(hwndDlg, IDC_SYSVAREDIT), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
		}
		return 0;

		case WM_CLOSE:
			set_dialog_status(DLG_SYSVAR, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			return 0;
	}
	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
/*
LRESULT CALLBACK GfxDialogProc(
	HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
)
{

	lParam; wParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			CHARFORMAT charfmt;

			charfmt.cbSize = sizeof(CHARFORMAT);
			charfmt.dwMask = CFM_CHARSET | CFM_FACE | CFM_SIZE;
			charfmt.dwEffects = 0;
			charfmt.yHeight = 8 * 20;
			charfmt.yOffset = 0;
			charfmt.crTextColor = 0;
			charfmt.bCharSet = ANSI_CHARSET;
			charfmt.bPitchAndFamily = 0;
			strcpy_s(charfmt.szFaceName, LF_FACESIZE, UI_FONT_NAME);

			SendDlgItemMessage(hwndDlg, IDC_SYSVAREDIT, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);

			SendDlgItemMessage(hwndDlg, IDC_SYSVAREDIT, WM_SETTEXT, 0, (LPARAM)"");
		}
		return 0;

		case WM_SIZING:
		{
			RECT edit;
			RECT parent;
			GetWindowRect(GetDlgItem(hwndDlg, IDC_SYSVAREDIT), &edit);
			parent = *(RECT *)lParam;

			edit.right = parent.right - 18;
			edit.bottom = parent.bottom - 20;

			SetWindowPos(GetDlgItem(hwndDlg, IDC_SYSVAREDIT), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
		}
		return 0;

		case WM_CLOSE:
			set_dialog_status(DLG_ZXGFX, 0);
			ShowWindow(hwndDlg, SW_HIDE);
			return 0;
	}
	return 0;
}
*/

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static int is_returning(void)
{
	//need to know if this is a ret instruction where the condition code is met
	REG_PACK *regs = get_regs();

	//need to know if we are at the same stack depth as the original stepout request
	if (regs->sp < stepout_sp) return 0;

	//C0 ret nz
	//C8 ret z
	//C9 ret
	//D0 ret nc
	//D8 ret c
	//E0 ret po
	//E8 ret pe
	//F0 ret p
	//F8 ret m
	//ED 4D reti
	//ED 45 retn

	unsigned char ret = peek_fast(regs->pc);
	if (ret == 0xC9) return 1;

	if ((ret & 0xC7) == 0xC0)
	{
		if (ret == 0xC0 && !(regs->b.f & FLAGS_ZF)) return 1;
		if (ret == 0xC8 && (regs->b.f & FLAGS_ZF)) return 1;
		if (ret == 0xD0 && !(regs->b.f & FLAGS_CF)) return 1;
		if (ret == 0xD8 && (regs->b.f & FLAGS_CF)) return 1;
		if (ret == 0xE0 && !(regs->b.f & FLAGS_PF)) return 1;
		if (ret == 0xE8 && (regs->b.f & FLAGS_PF)) return 1;
		if (ret == 0xF0 && !(regs->b.f & FLAGS_SF)) return 1;
		if (ret == 0xF8 && (regs->b.f & FLAGS_SF)) return 1;
		return 0;
	}
	if (ret == 0xED)
	{
		ret = peek_fast(regs->pc + 1);
		return ret == 0x4D || ret == 0x45;
	}

	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK BreakPointProc(
  HWND hwndDlg,  // handle to dialog box
  UINT uMsg,     // message
  WPARAM wParam, // first message parameter
  LPARAM lParam  // second message parameter
  )
{
	char txt[18];
	lParam; wParam;
	switch (uMsg)
	{
		case WM_INITDIALOG:
			{
			int z;
			BREAKPOINT *breakpoints;
			int n_breakpoints = get_breakpoints(&breakpoints);
			SendDlgItemMessage(hwndDlg, IDC_BPLIST, LB_RESETCONTENT, 0,0);
			for (z = 0; z < n_breakpoints; z++)
			{
				if (breakpoints[z].flags & BP_ACTIVE)
					sprintf_s(txt, 18, "Active   0x%04X", breakpoints[z].pc);
				else
					sprintf_s(txt, 18, "Inactive 0x%04X", breakpoints[z].pc);
				SendDlgItemMessage(hwndDlg, IDC_BPLIST, LB_ADDSTRING, 0, (LPARAM)txt);
			}
			}
			return 0;
			break;

		case WM_NOTIFY:
			break;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDCANCEL:
					EndDialog(hwndDlg, 0);
					return 1;

				case IDC_DELETE:
					{
					int i;
					i = (int)SendDlgItemMessage(hwndDlg, IDC_BPLIST, LB_GETCURSEL,0,0);
					SendDlgItemMessage(hwndDlg, IDC_BPLIST, LB_DELETESTRING, i,0);
					if (i-1 >= 0)
						SendDlgItemMessage(hwndDlg, IDC_BPLIST, LB_SETCURSEL, i-1, 0);
					remove_breakpoint(i);
					}
					break;

				case IDC_NEW:
					{
					int l;
					char *buffer;
					l=(int)SendDlgItemMessage(hwndDlg, IDC_BPADDRESS, WM_GETTEXTLENGTH, 0,0);
					buffer = malloc(l+1);
					assert(buffer);
					SendDlgItemMessage(hwndDlg, IDC_BPADDRESS, WM_GETTEXT, l+1, (LPARAM)buffer);
					set_breakpoint((unsigned short)strtoul(buffer, NULL, 16));
					BREAKPOINT *breakpoints;
					int n_breakpoints = get_breakpoints(&breakpoints);
					sprintf_s(txt, 18, "Active   0x%04X", breakpoints[n_breakpoints-1].pc);
					SendDlgItemMessage(hwndDlg, IDC_BPLIST, LB_ADDSTRING, 0, (LPARAM)txt);
					free(buffer);
					}
					return 1;
			}
			break;
	}
return 0;
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------
static WNDPROC old_richedit_drop_proc;
//----------------------------------------------------------------------------------------------------------
LRESULT CALLBACK RichEditDialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT result;

	switch (uMsg)
	{
		case WM_ERASEBKGND:
			//richedit control background
			//if (!custom_richedit_background)
			//	break;
			return 1;

//		case WM_CUT:
//		case WM_COPY:
//		case WM_PASTE:
//			return 0;
//			break;

	}

	result = CallWindowProc(old_richedit_drop_proc, hwndDlg, uMsg, wParam, lParam);

	switch (uMsg)
	{
		case WM_KEYDOWN:
			switch (wParam)
			{
				case 'N':
					DialogBox(dis_instance, (LPCTSTR)IDD_LABELNAME, hwndDlg, (DLGPROC)LabelDialogProc);
					break;

				case 'D':
					make_current_line_data();
					update_display();
					break;

				case 'C':
					make_current_line_code();
					update_display();
					break;

				case 'G':
					DialogBox(dis_instance, (LPCTSTR)IDD_GOTOADDRESS, hwndDlg, (DLGPROC)AddressDialogProc);
					break;

				case VK_F5://continue
					go();
					break;

				case VK_F9://breakpoint
					{
						int line = get_current_line();

						if (toggle_breakpoint(get_pc_at_line(line)) == 0)
							set_breakpoint(get_pc_at_line(line));
					}
					update_display();
					break;

				case VK_F10://step over
					stepover();
					break;

				case VK_F11://step into (shift F11 - step out)
					if (GetAsyncKeyState(VK_LSHIFT))
						stepout();
					else
						stepinto();
					break;

			}
			break;
		/*
		case WM_KEYDOWN:
			switch (wParam)
			{
				case VK_DELETE:
					colour_it_fast(TRUE);
					break;
				default:
					map_fn_keys(wParam);
			}
			break;

		case WM_CHAR:
			colour_it_fast(TRUE);
			break;
		*/
		case WM_RBUTTONDOWN:
			{
			//select the line where the right mouse was clicked
			SendMessage(hwndDlg, WM_LBUTTONDOWN, MK_LBUTTON, lParam);

			POINT p;
			p.x = GET_X_LPARAM(lParam);
			p.y = GET_Y_LPARAM(lParam);
			ClientToScreen(hwndDlg, &p);
			TrackPopupMenu(GetSubMenu(popup_menu,0), TPM_CENTERALIGN|TPM_TOPALIGN|TPM_RIGHTBUTTON, p.x, p.y, 0, dis_window, NULL);
			}
			break;

		case WM_VSCROLL:
			if (lParam == 0)
			{
				SCROLLINFO si = { 0 };
				si.cbSize = sizeof si;
				si.fMask = SIF_ALL;
				GetScrollInfo(hwndDlg, SB_VERT, &si);

				switch (LOWORD(wParam))
				{
					case SB_TOP: si.nPos = si.nMin; break;
					case SB_BOTTOM: si.nPos = si.nMax; break;
					case SB_LINEUP: si.nPos--; break;
					case SB_LINEDOWN: si.nPos++; break;
					case SB_PAGEUP: si.nPos -= si.nPage; break;
					case SB_PAGEDOWN: si.nPos += si.nPage; break;
					case SB_THUMBTRACK: si.nPos = si.nTrackPos; break;
					case SB_ENDSCROLL:
					case SB_THUMBPOSITION: return 0;
					default:
						assert(0);
				}
				si.fMask = SIF_POS;
				SetScrollInfo(hwndDlg, SB_VERT, &si, TRUE);
				GetScrollInfo(hwndDlg, SB_VERT, &si);
				return 0;
			}
			break;
	}

	return result;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK DisDialogProc(
  HWND hwndDlg,  // handle to dialog box
  UINT uMsg,     // message
  WPARAM wParam, // first message parameter
  LPARAM lParam  // second message parameter
  )
{

	lParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
			{
			CHARFORMAT charfmt;

			charfmt.cbSize = sizeof(CHARFORMAT);
			charfmt.dwMask = CFM_CHARSET|CFM_FACE|CFM_SIZE;
			charfmt.dwEffects = 0;
			charfmt.yHeight = 10*20;
			charfmt.yOffset = 0;
			charfmt.crTextColor = 0;
			charfmt.bCharSet = ANSI_CHARSET;
			charfmt.bPitchAndFamily = 0;
			strcpy_s(charfmt.szFaceName, LF_FACESIZE, UI_FONT_NAME);

			SendDlgItemMessage(hwndDlg, IDC_LISTING, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);

			SendDlgItemMessage(hwndDlg, IDC_LISTING, WM_SETTEXT, 0, (LPARAM)"");
			SendDlgItemMessage(hwndDlg, IDC_LISTING, EM_EXLIMITTEXT, 0, 65536*256);

			charfmt.yHeight = 8 * 20;
			SendDlgItemMessage(hwndDlg, IDC_LABELS, EM_SETCHARFORMAT, (WPARAM)SCF_ALL, (LPARAM)&charfmt);

			disview_get_labels();
			}
			return 0;

		case WM_PAINT:
			{
			PAINTSTRUCT paint={0};
			int e;
			paint.hdc = (HDC)wParam;
			e;
			if (GetUpdateRect(hwndDlg, &paint.rcPaint,TRUE))
			{
				BeginPaint(hwndDlg, &paint);
//				e=FillRect(paint.hdc, &paint.rcPaint, background);
				EndPaint(hwndDlg, &paint);
			}
			}
			break;

		case WM_COMMAND:
			//button message
			switch (LOWORD(wParam))
			{
				case IDC_LABELS:
				{
					if (HIWORD(wParam) == LBN_DBLCLK)
					{
						char label[100];
						int sel = (int)SendDlgItemMessage(hwndDlg, IDC_LABELS, LB_GETCURSEL, 0, 0);
						SendDlgItemMessage(hwndDlg, IDC_LABELS, LB_GETTEXT, sel, (LPARAM)label);
						disview_skip_to_label(label);
					}
				}
				break;
			}
			break;

		//case WM_NOTIFY:
		//{
		//	if (wParam == IDC_LISTING)
		//	{
		//		NMHDR *nmhdr = (NMHDR *)lParam;

		//		if (nmhdr->code == EN_LINK)
		//		{
		//			ENLINK *enlink = (ENLINK *)lParam;
		//			TEXTRANGE range = { 0 };
		//			range.chrg = enlink->chrg;
		//			char txt[1000];
		//			range.lpstrText = txt;
		//			SendDlgItemMessage(hwndDlg, IDC_LISTING, EM_GETTEXTRANGE, 0, (LPARAM)&range);
		//			elog("linky %s\n", txt);

		//			if (enlink->msg == WM_LBUTTONDOWN)
		//			{
		//				elog("clicky!\n");
		//			}

		//			elog("%u\n", nmhdr->code);
		//		}
		//	}
		//	break;
		//}

		case WM_SIZING:
			{
				RECT edit;
				RECT parent;
				GetWindowRect(GetDlgItem(hwndDlg, IDC_LISTING), &edit);
				parent = *(RECT *)lParam;

				RECT ctl;
				GetWindowRect(disctl_dialog, &ctl);

				edit.right = parent.right - 18 - (ctl.right - ctl.left);
				edit.bottom = parent.bottom - 20 - 14;//20 for some space, extra 14 for status bar

				//extend the dialog
				SetWindowPos(hwndDlg, NULL, 0, 0, (parent.right - parent.left) - (ctl.right - ctl.left) - 10, (parent.bottom - parent.top), SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);

				//extend the list box
				SetWindowPos(GetDlgItem(hwndDlg, IDC_LISTING), NULL, 0, 0, edit.right - edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);

				//extend the labels box
				GetWindowRect(GetDlgItem(hwndDlg, IDC_LABELS), &edit);
				edit.bottom = parent.bottom - 20 - 14;
				SetWindowPos(GetDlgItem(hwndDlg, IDC_LABELS), NULL, 0, 0, edit.right-edit.left, edit.bottom - edit.top, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOREPOSITION | SWP_NOZORDER);
		}
	}

	return 0;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
LRESULT CALLBACK CtlDialogProc(
	HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
)
{

	lParam;

	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			//put the symbols on the tape buttons
			SetDlgItemTextW(hwndDlg, IDC_PLAY_TAPE, L"\x23f5\xfe0e");
			SetDlgItemTextW(hwndDlg, IDC_STOP_TAPE, L"\x23f9\xfe0e");
			SetDlgItemTextW(hwndDlg, IDC_REW_TAPE, L"\x23ea\xfe0e");
			SetDlgItemTextW(hwndDlg, IDC_FF_TAPE, L"\x23e9\xfe0e");
			SetDlgItemTextW(hwndDlg, IDC_RESET_TAPE, L"\x23ee\xfe0e");
		}
		return 0;

		case WM_PAINT:
		{
			PAINTSTRUCT paint = { 0 };
			int e;
			paint.hdc = (HDC)wParam;
			e;
			if (GetUpdateRect(hwndDlg, &paint.rcPaint, TRUE))
			{
				BeginPaint(hwndDlg, &paint);
//				e=FillRect(paint.hdc, &paint.rcPaint, background);
				EndPaint(hwndDlg, &paint);
			}
		}
		break;

		case WM_COMMAND:
			//button message
			switch (LOWORD(wParam))
			{
				case IDOK:
				case IDCANCEL:
					quit = 1;
					break;

				case IDC_GO:
					go();
					break;

				case IDC_STOP:
					stop();
					break;

				case IDC_STEPOVER:
					stepover();
					break;

				case IDC_STEPINTO:
					stepinto();
					break;

				case IDC_STEPOUT:
					stepout();
					break;

				case IDC_BREAKPOINT:
					DialogBox(dis_instance, (LPCTSTR)IDD_BREAKPOINTS, hwndDlg, (DLGPROC)BreakPointProc);
					break;

				case IDC_RESTART:
					reset_machine();
					update_display();
					break;

				case IDC_TOGGLE_MEM:
					toggle_dialog(mem_dialog, DLG_MEM, -1);
					break;

				case IDC_TOGGLE_REG:
					toggle_dialog(reg_dialog, DLG_REG, -1);
					break;

				case IDC_TOGGLE_STACK:
					toggle_dialog(stack_dialog, DLG_STK, -1);
					break;

				case IDC_TOGGLE_FPSTACK:
					toggle_dialog(fpu_dialog, DLG_FPU, -1);
					break;

				case IDC_TOGGLE_BASIC:
					toggle_dialog(basic_dialog, DLG_BASIC, -1);
					break;

				case IDC_TOGGLE_SYSVAR:
					toggle_dialog(sysvar_dialog, DLG_SYSVAR, -1);
					break;

				case IDC_TOGGLE_COVERAGE:
					toggle_dialog(coverage_window, DLG_COV, -1);
					break;

				case IDC_TOGGLE_STATS:
					toggle_dialog(stats_dialog, DLG_STATS, -1);
					break;

				case IDC_TOGGLE_GFX:
					toggle_dialog(zxgfx_dialog, DLG_ZXGFX, -1);
					break;

				case IDC_TOGGLE_TRACE:
					toggle_dialog(pctrace_dialog, DLG_PCT, -1);
					break;

				case IDC_TOGGLE_CONSOLE:
					toggle_dialog(console_dialog, DLG_CONSOLE, -1);
					break;

				case IDC_TOGGLE_POKES:
					toggle_dialog(pokes_dialog, DLG_POKES, -1);
					break;

				case IDC_RESET_COVERAGE:
					reset_coverage();
					update_display();
					break;

				case IDC_APPLY_COVERAGE:
					set_wait_cursor();
					disview_apply_coverage();
					unset_wait_cursor();
					update_display();
					break;

				case IDC_PLAY_TAPE:
					machine_play_tape(1);
					break;
				case IDC_STOP_TAPE:
					machine_play_tape(0);
					break;
				case IDC_FF_TAPE:
					ff_tape();
					break;
				case IDC_REW_TAPE:
					rew_tape();
					break;
				case IDC_RESET_TAPE:
					reset_tape();
					break;

				case IDC_PAGE_ROM:
					machine_page_rom();
					break;

				case IDC_DISASSEMBLE:
					//disassemble_plain();
					disassemble_advanced();
					disview_skip_to_pc();
					break;

				case IDC_SOUND:
					toggle_sound();
					update_status_bar();
					break;

				case IDC_FASTSCREEN:
					toggle_fastscreen();
					update_status_bar();
					{

					}
					break;

				case IDC_AUTOUPDATE:
					auto_update ^= 1;
					update_status_bar();
					break;
			}
			break;

		case WM_SIZING:
			{
				RECT parent = *(RECT *)lParam;
				RECT dlgrect;
				GetWindowRect(hwndDlg, &dlgrect);
				SetWindowPos(hwndDlg, NULL, (parent.right - parent.left) - (dlgrect.right - dlgrect.left) - 18, 0, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
			}
			break;
	}

	return 0;
}

extern int code_stats[7][256];
extern MK_TYPE coverage[65536];
extern const char * const *mnemtab[7];

extern unsigned short pc_track[];
extern int pc_track_pos;



static char *peek_N(unsigned short a, int count)
{
	static char m8[1000];
	char *t = m8;
	for (int i = 0; i < count; i++)
		t += sprintf_s(t, m8-t+1000, "%02X ", peek_fast((unsigned short)(a + i)));
	*t++ = ' ';
	for (int i = 0; i < count; i++)
	{
		unsigned char p = peek_fast((unsigned short)(a + i));
		t += sprintf_s(t, m8 - t + 1000, "%c", (p>=32 && p<=127)?p:'.');
	}
	return m8;
}

void update_regs_view(void)
{
	//registers window
	REG_PACK *regs = get_regs();
	static char regs_text[1024];

	/*
	sprintf_s(regs_text, 1024,
		"BC %04X BC' %04X\x0d\x0a"
		"DE %04X DE' %04X\x0d\x0a"
		"HL %04X HL' %04X\x0d\x0a"
		"AF %04X AF' %04X\x0d\x0a"
		"IX %04X  IY %04X\x0d\x0a"
		"SP %04X  PC %04X\x0d\x0a"
		"I %02X  R %02X R7 %d\x0d\x0a"
		"IM %d IFF1 %d IFF2 %d MEMPTR %04X\x0d\x0a"
		"S Z 5 H 3 V N C\x0d\x0a"
		"%d %d %d %d %d %d %d %d\x0d\x0a",

		regs->w.bc, regs->exregs.bc,
		regs->w.de, regs->exregs.de,
		regs->w.hl, regs->exregs.hl,
		regs->w.af, regs->exregs.af,
		regs->ix[0].i, regs->ix[1].i,
		regs->sp, regs->pc,
		regs->i, regs->r, !!(regs->r7),
		regs->im, regs->iff1, regs->iff2, regs->memptr,
		!!(regs->b.f & FLAGS_SF),
		!!(regs->b.f & FLAGS_ZF),
		!!(regs->b.f & FLAGS_YF),
		!!(regs->b.f & FLAGS_HF),
		!!(regs->b.f & FLAGS_XF),
		!!(regs->b.f & FLAGS_PF),
		!!(regs->b.f & FLAGS_NF),
		!!(regs->b.f & FLAGS_CF)
	);
	*/

	char *t = regs_text;
	t += sprintf_s(t, regs_text - t + 1024, "BC  %04x %5d %s\r\n", regs->w.bc, regs->w.bc, peek_N(regs->w.bc, 8));
	t += sprintf_s(t, regs_text - t + 1024, "DE  %04x %5d %s\r\n", regs->w.de, regs->w.de, peek_N(regs->w.de, 8));
	t += sprintf_s(t, regs_text - t + 1024, "HL  %04x %5d %s\r\n", regs->w.hl, regs->w.hl, peek_N(regs->w.hl, 8));
	t += sprintf_s(t, regs_text - t + 1024, "AF  %04x %5d %s\r\n", regs->w.af, regs->w.af, peek_N(regs->w.af, 8));
	t += sprintf_s(t, regs_text - t + 1024, "BC' %04x %5d %s\r\n", regs->exregs.bc, regs->exregs.bc, peek_N(regs->exregs.bc, 8));
	t += sprintf_s(t, regs_text - t + 1024, "DE' %04x %5d %s\r\n", regs->exregs.de, regs->exregs.de, peek_N(regs->exregs.de, 8));
	t += sprintf_s(t, regs_text - t + 1024, "HL' %04x %5d %s\r\n", regs->exregs.hl, regs->exregs.hl, peek_N(regs->exregs.hl, 8));
	t += sprintf_s(t, regs_text - t + 1024, "AF' %04x %5d %s\r\n", regs->exregs.af, regs->exregs.af, peek_N(regs->exregs.af, 8));
	t += sprintf_s(t, regs_text - t + 1024, "IX  %04x %5d %s\r\n", regs->ix[0].i, regs->ix[0].i, peek_N(regs->ix[0].i, 8));
	t += sprintf_s(t, regs_text - t + 1024, "IY  %04x %5d %s\r\n", regs->ix[1].i, regs->ix[1].i, peek_N(regs->ix[1].i, 8));
	t += sprintf_s(t, regs_text - t + 1024, "SP  %04x %5d %s\r\n", regs->sp, regs->sp, peek_N(regs->sp, 8));
	t += sprintf_s(t, regs_text - t + 1024, "PC  %04x %5d %s\r\n", regs->pc, regs->pc, peek_N(regs->pc, 8));
	t += sprintf_s(t, regs_text -t + 1024,
	"I %02X  R %02X R7 %d\x0d\x0a"
		"IM %d IFF1 %d IFF2 %d MEMPTR %04X\x0d\x0a"
		"S Z 5 H 3 V N C\x0d\x0a"
		"%d %d %d %d %d %d %d %d\x0d\x0a", regs->i, regs->r, !!(regs->r7),
		regs->im, regs->iff1, regs->iff2, regs->memptr,
		!!(regs->b.f & FLAGS_SF),
		!!(regs->b.f & FLAGS_ZF),
		!!(regs->b.f & FLAGS_YF),
		!!(regs->b.f & FLAGS_HF),
		!!(regs->b.f & FLAGS_XF),
		!!(regs->b.f & FLAGS_PF),
		!!(regs->b.f & FLAGS_NF),
		!!(regs->b.f & FLAGS_CF)
	);

	SendDlgItemMessage(reg_dialog, IDC_REGISTERS, WM_SETTEXT, 0, (LPARAM)regs_text);
}

static int get_edit_current_line(HWND dlg, int ctrl)
{
	//4 is a fudge factor
	int char_start = (int)SendDlgItemMessage(dlg, ctrl, EM_CHARFROMPOS, 0, 4<<16);
	int line = HIWORD(char_start);
	return line;
}

static void set_edit_current_line(HWND dlg, int ctrl, int line)
{
	SendDlgItemMessage(dlg, ctrl, EM_LINESCROLL, 0, line);
}

void update_memory_view(void)
{
	int line = get_edit_current_line(mem_dialog, IDC_MEMORY);

	//memory window
	char *mem_text = malloc(310000);
	if (mem_text == NULL) return;

	char *out = mem_text;
	for (int y = 0; y < 65536; y += 16)
	{
		out += sprintf_s(out, mem_text-out+310000, "%04X  ", y);
		for (int i = 0; i < 8; i++)
			out += sprintf_s(out, mem_text - out + 310000, "%02X ", peek_fast((unsigned short)(y + i)));
		*out++ = ' ';
		for (int i = 0; i < 8; i++)
			out += sprintf_s(out, mem_text - out + 310000, "%02X ", peek_fast((unsigned short)(y + 8 + i)));
		*out++ = ' ';
		for (int i = 0; i < 16; i++)
		{
			unsigned char c = peek_fast((unsigned short)(y + i));
			*out++ = (c < 32 || c > 127) ? '.' : c;
		}
		*out++ = '\r';
		*out++ = '\n';
	}
	*out++ = '\0';
	SendDlgItemMessage(mem_dialog, IDC_MEMORY, WM_SETTEXT, 0, (LPARAM)mem_text);

	set_edit_current_line(mem_dialog, IDC_MEMORY, line);
	
	free(mem_text);
}

void update_stack_view(void)
{
	//stack window (last 32 stack entries)
	REG_PACK *regs = get_regs();
	unsigned short mem;
	char mem_text[4000];
	char *out = mem_text;
	mem = regs->sp;
	for (int y = 0; y < 32; y++)
	{
		unsigned short value = peek_fastw(mem);
		out += sprintf_s(out, mem_text - out + 4000, "%04X : %04X %5d %s\r\n", mem, value, value, pc_to_label(value,0,0, 0));
		mem += 2;
	}
	SendDlgItemMessage(stack_dialog, IDC_STACK, WM_SETTEXT, 0, (LPARAM)mem_text);
}

void update_fpstack_view(void)
{
	//fp-stack window
	unsigned short mem;
	char mem_text[4000];
	char *out = mem_text;
	mem = peek_fastw(23656);
	for (int y = 0; y < 6; y++)
	{
		unsigned char fp[5];
		for (int i = 0; i < 5; i++)
			fp[i] = peek_fast((unsigned short)(mem + i));
		out += sprintf_s(out, mem_text - out + 4000, "%04X : %02X %02X %02X %02X %02X  %lf\x0d\x0a", mem, fp[0], fp[1], fp[2], fp[3], fp[4], decodeFP(fp));
		mem += 5;
	}
	SendDlgItemMessage(fpu_dialog, IDC_FPSTACK, WM_SETTEXT, 0, (LPARAM)mem_text);
}

void update_disassembly_view(void)
{
	//scroll text window
	disassemble_advanced();
	//disview_skip_to_pc();
}

void update_coverage_view(void)
{
	//coverage bitmap

	static const BITMAPINFO coverage_bmi =
	{
		sizeof(BITMAPINFOHEADER),
		256,
		-256,
		1,
		32,
		BI_RGB,
		0,
		75,
		75,
		0,
		0,
	};
	//MK_UNKN = 0,
	//MK_CODE = 1,
	//MK_DATA_RD = 2,
	//MK_DATA_WR = 4,
	static const unsigned int cols[8] =
	{
		0xffffff,//0
		0x00ff00,//1
		0xff0000,//2
		0xffff00,//3
		0x0000ff,//4
		0x00ffff,//5
		0xff00ff,//6
		0x000000,//7
	};
	static int screen_out[65536];

	for (int x = 0; x < 65536; x++)
		screen_out[x] = cols[coverage[x] & 7];

	HDC hdc = GetDC(coverage_window);
	//SetDIBitsToDevice(hdc, 0, 0, 256, 256, 0, 0, 0, 256, screen_out, &coverage_bmi, DIB_RGB_COLORS);
	RECT rect;
	GetClientRect(coverage_window, &rect);
	StretchDIBits(hdc, 0, 0, rect.right, rect.bottom, 0, 0, 256, 256, screen_out, &coverage_bmi, DIB_RGB_COLORS, SRCCOPY);
	ReleaseDC(coverage_window, hdc);
}

#pragma warning(push)
#pragma warning(disable: 4706)

void update_stats_view(void)
{
	//instruction stats
	char *mem_text = malloc(50000);
	if (mem_text == NULL) return;

	char *out = mem_text;
	for (int y = 0; y < 256; y++)
	{
		for (int x = 0; x < 7; x++)
		{
			char *p;
			char *last;

			last = out;
			//out += sprintf(out, "%-8.8s %4d ", mnemtab[x][y], code_stats[x][y]);
			out += sprintf_s(out, mem_text - out + 50000, "%-*.*s %8d | ", 13, 13, mnemtab[x][y], code_stats[x][y]);

			// % - IX or IY
			// \1 - immediate byte
			// \2 - immediate byte offset
			// \3 - immediate word (load)
			// \4 - immediate word (load relative)
			// \5 - immediate word (call/jump)
			// \6 - immediate byte offset from IX/IY

			if (p = strchr(last, '%')) *p = ((x - 3) & 1) ? 'y' : 'x';
			if (p = strchr(last, '%')) *p = ((x - 3) & 1) ? 'y' : 'x';//can be in twice
			if (p = strchr(last, '\1')) *p = 'N';
			if (p = strchr(last, '\2')) *p = 'N';
			if (p = strchr(last, '\3')) { memmove(p + 1, p, 10); *p = 'N', *(p + 1) = 'N'; }
			if (p = strchr(last, '\4')) { memmove(p + 1, p, 10); *p = 'N', *(p + 1) = 'N'; }
			if (p = strchr(last, '\5')) { memmove(p + 1, p, 10); *p = 'N', *(p + 1) = 'N'; }
			if (p = strchr(last, '\6')) { memmove(p + 1, p, 10); *p = '+', *(p + 1) = 'd'; }
		}
		out -= 3;
		out += sprintf_s(out, mem_text - out + 50000, "\x0d\x0a");
	}
	SendDlgItemMessage(stats_dialog, IDC_STATS, WM_SETTEXT, 0, (LPARAM)mem_text);
	free(mem_text);
}

#pragma warning(pop)

void update_pctrace_view(void)
{
	//pc trace
	REG_PACK *regs = get_regs();
	char mem_text[1000];

	SendDlgItemMessage(pctrace_dialog, IDC_PCTRACE, LB_RESETCONTENT, 0, 0);

	sprintf_s(mem_text, 1000, "%04X %s\x0d\x0a", regs->pc, pc_to_label(regs->pc, 0, 0, 0));
	SendDlgItemMessage(pctrace_dialog, IDC_PCTRACE, LB_ADDSTRING, 0, (LPARAM)mem_text);

	for (int x = pc_track_pos - 1; x >= 0; x--)
	{
		sprintf_s(mem_text, 1000, "%04X %s\x0d\x0a", pc_track[x], pc_to_label(pc_track[x], 0, 0, 0));
		SendDlgItemMessage(pctrace_dialog, IDC_PCTRACE, LB_ADDSTRING, 0, (LPARAM)mem_text);
	}
	for (int x = PC_TRACK_SIZE; x >= pc_track_pos; x--)
	{
		sprintf_s(mem_text, 1000, "%04X %s\x0d\x0a", pc_track[x], pc_to_label(pc_track[x], 0, 0, 0));
		SendDlgItemMessage(pctrace_dialog, IDC_PCTRACE, LB_ADDSTRING, 0, (LPARAM)mem_text);
	}
}

void update_basic_view(void)
{
	char *basic_text = update_basic();
	size_t len = strlen(basic_text);
	int wlen = MultiByteToWideChar(CP_UTF8, 0, basic_text, (int)len, NULL, 0);
	wchar_t *wtxt = malloc((wlen + 1) * sizeof * wtxt);
	MultiByteToWideChar(CP_UTF8, 0, basic_text, (int)len, wtxt, wlen);
	wtxt[wlen] = 0;
	SendDlgItemMessageW(basic_dialog, IDC_BASICEDIT, WM_SETTEXT, 0, (LPARAM)wtxt);
	free(wtxt);
}

void update_sysvars_view(void)
{
	char *sysvar_text = get_sysvars();

	LRESULT dlgLine = SendDlgItemMessage(sysvar_dialog, IDC_SYSVAREDIT, EM_GETFIRSTVISIBLELINE, 0, 0);
	SendDlgItemMessage(sysvar_dialog, IDC_SYSVAREDIT, WM_SETTEXT, 0, (LPARAM)sysvar_text);
	SendDlgItemMessage(sysvar_dialog, IDC_SYSVAREDIT, EM_LINESCROLL, 0, dlgLine);
}

void update_tapeinfo_view(void)
{
	char *tapeinfo = show_tape_info();
	char *line = tapeinfo;
	while (line[0] != '\0')
	{
		SendDlgItemMessage(tapeinfo_dialog, IDC_TAPEINFOLIST, LB_ADDSTRING, 0, (LPARAM)line);
		line += strlen(line) + 1;
	}
	free(tapeinfo);
}

void update_gfx_view(void) {}

void update_console_view(void) {}

void update_pokes_view(void) {}

void update_display(void)
{
	for (int i = 0; i < sizeof dialog_status / sizeof dialog_status[0]; i++)
	{
		if (dialog_status[i].shown)
			dialog_status[i].update();
	}

	update_disassembly_view();

	update_screen();

	return;
}

void update_aux_display(void)
{
	for (int i = 0; i < sizeof dialog_status / sizeof dialog_status[0]; i++)
	{
		if (dialog_status[i].shown)
			dialog_status[i].update();
	}

	//update_disassembly_view();
	//update_screen();
	update_status_bar();

	return;
}

static void update_status_bar(void)
{
	switch (get_screen_mode())
	{
		case FS_SCANLINE: SendMessage(status_window, SB_SETTEXT, 0, (LPARAM)"mode 0 (scanline)"); break;
		case FS_SCREEN: SendMessage(status_window, SB_SETTEXT, 0, (LPARAM)"mode 1 (frame)"); break;
		case FS_IMMEDIATE: SendMessage(status_window, SB_SETTEXT, 0, (LPARAM)"mode 2 (repaint)"); break;
		case FS_OFF: SendMessage(status_window, SB_SETTEXT, 0, (LPARAM)"mode 3 (off)"); break;
		case FS_ULA: SendMessage(status_window, SB_SETTEXT, 0, (LPARAM)"mode 4 (ULA)"); break;
		default: assert(0);
	}
	SendMessage(status_window, SB_SETTEXT, 1, (LPARAM)(is_sound_on() ? "Sound ON" : "Sound OFF"));
	SendMessage(status_window, SB_SETTEXT, 2, (LPARAM)(auto_update ? "Dlg Update ON" : "Dlg Update OFF"));
	ShowWindow(status_window, SW_SHOW);
}

static int was_sound_on=1;
static char was_in_ula_mode = 1;
void switch_to_fast_mode(void)
{
	was_sound_on = is_sound_on();
	if (was_sound_on)
		toggle_sound();
	
	was_in_ula_mode = (get_screen_mode() == FS_ULA);
	if (was_in_ula_mode)
		set_screen_mode(FS_SCANLINE);
	
	update_status_bar();
}

void switch_out_of_fast_mode(void)
{
	if (was_sound_on)
		toggle_sound();

	if (was_in_ula_mode)
		set_screen_mode(FS_ULA);

	was_sound_on = 0;
	was_in_ula_mode = 0;

	update_status_bar();
}

static char working_directory[_MAX_PATH];
char *get_working_folder(void)
{
return working_directory;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void typetest();

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static void invalid_param_handler(wchar_t const *expression, wchar_t const *function, wchar_t const *file, unsigned int line, uintptr_t pReserved)
{
	pReserved;

	OutputDebugString("[PANIC] Invalid Parameter Handler invoked!\n");
	if (expression && wcslen(expression))
	{
		OutputDebugString("expression: ");
		OutputDebugStringW(expression);
		OutputDebugString("\n");
	}
	if (function && wcslen(function))
	{
		OutputDebugString("function:   ");
		OutputDebugStringW(function);
		OutputDebugString("\n");
	}
	if (file && wcslen(file))
	{
		OutputDebugString("file:       ");
		OutputDebugStringW(file);
		OutputDebugString("\n");
	}
	if (line)
	{
		OutputDebugString("line:       ");
		char tmp[12];
#pragma warning(push)
#pragma warning(disable: 4996)
		OutputDebugString(_itoa(line, tmp, 10));
#pragma warning(pop)
		OutputDebugString("\n");
	}
	return;
}

int WINAPI WinMain(
	_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPSTR lpCmdLine,
	_In_ int nCmdShow
)
{
	WNDCLASSEX clas;
	MSG msg;
	int style, ex_style;
	RECT rect, rect2;
	HINSTANCE rich_lib, msft_lib;
	HBITMAP hbmp;
	HRESULT err;

	lpCmdLine;
	hPrevInstance;

	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	_set_invalid_parameter_handler(invalid_param_handler);

	err = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	if (err != S_OK) return 0;

	test_asm();
	test_dasm();

	InitCommonControls();
	
	rich_lib = LoadLibrary("RICHED32.DLL");
	if (rich_lib == NULL) return 0;

	msft_lib = LoadLibrary("Msftedit.dll");
	if (msft_lib == NULL) return 0;

	timeBeginPeriod(1);

	graph_init();

	gfx_init();

	GetCurrentDirectory(_MAX_PATH, working_directory);
	CreateDirectory("databases", NULL);
	daos_init();

	typetest();
	graphtest();

	dis_instance = hInstance;

	clas.cbSize = sizeof(WNDCLASSEX);
	clas.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	clas.lpfnWndProc = WindowProc;
	clas.cbClsExtra = 0;
	clas.cbWndExtra = 0;
	clas.hInstance = hInstance;
	clas.hIcon = LoadIcon(hInstance, (LPCSTR)IDI_ZXICON);
	clas.hCursor = NULL;
	clas.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
	clas.lpszMenuName = NULL;
	clas.lpszClassName = specconv_class;
	clas.hIconSm = 0;
	RegisterClassEx(&clas);

	ex_style=0;
	style = WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX|WS_VISIBLE|WS_SIZEBOX;
	dis_window = CreateWindowEx(ex_style, specconv_class, "Speccy Decompiler", style, 50,50, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, 0);
	main_menu = LoadMenu(hInstance, (LPCTSTR)IDR_MENU1);
	SetMenu(dis_window, main_menu);

	dis_dialog = CreateDialogW(hInstance, (LPCWSTR)IDD_DISDIALOG, dis_window, (DLGPROC)DisDialogProc);
	disctl_dialog = CreateDialogW(hInstance, (LPCWSTR)IDD_DISCONTROLS, dis_window, (DLGPROC)CtlDialogProc);
	GetWindowRect(dis_dialog, &rect);
	GetWindowRect(disctl_dialog, &rect2);
	rect.right += rect2.right - rect2.left;
	AdjustWindowRectEx(&rect, style , TRUE, ex_style);
	SetWindowPos(dis_window, NULL, 0,0, rect.right - rect.left, rect.bottom - rect.top, SWP_NOMOVE|SWP_NOZORDER);
	GetWindowRect(dis_dialog, &rect);
	SetWindowPos(disctl_dialog, NULL, rect.right-rect.left, 0, 0, 0, SWP_NOSIZE|SWP_NOZORDER);

	set_database_caption(NULL);

	clas.lpfnWndProc = EmuWindowProc;
	clas.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);//NULL;
	clas.lpszClassName = emu_class;
	RegisterClassEx(&clas);
	ex_style = WS_EX_TOOLWINDOW /*| WS_EX_COMPOSITED*/;
	style = WS_CAPTION|WS_VISIBLE|WS_SIZEBOX;
	rect.top=rect.left=0;
	rect.right=256+2* PRETTY_BORDER_WIDTH;//border;
	rect.bottom=192+2* PRETTY_BORDER_WIDTH;//border;
	AdjustWindowRectEx(&rect, style , FALSE, ex_style);
	emu_window = CreateWindowEx(ex_style, emu_class, "Emulation", style, CW_USEDEFAULT, CW_USEDEFAULT,rect.right - rect.left, rect.bottom - rect.top, dis_window, NULL, hInstance, 0);

	clas.lpfnWndProc = CoverageWindowProc;
	clas.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);//NULL;
	clas.lpszClassName = coverage_class;
	RegisterClassEx(&clas);
	ex_style = WS_EX_TOOLWINDOW;
	style = WS_CAPTION|WS_VISIBLE|WS_SYSMENU|WS_SIZEBOX;
	rect.top=rect.left=0;
	rect.right=256;
	rect.bottom=256;
	AdjustWindowRectEx(&rect, style , FALSE, ex_style);
	coverage_window = CreateWindowEx(ex_style, coverage_class, "Coverage", style, CW_USEDEFAULT, CW_USEDEFAULT,rect.right - rect.left, rect.bottom - rect.top, dis_window, NULL, hInstance, 0);
	SendMessage(coverage_window, WM_INITDIALOG, 0, 0);

	reg_dialog = CreateDialog(hInstance, (LPCTSTR)IDD_REGISTERS, dis_window, (DLGPROC)RegDialogProc);
	mem_dialog = CreateDialog(hInstance, (LPCTSTR)IDD_MEMORY, dis_window, (DLGPROC)MemDialogProc);
	stats_dialog = CreateDialog(hInstance, (LPCTSTR)IDD_STATS, dis_window, (DLGPROC)StatsDialogProc);
	pctrace_dialog = CreateDialog(hInstance, (LPCTSTR)IDD_PCTRACE, dis_window, (DLGPROC)TraceDialogProc);
	console_dialog = CreateDialog(hInstance, (LPCTSTR)IDD_CONSOLE, dis_window, (DLGPROC)ConsoleDialogProc);

	old_console_richedit_proc = (WNDPROC)GetWindowLongPtr(GetDlgItem(console_dialog, IDC_CONSOLEEDIT), GWLP_WNDPROC);
	SetWindowLongPtr(GetDlgItem(console_dialog, IDC_CONSOLEEDIT), GWLP_WNDPROC, (LONG_PTR)ConsoleRichEditDialogProc);

	old_richedit_drop_proc = (WNDPROC)GetWindowLongPtr(GetDlgItem(dis_dialog, IDC_LISTING), GWLP_WNDPROC);
	SetWindowLongPtr(GetDlgItem(dis_dialog, IDC_LISTING), GWLP_WNDPROC, (LONG_PTR)RichEditDialogProc);

	{
	int parts[5];
	int status_height;
	status_window = CreateStatusWindow(WS_VISIBLE|WS_CHILD, "", dis_window, 0);
	GetWindowRect(status_window, &rect);
	status_height = rect.bottom-rect.top;

	parts[0]=100;
	parts[1]=200;
	parts[2]=300;

	SendMessage(status_window, SB_SETPARTS, 3, (LPARAM)&parts);
	SendMessage(status_window, SB_SETTEXT, 2, (LPARAM)"\tFor Rent\t");
	GetWindowRect(dis_window, &rect);
	MoveWindow(status_window, 0, rect.bottom, 0,0, TRUE);
	}
	update_status_bar();

	stack_dialog = CreateDialog(hInstance, (LPCTSTR)IDD_STACK, dis_window, (DLGPROC)StackDialogProc);
	fpu_dialog = CreateDialog(hInstance, (LPCTSTR)IDD_FPSTACK, dis_window, (DLGPROC)FPStackDialogProc);

	basic_dialog = CreateDialogW(hInstance, (LPCWSTR)IDD_BASIC, dis_window, (DLGPROC)BASICDialogProc);
	sysvar_dialog = CreateDialog(hInstance, (LPCTSTR)IDD_SYSVARS, dis_window, (DLGPROC)SYSVARDialogProc);
	tapeinfo_dialog = CreateDialog(hInstance, (LPCTSTR)IDD_TAPEINFO, dis_window, (DLGPROC)TapeInfoDialogProc);

	hbmp = LoadBitmap(hInstance, (LPCSTR)IDB_BACKGROUND);
	background = CreatePatternBrush(hbmp);
	DeleteObject(hbmp);

	popup_menu = LoadMenu(hInstance, (LPCTSTR)IDR_POPUP);

	ShowWindow(dis_dialog, SW_SHOWNORMAL);
	ShowWindow(disctl_dialog, SW_SHOWNORMAL);
	ShowWindow(dis_window, nCmdShow);
	ShowWindow(emu_window, SW_SHOWNORMAL);
	ShowWindow(coverage_window, SW_SHOWNORMAL);
	ShowWindow(reg_dialog, SW_SHOWNORMAL);
	ShowWindow(mem_dialog, SW_SHOWNORMAL);
	ShowWindow(pctrace_dialog, SW_SHOWNORMAL);

	set_emu_size_scale(emu_window, get_setting_int("screenscale", 2));

	GetWindowRect(dis_window, &rect);
	SetWindowPos(mem_dialog, NULL, rect.right, rect.top, 0,0, SWP_NOSIZE);
	GetWindowRect(mem_dialog, &rect2);
	SetWindowPos(reg_dialog, NULL, rect.right, rect2.bottom, 0,0, SWP_NOSIZE);
	GetWindowRect(dis_window, &rect);
	SetWindowPos(emu_window, NULL, rect.right, rect2.top, 0,0, SWP_NOSIZE);

	set_mru_menu();

	hourglass = LoadCursor(NULL, IDC_WAIT);

	SetTimer(dis_window, 1, 1000, NULL);

//	EMU_STOP,
//	EMU_RUN,
//	EMU_STEP,
//	EMU_STEPINTO,

	ShowWindow(coverage_window, SW_HIDE);
	ShowWindow(reg_dialog, SW_HIDE);
	ShowWindow(mem_dialog, SW_HIDE);
	ShowWindow(pctrace_dialog, SW_HIDE);
	ShowWindow(stats_dialog, SW_HIDE);
	ShowWindow(stack_dialog, SW_HIDE);
	ShowWindow(fpu_dialog, SW_HIDE);

	emulate_init();
	init_sound();
	AY_init();
	init_tape();
	init_machine(SPEC_48K);

	zxgfx_dialog = CreateDialog(hInstance, (LPCTSTR)IDD_GFXDIALOG, dis_window, (DLGPROC)GfxDialogProc);
	ShowWindow(zxgfx_dialog, SW_SHOWNORMAL);
	ShowWindow(zxgfx_dialog, SW_HIDE);

	disassemble_advanced();

	pokes_dialog = CreateDialog(hInstance, (LPCTSTR)IDD_POKES, dis_window, (DLGPROC)PokesDialogProc);
	ShowWindow(pokes_dialog, SW_SHOWNORMAL);
	ShowWindow(pokes_dialog, SW_HIDE);

	go();

	quit = 0;
	do
	{
		if (emulating==EMU_RUN || emulating == EMU_STEPOVER || emulating == EMU_STEPOUT)
		{
			int granularity = 10000;
			while (granularity--)
			{
				if (emulating == EMU_STEPOUT && is_returning())
				{
					emulating = EMU_STEPINTO;
					break;
				}
				emulate_next();
				if (check_breakpoints())
				{
					stop();
					break;
				}
			}
		}
		else if (emulating==EMU_STEPINTO)
		{
			emulate_next();
			stop();
		}

		while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		{
			if (GetMessage(&msg, NULL, 0, 0) <= 0)
			{
				quit = 1;
				break;
			}
		
			if (!IsDialogMessage(dis_dialog, &msg) && !IsDialogMessage(disctl_dialog, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

	} while (!quit);

	KillTimer(dis_window, 1);

	emulate_stop();
	AY_shutdown();
	kill_sound();
	free_tape();
	kill_machine();
	disview_close();
	kill_gfxview();
	graph_shutdown();
	daos_shutdown();
	gfx_shutdown();

	DeleteObject(background);
	timeEndPeriod(1);
	FreeLibrary(rich_lib);
	FreeLibrary(msft_lib);
	CoUninitialize();

	return 0;
}
