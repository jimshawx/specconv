//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
#include <string.h>
#include <float.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>

#include "daos.h"
#include "settings.h"
#include "logger.h"

void get_settings(void)
{
	DB_RES *res = search_settings(NULL);

	ZX_SETTING *setting = (ZX_SETTING *)res->rows;

	while (res->count--)
	{
		if (setting->valuestring)
			elog("%s : %s\n", setting->name, setting->valuestring);
		else
			elog("%s : %d\n", setting->name, setting->valueint);
		setting++;
	}
	free_result(res);
}

int get_setting_int(char *name, int dflt)
{
	int rv;
	DB_RES *res = search_settings(name);
	if (res->count == 1)
	{
		ZX_SETTING *setting = (ZX_SETTING *)res->rows;
		rv = setting->valueint;
	}
	else
	{
		update_setting(name, dflt, NULL);
		rv = dflt;
	}
	free_result(res);
	return dflt;
}

char *get_setting_string(char *name, char *dflt)
{
	static char value[1000];
	value[0] = '\0';

	DB_RES *res = search_settings(name);
	if (res->count == 1)
	{
		ZX_SETTING *setting = (ZX_SETTING *)res->rows;
		if (setting->valuestring) strcpy_s(value, 1000, setting->valuestring);
	}
	else if (dflt)
	{
		update_setting(name, 0, dflt);
		strcpy_s(value, 1000, dflt);
	}
	free_result(res);
	return value;
}

int update_setting(char *name, int valueint, char *valuestring)
{
	DB_RES *res = search_settings(name);
	ZX_SETTING setting;

	if (res->count == 1)
		setting.id = ((ZX_SETTING *)res->rows)[0].id;
	else
		setting.id = 0;

	free_result(res);

	setting.name = name;
	setting.valueint = valueint;
	setting.valuestring = valuestring;

	return save_or_update_setting(&setting);
}

#define MAX_MRU 10

int save_mru(MRU_TYPE type, char *fname)
{
	if (type == MRU_DB && strcmp(fname, "default") == 0)
		return 0;

	DB_RES *res = search_settings(NULL);
	ZX_SETTING *setting = res->rows;

	char *mrutype = type == MRU_SNA ? "mrusna" : (type == MRU_DB ? "mrudbx" : "mrutap");

	ZX_SETTING *mru[MAX_MRU];
	int n_mru = 0;

	while (res->count--)
	{
		if (strncmp(setting->name, mrutype, 6) == 0)
			mru[n_mru++] = setting;
		setting++;
	}

	//value already exists?
	for (int i = 0; i < n_mru; i++)
	{
		if (strcmp(mru[i]->valuestring, fname) == 0)
		{
			save_or_update_setting(mru[i]);
			goto finished;
		}
	}

	//there are spaces?
	if (n_mru < MAX_MRU)
	{
		char sname[100];
		sprintf_s(sname, 100, "%s_%d", mrutype, n_mru);
		ZX_SETTING zx = { 0 };
		zx.name = sname;
		zx.valuestring = fname;
		save_or_update_setting(&zx);
	}
	//overwrite the oldest one
	else
	{
		ZX_SETTING *zx = mru[0];
		for (int i = 1; i < MAX_MRU; i++)
		{
			if (mru[i]->time < zx->time)
				zx = mru[i];
		}
		zx->valuestring = fname;
		save_or_update_setting(zx);
	}

finished:
	free_result(res);

	return 0;
}

static int ct(const void *c0, const void *c1)
{
	ZX_SETTING *s0 = *(ZX_SETTING **)c0;
	ZX_SETTING *s1 = *(ZX_SETTING **)c1;

	if (s0->time == s1->time) return 0;
	if (s0->time < s1->time) return 1;
	return -1;
}

char **search_mru(MRU_TYPE type)
{
	DB_RES *res = search_settings(NULL);
	ZX_SETTING *setting = res->rows;

	char *mrutype = type == MRU_SNA ? "mrusna" : (type == MRU_DB ? "mrudbx" : "mrutap");

	ZX_SETTING *mru[MAX_MRU];
	int n_mru = 0;

	while (res->count--)
	{
		if (strncmp(setting->name, mrutype, 6) == 0)
			mru[n_mru++] = setting;
		setting++;
	}

	qsort(mru, n_mru, sizeof * mru, ct);

	static char names[MAX_MRU][1000];
	static char *nameptr[MAX_MRU+1];
	memset(nameptr, 0, sizeof nameptr);
	for (int i = 0; i < n_mru; i++)
	{
		strcpy_s(names[i], 1000, mru[i]->valuestring);
		nameptr[i] = names[i];
	}

	free_result(res);

	return nameptr;
}