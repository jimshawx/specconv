//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>

#include "sqlite/sqlite3.h"

#include "daos.h"
#include "database.h"
#include "logger.h"
#include "dbgmem.h"
#include "coverage.h"
#include "trace.h"
#include "labels.h"

static sqlite3 *database = NULL;
static int databaseid = -1;

void daos_init(void)
{
	database = db_init();
}

void daos_shutdown()
{
	db_shutdown();
	database = NULL;
}

void bulk_insert_begin(void)
{
	begin(database);
}

void bulk_insert_end(void)
{
	commit(database);
}

DB_RES *search_databases(void)
{
	return search_ZXDB(database);
}

DB_RES *search_labels(void)
{
	return search_ZXLABEL_by_dbid(database, databaseid);
}

int get_label_by_name(char *lname, ZX_LABEL *zx)
{
	DB_RES *res = search_ZXLABEL_by_name(database, databaseid, lname);
	int count = res->count;
	if (count == 1)
	{
		static char name[100];
		*zx = *(ZX_LABEL *)res->rows;
		strcpy_s(name, 100, zx->name);
		zx->name = name;
	}
	free_result(res);
	return count == 1;
}

int get_label_by_address(unsigned int address, ZX_LABEL *zx)
{
	DB_RES *res = search_ZXLABEL_by_address(database, databaseid, address);
	int count = res->count;
	if (count == 1)
	{
		static char name[100];
		*zx = *(ZX_LABEL *)res->rows;
		strcpy_s(name, 100, zx->name);
		zx->name = name;
	}
	free_result(res);
	return count == 1;
}

DB_RES *search_types(void)
{
	return search_ZXTYPE_by_dbid(database, databaseid);
}

DB_RES *search_settings(char *name)
{
	if (name == NULL)
		return search_ZXSETTING(database);

	return search_ZXSETTING_by_name(database, name);
}

int save_label(ZX_LABEL *zx)
{
	zx->dbid = databaseid;
	return save_ZXLABEL(database, zx);
}

void delete_label(ZX_LABEL *label)
{
	delete_ZXLABEL(database, label->id);
}

int update_label(ZX_LABEL *zx)
{
	return update_ZXLABEL(database, zx);
}

int save_type(ZX_TYPE *zx)
{
	zx->dbid = databaseid;
	return save_ZXTYPE(database, zx);
}

int delete_type(ZX_TYPE *zx)
{
	return delete_ZXTYPE(database, zx->id);
}

int update_type(ZX_TYPE *zx)
{
	return update_ZXTYPE(database, zx);
}

int save_or_update_setting(ZX_SETTING *setting)
{
	return save_or_update_ZXSETTING(database, setting);
}

DB_RES *search_types_neighbour(unsigned int address, unsigned int size)
{
	return search_ZXTYPE_neighbours(database, databaseid, address, size);
}

int get_type_by_address(unsigned int address, ZX_TYPE *zx)
{
	DB_RES *res = search_ZXTYPE_by_address(database, databaseid, address);
	int count = res->count;
	if (count == 1)
		*zx = *(ZX_TYPE *)res->rows;
	free_result(res);
	return count == 1;
}

int get_type_at_address(unsigned int address, ZX_TYPE *zx)
{
	DB_RES *res = search_ZXTYPE_at_address(database, databaseid, address);
	int count = res->count;
	if (count == 1)
		*zx = *(ZX_TYPE *)res->rows;
	free_result(res);
	return count == 1;
}

DB_RES *search_types_by_type(ZX_DATA_TYPE type)
{
	return search_ZXTYPE_by_type(database, databaseid, type);
}

int compare_types(ZX_TYPE *t0, ZX_TYPE *t1)
{
	ZX_TYPE z0 = *t0, z1 = *t1;
	z0.id = z1.id = 0;
	z0.time = z1.time = 0.0;
	return memcmp(&z0, &z1, sizeof z1) == 0;
}

int compare_labels(ZX_LABEL *t0, ZX_LABEL *t1)
{
	ZX_LABEL z0 = *t0, z1 = *t1;
	z0.id = z1.id = 0;
	z0.time = z1.time = 0.0;
	return memcmp(&z0, &z1, sizeof z1) == 0;
}

void free_result(DB_RES *res)
{
	db_free(res);
}

int open_db(char *name)
{
	databaseid = get_ZXDB_id_by_name(database, name);

	dedupe();

	return databaseid;
}

int create_db(char *name)
{
	if (strlen(name) + 1 >= MAX_DB_NAME_LEN)
		return -1;

	int existingid = get_ZXDB_id_by_name(database, name);
	if (existingid == -1)
	{
		ZX_DB db;
		db.name = name;
		databaseid = save_ZXDB(database, &db);
		gen_rom_labels();
		return databaseid;
	}
	return -1;
}

int delete_db(char *name)
{
	int deletedbid = get_ZXDB_id_by_name(database, name);
	if (deletedbid == -1)
		return -1;
	return delete_ZXDB(database, deletedbid);
}

void merge_db(char *name)
{
	int mergedbid = get_ZXDB_id_by_name(database, name);
	DB_RES *labels = search_ZXLABEL_by_dbid(database, mergedbid);
	DB_RES *types = search_ZXTYPE_by_dbid(database, mergedbid);
	save_ZXLABEL_batch(database, (ZX_LABEL *)labels->rows, labels->count);
	save_ZXTYPE_batch(database, (ZX_TYPE *)types->rows, types->count);
	free_result(labels);
	free_result(types);
}

void close_db(void)
{
	databaseid = -1;
}

char is_db_open(void)
{
	return databaseid != -1;
}

char *get_db_name()
{
	static char dbname[MAX_DB_NAME_LEN];

	DB_RES *res = get_ZXDB_by_dbid(database, databaseid);
	if (res->count == 1)
	{
		strcpy_s(dbname, MAX_DB_NAME_LEN, ((ZX_DB *)res->rows)[0].name);
		free_result(res);
		return dbname;
	}
	return NULL;
}

// https://github.com/DaveGamble/cJSON
#include "cjson/cjson.h"

int export_db(char *filename)
{
	DB_RES *db = get_ZXDB_by_dbid(database, databaseid);
	if (db->count == 0)
	{
		free_result(db);
		return -1;
	}

	DB_RES *labels = search_labels();
	DB_RES *types = search_types();

	ZX_DB *zxdb = (ZX_DB *)db->rows;

	cJSON *json = cJSON_CreateObject();
	cJSON_AddStringToObject(json, "name", zxdb->name);

	cJSON *jsonlabels = cJSON_AddArrayToObject(json, "labels");
	ZX_LABEL *l = (ZX_LABEL *)labels->rows;
	for (int i = 0; i < labels->count; i++, l++)
	{
		cJSON *label = cJSON_CreateObject();
		cJSON_AddNumberToObject(label, "address", l->address);
		cJSON_AddStringToObject(label, "name", l->name);
		cJSON_AddNumberToObject(label, "type", l->type);
		cJSON_AddNumberToObject(label, "source", l->source);
		cJSON_AddItemToArray(jsonlabels, label);
	}

	cJSON *jsontypes = cJSON_AddArrayToObject(json, "types");
	ZX_TYPE *t = (ZX_TYPE *)types->rows;
	for (int i = 0; i < types->count; i++, t++)
	{
		cJSON *type = cJSON_CreateObject();
		cJSON_AddNumberToObject(type, "address", t->address);
		cJSON_AddNumberToObject(type, "size", t->size);
		cJSON_AddNumberToObject(type, "type", t->type);
		cJSON_AddNumberToObject(type, "source", t->source);
		if (type->type == GFX)
		{
			cJSON_AddNumberToObject(type, "gfxtype", t->gfxtype);
			cJSON_AddNumberToObject(type, "gfxwidth", t->gfxwidth);
			cJSON_AddNumberToObject(type, "gfxheight", t->gfxheight);
		}
		cJSON_AddItemToArray(jsontypes, type);
	}

	free_result(types);
	free_result(labels);
	free_result(db);

	char *string = cJSON_Print(json);
	//elograw(string);

	FILE *f;
	fopen_s(&f, filename, "w");
	if (f != NULL)
	{
		fputs(string, f);
		fclose(f);
	}

	cJSON_Delete(json);

	if (f == NULL)
		return -1;

	return 0;
}

int import_db(char *filename)
{
	FILE *f;
	int rv;

	fopen_s(&f, filename, "rb");
	if (f == NULL)
		return -1;

	char *source;
	long len;

	fseek(f, 0, SEEK_END);
	len = ftell(f);
	source = malloc(len);
	if (!source)
	{
		fclose(f);
		return -1;
	}

	fseek(f, 0, SEEK_SET);
	fread(source, len, 1, f);
	fclose(f);
	
	cJSON *json = cJSON_ParseWithLength(source, len);

	cJSON *jdbname = cJSON_GetObjectItemCaseSensitive(json, "name");
	if (!cJSON_IsString(jdbname) || jdbname->valuestring == NULL)
		return -1;

	begin(database);

	if (create_db(jdbname->valuestring) == -1)
	{
		rollback(database);
		return -1;
	}

	cJSON *labels = cJSON_GetObjectItemCaseSensitive(json, "labels");
	if (cJSON_IsArray(labels))
	{
		cJSON *label;
		cJSON_ArrayForEach(label, labels)
		{
			cJSON *jaddress = cJSON_GetObjectItemCaseSensitive(label, "address");
			cJSON *jname = cJSON_GetObjectItemCaseSensitive(label, "name");
			cJSON *jsource = cJSON_GetObjectItemCaseSensitive(label, "source");
			cJSON *jtype = cJSON_GetObjectItemCaseSensitive(label, "type");
			if (!cJSON_IsNumber(jaddress) ||
				!cJSON_IsString(jname) ||
				!cJSON_IsString(jsource) ||
				!cJSON_IsString(jtype) ||
				jname->valuestring == NULL)
				goto fail;

			ZX_LABEL zx = { 0 };
			zx.address = (unsigned short)jaddress->valueint;
			zx.name = jname->valuestring;
			zx.dbid = databaseid;
			zx.source = (ZX_SOURCE)jsource->valueint;
			zx.type = (ZX_LABELTYPE)jtype->valueint;

			if (save_ZXLABEL(database, &zx) == -1)
				goto fail;
		}
	}
	cJSON *types = cJSON_GetObjectItemCaseSensitive(json, "types");
	if (cJSON_IsArray(types))
	{
		cJSON *type;
		cJSON_ArrayForEach(type, types)
		{
			cJSON *jaddress = cJSON_GetObjectItemCaseSensitive(type, "address");
			cJSON *jsize = cJSON_GetObjectItemCaseSensitive(type, "size");
			cJSON *jtype = cJSON_GetObjectItemCaseSensitive(type, "type");
			cJSON *jsource = cJSON_GetObjectItemCaseSensitive(type, "source");

			if (!cJSON_IsNumber(jaddress) ||
				!cJSON_IsNumber(jsize) ||
				!cJSON_IsNumber(jtype) ||
				!cJSON_IsNumber(jsource))
				goto fail;

			ZX_TYPE zx = { 0 };
			zx.address = (unsigned short)jaddress->valueint;
			zx.size = (unsigned short)jsize->valueint;
			zx.type = (ZX_DATA_TYPE)jtype->valueint;
			zx.source = (ZX_SOURCE)jsource->valueint;
			zx.dbid = databaseid;
			if (zx.type == GFX)
			{
				cJSON *jgfxtype = cJSON_GetObjectItemCaseSensitive(type, "gfxtype");
				cJSON *jgfxwidth = cJSON_GetObjectItemCaseSensitive(type, "gfxwidth");
				cJSON *jgfxheight = cJSON_GetObjectItemCaseSensitive(type, "gfxheight");

				if (!cJSON_IsNumber(jgfxtype) ||
					!cJSON_IsNumber(jgfxwidth) ||
					!cJSON_IsNumber(jgfxheight))
					goto fail;

				zx.gfxtype = (ZX_GFX_TYPE)jgfxtype->valueint;
				zx.gfxwidth = (short)jgfxwidth->valueint;
				zx.gfxheight = (short)jgfxheight->valueint;
			}

			if (save_ZXTYPE(database, &zx) == 01)
				goto fail;
		}
	}
	commit(database);
	rv = 0;
	goto success;

fail:
	rollback(database);
	databaseid = -1;
	rv = -1;

success:

	cJSON_Delete(json);
	free(source);
	
	return rv;
}

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

char *local_time(double julianday)
{
	static char tmp[24];
	union
	{
		unsigned long long u;
		FILETIME t;
	} ft;
	SYSTEMTIME st;

	//2440587.5 is the julian day at 1/1/1970 0:00 UTC. 86400 is seconds in a day.
	//Julian day number 0 assigned to the day starting at noon on Monday, January 1, 4713 BC.
	//FileTime epoch is 1/1/1601, that's 134774.0 days apart from 1/1/1970 (ask Wolfram Alpha).

	double utc = julianday - 2440587.5 + 134774.0;//days since 1/1/1601
	ft.u = (unsigned long long)(utc * 86400.0 * 1000.0 * 1000.0 * 10.0);//100ns since 1/1/1601

	FILETIME ws;
	FileTimeToLocalFileTime(&ft.t, &ws);
	FileTimeToSystemTime(&ws, &st);
	char *t = tmp;
	t += GetDateFormat(LOCALE_USER_DEFAULT, 0, &st, "yyyy-MM-dd", t, (int)(tmp - t + 24)); t--;
	t += GetTimeFormat(LOCALE_USER_DEFAULT, TIME_FORCE24HOURFORMAT, &st, "THH:mm:ss", t, (int)(tmp - t + 24)); t--;
	sprintf_s(t, tmp - t + 24, ".%03d", st.wMilliseconds);
	return tmp;
}
