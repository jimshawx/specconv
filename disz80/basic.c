//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
#include <windows.h>

#include <math.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>

#include "basic.h"
#include "daos.h"
#include "machine.h"
#include "logger.h"
#include "dbgmem.h"
#include "coverage.h"
#include "trace.h"
#include "labels.h"

//---------------------------------------------------------------------------------------
// ZX Spectrum BASIC decoder
//---------------------------------------------------------------------------------------
static char basic[200000];
extern MACHINE machine;

typedef enum
{
	KT_COMMAND=1,
	KT_OPERATOR=2,
	KT_FN=4,
	KT_PRINT=8,
} KEYWORD_TYPE;

typedef struct
{
	const char *const name;
	const char *const program;
	KEYWORD_TYPE type;
} KEYWORD;

//b - bracketed pair
//p - unbracketed pair
//n - numeric
//o - operator
KEYWORD keywords[255 - 165 + 1] = {
	{"RND","", KT_FN},
	{"INKEY$","", KT_FN},
	{"PI","", KT_FN},
	{"FN","", KT_FN},
	{"POINT","b", KT_FN},
	{"SCREEN$","b", KT_FN},
	{"ATTR","b", KT_FN},
	{"AT","p", KT_PRINT},
	{"TAB","n", KT_PRINT},
	{"VAL$","s", KT_FN},
	{"CODE","s", KT_FN},
	{"VAL","s", KT_FN},
	{"LEN","s", KT_FN},
	{"SIN","n", KT_FN},
	{"COS","n", KT_FN},
	{"TAN","n", KT_FN},
	{"ASN","n", KT_FN},
	{"ACS","n", KT_FN},
	{"ATN","n", KT_FN},
	{"LN","n", KT_FN},
	{"EXP","n", KT_FN},
	{"INT","n", KT_FN},
	{"SOR","n", KT_FN},
	{"SGN","n", KT_FN},
	{"ABS","n", KT_FN},
	{"PEEK","p", KT_FN},
	{"IN","n", KT_FN},
	{"USR","n", KT_FN},
	{"STR$","n", KT_FN},
	{"CHR$","n", KT_FN},
	{"NOT","n", KT_FN},
	{"BIN","n", KT_FN},
	{"OR","o", KT_OPERATOR},
	{"AND","o", KT_OPERATOR},
	{"<=","o", KT_OPERATOR},
	{">=","o", KT_OPERATOR},
	{"<>","o", KT_OPERATOR},
	{"LINE","", KT_COMMAND},
	{"THEN","", KT_COMMAND},
	{"TO","", KT_COMMAND},
	{"STEP","", KT_COMMAND},
	{"DEF FN","5", KT_COMMAND},
	{"CAT","t", KT_COMMAND},//modified
	{"FORMAT","U", KT_COMMAND},//modified
	{"MOVE","S", KT_COMMAND},//modified
	{"ERASE","U", KT_COMMAND},//modified
	{"OPEN #","6;U", KT_COMMAND},//modified
	{"CLOSE #","6", KT_COMMAND},
	{"MERGE","B", KT_COMMAND},
	{"VERIFY","B", KT_COMMAND},
	{"BEEP","8", KT_COMMAND},
	{"CIRCLE","V", KT_COMMAND},//modified
	{"INK","7", KT_COMMAND|KT_PRINT},
	{"PAPER","7", KT_COMMAND | KT_PRINT},
	{"FLASH","7", KT_COMMAND | KT_PRINT},
	{"BRIGHT","7", KT_COMMAND | KT_PRINT},
	{"INVERSE","7", KT_COMMAND | KT_PRINT},
	{"OVER","7", KT_COMMAND | KT_PRINT},
	{"OUT","8", KT_COMMAND},
	{"LPRINT","W", KT_COMMAND},//modified
	{"LLIST","5", KT_COMMAND},
	{"STOP","", KT_COMMAND},
	{"READ","5", KT_COMMAND},
	{"DATA","5", KT_COMMAND},
	{"RESTORE","3", KT_COMMAND},
	{"NEW","", KT_COMMAND},
	{"BORDER","6", KT_COMMAND},
	{"CONTINUE","", KT_COMMAND},
	{"DIM","5", KT_COMMAND},
	{"REM","Y", KT_COMMAND},//modified
	{"FOR","4=6k2TO6Z", KT_COMMAND},//modified
	{"GO TO","6", KT_COMMAND},
	{"GO SUB","6", KT_COMMAND},
	{"INPUT","W", KT_COMMAND},//modified
	{"LOAD","B", KT_COMMAND},
	{"LIST","5", KT_COMMAND},
	{"LET","1=2", KT_COMMAND},
	{"PAUSE","6", KT_COMMAND},
	{"NEXT","4", KT_COMMAND},
	{"POKE","8", KT_COMMAND},
	{"PRINT","W", KT_COMMAND},//modified
	{"PLOT","V", KT_COMMAND},//modified
	{"RUN","3", KT_COMMAND},
	{"SAVE","B", KT_COMMAND},
	{"RANDOMIZE","3", KT_COMMAND},
	{"IF","6k4THENX", KT_COMMAND},//modified
	{"CLS","", KT_COMMAND},
	{"DRAW","V", KT_COMMAND},//modified
	{"CLEAR","3", KT_COMMAND},
	{"RETURN","", KT_COMMAND},
	{"COPY", "", KT_COMMAND}
};

char *update_basic(void)
{
	unsigned short PROG = peek_fastw(23635);
	unsigned short VARS = peek_fastw(23627);

	if (PROG == 0 || VARS == 0 || VARS <= PROG)
	{
		basic[0] = '\0';
		return basic;
	}

	/*
	2bytes line number (Big Endian)
	2bytes length of text (Little Endian) (including Enter)
	text
	Enter
	*/

	unsigned short p = PROG;
	char *t = basic;
	while (p < VARS)
	{
		unsigned short q = p + 4;

		int line = peek_fast(p) * 256 + peek_fast(p+1);
		int length = peek_fast(p+2) + peek_fast(p+3) * 256;
		p += (unsigned short)length + 4;

		t += sprintf_s(t, 7 , "%4d ", line);

		while (--length > 0)
		{
			unsigned char c = peek_fast(q++);
			if (c == 0x0E)
			{
				//FP number follows
				unsigned char fp[5];
				for (int i = 0; i < 5; i++) fp[i] = peek_fast((unsigned short)(q + i));
				decodeFP(fp);

				length -= 5;
				q += 5;
			}
			else if (c < 32)
			{
				//non-printing character
				t += sprintf_s(t, 7, "%02X ", (int)c);
			}
			else if (c == 96)
			{
				// �
				t += sprintf_s(t, 7, "\xa3");
			}
			else if (c == 127)
			{
				// �
				t += sprintf_s(t, 7, "\xa9");
			}
			else if (c >= 32 && c < 128)
			{
				//ASCII (almost)
				*t++ = c;
			}
			else if (c >= 128 && c < 144)
			{
				//block gfx
				const unsigned short gfx[16] = { ' ', 0x259d, 0x2598, 0x2580, 0x2597, 0x2590, 0x259a, 0x259c, 0x2596, 0x259e, 0x258d, 0x259b, 0x2583, 0x259f, 0x2599, 0x2588 };
				t += WideCharToMultiByte(CP_UTF8, 0, &gfx[c - 128], 1, t, 10, NULL, FALSE);
			}
			else if (c >= 144 && c < 165)
			{
				//21 x UDG A-U
				t += sprintf_s(t, 7, "%c", c-144+'A');
			}
			else if (c >= 165)
			{
				//keyword
				t += sprintf_s(t, 20, "%s ", keywords[c - 165].name);
			}
			else
			{
				assert(0);
			}
		}
		*t++ = '\r';
		*t++ = '\n';
		*t = '\0';
	}

	get_vars();

	return basic;
}

const char *get_keyword_txt(int k)
{
	if (k >= 165 && k <= 255)
	{
		k -= 165;
		return keywords[k].name;
	}
	return "";
}

double decodeFP(unsigned char *q)
{
	/*
ZX81 Manual (Ch 27)
	byte 1: the exponent - its value is 128 more than the real value(+128 offset)
		(1 means 2 ^ -127, 128 means 2 ^ 0 and 255 means 2 ^ +127)

	if its value is zero, then the whole number is zero(the next 4 bytes are also 0)

	byte 2 : the MSB of the mantissa - bit 7 is the signum bit
	..
	byte 5 : the LSB of the mantissa.

ZX Spectrum Manual (Ch 24)
	To store the number in the computer, we use five bytes, as follows:

write the first eight bits of the mantissa in the second byte (we know that the first bit is 1), the second eight bits in the third byte, the third eight bits in the fourth byte and the fourth eight bits in the fifth byte;
replace the first bit in the second byte which we know is 1 by the sign: 0 for plus, 1 for minus;
write the exponent +128 in the first byte. For instance, suppose our number is 1/10
1/10 =4/5x2-3
	
	There is an alternative way of storing whole numbers between -65535 and +65535:

the first byte is 0.
the second byte is 0 for a positive number, FFh for a negative one,
the third and fourth bytes are the less and more significant bytes of the number (or the number +131072 if it is negative),
the fifth byte is 0.
	*/

	//elog("%02X %02X %02X %02X %02X -> ", (int)q[0], (int)q[1], (int)q[2], (int)q[3], (int)q[4]);

	double answer;

	if (q[0] == 0 && (q[1] == 0 || q[1] == 0xff))
	{
		if (q[4] != 0)
			answer = NAN;
		else
			answer = ((unsigned int)q[2] + (unsigned int)q[3] * 256) * (q[1] == 0xff ? -1.0 : 1.0);
	}
	else
	{
		//can stick it all in a double
		//a double is 1:11:53 (really, 1:11:'1'52

		unsigned long long sign = ((unsigned long long)q[1] & 0x80) << 56;

		int unbiasedE = (int)q[0] - 128 - 1;//ZX fp is biased +128
		//Why do we need the extra -1?
		//I think it's because of the implied 1.x in double ie. we end up with 100.1 being 2^7*1.788b2 instead of 2^7*0.788b2
		//So we compensate and make it 2^6*1.782b2

		unsigned long long biasedE = unbiasedE + 1023;//double is biased +1023

		unsigned long long exponent = biasedE<<52;
		unsigned long long mantissa;
		mantissa  = ((unsigned long long)q[1]&0x7f) << 24;
		mantissa |= ((unsigned long long)q[2]) << 16;
		mantissa |= ((unsigned long long)q[3]) << 8;
		mantissa |= ((unsigned long long)q[4]) << 0;
		
		//ZX mantissa has 31 bits, so we need to shift up 21
		mantissa <<= 21;

		unsigned long long result = sign | exponent | mantissa;
		answer = *(double *)&result;
	}

	//elog("%lf\n", answer);

	return answer;
}

//---------------------------------------------------------------------------------------
// SYSVARS
//---------------------------------------------------------------------------------------
typedef struct
{
	char size;
	unsigned short address;
	const char *const name;
	const char *const description;
} SYSVAR;

static const SYSVAR sysvars[] = {
	{8, 23552, "KSTATE", "Used in reading the keyboard."},
	{1, 23560, "LAST_K", "Stores newly pressed key."},
	{1, 23561, "REPDEL", "Time (in 50ths of a second in 60ths of a second in N.America) that a key must be held down before it repeats. This starts off at 35, but you can POKE in other values."},
	{1, 23562, "REPPER", "Delay (in 50ths of a second in 60ths of a second in N.America) between successive repeats of a key held down : initially 5."},
	{2, 23563, "DEFADD", "Address of arguments of user defined function if one is being evaluated; otherwise 0."},
	{1, 23565, "K_DATA", "Stores 2nd byte of colour controls entered from keyboard."},
	{2, 23566, "TVDATA", "Stores bytes of colour, AT and TAB controls going to television."},
	{38, 23568, "STRMS", "Addresses of channels attached to streams."},
	{2, 23606, "CHARS", "256 less than address of character set (which starts with space and carries on to the copyright symbol). Normally in ROM, but you can set up your own in RAM andmake CHARS point to it."},
	{1, 23608, "RASP", "Length of warning buzz."},
	{1, 23609, "PIP", "Length of keyboard click."},
	{1, 23610, "ERR_NR", "1 less than the report code. Starts off at 255 (for 1) so PEEK 23610 gives 255."},
	{1, 23611, "FLAGS", "Various flags to control the BASIC system."},
	{1, 23612, "TV_FLAG", "Flags associated with the television."},
	{2, 23613, "ERR_SP", "Address of item on machine stack to be used as error return."},
	{2, 23615, "LIST_SP", "Address of return address from automatic listing."},
	{1, 23617, "MODE", "Specifies K, L, C, E or G cursor."},
	{2, 23618, "NEWPPC", "Line to be jumped to."},
	{1, 23620, "NSPPC", "Statement number in line to be jumped to. Poking first NEWPPC and then NSPPC forces a jump to a specified statement in a line."},
	{2, 23621, "PPC", "Line number of statement currently being executed."},
	{1, 23623, "SUBPPC", "Number within line of statement being executed."},
	{1, 23624, "BORDCR", "Border colour * 8; also contains the attributes normally used for the lower half of the screen."},
	{2, 23625, "E_PPC", "Number of current line (with program cursor)."},
	{2, 23627, "VARS", "Address of variables."},
	{2, 23629, "DEST", "Address of variable in assignment."},
	{2, 23631, "CHANS", "Address of channel data."},
	{2, 23633, "CURCHL", "Address of information currently being used for input and output."},
	{2, 23635, "PROG", "Address of BASIC program."},
	{2, 23637, "NXTLIN", "Address of next line in program."},
	{2, 23639, "DATADD", "Address of terminator of last DATA item."},
	{2, 23641, "E_LINE", "Address of command being typed in."},
	{2, 23643, "K_CUR", "Address of cursor."},
	{2, 23645, "CH_ADD", "Address of the next character to be interpreted : the character after the argument of PEEK, or the NEWLINE at the end of a POKE statement."},
	{2, 23647, "X_PTR", "Address of the character after the ? marker."},
	{2, 23649, "WORKSP", "Address of temporary work space."},
	{2, 23651, "STKBOT", "Address of bottom of calculator stack."},
	{2, 23653, "STKEND", "Address of start of spare space."},
	{1, 23655, "BREG", "Calculator's b register."},
	{2, 23656, "MEM", "Address of area used for calculator's memory. (Usually MEMBOT, but not always.)"},
	{1, 23658, "FLAGS2", "More flags."},
	{1, 23659, "DF_SZ", "The number of lines (including one blank line) in the lower part of the screen."},
	{2, 23660, "S_TOP", "The number of the top program line in automatic listings."},
	{2, 23662, "OLDPPC", "Line number to which CONTINUE jumps."},
	{1, 23664, "OSPCC", "Number within line of statement to which CONTINUE jumps."},
	{1, 23665, "FLAGX", "Various flags."},
	{2, 23666, "STRLEN", "Length of string type destination in assignment."},
	{2, 23668, "T_ADDR", "Address of next item in syntax table (very unlikely to be useful)."},
	{2, 23670, "SEED", "The seed for RND. This is the variable that is set by RANDOMIZE."},
	{3, 23672, "FRAMES", "3 byte (least significant first), frame counter. Incremented every 20ms. See Chapter 18."},
	{2, 23675, "UDG", "Address of 1st user defined graphic. You can change this for instance to save space by having fewer user defined graphics."},
	{1, 23677, "COORDSX", "x - coordinate of last point plotted."},
	{1, 23678, "COORDSY", "y - coordinate of last point plotted."},
	{1, 23679, "P_POSN", "33 column number of printer position."},
	{2, 23680, "PR_CC", "Full address of next position for LPRINT to print at (in ZX printer buffer). Legal values 5B00 - 5B1F.[Not used in 128K mode or when certain peripherals are attached]."},
	{2, 23682, "ECHO_E", "33 column number and 24 line number (in lower half) of end of input buffer."},
	{2, 23684, "DF_CC", "Address in display file of PRINT position."},
	{2, 23686, "DFCCL", "Like DF CC for lower part of screen."},
	{1, 23688, "S_POSNX", "33 column number for PRINT position."},
	{1, 23689, "S_POSNY", "24 line number for PRINT position."},
	{2, 23690, "SPOSNL", "Like S_POSN for lower part."},
	{1, 23692, "SCR_CT", "Counts scrolls : it is always 1 more than the number of scrolls that will be done before stopping with scroll ? If you keep poking this with a number bigger than 1 (say 255), the screen will scroll on and on without asking you."},
	{1, 23693, "ATTR_P", "Permanent current colours, etc (as set up by colour statements)."},
	{1, 23694, "MASK_P", "Used for transparent colours, etc. Any bit that is 1 shows that the corresponding attribute bit is taken not from ATTR P, but from what is already on the screen."},
	{1, 23695, "ATTR_T", "Temporary current colours, etc (as set up by colour items)."},
	{1, 23696, "MASK_T", "Like MASK P, but temporary."},
	{1, 23697, "P_FLAG", "More flags."},
	{30, 23698, "MEMBOT", "Calculator's memory area; used to store numbers that cannot conveniently be put on the calculator stack."},
	{2, 23728, "NMIADD", "This is the address of a user supplied NMI address which is read by the standard ROM when a peripheral activates the NMI."
						//"\r\n"
						//"Probably intentionally disabled so that the effect is to perform a reset if both locations hold zero, but do nothing if the locations hold a non - zero value.\r\n"
						//"Interface 1's with serial number greater than 87315 will initialize these locations to 0 and 80 to allow the RS232 "T" channel to use a variable line width. 23728 is the current print position and 23729 the width - default 80."
						},
	{2, 23730, "RAMTOP", "Address of last byte of BASIC system area."},
	{2, 23732, "P_RAMT", "Address of last byte of physical RAM."},
};

static const SYSVAR if1_sysvars[] = {
	{1,23734, "FLAGS3","Flags"},
	{2,23735, "VECTOR","Address used to extend the BASIC interpreter."},
	{10, 23737, "SBRT","ROM paging subroutine."},
	{2, 23747, "BAUD","Two byte number determining the, baud rate calculated as follows : BAUD = (3500000 / (26 * baud rate)) - 2. You can use this to set up non-standard baud rates."},
	{1, 23749, "NTSTAT","Own network station number."},
	{1, 23750, "IOBORD","Border colour used during I/O. You can POKE any colour you want."},
	{2, 23751, "SER_FL","2 byte workspace used by RS232"},
	{2, 23753, "SECTOR","2 byte workspace used by Microdrive"},
	{2, 23755, "CHADD_","Temporary store for CH_ADD"},
	{1, 23757, "NTRESP","Store for network response code"},
	{1, 23758, "NTDEST","Beginning of network buffer. Contains destination station number 0 - 64."},
	{1, 23759, "NTSRCE","Source station number."},
	{2, 23760, "NTNUMB","Network block number 0-65535."},
	{1, 23762, "NTTYPE","Header type code."},
	{1, 23763, "NTLEN","Data block length 0-255."},
	{1, 23764, "NTDCS","Data block checksum."},
	{1, 23765, "NTHCS","Header block checksum."},
	{2, 23766, "D_STR1","Start of 8 byte file specifier. 2 byte drive number 1-8."},
	{1, 23768, "S_STR1","Stream number 1-15."},
	{1, 23769, "L_STR1","Device type 'm', 'n', 't' or 'b'."},
	{2, 23770, "N_STR1","Length of filename."},
	{2, 23772, "", "Start of filename."},
	{2, 23774, "D_STR2","Second 8 byte file specifier. used by MOVE and LOAD commands."},
	{1, 23776, "S_STR2","Stream number 1-15."},
	{1, 23777, "L_STR2","Device type 'm', 'n', 't' or 'b'."},
	{2, 23778, "N_STR2","Length of filename."},
	{2, 23780, "", "Start of filename."},
	{1, 23782, "HD_00","Start of workspace for SAVE, LOAD, VERIFY and MERGE data type code."},
	{2, 23783, "HD_08", "Length of data 0-65535"},
	{2, 23785, "HD_0D", "Start of data 0-65535"},
	{2, 23787, "HD_0F", "Program length 0-65535"},
	{2, 23789, "HD_11", "Line number" },
	{1, 23791, "COPIES", "Number of copies made by SAVE"},
	{0, 23792, "", "Start of Microdrive MAPS or CHANS"},
};

static void save_sysvar(const SYSVAR *sv)
{
	if (sv->name[0] == '\0') return;

	//ZX_LABEL label = { 0 };
	//label.address = sv->address;
	//label.name = (char *)sv->name;
	//label.source = SR_LIBRARY;
	//label.type = LT_DATA;
	//save_label(&label);
	make_label(sv->name, SR_LIBRARY, sv->address, LT_DATA);

	//ZX_TYPE type = { 0 };
	//type.address = sv->address;
	//if (sv->size == 2)
	//	type.type = DEFW;
	//else
	//	type.type = DEFB;
	//type.size = sv->size;
	//type.source = SR_LIBRARY;
	//save_type(&type);
	if (sv->size == 2)
		make_dataw(SR_LIBRARY, sv->address, sv->size);
	else
		make_data(SR_LIBRARY, sv->address, sv->size);
}

void gen_sysvar_labels_48k(void)
{
	const SYSVAR *sv = sysvars;
	for (int i = 0; i < sizeof sysvars / sizeof * sysvars; i++, sv++)
		save_sysvar(sv);
}

void gen_sysvar_if1_labels(void)
{
	const SYSVAR *sv = if1_sysvars;
	for (int i = 0; i < sizeof if1_sysvars / sizeof * if1_sysvars; i++, sv++)
		save_sysvar(sv);
}

static char sysvartxt[100000];

void render_sysvar(const SYSVAR *sv, char **w)
{
	char *t = *w;
	if (strcmp(sv->name, "SBRT") == 0)
		t += sprintf_s(t, 1000, "%-7s %5d   %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %s\r\n%s", sv->name, sv->address,
			peek_fast(sv->address), peek_fast(sv->address+1),
			peek_fast(sv->address+2), peek_fast(sv->address + 3),
			peek_fast(sv->address+4), peek_fast(sv->address + 5),
			peek_fast(sv->address+6), peek_fast(sv->address + 7),
			peek_fast(sv->address+8), peek_fast(sv->address + 9),
			sv->description,
			"                           5CB9 21 26 04    ld hl,0426h\r\n"\
			"                           5CBC CD 10 00    call 0010h\r\n"\
			"                           5CBF 22 BA 5C    ld (5CBAh),hl\r\n"\
			"                           5CC2 C9          ret"
			);
	else if (sv->size == 1)
		t += sprintf_s(t, 1000, "%-7s %5d   %02X (%5d) %s", sv->name, sv->address, peek_fast(sv->address), peek_fast(sv->address), sv->description);
	else if (sv->size == 2)
		t += sprintf_s(t, 1000, "%-7s %5d %04X (%5d) %s", sv->name, sv->address, peek_fastw(sv->address), peek_fastw(sv->address), sv->description);
	else if (sv->size == 3)
		t += sprintf_s(t, 1000, "%-7s %5d   (%8u) %s", sv->name, sv->address, (unsigned int)peek_fastw(sv->address) + 65536 * (unsigned int)peek_fast(sv->address + 2), sv->description);
	else
		t += sprintf_s(t, 1000, "%-7s %5d              %s", sv->name, sv->address, sv->description);
	t += sprintf_s(t, 10, "\r\n");
	*w = t;
}

char *get_sysvars(void)
{
	char *t = sysvartxt;

	const SYSVAR *sv;
	sv = sysvars;
	for (int i = 0; i < sizeof sysvars / sizeof *sysvars; i++, sv++)
		render_sysvar(sv, &t);

	if (machine.has_if1)
	{
		t += sprintf_s(t, 1000, "\r\nInterface 1\r\n");
		sv = if1_sysvars;
		for (int i = 0; i < sizeof if1_sysvars / sizeof *if1_sysvars; i++, sv++)
			render_sysvar(sv, &t);
	}

	return sysvartxt;
}

//---------------------------------------------------------------------------------------
// BASIC VARS
//---------------------------------------------------------------------------------------
static double decode_fp(unsigned short q)
{
	unsigned char fp[5];
	for (int i = 0; i < 5; i++) fp[i] = peek_fast((unsigned short)(q + i));
	return decodeFP(fp);
}

#pragma warning(push)
#pragma warning(disable: 4996)
static unsigned short decode_array_of_number(char **t, unsigned short var)
{
	unsigned short ovar = var;
	char varname = (peek_fast(var++)&0x1F)+0x60;
	unsigned short size = peek_fastw(var); var += 2;
	unsigned short rv = var + size;

	unsigned char n_dims = peek_fast(var++);
	*t += sprintf(*t, "%04X %5d : DIM %c(", ovar, ovar, varname);
	int n_entries = 1;
	for (int i = 0; i < n_dims; i++)
	{
		unsigned short dim = peek_fastw(var); var += 2;
		n_entries *= dim;
		*t += sprintf(*t, "%d,", dim);
	}
	*t += sprintf(*t, ") = (");
	for (int i = 0; i < n_entries; i++)
	{
		double d = decode_fp(var); var += 5;
		*t += sprintf(*t, "%g, ", d);
		if (((i + 1) % 8) == 0) *t += sprintf(*t, "\n");
	}
	*t += sprintf(*t, ")\n");
	assert(rv == var);

	return rv;
}

static unsigned short decode_variable_1(char **t, unsigned short var)
{
	unsigned short ovar = var;
	char varname = (peek_fast(var++) & 0x1F) + 0x60;
	double v = decode_fp(var); var += 5;

	*t += sprintf(*t, "%04X %5d : LET %c = %g\n", ovar, ovar, varname, v);

	return var;
}

static unsigned short decode_variable_N(char **t, unsigned short var)
{
	unsigned short ovar = var;
	char varname[100];
	int i = 0;
	varname[i++] = (peek_fast(var++) & 0x1F) + 0x60;
	for (;;)
	{
		unsigned char c = peek_fast(var++);
		varname[i++] = c & 0x7f;
		if (c & 0x80) break;
	}
	varname[i++] = '\0';
	double v = decode_fp(var); var += 5;

	*t += sprintf(*t, "%04X %5d : LET %s = %g\n", ovar, ovar, varname, v);

	return var;
}

static unsigned short decode_for(char **t, unsigned short var)
{
	unsigned short ovar = var;
	char varname = (peek_fast(var++) & 0x1F) + 0x60;
	double from = decode_fp(var); var += 5;
	double to = decode_fp(var); var += 5;
	double step = decode_fp(var); var += 5;
	unsigned short line = peek_fastw(var); var += 2;
	unsigned char statement = peek_fast(var++);

	*t += sprintf(*t, "%04X %5d : FOR %c = %g TO %g STEP %g at line %d, statement %d\n", ovar, ovar, varname, from, to, step, line, statement);

	return var;
}

unsigned short decode_string(char **t, unsigned short var)
{
	unsigned short ovar = var;
	char varname = (peek_fast(var++) & 0x1F) + 0x60;
	unsigned short charcnt = peek_fastw(var); var += 2;
	static char txt[1000];
	char *n = txt;
	for (unsigned short i = 0; i < charcnt; i++)
		*n++ = peek_fast(var++);
	*n++ = '\0';
	*t += sprintf(*t, "%04X %5d : LET %c$ = \"%s\"\n", ovar, ovar, varname, txt);
	return var;
}

unsigned short decode_array_of_char(char **t, unsigned short var)
{
	unsigned short ovar = var;
	char varname = (peek_fast(var++) & 0x1F) + 0x60;
	unsigned short size = peek_fastw(var); var += 2;
	unsigned short rv = var + size;

	unsigned char n_dims = peek_fast(var++);
	*t += sprintf(*t, "%04X %5d : DIM %c(", ovar, ovar, varname);
	int n_entries = 1;
	for (int i = 0; i < n_dims; i++)
	{
		unsigned short dim = peek_fastw(var); var += 2;
		n_entries *= dim;
		*t += sprintf(*t, "%d,", dim);
	}
	*t += sprintf(*t, ") = (");
	for (int i = 0; i < n_entries; i++)
	{
		unsigned char d = peek_fast(var++);
		*t += sprintf(*t, "%02X, ", d);
		if (((i + 1) % 8) == 0) *t += sprintf(*t, "\n");
	}
	*t += sprintf(*t, ")\n");
	assert(rv == var);

	return rv;
}
#pragma warning(pop)

char *get_vars(void)
{
	char *t = sysvartxt;
	*t = '\0';

	unsigned short VARS = peek_fastw(23627);
	unsigned short E_LINE = peek_fastw(23641);

	unsigned short var = VARS;
	unsigned char c;
	for(;;)
	{
		c = peek_fast(var);
		if (c == 0x80 || var >= E_LINE) break;
		switch (c & 0xE0)
		{
			case 0x80: // array of numbers
				var = decode_array_of_number(&t, var); break;
			case 0x60: // variable whose name is 1 char
				var = decode_variable_1(&t, var); break;
			case 0xA0: // variable whose name is longer than 1 char
				var = decode_variable_N(&t, var); break;
			case 0xE0: // for loop
				var = decode_for(&t, var); break;
			case 0x40: // string
				var = decode_string(&t, var); break;
			case 0xC0: // array of characters
				var = decode_array_of_char(&t, var); break;
			default:
				elog("don't know VARS format %02x\n", c & 0xE0);
				//assert(0);
				var++;
		}
	}
	elog("%s\n", sysvartxt);
	return sysvartxt;
}

//---------------------------------------------------------------------------------------
// ZX Spectrum BASIC Parser
//---------------------------------------------------------------------------------------
/*
the name of a string variable has to be a single letter followed by $; 
and the name of the control variable of a FOR-NEXT loop must be a single letter;
but the names of ordinary numeric variables are much freer.
They can use any letters or digits as long as the first one is a letter.
You can put spaces in as well to make it easier to read, but they won't count as part of the name.
*/
typedef enum
{
	OK,
	EXPECTED_LITERAL,
	EXPECTED_KEYWORD,
	COLOUR_EXPRESSION,
	COLOUR_EXPRESSION_2,
	EXPECTED_LETTER,
	UNTERMINATED_STRING,
	UNEXPECTED_KEYWORD,
} ZX_ERROR;

typedef struct
{
	char *src;
	unsigned short line_number;
	char *line_start;
	ZX_ERROR error;
	int errval1;
	int errval2;
	char *errpos;
} TSTATE;

int get_class_03_optional_expression_numeric(TSTATE *t);
int get_class_06_expression_numeric(TSTATE *t);
int get_class_08_two_numerical_expressions(TSTATE *t);
int get_class_0A_expression_string(TSTATE *t);
int get_class_0B_cassette_routines(TSTATE *t);
int get_sequence_of_statements(TSTATE *t);
int get_keyword(TSTATE *t, KEYWORD **k);

typedef enum
{
	TT_NUMBER,
	TT_KEYWORD,
	TT_LITERAL,
	TT_VARIABLE,
	TT_STRING,
	TT_OPERATOR,
	TT_LINENUMBER
} ZXTOKEN_TYPE;

typedef struct __ZXTOKEN__
{
	ZXTOKEN_TYPE type;
	char *string_value;
	double number_value;
	char char_value;
	struct __ZXTOKEN__ *next_token;
} ZXTOKEN;

ZXTOKEN *token_head = NULL;
ZXTOKEN *curr_token = NULL;

ZXTOKEN *new_token(ZXTOKEN_TYPE tt)
{
	ZXTOKEN *t;
	t = calloc(sizeof *t, 1);
	assert(t);

	t->type = tt;
	t->next_token = NULL;

	if (token_head == NULL)
		token_head = t;
	else
		curr_token->next_token = t;

	curr_token = t;

	return t;
}

void set_token_string(ZXTOKEN *t, const char * const s)
{
	size_t len = strlen(s)+1;
	t->string_value = malloc(len);
	assert(t->string_value);
	strcpy_s(t->string_value, len, s);
}

void free_token(ZXTOKEN *t)
{
	if (t->string_value)
		free(t->string_value);
	memset(t, 0, sizeof * t);
	free(t);
}

void clear_tokens()
{
	curr_token = token_head;
	while (curr_token != NULL)
	{
		ZXTOKEN *n = curr_token->next_token;
		free_token(curr_token);
		curr_token = n;
	}
	token_head = NULL;
	curr_token = NULL;
}

int tab = 0;
void popx() { tab--; assert(tab >= 0); }
void pushx() { tab++; }

void display_token(ZXTOKEN *t)
{
	char txt[100];

	switch ((int)t->type)
	{
		case TT_NUMBER:
			sprintf_s(txt, 100, "[NUM] %lf", t->number_value);
			break;
		case TT_KEYWORD:
			sprintf_s(txt, 100, "[KEY] %s", t->string_value);
			break;
		case TT_LITERAL:
			if (t->char_value == '(') pushx();
			if (t->char_value == ')') popx();
			if (t->string_value)
				sprintf_s(txt, 100, "[LIT] %s", t->string_value);
			else
				sprintf_s(txt, 100, "[LIT] %c", t->char_value);
			break;
		case TT_VARIABLE:
			if (t->string_value)
				sprintf_s(txt, 100, "[VAR] %s", t->string_value);
			else
				sprintf_s(txt, 100, "[VAR] %c", t->char_value);
			break;
		case TT_STRING:
			sprintf_s(txt, 100, "[STR] %s", t->string_value);
			break;
		case TT_OPERATOR:
			sprintf_s(txt, 100, "[OP ] %s", t->string_value);
			break;
		case TT_LINENUMBER:
			sprintf_s(txt, 100, "\n[LIN] %u", (unsigned int)t->number_value);
			break;
		default:
			assert(0);
	}
	for (int i = 0; i < tab; i++)
		OutputDebugString("\t");

	strcat_s(txt, 100, "\n");
	OutputDebugString(txt);
}

void dump_tokens(void)
{
	curr_token = token_head;
	while (curr_token != NULL)
	{
		display_token(curr_token);
		curr_token = curr_token->next_token;
	}
}


unsigned char *programbuffer = NULL;
unsigned char *program = NULL;

void inject_program(void)
{
	//set up PROG
	//jam in the program
	//setup VARS
	//etc.

	unsigned short PROG = peek_fastw(23635);

	unsigned short length = (unsigned short)(program - programbuffer);
	poke_fast_memcpy(PROG, programbuffer, length);

	unsigned short VARS = 23627;
	poke_fastw(VARS, PROG + length);

	poke_fast(PROG + length, 0x80);
	poke_fast(PROG + length+1, 0x0D);
	poke_fast(PROG + length+2, 0x80);

	unsigned short E_LINE = 23641;
	poke_fastw(E_LINE, PROG + length + 1);

	unsigned short K_CUR = 23643;
	poke_fastw(K_CUR, PROG + length + 1);

	unsigned short WORKSP = 23649;
	poke_fastw(WORKSP, PROG + length + 3);

	unsigned short STKBOT = 23651;
	poke_fastw(STKBOT, PROG + length + 3);

	unsigned short STKEND = 23653;
	poke_fastw(STKEND, PROG + length + 3);
}

//---------------------------------------------------------------------------------------
// Given a tokenised ZX Spectrum BASIC program
// Convert it and inject it into the emulation's memory
//---------------------------------------------------------------------------------------

/*
	2bytes line number (Big Endian)
	2bytes length of text (Little Endian) (including Enter)
	text
	Enter
	*/

unsigned char *lengthptr = NULL;
void terminate_line(void)
{
	if (!lengthptr) return;

	//terminate the previous line
	*program++ = 13;
	unsigned short length = (unsigned short)(program - lengthptr)-2;
	*lengthptr++ = (unsigned char)length;
	*lengthptr++ = (unsigned char)(length >> 8);
	lengthptr = NULL;
}

unsigned char get_keyword_index(char *kwd)
{
	KEYWORD *kw = keywords;
	for (int i = 0; i < sizeof keywords / sizeof *keywords; i++, kw++)
	{
		if (strcmp(kw->name, kwd) == 0)
			return (unsigned char)(i + 165);
	}
	assert(0);
	return 0;
}

void convert_number_to_text(double n, ZXTOKEN *token)
{
	if (token->type == TT_KEYWORD && strcmp(token->string_value, "BIN") == 0)
	{
		program += sprintf_s((char *)program, 100, "%llu", (unsigned long long)n);
	}
	else
	{
		program += sprintf_s((char *)program, 100, "%g", n);
	}
}

void convert_number_to_ZXFP(double n, ZXTOKEN *token)
{
	*program++ = 0x0E;

	if (token->type == TT_KEYWORD && strcmp(token->string_value, "BIN") == 0)
	{
		unsigned short i = 0;
		unsigned long long bin = (unsigned long long)n;
		for (int e = 0; e < 16; e++)
		{
			lldiv_t dv = lldiv(bin, 10LL);
			i |= (unsigned short)dv.rem << e;
			bin = dv.quot;
		}
		*program++ = 0;
		*program++ = 0;
		*program++ = (unsigned char)i;
		*program++ = (unsigned char)(i >> 8);
		*program++ = 0;
	}
	else if (n >= -65535.0 && n <= 65535.0 && n == (int)n)
	{
		*program++ = 0;
		if (n < 0)
		{
			n = -n;
			*program++ = 0xff;
		}
		else
		{
			*program++ = 0;
		}
		unsigned short i = (unsigned short)n;
		*program++ = (unsigned char)i;
		*program++ = (unsigned char)(i>>8);
		*program++ = 0;
	}
	else
	{
		unsigned long long l = *(unsigned long long *)&n;
		unsigned long long sign = l & 0x8000000000000000ULL;
		unsigned long long exponent = l & 0x7ff0000000000000ULL;
		unsigned long long mantissa = l & 0x000fffffffffffffULL;
/*
		byte 1: the exponent - its value is 128 more than the real value(+128 offset)
			(1 means 2 ^ -127, 128 means 2 ^ 0 and 255 means 2 ^ +127)

			if its value is zero, then the whole number is zero (the next 4 bytes are also 0)

		byte 2 : the MSB of the mantissa - bit 7 is the signum bit
		...
		byte 5 : the LSB of the mantissa.
*/
		mantissa >>= 21;
		int exp = (int)(exponent >> 52);
		exp -= 1023;
		exp += 128 + 1;

		*program++ = (unsigned char)exp;
		*program++ = (unsigned char)((sign ? 0x80 : 0) | (mantissa >> 24));
		*program++ = (unsigned char)(mantissa >> 16);
		*program++ = (unsigned char)(mantissa >> 8);
		*program++ = (unsigned char)mantissa;
	}

	//double check = decodeFP(program - 5);
	//assert(check == n);
}

void convert_token(ZXTOKEN *token, ZXTOKEN *prev_token)
{
	switch ((int)token->type)
	{
		case TT_NUMBER:
			convert_number_to_text(token->number_value, prev_token);
			convert_number_to_ZXFP(token->number_value, prev_token);
			break;

		case TT_KEYWORD:
			*program++ = get_keyword_index(token->string_value);
			break;

		case TT_LITERAL:
			if (token->string_value)
			{
				size_t len = strlen(token->string_value);
				memcpy(program, token->string_value, len);
				program += len;
			}
			else
			{
				*program++ = token->char_value;
			}
			break;

		case TT_VARIABLE:
			if (token->string_value)
			{
				size_t len = strlen(token->string_value);
				memcpy(program, token->string_value, len);
				program += len;
			}
			else
			{
				*program++ = token->char_value;
			}
			break;

		case TT_STRING:
			{
			*program++ = '"';
			size_t len = strlen(token->string_value);
			memcpy(program, token->string_value, len);
			program += len;
			*program++ = '"';
			}
			break;

		case TT_OPERATOR:
			{
			size_t len = strlen(token->string_value);
			if (len == 1)
				*program++ = token->string_value[0];
			else if (strcmp(token->string_value, "<>") == 0)
				*program++ = 0xC9;
			else if (strcmp(token->string_value, "<=") == 0)
				*program++ = 0xC7;
			else if (strcmp(token->string_value, ">=") == 0)
				*program++ = 0xC8;
			else if (strcmp(token->string_value, "AND") == 0)
				*program++ = 0xC6;
			else if (strcmp(token->string_value, "OR") == 0)
				*program++ = 0xC5;
			else
				assert(0);
			}
			break;

		case TT_LINENUMBER:
			{
			terminate_line();

			unsigned short s = (unsigned short)token->number_value;
			*program++ = (unsigned char)(s >> 8);
			*program++ = (unsigned char)s;
			lengthptr = program;
			*program++ = 0xff;
			*program++ = 0xff;
			}
			break;
		default:
			assert(0);
	}
}

int literal_matches(ZXTOKEN *t, char *literal)
{
	char *c = literal;
	while (*c != '\0' && t && t->type == TT_LITERAL && t->char_value == *c)
	{
		t = t->next_token;
		c++;
	}
	return *c == '\0';
}

void compress_token(ZXTOKEN *t, char *literal)
{
	size_t length = strlen(literal);

	t->type = TT_KEYWORD;
	t->char_value = 0;
	t->string_value = malloc(length+1);
	assert(t->string_value);
	strcpy_s(t->string_value, length+1, literal);

	ZXTOKEN *w = t->next_token, *x;
	while (--length)
	{
		x = w;
		w = w->next_token;
		free_token(x);
	}

	t->next_token = w;
}

void convert_tokens(void)
{
	program = programbuffer = malloc(1000000);

	ZXTOKEN *prev_token = NULL;
	curr_token = token_head;
	while (curr_token != NULL)
	{
		convert_token(curr_token, prev_token);
		prev_token = curr_token;
		curr_token = curr_token->next_token;
	}
	terminate_line();
	inject_program();
	free(programbuffer);
}

//---------------------------------------------------------------------------------------
// A tokeniser to ZX Spectrum BASIC
//---------------------------------------------------------------------------------------
#define ERROR_CHECK() if (w.error) return copy_error(t, &w)

int copy_error(TSTATE *dststate, TSTATE *srcstate)
{
	dststate->error = srcstate->error;
	dststate->errval1 = srcstate->errval1;
	dststate->errval2 = srcstate->errval2;
	dststate->errpos = srcstate->errpos;
	return (int)(srcstate->src - dststate->src);
}

void set_error(TSTATE *t, ZX_ERROR err, int e1, int e2)
{
	t->error = err;
	t->errval1 = e1;
	t->errval2 = e2;
	t->errpos = t->src;
}

void emit(ZXTOKEN_TYPE tt, KEYWORD *k, char *s, double d, char c)
{
	ZXTOKEN *token = new_token(tt);

	switch ((int)tt)
	{
		case TT_NUMBER:
			token->number_value = d;
			break;
		case TT_KEYWORD:
			set_token_string(token, k->name);
			break;
		case TT_LITERAL:
			if (s) set_token_string(token, s);
			else token->char_value = c;
			break;
		case TT_VARIABLE:
			if (s) set_token_string(token, s);
			else token->char_value = c;
			break;
		case TT_STRING:
			set_token_string(token, s);
			break;
		case TT_OPERATOR:
			set_token_string(token, s);
			break;
		case TT_LINENUMBER:
			token->number_value = d;
			break;
		default:
			assert(0);
	}

	//display_token(token);
}

int whitespace(TSTATE *t)
{
	TSTATE w = *t;
	while (*w.src == ' ' || *w.src == 9)
		w.src++;
	return copy_error(t, &w);
}

int whitespacenl(TSTATE *t)
{
	TSTATE w = *t;
	while (isspace((unsigned char)*w.src))
		w.src++;
	return copy_error(t, &w);
}

void rtrim(char *w)
{
	char *q = w + strlen(w) - 1;
	while (*q == ' ' && q > w)
		*q-- = '\0';
}

int is_eol(TSTATE *w)
{
	return *w->src == ':' || *w->src == '\r' || *w->src == '\n' || *w->src == '\0';
}

int is_numeric_operator(TSTATE *w)
{
	return *w->src == '+' || *w->src == '-' || *w->src == '*' || *w->src == '/' || *w->src == '^' || *w->src == '<' || *w->src == '>' || *w->src == '='
		|| strncmp(w->src, "AND", 3)==0 || strncmp(w->src, "OR", 2)==0;
}

int is_string_operator(TSTATE *w)
{
	return *w->src == '+' || *w->src == '<' || *w->src == '>' || *w->src == '=';
}

int get_operator(TSTATE *t)
{
	TSTATE w = *t;

	if (*w.src == '+' || *w.src == '-' || *w.src == '*' || *w.src == '/' || *w.src == '^' || *w.src == '=')
		w.src++;
	else if (*w.src == '<')
	{
		if (w.src[1] == '=' || w.src[1] == '>')
			w.src++;
		w.src++;
	}
	else if (*w.src == '>')
	{
		if (w.src[1] == '=')
			w.src++;
		w.src++;
	}
	else if (*w.src == 'A')
	{
		if (w.src[1] == 'N')
			w.src++;
		if (w.src[1] == 'D')
			w.src++;
		w.src++;
	}
	else if (*w.src == 'O')
	{
		if (w.src[1] == 'R')
			w.src++;
		w.src++;
	}

	char op[100];
	char *o = op, *p = t->src;
	while (p != w.src)
		*o++ = *p++;
	*o = '\0';

	emit(TT_OPERATOR, NULL, op, 0.0, 0);

	return copy_error(t, &w);
}

int get_linenumber(TSTATE *t)
{
	TSTATE w = *t;
	w.src += whitespacenl(&w);

	t->line_start = w.src;

	unsigned short num = 0;
	while (isdigit((unsigned char)*w.src))
	{
		num *= 10;
		num += *w.src - '0';
		w.src++;
	}

	emit(TT_LINENUMBER, NULL, NULL, (double)num, 0);

	t->line_number = num;
	
	return copy_error(t, &w);
}

int get_literal(TSTATE *t, char p)
{
	TSTATE w = *t;
	w.src += whitespace(&w);
	if (*w.src != p)
		set_error(&w, EXPECTED_LITERAL, p, *w.src);

	emit(TT_LITERAL, NULL, NULL, 0.0, p);
	w.src++;

	return copy_error(t, &w);
}

int get_comment(TSTATE *t)
{
	TSTATE w = *t;
	while (!is_eol(&w))
	{
		emit(TT_LITERAL, NULL, NULL, 0.0, *w.src);
		w.src++;
	}

	return copy_error(t, &w);
}

static char tmp[1000];
int get_keyword_internal(TSTATE *t, KEYWORD **k, int emit_keyword)
{
	TSTATE w = *t;
	w.src += whitespace(&w);

	char *q = tmp;
	if (isalpha((unsigned char)*w.src) && isupper((unsigned char)*w.src))
	{
		*q++ = *w.src++;
		while ((isalpha((unsigned char)*w.src) && isupper((unsigned char)*w.src)) || *w.src == '$' || *w.src == ' ')
			*q++ = *w.src++;
	}
	*q = '\0';
	rtrim(tmp);

	KEYWORD *kw = keywords;
	*k = NULL;
	for (int i = 0; i < sizeof keywords / sizeof * keywords; i++, kw++)
	{
		if (strcmp(kw->name, tmp) == 0)
		{
			*k = kw;
			break;
		}
	}
	if (*k == NULL)
	{
		kw = keywords;
		for (int i = 0; i < sizeof keywords / sizeof * keywords; i++, kw++)
		{
			size_t len = strlen(kw->name);
			if (strncmp(kw->name, tmp, len) == 0)
			{
				w.src -= strlen(tmp) - len;
				*k = kw;
				break;
			}
		}
	}
	if (*k == NULL)
		set_error(&w, EXPECTED_KEYWORD, 0, 0);
	else if (emit_keyword)
		emit(TT_KEYWORD, kw, NULL, 0.0, 0);

	return copy_error(t, &w);
}

int get_keyword(TSTATE *t, KEYWORD **k)
{
	return get_keyword_internal(t, k, 1);
}

int is_keyword(TSTATE *t, KEYWORD **k)
{
	TSTATE w = *t;
	KEYWORD *nk;
	if (k == NULL) k = &nk;
	get_keyword_internal(&w, k, 0);
	return *k != NULL;
}

int is_fn(KEYWORD *k)
{
	return k && (k->type & KT_FN);
}

int get_optional_step(TSTATE *t)
{
	TSTATE w = *t;
	w.src += whitespace(&w);

	KEYWORD *keyword;
	if (is_keyword(&w, &keyword))
	{
		if (strcmp(keyword->name, "STEP") == 0)
		{
			KEYWORD *kw;
			w.src += get_keyword(&w, &kw);
			w.src += get_class_06_expression_numeric(&w);
		}
		else
		{
			set_error(&w, UNEXPECTED_KEYWORD, 0, 0);
		}
	}

	return copy_error(t, &w);
}

int get_number(TSTATE *t)
{
	//literal number -*[0-9]*[.[0-9]*]*[[e|E]-*[0-9]{1-2}]*

	//- is already gone, unary - eats it

	TSTATE w = *t;
	char *q = tmp;

	while (isdigit((unsigned char)*w.src))
		*q++ = *w.src++;
	if (*w.src == '.')
	{
		*q++ = *w.src++;
		while (isdigit((unsigned char)*w.src))
			*q++ = *w.src++;
	}
	if (*w.src == 'e' || *w.src == 'E')
	{
		*q++ = *w.src++;
		int c = 0;
		while (isdigit((unsigned char)*w.src) && c < 2)
		{
			*q++ = *w.src++;
			c++;
		}
	}
	*q = '\0';
	double d = strtod(tmp, NULL);
	emit(TT_NUMBER, NULL, NULL, d, 0);

	return copy_error(t, &w);
}

int get_string(TSTATE *t)
{
	TSTATE w = *t;

	assert(*w.src == '"');

	w.src++;
	
	for (;;)
	{
		if (*w.src == '"' && w.src[1] != '"')
		{
			char *inquote = t->src+1;
			char *o = tmp;
			while (inquote < w.src)
				*o++ = *inquote++;
			*o = '\0';
			emit(TT_STRING, NULL, tmp, 0.0, 0);
			w.src++;
			break;
		}
		else if (is_eol(&w) && *w.src != ':')
		{
			set_error(&w, UNTERMINATED_STRING, 0, 0);
		}

		ERROR_CHECK();

		w.src++;
	}
	return copy_error(t, &w);
}

int get_class_01_variable_name(TSTATE *t)
{
	TSTATE w = *t;
	w.src += whitespace(&w);

	// a single letter followed by $
	// or a letter followed by letters numbers and spaces

	char *q = tmp;
	if (isalpha((unsigned char)*w.src))
	{
		*q++ = *w.src++;
		if (*w.src == '$')
		{
			*q++ = *w.src++;
		}
		else
		{
			while (isalnum((unsigned char)*w.src) || *w.src == ' ')
			{
				*q++ = *w.src++;

				//dirty hacks, if the variable ends with " TO " then go back
				if (w.src - 4 >= t->src && strncmp(w.src - 4, " TO ", 4) == 0)
				{
					w.src -= 4;
					q -= 4;
					break;
				}
				if (w.src - 6 >= t->src && strncmp(w.src - 6, " THEN ", 6) == 0)
				{
					w.src -= 6;
					q -= 6;
					break;
				}
			}
		}
	}
	*q = '\0';
	rtrim(tmp);
	emit(TT_VARIABLE, NULL, tmp, 0.0, 0);

	//optional array indexes : (range[,range][,range]) where range is [numeric|numeric TO|TO numeric|TO]
	w.src += whitespace(&w);
	if (*w.src == '(')
	{
		w.src += get_literal(&w, '(');

		pushx();

		for (;;)
		{
			w.src += get_class_03_optional_expression_numeric(&w);
			KEYWORD *keyword;
			if (is_keyword(&w, &keyword))
			{
				if (strcmp(keyword->name, "TO") == 0)
				{
					w.src += get_keyword(&w, &keyword);
					w.src += get_class_03_optional_expression_numeric(&w);
				}
				else
					break;
			}
			
			w.src += whitespace(&w);
			if (*w.src != ',')
				break;
			w.src += get_literal(&w, ',');

			ERROR_CHECK();
		}
	}

	return copy_error(t, &w);
}

int get_class_02_expression_numeric_or_string(TSTATE *t)
{
	TSTATE w = *t;
	w.src += whitespace(&w);

	w.src += get_class_06_expression_numeric(&w);
	//w.src += get_class_0A_expression_string(&w);

	//either class_0A or class_06
	return copy_error(t, &w);
}

int get_class_03_optional_expression_numeric(TSTATE *t)
{
	TSTATE w = *t;
	w.src += whitespace(&w);

	//either 
	//':'
	//New Line
	//numeric expression (class_06)

	if (!is_eol(&w))
		w.src += get_class_06_expression_numeric(&w);

	return copy_error(t, &w);
}

int get_class_04_single_character(TSTATE *t)
{
	TSTATE w = *t;
	w.src += whitespace(&w);

	if (!isalpha((unsigned char)*w.src))
		set_error(&w, EXPECTED_LETTER, 0, 0);

	emit(TT_VARIABLE, NULL, NULL, 0.0, *w.src);

	w.src++;
	return copy_error(t, &w);
}

int get_class_05_set_of_items(TSTATE *t)
{
	TSTATE w = *t;

	//class_02
	//followed by
	//',' class_02

	for (;;)
	{
		w.src += get_class_02_expression_numeric_or_string(&w);
		w.src += whitespace(&w);
		if (*w.src != ',')
			break;
		w.src+= get_literal(&w,',');
	
		ERROR_CHECK();
	}

	return copy_error(t, &w);
}

int get_class_06_expression_numeric(TSTATE *t)
{
	//
	//either
	//(
	//a multi-character variable
	//a KEYWORD function
	//+-*/^ operator
	//literal number -*[0-9]*[.[0-9]*]*[e-*[0-9]{1-2}]*
	//)

	TSTATE w = *t;

	for (;;)
	{
		w.src += whitespace(&w);
		if (is_eol(&w))
			break;

		if (*w.src == '(')
		{
			emit(TT_LITERAL, NULL, NULL, 0.0, '(');
			pushx();
			w.src++;
			w.src += get_class_06_expression_numeric(&w);
		}
		else if (*w.src == '"') w.src += get_string(&w);
		else if (isdigit((unsigned char)*w.src) || *w.src == '.') w.src += get_number(&w);
		else if (is_numeric_operator(&w))
		{
			w.src += get_operator(&w);
			w.src += get_class_06_expression_numeric(&w);
		}
		else
		{
			KEYWORD *keyword=NULL;
			if (is_keyword(&w, &keyword))
			{
				if (is_fn(keyword))
				{
					w.src += get_keyword(&w, &keyword);

					if (keyword->program[0] == 'n')
						w.src += get_class_06_expression_numeric(&w);
					else if (keyword->program[0] == 's')
						w.src += get_class_0A_expression_string(&w);
					else if (keyword->program[0] == 'p')
						w.src += get_class_08_two_numerical_expressions(&w);
					else if (keyword->program[0] == 'b')
					{
						w.src += get_literal(&w, '(');
						w.src += get_class_08_two_numerical_expressions(&w);
						w.src += get_literal(&w, ')');
					}
				}
			}
			else if (isalpha((unsigned char)*w.src))
			{
				//it's a variable
				w.src += get_class_01_variable_name(&w);
			}
		}

		ERROR_CHECK();

		w.src += whitespace(&w);

		if (*w.src == ')')
		{
			popx();
			w.src += get_literal(&w, ')');
			break;
		}

		if (!is_numeric_operator(&w))
			break;
		w.src += get_operator(&w);
	}

	return copy_error(t, &w);
}

int get_class_07_colour(TSTATE *t)
{
	TSTATE w = *t;
	w.src += get_class_06_expression_numeric(&w);
	return copy_error(t, &w);
}

int get_class_08_two_numerical_expressions(TSTATE *t)
{
	TSTATE w = *t;

	//class_06 ',' class_06
	w.src += get_class_06_expression_numeric(&w);
	w.src += get_literal(&w, ',');
	w.src += get_class_06_expression_numeric(&w);

	return copy_error(t, &w);
}

int get_class_09_colour_with_two_numerical_expressions(TSTATE *t)
{
	TSTATE w = *t;
	w.src += get_class_08_two_numerical_expressions(&w);
	return copy_error(t, &w);
}

int get_class_0A_expression_string(TSTATE *t)
{
	//
	//either
	//(
	//a single letter $ variable
	//INKEY$ SCREEN$ VAL$ STR$ CHR$
	//+ operator
	//"" quoted string
	//)

	TSTATE w = *t;

	for (;;)
	{
		w.src += whitespace(&w);
		if (is_eol(&w))
			break;

		if (*w.src == '(')
		{
			w.src++;
			w.src += get_class_0A_expression_string(&w);
		}
		else if (*w.src == '"') w.src += get_string(&w);
		else if (isalpha((unsigned char)*w.src) && w.src[1] == '$') w.src += get_class_01_variable_name(&w);
		else if (is_string_operator(&w))
		{
			w.src += get_operator(&w);
			w.src += get_class_0A_expression_string(&w);
		}
		else
		{
			KEYWORD *keyword;
			if (is_keyword(&w, &keyword))
			{
				if (is_fn(keyword))
				{
					w.src += get_keyword(&w, &keyword);

					if (keyword->program[0] == 'n')
						w.src += get_class_06_expression_numeric(&w);
					else if (keyword->program[0] == 's')
						w.src += get_class_0A_expression_string(&w);
					else if (keyword->program[0] == 'p')
						w.src += get_class_08_two_numerical_expressions(&w);
					else if (keyword->program[0] == 'b')
					{
						w.src += get_literal(&w, '(');
						w.src += get_class_08_two_numerical_expressions(&w);
						w.src += get_literal(&w, ')');
					}
				}
			}
		}
		
		ERROR_CHECK();

		w.src += whitespace(&w);

		if (*w.src == ')')
		{
			emit(TT_LITERAL, NULL, NULL, 0.0, ')');
			w.src++;
			break;
		}

		if (!is_string_operator(&w))
			break;

		w.src += get_operator(&w);
	}

	return copy_error(t, &w);
}

int get_microdrive(TSTATE *t)
{
	TSTATE w = *t;
	// ERASE "m";1;"file"
	// FORMAT "m";1;"file"
	// FORMAT "n";64

	//Once the #4; has been processed, we end up here
	//OPEN #4;"m";1;"file"
	//OPEN #4;"S"
	//OPEN #4;"K"
	//OPEN #4;"P"

	w.src += whitespace(&w);
	w.src += get_class_0A_expression_string(&w);
	w.src += whitespace(&w);
	if (*w.src == ';')
	{
		w.src += get_literal(&w, ';');
		w.src += get_class_06_expression_numeric(&w);
		w.src += get_literal(&w, ';');
		w.src += get_class_0B_cassette_routines(&w);
	}
	return copy_error(t, &w);
}

int get_cat(TSTATE *t)
{
	TSTATE w = *t;

	//CAT n
	//CAT #n,n

	w.src += whitespace(&w);
	if (*w.src == '#')
	{
		w.src += get_literal(&w, '#');
		w.src += get_class_06_expression_numeric(&w);
		w.src += get_literal(&w, ',');
	}
	w.src += get_class_06_expression_numeric(&w);
	return copy_error(t, &w);
}

int get_move(TSTATE *t)
{
	TSTATE w = *t;

	//MOVE #1 to #2
	//MOVE "m";1;"file" TO #2
	//MOVE #2 TO "m";1;"file" TO #2
	//MOVE "m";1;"file" TO "m";2;"file"
	w.src += whitespace(&w);
	if (*w.src == '#')
	{
		w.src += get_literal(&w, '#');
		w.src += get_class_06_expression_numeric(&w);
		w.src += get_literal(&w, ',');
	}
	else
	{
		w.src += get_microdrive(&w);
	}

	//TO
	KEYWORD *keyword;
	w.src += get_keyword(&w, &keyword);

	w.src += whitespace(&w);
	if (*w.src == '#')
	{
		w.src += get_literal(&w, '#');
		w.src += get_class_06_expression_numeric(&w);
		w.src += get_literal(&w, ',');
	}
	else
	{
		w.src += get_microdrive(&w);
	}

	return copy_error(t, &w);
}


int get_class_0B_cassette_routines(TSTATE *t)
{
	TSTATE w = *t;

	// LOAD ""
	// LOAD "file"
	// LOAD "file" CODE 
	// LOAD "file" CODE start
	// LOAD "file" CODE start,length
	// LOAD "file" SCREEN$
	// LOAD "file" DATA array()
	// LOAD "file" DATA a$()
	// LOAD *"m";1;"file"...
	// SAVE "file"
	// SAVE "file" CODE start,length
	// SAVE "file" SCREEN$
	// SAVE "file" LINE number
	// SAVE "file" DATA array()
	// SAVE "file" DATA a$()
	// SAVE *"m";1;"file"..
	// MERGE "file"
	// MERGE *"m";1;"file"...
	// VERIFY "file" CODE 
	// VERIFY "file" CODE start
	// VERIFY "file" CODE start,length
	// VERIFY "file" SCREEN$
	// VERIFY "file" DATA array()
	// VERIFY "file" DATA a$()

	w.src += whitespace(&w);
	if (*w.src == '*')
	{
		w.src += get_literal(&w, '*');
		w.src += get_microdrive(&w);
		return copy_error(t, &w);
	}

	w.src += get_class_0A_expression_string(&w);

	if (is_keyword(&w, NULL))
	{
		KEYWORD *keyword;
		w.src += get_keyword(&w, &keyword);
		if (strcmp(keyword->name, "SCREEN$") == 0)
		{
			//do nothing
		}
		else if (strcmp(keyword->name, "CODE")==0)
		{
			w.src += get_class_05_set_of_items(&w);
		}
		else if (strcmp(keyword->name, "DATA")==0)
		{
			w.src += get_class_01_variable_name(&w);
			w.src += get_literal(&w, '(');
			w.src += get_literal(&w, ')');
		}
		else if (strcmp(keyword->name, "LINE") == 0)
		{
			w.src += get_number(&w);
		}
	}

	return copy_error(t, &w);
}

int get_print_input_statement(TSTATE *t)
{
	TSTATE w = *t;
	w.src += whitespace(&w);

	//PRINT
	//[
	//AT x,y |
	//TAB n
	//INK/PAPER/INVERSE/OVER/FLASH/BRIGHT n |
	//[,;']* |
	//string/number
	//]*

	for (;;)
	{
		KEYWORD *keyword;
		if (is_keyword(&w, &keyword))
		{
			if (strcmp(keyword->name, "AT") == 0)
			{
				w.src += get_keyword(&w, &keyword);
				w.src += get_class_08_two_numerical_expressions(&w);
			}
			else if (strcmp(keyword->name, "INK") == 0 ||
				strcmp(keyword->name, "PAPER") == 0 ||
				strcmp(keyword->name, "INVERSE") == 0 ||
				strcmp(keyword->name, "OVER") == 0 ||
				strcmp(keyword->name, "FLASH") == 0 ||
				strcmp(keyword->name, "BRIGHT") == 0 ||
				strcmp(keyword->name, "TAB") == 0)
			{
				w.src += get_keyword(&w, &keyword);
				w.src += get_class_06_expression_numeric(&w);
			}
			else
			{
				//it's something else, which means it's an expression
				w.src += get_class_02_expression_numeric_or_string(&w);
			}
		}
		else if (*w.src == '#')
		{
			w.src += get_literal(&w, ',');
			w.src += get_class_06_expression_numeric(&w);
		}
		else if (*w.src == ',')
		{
			w.src += get_literal(&w, ',');
		}
		else if (*w.src == ';')
		{
			w.src += get_literal(&w, ';');
		}
		else if (*w.src == '\'')
		{
			w.src += get_literal(&w, '\'');
		}
		else
		{
			w.src += get_class_02_expression_numeric_or_string(&w);
		}

		w.src += whitespace(&w);
		if (is_eol(&w))
			break;

		ERROR_CHECK();
	}

	return copy_error(t, &w);
}

int get_plot_circle_draw_statement(TSTATE *t, const char *const plot)
{
	TSTATE w = *t;
	w.src += whitespace(&w);

	//PLOT/CIRCLE/DRAW
	//[
	//INK/PAPER/INVERSE/OVER/FLASH/BRIGHT n |
	//[,;']*
	//]
	//number,number
	//if PLOT nothing
	//if DRAW optional number
	//if CIRCLE number

	for (;;)
	{
		KEYWORD *keyword;
		if (is_keyword(&w, &keyword))
		{
			if (strcmp(keyword->name, "INK") == 0 ||
				strcmp(keyword->name, "PAPER") == 0 ||
				strcmp(keyword->name, "INVERSE") == 0 ||
				strcmp(keyword->name, "OVER") == 0 ||
				strcmp(keyword->name, "FLASH") == 0 ||
				strcmp(keyword->name, "BRIGHT") == 0)
			{
				w.src += get_keyword(&w, &keyword);
				w.src += get_class_06_expression_numeric(&w);
			}
			else
			{
				//it's something else, which means it's an expression
				break;
			}
		}
		else if (*w.src == ',')
		{
			w.src += get_literal(&w, ',');
		}
		else if (*w.src == ';')
		{
			w.src += get_literal(&w, ';');
		}
		else if (*w.src == '\'')
		{
			w.src += get_literal(&w, '\'');
		}
		else
		{
			break;
		}

		w.src += whitespace(&w);
		if (is_eol(&w))
			break;

		ERROR_CHECK();
	}

	w.src += get_class_06_expression_numeric(&w);
	w.src += get_literal(&w, ',');
	w.src += get_class_06_expression_numeric(&w);
	w.src += whitespace(&w);

	if (*w.src == ',' && strcmp(plot, "PLOT") != 0)
	{
		w.src += get_literal(&w, ',');
		w.src += get_class_06_expression_numeric(&w);
	}

	return copy_error(t, &w);
}

int get_statement(TSTATE *t)
{
	TSTATE w = *t;
	KEYWORD *keyword;

	//keyword, followed by stuff determined by syntax table
	w.src += get_keyword(&w, &keyword);

	ERROR_CHECK();

	const char *p = keyword->program;
	while (*p != '\0')
	{
		switch (*p)
		{
			case '0': break;//done
			case '1': w.src += get_class_01_variable_name(&w); break;
			case '2': w.src += get_class_02_expression_numeric_or_string(&w); break;
			case '3': w.src += get_class_03_optional_expression_numeric(&w); break;
			case '4': w.src += get_class_04_single_character(&w); break;
			case '5': w.src += get_class_05_set_of_items(&w); break;
			case '6': w.src += get_class_06_expression_numeric(&w); break;
			case '7': w.src += get_class_07_colour(&w); break;
			case '8': w.src += get_class_08_two_numerical_expressions(&w); break;
			case '9': w.src += get_class_09_colour_with_two_numerical_expressions(&w); break;
			case 'A': w.src += get_class_0A_expression_string(&w); break;
			case 'B': w.src += get_class_0B_cassette_routines(&w); break;

			case 'k':
			{
				p++;
				char len = *p - '0';
				KEYWORD *kw;
				w.src += get_keyword(&w, &kw);
				if (strncmp(kw->name, p+1, len) != 0)
					set_error(&w, EXPECTED_KEYWORD, 0, 0);
				p += len;
			}
			break;

			case 'S': w.src += get_move(&w); break;
			case 't': w.src += get_cat(&w); break;
			case 'U': w.src += get_microdrive(&w); break;
			case 'V': w.src += get_plot_circle_draw_statement(&w, keyword->name); break;
			case 'W': w.src += get_print_input_statement(&w); break;
			case 'X': w.src += get_sequence_of_statements(&w); break;
			case 'Y': w.src += get_comment(&w); break;
			case 'Z': w.src += get_optional_step(&w); break;

			default:
				w.src += get_literal(&w, *p);
				break;
		}
		p++;

		ERROR_CHECK();
	}

	return copy_error(t, &w);
}

int get_sequence_of_statements(TSTATE *t)
{
	TSTATE w = *t;

	do
	{
		w.src += get_statement(&w);
		
		ERROR_CHECK();
		w.src += whitespace(&w);

		if (*w.src == ':')
			w.src += get_literal(&w, ':');

	} while (!is_eol(&w));

	return copy_error(t, &w);
}

char *sample1, *sample2;
static char token_error[1000];
char *tokenise(char *txt)
{
	clear_tokens();

	size_t len = strlen(txt);

	TSTATE w = { 0 };
	w.src = txt;

	while (w.src < txt+len && !w.error)
	{
		//line number 0-65535 followed by statements separated by ':'
		w.src += get_linenumber(&w);
		w.src += get_sequence_of_statements(&w);
		w.src += whitespacenl(&w);
	}

	//dump_tokens();

	char *rv = NULL;
	if (!w.error)
	{
		convert_tokens();
		sprintf_s(token_error, 1000, "0 OK, 0:1");
		rv = token_error;
	}
	else
	{
		char posn[100];
		sprintf_s(posn, 100, "at line %u column %u", (unsigned int)w.line_number, (unsigned int)(w.errpos - w.line_start));
		switch (w.error)
		{
			case EXPECTED_LITERAL:
				sprintf_s(token_error, 1000, "Expected literal %c, got %c, %s", w.errval1, w.errval2, posn);
				break;
			case EXPECTED_KEYWORD:
				sprintf_s(token_error, 1000, "Expected a KEYWORD %s", posn);
				break;
			case COLOUR_EXPRESSION:
				sprintf_s(token_error, 1000, "Unhandled Colour Expression %s", posn);
				break;
			case COLOUR_EXPRESSION_2:
				sprintf_s(token_error, 1000, "Unhandled Double Colour Expression %s", posn);
				break;
			case EXPECTED_LETTER:
				sprintf_s(token_error, 1000, "Expected letter %s", posn);
				break;
			case UNTERMINATED_STRING:
				sprintf_s(token_error, 1000, "Unterminated string %s", posn);
				break;
			case UNEXPECTED_KEYWORD:
				sprintf_s(token_error, 1000, "Unexpected KEYWORD %s", posn);
				break;
			default:
				sprintf_s(token_error, 1000, "Unknown error %s", posn);
				break;
		}
		rv = token_error;
	}

	clear_tokens();

	return rv;
}

char *basic_sample()
{
	return sample1;
}
/*
10 LET f$="["
20 LET n$="[[1], 2, [[3,4], 5], [[[]]], [[[6]]], 7, 8 []]"
30 FOR i=2 TO (LEN n$)-1
40 IF n$(i)>"/" AND n$(i)<":" THEN LET f$=f$+n$(i): GO TO 60
50 IF n$(i)="," AND f$(LEN f$)<>"," THEN LET f$=f$+","
60 NEXT i
70 LET f$=f$+"]": PRINT f$
*/

char *sample1 =
"10 LET f$=\"[\"\r\n"\
"20 LET n$=\"[[1], 2, [[3,4], 5], [[[]]], [[[6]]], 7, 8 []]\"\r\n"\
"30 FOR i=2 TO (LEN n$)-1\r\n"\
"40 IF n$(i)>\"/\" AND n$(i)<\":\" THEN LET f$=f$+n$(i): GO TO 60\r\n"\
"50 IF n$(i)=\",\" AND f$(LEN f$)<>\",\" THEN LET f$=f$+\",\"\r\n"\
"60 NEXT i\r\n"\
"70 LET f$=f$+\"]\": PRINT f$\r\n";


/*
10 LET AYS=65533
20 LET AYW=49149
30 OUT AYS,0
40 OUT AYW,242
50 OUT AYS,1
60 OUT AYW,7
70 OUT AYS,8
80 OUT AYW,15
90 OUT AYS,7
100 OUT AYW,BIN 11111110
*/

char *sample2 =
"10 LET AYS=65533\r\n"\
"20 LET AYW=49149\r\n"\
"30 OUT AYS,0\r\n"\
"40 OUT AYW,242\r\n"\
"50 OUT AYS,1\r\n"\
"60 OUT AYW,7\r\n"\
"70 OUT AYS,8\r\n"\
"80 OUT AYW,15\r\n"\
"90 OUT AYS,7\r\n"\
"100 OUT AYW,BIN 11111110\r\n";

