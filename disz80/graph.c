//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include "resource.h"

#include "graphviz\cgraph.h"
#include "graphviz\gvc.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "dbgmem.h"
#include "logger.h"
#include "trace.h"
#include "graph.h"
#include "daos.h"
#include "disview.h"
#include "dasm.h"

#define MARGIN_X 10
#define MARGIN_Y 10

typedef struct
{
	int maxwidth, maxheight;
} SIZEINFO;

static HMODULE cgraph_dll = NULL;
static HMODULE gvc_dll = NULL;

extern HINSTANCE dis_instance;
extern HWND dis_window;

int graph_init(void)
{
#ifdef _WIN64
	SetDllDirectory(".\\graphviz\\x64");
#else
	SetDllDirectory(".\\graphviz\\x86");
#endif

	cgraph_dll = LoadLibrary("cgraph.dll");
	gvc_dll = LoadLibrary("gvc.dll");
	
	GVC_t *gvc = gvContext();
	elog("[GVC] version %s\n", gvcVersion(gvc));
	//elog("[GVC] %s\n", gvcBuildDate(gvc));
	//doesn't seem any way of telling how much is here
	//char **info = gvcInfo(gvc);
	//while (*info != NULL && *info != (char *)-1)
	//	elog("[GVC]\t%s\n", *info++);
	//gvPluginList
	//how to free what it returns?
	gvFreeContext(gvc);

	return cgraph_dll == NULL || gvc_dll == NULL;
}

void graph_shutdown(void)
{
	if (gvc_dll)
		FreeLibrary(gvc_dll);
	if (cgraph_dll)
		FreeLibrary(cgraph_dll);
}

int graph_test_on = 0;
void graphtest(void)
{
	//can't use the built-in because then the gchart.dll cannot be delay-loaded.

	if (!graph_test_on)
		return;

	struct Agdesc_s strictdirected = { 1, 1, 0, 1 };

	Agraph_t *g;
	g = agopen("G", strictdirected, NULL);

	Agnode_t *n0, *n1, *n2, *n3, *n4;
	n0 = agnode(g, "n0", TRUE);
	n1 = agnode(g, "n1", TRUE);
	n2 = agnode(g, "n2", TRUE);
	n3 = agnode(g, "n3", TRUE);
	n4 = agnode(g, "n4", TRUE);

	Agedge_t *e0, *e1, *e2, *e3, *e4, *e5;
	e0 = agedge(g, n0, n1, "e0", TRUE);
	e1 = agedge(g, n1, n2, "e1", TRUE);
	e2 = agedge(g, n2, n3, "e2", TRUE);
	e3 = agedge(g, n2, n4, "e3", TRUE);
	e4 = agedge(g, n4, n0, "e3", TRUE);
	e5 = agedge(g, n1, n3, "e5", TRUE);

	agattr(g, AGRAPH, "splines", "polyline");
	agattr(g, AGNODE, "shape", "box");

	Agsym_t *w, *h;
	w = agattr(g, AGNODE, "width", "1.0");
	h = agattr(g, AGNODE, "height", "1.0");
	agxset(n4, w, "0.2");
	agxset(n4, h, "0.3");

	GVC_t *gvc;
	gvc = gvContext();

	int err;
	err = gvLayout(gvc, g, "dot");
	err = gvRenderFilename(gvc, g, "dot", "gv.txt");
	err = gvRenderFilename(gvc, g, "png", "gv.png");
	assert(err == 0);

	char *graph=NULL; unsigned int length;
	err = gvRenderData(gvc, g, "dot", &graph, &length);
	if (graph)
	{
		elog("%s", graph);
		gvFreeRenderData(graph);
	}
	gvFreeLayout(gvc, g);

	gvFreeContext(gvc);

	agclose(g);

	return;
}

static char **tok(char *s)
{
	static char *bits[100];
	static char dup[2000];
	strcpy_s(dup, 2000, s);
	char *context=NULL;
	int idx = 0;
	bits[idx] = strtok_s(dup, " ,", &context);
	while (bits[idx] != NULL && idx < 100)
		bits[++idx] = strtok_s(NULL, " ,", &context);
	bits[++idx] = NULL;
	return bits;
}

static INT_PTR CALLBACK GraphDlgProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	wParam;
	int whichBar;
	SIZEINFO *s = (SIZEINFO *)GetWindowLongPtr(hwndDlg, GWLP_USERDATA);

	switch (message)
	{
		case WM_INITDIALOG:
		{
			SCROLLBARINFO sbi = { 0 };
			sbi.cbSize = sizeof sbi;
			GetScrollBarInfo(hwndDlg, OBJID_HSCROLL, &sbi);
			SCROLLINFO si = { 0 };
			si.cbSize = sizeof si;
			si.fMask = SIF_ALL;
			GetScrollInfo(hwndDlg, SB_HORZ, &si);
			return 0;
		}
		case WM_ERASEBKGND:
			return 1;

		case WM_CTLCOLORSTATIC:
		{
			char name[10];
			RealGetWindowClass((HWND)lParam, name, 10);
			if (strcmp(name, "Edit")==0)
				return (INT_PTR)GetStockObject(WHITE_BRUSH);
			return (INT_PTR)GetStockObject(DKGRAY_BRUSH);
		}
		//case WM_COMMAND:
		//	if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		//	{
		//		DestroyWindow(hwndDlg);
		//		PostQuitMessage(LOWORD(wParam) - 1);
		//		return (INT_PTR)TRUE;
		//	}
		//	break;
		//case WM_PAINT:
		//{
		//	PAINTSTRUCT paint = { 0 };
		//	int e;
		//	paint.hdc = (HDC)wParam;
		//	e;
		//	if (GetUpdateRect(hwndDlg, &paint.rcPaint, TRUE))
		//	{
		//		BeginPaint(hwndDlg, &paint);
		//		EndPaint(hwndDlg, &paint);
		//	}
		//}
		//break;

		//case WM_ENTERSIZEMOVE:
		//	SendMessage(hwndDlg, WM_SETREDRAW, FALSE, 0);
		//	break;

		//case WM_EXITSIZEMOVE:
		//	SendMessage(hwndDlg, WM_SETREDRAW, TRUE, 0);
		//	InvalidateRect(hwndDlg, NULL, TRUE);
		//	break;

		//case WM_SIZING:
		//	if (s && s->maxwidth != 0 && s->maxheight != 0)
		//	{
		//		RECT *r = (RECT *)lParam;
		//		//elog("s (%d,%d)-(%d,%d)\n", r->left, r->top, r->right, r->bottom);
		//		int width = r->right - r->left;
		//		int height= r->bottom - r->top;
		//		if (width > s->maxwidth) width = s->maxwidth;
		//		if (height > s->maxheight) height = s->maxheight;
		//		SetWindowPos(hwndDlg, NULL, 0, 0, width, height, SWP_NOZORDER | SWP_NOMOVE | SWP_NOREDRAW);
		//	}
		//	break;

		case WM_SIZE:
			if (s && s->maxwidth != 0 && s->maxheight != 0)
			{
				RECT box = { 0 };
				box.right = GET_X_LPARAM(lParam);
				box.bottom = GET_Y_LPARAM(lParam);

				box.right += GetSystemMetrics(SM_CXVSCROLL);
				box.bottom += GetSystemMetrics(SM_CYHSCROLL);
				AdjustWindowRectEx(&box, GetWindowLong(hwndDlg, GWL_STYLE), FALSE, GetWindowLong(hwndDlg, GWL_EXSTYLE));
				int width = box.right - box.left;
				int height = box.bottom - box.top;
				if (width > s->maxwidth) width = s->maxwidth;
				if (height > s->maxheight) height = s->maxheight;
				SetWindowPos(hwndDlg, NULL, 0, 0, width, height, SWP_NOZORDER | SWP_NOMOVE | SWP_NOREPOSITION);
				return 0;
			}

		case WM_HSCROLL:
			whichBar = SB_HORZ;
			goto doBar;

		case WM_VSCROLL:
			whichBar = SB_VERT;
doBar:
			if (lParam == 0)
			{
				SCROLLINFO si = { 0 };
				si.cbSize = sizeof si;
				si.fMask = SIF_ALL;
				GetScrollInfo(hwndDlg, whichBar, &si);

				switch (LOWORD(wParam))
				{
					case SB_TOP: si.nPos = si.nMin; break;
					case SB_BOTTOM: si.nPos = si.nMax; break;
					case SB_LINEUP: si.nPos--; break;
					case SB_LINEDOWN: si.nPos++; break;
					case SB_PAGEUP: si.nPos -= si.nPage; break;
					case SB_PAGEDOWN: si.nPos += si.nPage; break;
					case SB_THUMBTRACK: si.nPos = si.nTrackPos; break;
					case SB_ENDSCROLL:
					case SB_THUMBPOSITION: return 0;
					default:
						assert(0);
				}
				si.fMask = SIF_POS;
				SetScrollInfo(hwndDlg, whichBar, &si, TRUE);
				GetScrollInfo(hwndDlg, whichBar, &si);
				return 0;
			}
			break;

		case WM_CLOSE:
			free(s);
			EndDialog(hwndDlg, 0);
			return 0;
	}
	return (INT_PTR)FALSE;
}

static int nc(const void *s0, const void *s1)
{
	BRANCH_NODE *bs0 = *(BRANCH_NODE **)s0;
	BRANCH_NODE *bs1 = *(BRANCH_NODE **)s1;
	return (int)bs0->start - (int)bs1->start;
}

static double window_dpi = 96.0;
static double input_scale = 1.0;
static double output_scale = 1.0;

static int ptsToDev(char *c)
{
	return (int)ceil(window_dpi / 72.0 * atof(c) * output_scale);
}
static int insToDev(char *c)
{
	return (int)ceil(window_dpi * atof(c) * output_scale);
}
static double devToIns(int d)
{
	return d / window_dpi;
}
static HWND setup_window(SIZE *size, HFONT *font)
{
	HWND window = CreateDialog(dis_instance, (LPCTSTR)IDD_GRAPH, dis_window, GraphDlgProc);

	LOGFONT consolas = { 0 };
	HFONT xfont = (HFONT)SendMessage(window, WM_GETFONT, 0, 0);
	GetObject(xfont, sizeof consolas, &consolas);
	consolas.lfHeight = (int)((float)consolas.lfHeight * input_scale);
	consolas.lfQuality = PROOF_QUALITY;
	strcpy_s(consolas.lfFaceName, 32, "Consolas");
	*font = CreateFontIndirect(&consolas);
	SendMessage(window, WM_SETFONT, (WPARAM)*font, TRUE);

	window_dpi = (double)GetDpiForWindow(window);
	size->cx = size->cy = 0;
	{
		HDC edc = GetDC(NULL);

		HFONT of = SelectObject(edc, *font);

		char *test = "01234567801234567801234678012345678901234\r\n0\r\n";

		RECT rect = { 0 };
		DrawText(edc, test, -1, &rect, DT_CALCRECT | DT_EDITCONTROL);
		elog("DT (%d,%d)-(%d,%d)\n", rect.left, rect.top, rect.right, rect.bottom);
		size->cx = rect.right;
		size->cy = rect.bottom / 2;

		SelectObject(edc, of);
		ReleaseDC(NULL, edc);
	}
	return window;
}

static char *disassemble(char *asm, unsigned int pc, unsigned int end, int *n_lines, int max_lines)
{
	char *t = asm;
	do
	{
		DASM_RES res;
		int sz;
		sz = DAsmEx((unsigned short)pc, &res);
		t += sprintf_s(t, 90000, " %04X %s\r\n", pc, res.instr);
		pc += sz;
		(*n_lines)++;
	} while (pc < end && (max_lines == -1 || *n_lines < max_lines));

	if (*n_lines == max_lines)
	{
		t += sprintf_s(t, 90000, "...");
		(*n_lines)++;
	}
	return asm;
}

static HWND create_edit_for_node(BRANCH_NODE *node, HWND window, HFONT font, int *n_lines)
{
	static char asm[100000];
	char *t = asm;
	unsigned int pc = node->start;
	HWND editWnd;

	*n_lines = 0;
	t += sprintf_s(t, asm - t + 100000, "%s:\r\n", pc_to_label((unsigned short)pc, 0, 0, 0));
	(*n_lines)++;

	disassemble(t, pc, node->end, n_lines, 4);

	editWnd = CreateWindowEx(WS_EX_TOPMOST, "Edit", "", WS_CHILD | ES_MULTILINE | ES_READONLY, 0, 0, 1, 1, window, NULL, dis_instance, 0);
	SendMessage(editWnd, WM_SETFONT, (WPARAM)font, FALSE);
	SendMessage(editWnd, WM_SETTEXT, 0, (LPARAM)asm);
	SetWindowLongPtr(editWnd, GWLP_USERDATA, (LONG_PTR)node);

	{
		int unused;
		t = asm;
		t += sprintf_s(t, asm - t + 100000, "%s:\r\n", pc_to_label((unsigned short)pc, 0, 0, 0));
		disassemble(t, pc, node->end, &unused, -1);

		HWND tooltip;
		tooltip = CreateWindowEx(WS_EX_TOPMOST, TOOLTIPS_CLASS, NULL, WS_POPUP, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, window, NULL, dis_instance, 0);
		SendMessage(tooltip, WM_SETFONT, (WPARAM)font, FALSE);
		TOOLINFO ti = { 0 };
		ti.cbSize = sizeof ti;
		ti.hwnd = window;
		ti.uFlags = TTF_IDISHWND | TTF_SUBCLASS;
		ti.uId = (UINT_PTR)editWnd;
		ti.lpszText = asm;
		SendMessage(tooltip, TTM_ADDTOOL, 0, (LPARAM)&ti);
		SendMessage(tooltip, TTM_SETMAXTIPWIDTH, 0, 200);
	}

	return editWnd;
}

void graph_nodes(BRANCH_NODE **branches, int node_idx)
{
	BRANCH_NODE **sorter = malloc(node_idx * sizeof * sorter);
	if (sorter == NULL) return;

	elog("graphing...");

	output_scale = 1.0;
	input_scale = 0.8;

	for (int i = 0; i < node_idx; i++)
		sorter[i] = branches[i];
	qsort(sorter, node_idx, sizeof *sorter, nc);

	struct Agdesc_s strictdirected = { 1, 1, 0, 1 };

	Agraph_t *g;
	g = agopen("G", strictdirected, NULL);

	//graph attributes
	Agsym_t *bb = agattr(g, AGRAPH, "bb", "0,0,0,0");
	agattr(g, AGRAPH, "splines", "polyline");
	agattr(g, AGNODE, "shape", "box");
	//agattr(g, AGNODE, "overlap", "false");

	//node attrbiutes
	Agsym_t *w = agattr(g, AGNODE, "width", "1.0");
	Agsym_t *h = agattr(g, AGNODE, "height", "1.0");
	Agsym_t *id = agattr(g, AGNODE, "id", "-1");
	Agsym_t *pos = agattr(g, AGNODE, "pos", "0,0");

	//edge attributes
	Agsym_t *epos = agattr(g, AGEDGE, "pos", "0,0");

	Agnode_t **node = malloc(node_idx * sizeof * node);
	for (int i = 0; i < node_idx; i++)
	{
		char tmp[1000];
		
		sprintf_s(tmp, 1000, "%s",  pc_to_label(sorter[i]->start,0,0, 0));
		node[i] = agnode(g, tmp, TRUE);
		
		sprintf_s(tmp, 1000, "%d", i);
		agxset(node[i], id, tmp);

		sorter[i]->node = node[i];
	}

	int n_edges = 0;
	for (int i = 0; i < node_idx; i++)
		n_edges += !!sorter[i]->nottaken + !!sorter[i]->taken;

	Agedge_t **edges = malloc(sizeof *edges * n_edges), **edge = edges;
	if (edges == NULL) return;

	SIZE size;
	HFONT font;
	HWND window = setup_window(&size, &font);

	elog("creating nodes and edges...");
	
	//width and height are in inches (72 points), everything else is in points
	//node position is the middle of the box
	//need to convert points to device pixels (72 points is usually 96dp)

	for (int i = 0; i < node_idx; i++)
	{
		int n_lines = 0;
		sorter[i]->edit = create_edit_for_node(sorter[i], window, font, &n_lines);
		assert(sorter[i]->edit != NULL);

		double sx = devToIns(size.cx);
		double sy = devToIns(size.cy * n_lines);

		char dim[20];

		sprintf_s(dim, 20, "%9.4f", sx);
		agxset(sorter[i]->node, w, dim);

		sprintf_s(dim, 20, "%9.4f", sy);
		agxset(sorter[i]->node, h, dim);

		if (sorter[i]->nottaken)
			*edge++ = agedge(g, sorter[i]->node, sorter[i]->nottaken->node, "", TRUE);
		if (sorter[i]->taken)
			*edge++ = agedge(g, sorter[i]->node, sorter[i]->taken->node, "", TRUE);
	}

	elog("layout...");

	GVC_t *gvc;
	gvc = gvContext();

	int err;
	static char *algs[] = {"dot", "neato", "fdp", "sfdp", "twopi", "circo", "patchwork", "osage"};
	int alg_no = 0;
	err = gvLayout(gvc, g, algs[alg_no]);
	assert(err == 0);

	/*
	char fname[MAX_PATH];
	
	sprintf_s(fname, MAX_PATH, "gv%04X.txt", sorter[0]->start);
	err = gvRenderFilename(gvc, g, "dot", fname);

	sprintf_s(fname, MAX_PATH, "gv%04X.png", sorter[0]->start);
	err = gvRenderFilename(gvc, g, "png", fname);
	
	char *graph = NULL; unsigned int length;
	err = gvRenderData(gvc, g, "dot", &graph, &length);
	if (graph)
	{
		OutputDebugString(graph);
		OutputDebugString("\n");
		gvFreeRenderData(graph);
	}
	*/
	//if we don't render anything, need to run this to populate all the graph data
	attach_attrs(g);

	gvFreeLayout(gvc, g);

	elog("rendering...\n");

	int boxw, boxh, fboxh;
	char **bbox = tok(agxget(g, bb));
	boxw = ptsToDev(bbox[2]);
	boxh = ptsToDev(bbox[3]);

	//scale to 1024x1024 max
	{
		output_scale = min(1.0, 800.0 / max(boxw, boxh));
		elog("output scale %f\n", output_scale);
		boxw = (int)((float)boxw * output_scale);
		boxh = (int)((float)boxh * output_scale);
	}

	fboxh = boxh;

	boxw += 2 * MARGIN_X;
	boxh += 2 * MARGIN_Y;

	RECT box = { 0 };
	box.right = boxw + GetSystemMetrics(SM_CXVSCROLL);
	box.bottom = boxh + GetSystemMetrics(SM_CYHSCROLL);
	AdjustWindowRectEx(&box, GetWindowLong(window, GWL_STYLE), FALSE, GetWindowLong(window, GWL_EXSTYLE));

	BOOL rb;
	rb = SetWindowPos(window, NULL, 0, 0, box.right-box.left, box.bottom - box.top, SWP_NOMOVE | SWP_NOZORDER);
	assert(rb != 0);

	SIZEINFO *sizeinf = calloc(1, sizeof * sizeinf);
	sizeinf->maxwidth = box.right - box.left ;
	sizeinf->maxheight = box.bottom - box.top ;
	SetWindowLongPtr(window, GWLP_USERDATA, (LONG_PTR)sizeinf);

	HDC hdc = CreateCompatibleDC(NULL);
	
	SetGraphicsMode(hdc, GM_ADVANCED);
	XFORM pxf = { 0 };
	pxf.eM11 = 1.0f;
	pxf.eM22 = -1.0f;
	pxf.eDx = (float)MARGIN_X;
	pxf.eDy = (float)fboxh+(float)MARGIN_Y;
	SetWorldTransform(hdc, &pxf);
	
	//32 bit
	//HBITMAP bitmap = CreateCompatibleBitmap(GetDC(NULL), boxw, boxh);
	//1 bit
	HBITMAP bitmap = CreateCompatibleBitmap(hdc, boxw, boxh);
	assert(bitmap != NULL);
	HBITMAP obm = SelectObject(hdc, bitmap);
	
	{
		BITMAP bmp;
		GetObject(bitmap, sizeof(BITMAP), (LPSTR)&bmp);
		assert(bmp.bmBitsPixel == 1);
	}

	//rv = SendDlgItemMessage(window, IDC_GRAPHPIC, WM_SIZE, SIZE_RESTORED, (boxh << 16) | boxw);
	rb = SetWindowPos(GetDlgItem(window, IDC_GRAPHPIC), NULL, 0, 0, boxw, boxh, SWP_NOMOVE | SWP_NOZORDER);
	assert(rb != 0);

	HBRUSH pen = SelectObject(hdc, GetStockObject(DC_PEN));
	HBRUSH brush = SelectObject(hdc, GetStockObject(DC_BRUSH));

	SetDCPenColor(hdc, RGB(0xff, 0xff, 0xff));
	SetDCBrushColor(hdc, RGB(0, 0, 0));

	for (int i = 0; i < node_idx; i++)
	{
		Agnode_t *n = sorter[i]->node;
		int width = insToDev(agxget(n, w));
		int height = insToDev(agxget(n, h));

		char **ppos = tok(agxget(n, pos));
		int x = ptsToDev(ppos[0]);
		int y = ptsToDev(ppos[1]);

		x -= width / 2;
		y -= height / 2;

		//Rectangle(hdc, x, y, x + width, y + height);
		RECT r;
		r.left = x;
		r.top = y;
		r.right = x + width;
		r.bottom = y + height;
		FillRect(hdc, &r, brush);
	}

	for (int i = 0; i < node_idx; i++)
	{
		Agnode_t *n = sorter[i]->node;
		int width = insToDev(agxget(n, w));
		int height = insToDev(agxget(n, h));

		char **ppos = tok(agxget(n, pos));
		int x = ptsToDev(ppos[0]);
		int y = ptsToDev(ppos[1]);
		//MoveWindow unaffected by SetWorldTransform
		y = fboxh - y;

		x -= width / 2;
		y -= height / 2;

		//MoveWindow unaffected by SetWorldTransform
		MoveWindow(sorter[i]->edit, x + MARGIN_X, y + MARGIN_Y, width, height, TRUE);
	}

	edge = edges;
	for (int i = 0; i < n_edges; i++)
	{
		char **pp = tok(agxget(*edge++, epos));
		int x, y;
		if (pp[3] == NULL) continue;

		x = ptsToDev(pp[3]); y = ptsToDev(pp[4]);
		MoveToEx(hdc, x, y, NULL);
		int j = 5;
		while (pp[j])
		{
			x = ptsToDev(pp[j]); y = ptsToDev(pp[j+1]);
			LineTo(hdc, x, y);
			j += 2;
		}
		int ex, ey;
		ex = ptsToDev(pp[1]); ey = ptsToDev(pp[2]);
		LineTo(hdc, ex, ey);

		//arrowhead
		double dx = (double)(ex - x);
		double dy = (double)(ey - y);
		double a = atan2(dy, dx);
		const double arrowlen = max(2.0,7.0 * output_scale);
		double ax = ex + arrowlen * cos(a + M_PI * 1.1);
		double ay = ey + arrowlen * sin(a + M_PI * 1.1);
		LineTo(hdc, (int)ax, (int)ay);
		MoveToEx(hdc, ex, ey, NULL);
		ax = ex + arrowlen * cos(a - M_PI * 1.1);
		ay = ey + arrowlen * sin(a - M_PI * 1.1);
		LineTo(hdc, (int)ax, (int)ay);
	}

	ShowWindow(window, SW_SHOWNORMAL);
	for (int i = 0; i < node_idx; i++)
		ShowWindow(sorter[i]->edit, SW_SHOW);

	SelectObject(hdc, obm);
	SelectObject(hdc, pen);
	SelectObject(hdc, brush);

	LRESULT rv;
	rv = SendDlgItemMessage(window, IDC_GRAPHPIC, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)bitmap);
	HBITMAP img = (HBITMAP)SendDlgItemMessage(window, IDC_GRAPHPIC, STM_GETIMAGE, IMAGE_BITMAP, 0);
	assert(img == bitmap);
	InvalidateRect(window, NULL, TRUE);

	//ModifyWorldTransform(hdc, NULL, MWT_IDENTITY);
	//e3 = SetGraphicsMode(hdc, GM_COMPATIBLE);

	//DeleteDC(hdc);
	//DeleteObject(bitmap);

	gvFreeContext(gvc);

	agclose(g);

	free(edges);
	free(node);
	free(sorter);

	elog("done\n");
}
