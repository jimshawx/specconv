//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "coverage.h"
#include "daos.h"
#include "trace.h"
#include "labels.h"
#include "dbgmem.h"

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
typedef struct
{
	int count;
	JUMP_TYPE type;
} JUMP_TARGET;

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
int code_stats[7][256];
MK_TYPE coverage[65536];
unsigned short pc_track[PC_TRACK_SIZE];
int pc_track_pos=0;

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static JUMP_TARGET jumptargets[65536];

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void do_stats(int type, unsigned char i)
{
	code_stats[type][i]++;
}

void reset_coverage(void)
{
	memset(code_stats, 0, sizeof code_stats);
	memset(coverage, 0, sizeof coverage);
	memset(jumptargets, 0, sizeof jumptargets);
	pc_track_pos = 0;
}

void mark_type(unsigned short address, MK_TYPE type)
{
	coverage[address] |= type;
}

void mark_jump(unsigned short address, JUMP_TYPE type)
{
	jumptargets[address].type = type;
	jumptargets[address].count++;
}

void pc_trace(unsigned short address)
{
	pc_track[pc_track_pos++] = address;
	pc_track_pos %= PC_TRACK_SIZE;
}

void apply_coverage(void)
{
	bulk_insert_begin();

	//no point doing the ROM or the screen
	int pc = 16384 + 6912;
	while (pc < 65536)
	{
		ZX_TYPE zx = { 0 };

		if (coverage[pc] & MK_CODE)
		{
			//scan for the next non-code
			int scan = pc;
			while (scan < 65536 && coverage[scan] & MK_CODE)
				scan++;

			zx.id = 0;
			zx.address = (unsigned short)pc;
			zx.size = scan - pc;
			zx.source = SR_COVERAGE;
			zx.type = CODE;
			//save_type(&zx);
			make_code_with_size(zx.source, zx.address, zx.size);

			pc += zx.size;
		}
		else if (coverage[pc] & (MK_DATA_RD | MK_DATA_WR))
		{
			//scan for the next non-data
			int scan = pc;
			while (scan < 65536 && coverage[scan] & (MK_DATA_RD | MK_DATA_WR))
				scan++;

			zx.id = 0;
			zx.address = (unsigned short)pc;
			zx.size = scan - pc;
			zx.source = SR_COVERAGE;
			zx.type = DEFB;
			//save_type(&zx);
			make_data(zx.source, zx.address, zx.size);

			pc += zx.size;
		}
		else
		{
			pc++;
		}
	}

	bulk_insert_end();

	bulk_insert_begin();

	//no point doing the ROM or the screen
	for (int i = 16384; i < 65536; i++)
	{
		ZX_LABEL zx;

		if (jumptargets[i].count)
		{
			char label[100];
			if (jumptargets[i].type == JT_JUMP)
				format_label(label, 100, (unsigned short)i, LBL_JUMP);
			else if (jumptargets[i].type == JT_CALL)
				format_label(label, 100, (unsigned short)i, LBL_SUB);
			else
				assert(0);

			zx.address = (unsigned short)i;
			zx.name = label;
			zx.source = SR_COVERAGE;
			zx.type = label_type_from_jump_type(jumptargets[i].type);
			make_label(zx.name, zx.source, zx.address, zx.type);
		}
	}

	bulk_insert_end();
}
