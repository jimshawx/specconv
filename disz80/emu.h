//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

enum
{
	FLAGS_CF = 1,
	FLAGS_NF = 2,
	FLAGS_PF = 4,
	FLAGS_XF = 8,
	FLAGS_HF = 16,
	FLAGS_YF = 32,
	FLAGS_ZF = 64,
	FLAGS_SF = 128,

	ALL_FLAGS = FLAGS_CF | FLAGS_NF | FLAGS_PF | FLAGS_XF | FLAGS_HF | FLAGS_YF | FLAGS_ZF | FLAGS_SF
};

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
#pragma pack(push,1)

typedef struct
{
	unsigned char l, h;
} IXIY_HIGHLOW;

#pragma warning(push)
#pragma warning(disable:4201)
typedef struct
{
	union
	{
		IXIY_HIGHLOW b;
		unsigned short i;
	};
} IXIY_PACK;
#pragma warning(pop)

typedef struct
{
	unsigned char f,a,c,b,e,d,l,h;
} BYTE_REGS;

typedef struct
{
	unsigned short af,bc,de,hl;
} WORD_REGS;

#pragma warning(push)
#pragma warning(disable:4201)
typedef struct
{
	union
	{
		BYTE_REGS b;
		WORD_REGS w;
	};
	unsigned short sp;
	unsigned short pc;
	unsigned char i, r, r7;
	unsigned char iff1, iff2, im;
	IXIY_PACK ix[2];
	WORD_REGS exregs;
	unsigned short memptr;
} REG_PACK;
#pragma warning(pop)

#pragma pack(pop)


//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void out(unsigned short p, unsigned char d);
REG_PACK *get_regs(void);

void poke_fast(unsigned short a, unsigned char b);
unsigned char peek_fast(unsigned short a);
void poke(unsigned short a, unsigned char b);
void setr(unsigned char r);
void incr(void);
unsigned char get_imm8(void);
void execute(unsigned char i);
void interrupt(void);
void nmi();
void init_parity(void);

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
enum
{
	CS_NORMAL,
	CS_CB,
	CS_ED,
	CS_FD,
	CS_DD,
	CS_FDCB,
	CS_DDCB,
};

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
#define R_IX 0
#define R_IY 1
#define R_CBIY 2
#define R_CBIX 3
