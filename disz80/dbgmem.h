#include <crtdbg.h>

#ifdef _DEBUG
#define new new(_CLIENT_BLOCK, __FILE__, __LINE__)
#define malloc(size) _malloc_dbg(size,_CLIENT_BLOCK, __FILE__, __LINE__)
#define realloc(memblock, size) _realloc_dbg(memblock, size, _CLIENT_BLOCK, __FILE__, __LINE__)
#define calloc(number,size) _calloc_dbg(number,size, _CLIENT_BLOCK, __FILE__, __LINE__)
#define free(memblock) _free_dbg(memblock,_CLIENT_BLOCK)
#define _strdup(s) _strdup_dbg(s, _CLIENT_BLOCK, __FILE__, __LINE__)
#endif
