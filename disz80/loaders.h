//-----------------------------------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//-----------------------------------------------------------------------------------------------------------------
void detect_loader(unsigned short pc);
void run_hotpatch(unsigned char i);
void reset_hotpatches(void);
