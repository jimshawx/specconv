//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

#define _CRT_SECURE_NO_DEPRECATE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <assert.h>

#include "machine.h"
#include "files.h"
#include "daos.h"
#include "disview.h"
#include "emu.h"
#include "dbgmem.h"
#include "dasm.h"
#include "logger.h"

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------

// % - IX or IY
// \1 - immediate byte
// \2 - immediate byte offset
// \3 - immediate word (load)
// \4 - immediate word (load relative)
// \5 - immediate word (call/jump)
// \6 - immediate byte offset from IX/IY

const char *const mnemonics[256] = 
{
	"nop", "ld bc,\3", "ld (bc),a", "inc bc", "inc b", "dec b", "ld b,\1", "rlca",
	"ex af,af'", "add hl,bc", "ld a,(bc)", "dec bc", "inc c", "dec c", "ld c,\1", "rrca",
	"djnz \2", "ld de,\3", "ld (de),a", "inc de", "inc d", "dec d", "ld d,\1", "rla",
	"jr \2", "add hl,de", "ld a,(de)", "dec de", "inc e", "dec e", "ld e,\1", "rra",
	"jr nz,\2", "ld hl,\3", "ld (\4),hl", "inc hl", "inc h", "dec h", "ld h,\1", "daa",
	"jr z,\2", "add hl,hl", "ld hl,(\4)", "dec hl", "inc l", "dec l", "ld l,\1", "cpl",
	"jr nc,\2", "ld sp,\3", "ld (\4),a", "inc sp", "inc (hl)", "dec (hl)", "ld (hl),\1", "scf",
	"jr c,\2", "add hl,sp", "ld a,(\4)", "dec sp", "inc a", "dec a", "ld a,\1", "ccf",
	"ld b,b", "ld b,c", "ld b,d", "ld b,e", "ld b,h", "ld b,l", "ld b,(hl)", "ld b,a",
	"ld c,b", "ld c,c", "ld c,d", "ld c,e", "ld c,h", "ld c,l", "ld c,(hl)", "ld c,a",
	"ld d,b", "ld d,c", "ld d,d", "ld d,e", "ld d,h", "ld d,l", "ld d,(hl)", "ld d,a",
	"ld e,b", "ld e,c", "ld e,d", "ld e,e", "ld e,h", "ld e,l", "ld e,(hl)", "ld e,a",
	"ld h,b", "ld h,c", "ld h,d", "ld h,e", "ld h,h", "ld h,l", "ld h,(hl)", "ld h,a",
	"ld l,b", "ld l,c", "ld l,d", "ld l,e", "ld l,h", "ld l,l", "ld l,(hl)", "ld l,a",
	"ld (hl),b", "ld (hl),c", "ld (hl),d", "ld (hl),e", "ld (hl),h", "ld (hl),l", "halt", "ld (hl),a",
	"ld a,b", "ld a,c", "ld a,d", "ld a,e", "ld a,h", "ld a,l", "ld a,(hl)", "ld a,a",
	"add b", "add c", "add d", "add e", "add h", "add l", "add (hl)", "add a",
	"adc b", "adc c", "adc d", "adc e", "adc h", "adc l", "adc (hl)", "adc a",
	"sub b", "sub c", "sub d", "sub e", "sub h", "sub l", "sub (hl)", "sub a",
	"sbc b", "sbc c", "sbc d", "sbc e", "sbc h", "sbc l", "sbc (hl)", "sbc a",
	"and b", "and c", "and d", "and e", "and h", "and l", "and (hl)", "and a",
	"xor b", "xor c", "xor d", "xor e", "xor h", "xor l", "xor (hl)", "xor a",
	"or b", "or c", "or d", "or e", "or h", "or l", "or (hl)", "or a",
	"cp b", "cp c", "cp d", "cp e", "cp h", "cp l", "cp (hl)", "cp a",
	"ret nz", "pop bc", "jp nz,\5", "jp \5",      "call nz,\5", "push bc", "add \1", "rst 00h",
	"ret z",  "ret",    "jp z,\5",  "pfx_cb",    "call z,\5", "call \5", "adc \1", "rst 08h",
	"ret nc", "pop de", "jp nc,\5", "out (\1),a", "call nc,\5", "push de", "sub \1", "rst 10h",
	"ret c",  "exx",    "jp c,\5",  "in a,(\1)",  "call c,\5", "pfx_dd", "sbc \1", "rst 18h",
	"ret po", "pop hl", "jp po,\5", "ex (sp),hl","call po,\5", "push hl", "and \1", "rst 20h",
	"ret pe", "jp (hl)","jp pe,\5", "ex de,hl",  "call pe,\5", "pfx_ed", "xor \1", "rst 28h",
	"ret p",  "pop af",  "jp p,\5", "di",        "call p,\5", "push af", "or \1", "rst 30h",
	"ret m",  "ld sp,hl","jp m,\5", "ei",        "call m,\5", "pfx_fd", "cp \1", "rst 38h"
};

const char *const mnemC[256] =
{
	"",					"bc=\3",		"memory[bc]=a", "bc++", "b++", "b--", "b=\1", "a=rlc(a)",
	"ex_af_af_()",		"hl+=bc",	"a=memory[bc]", "bc--", "c++", "c--", "c=\1", "a=rrc(a)",
	"if (!--b) goto \2",	"de=\3",		"memory[de]=a", "de++", "d++", "d--", "d=\1", "a=rl(a)",
	"goto \2",			"hl+=de",	"a=memory[de]", "de--", "e++", "e--", "e=\1", "a=rr(a)",
	"if (!Z) goto \2",	"hl=\3",		"memory[\4]=hl", "hl++", "h++", "h--", "h=\1", "a=daa(a)",
	"if (Z) goto \2",	"hl+=hl",	"hl=memory[\4]", "hl--", "l++", "l--", "l=\1", "a=~a",
	"if (!C) goto \2",	"sp=\3",		"memory[\4]=a",  "sp++", "memory[hl]++", "memory[hl]--", "memory[hl]=\1", "C=1",
	"if (C) goto \2",	"hl+=sp",	"a=memory[\4]",  "sp--", "a++", "a--", "a=\1", "C^=1",
	"b=b", "b=c", "b=d", "b=e", "b=h", "b=l", "b=memory[hl]", "b=a",
	"c=b", "c=c", "c=d", "c=e", "c=h", "c=l", "c=memory[hl]", "c=a",
	"d=b", "d=c", "d=d", "d=e", "d=h", "d=l", "d=memory[hl]", "d=a",
	"e=b", "e=c", "e=d", "e=e", "e=h", "e=l", "e=memory[hl]", "e=a",
	"h=b", "h=c", "h=d", "h=e", "h=h", "h=l", "h=memory[hl]", "h=a",
	"l=b", "l=c", "l=d", "l=e", "l=h", "l=l", "l=memory[hl]", "l=a",
	"memory[hl]=b", "memory[hl]=c", "memory[hl]=d", "memory[hl]=e", "memory[hl]=h", "memory[hl]=l", "halt()", "memory[hl]=a",
	"a=b", "a=c", "a=d", "a=e", "a=h", "a=l", "a=memory[hl]", "a=a",
	"a+=b", "a+=c", "a+=d", "a+=e", "a+=h", "a+=l", "a+=memory[hl]", "a+=a",
	"a+=b+C", "a+=c+C", "a+=d+C", "a+=e+C", "a+=h+C", "a+=l+C", "a+=memory[hl]+C", "a+=a+C",
	"a-=b", "a-=c", "a-=d", "a-=e", "a-=h", "a-=l", "a-=memory[hl]", "a-=a",
	"a-=b+C", "a-=c+C", "a-=d+C", "a-=e+C", "a-=h+C", "a-=l+C", "a-=memory[hl]+C", "a-=a+C",
	"a&=b", "a&=c", "a&=d", "a&=e", "a&=h", "a&=l", "a&=memory[hl]", "a&=a",
	"a^=b", "a^=c", "a^=d", "a^=e", "a^=h", "a^=l", "a^=memory[hl]", "a^=a",
	"a|=b", "a|=c", "a|=d", "a|=e", "a|=h", "a|=l", "a|=memory[hl]", "a|=a",
	"Z=(a==b)", "Z=(a==c)", "Z=(a==d)", "Z=(a==e)", "Z=(a==h)", "Z=(a==l)", "Z=(a==memory[hl])", "Z=(a==a)",
	"if (!Z) return",	"bc=pop()", "if (!Z) goto \5",	"goto \5",		"if (!Z) \5()",	"push(bc)",	"a+=\1",		"_0x0000()",
	"if (Z) return",	"return",	"if (Z) goto \5",	"",				"if (Z) \5()",	"\5()",		"a+=\1+C",	"_0x0008()",
	"if (!C) return",	"de=pop()",	"if (!C) goto \5",	"out(\1,a)",		"if (!C) \5()",	"push(de)",	"a-=\1",		"_0x0010()",
	"if (C) return",	"exx()",	"if (C) goto \5",	"a=in(\1)",		"if (C) \5()",	"",			"a-=\1+C",	"_0x0018()",
	"if (!P) return",	"hl=pop()",	"if (!P) goto \5",	"ex_sp_hl()",	"if (!P) \5()",	"push(hl)",	"a&=\1",		"_0x0020()",
	"if (P) return",	"goto hl",	"if (P) goto \5",	"ex_de_hl()",	"if (P) \5()",	"",			"a^=\1",		"_0x0028()",
	"if (!S) return",	"af=pop()",	"if (!S) goto \5",	"di()",			"if (!S) \5()",	"push(af)",	"a|=\1",		"_0x0030()",
	"if (S) return",	"sp=hl",	"if (S) goto \5",	"ei()",			"if (S) \5()",	"",			"Z=(a==\1)",	"_0x0038()"
};

static const int mnemtime[256] =
{
	4 ,10 ,7 ,6 ,4 ,4 ,7 ,4 ,
	4 ,11 ,7 ,6 ,4 ,4 ,7 ,4 ,
	(13<<8)|8, 10 ,7 ,6 ,4 ,4 ,7 ,4 ,
	12 ,11 ,7 ,6 ,4 ,4 ,7 ,4 ,
	(12<<8)|7, 10 ,16 ,6 ,4 ,4 ,7 ,4 ,
	(12<<8)|7, 11 ,16 ,6 ,4 ,4 ,7 ,4 ,
	(12<<8)|7, 10 ,13 ,6 ,11 ,11 ,10 ,4 ,
	(12<<8)|7, 11 ,13 ,6 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	7 ,7 ,7 ,7 ,7 ,7 ,4 ,7 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,7 ,4 ,
	(11<<8)|5 ,10 ,10 ,10  ,(17<<8)|10 ,11 ,7 ,11 ,
	(11<<8)|5 ,10 ,10 ,8   ,(17<<8)|10 ,17 ,7 ,11 ,
	(11<<8)|5 ,10 ,10 , 11 ,(17<<8)|10 ,11 ,7 ,11 ,
	(11<<8)|5 ,4  ,10 ,11  ,(17<<8)|10 ,4  ,7 ,11 ,
	(11<<8)|5 ,10 ,10 ,19  ,(17<<8)|10 ,11 ,7 ,11 ,
	(11<<8)|5 ,4  ,10 ,4   ,(17<<8)|10 ,4  ,7 ,11 ,
	(11<<8)|5 ,10 ,10 ,4   ,(17<<8)|10 ,11 ,7 ,11 ,
	(11<<8)|5 ,6  ,10 ,4   ,(17<<8)|10 ,4  ,7 ,11 ,
};

//#define M_CDST 1
//#define M_DDST 2
//#define M_DSRC 4
//#define M_CDSTR 8
//#define M_DDSTR 16
//#define M_DSRCR 32
//#define M_CDSTS 64

static const M_TYPE mnemtype[256]=
{
	/* nop */		0,0,0,0,0,0,0,0,
	/* ex af,af\5 */	0,0,0,0,0,0,0,0,
	/* djnz \2 */	M_CDSTR,0,0,0,0,0,0,0,
	/* jr \2 */		M_CDSTR,0,0,0,0,0,0,0,
	/* jr nz,\2 */	M_CDSTR,0,M_DDSTW,0,0,0,0,0,
	/* jr z,\2 */	M_CDSTR,0,M_DSRCW,0,0,0,0,0,
	/* jr nc,\2 */	M_CDSTR,0,M_DDSTB,0,0,0,0,0,
	/* jr c,\2 */	M_CDSTR,0,M_DSRCB,0,0,0,0,0,
	/* ld b,b */	0,0,0,0,0,0,0,0,
	/* ld c,b */	0,0,0,0,0,0,0,0,
	/* ld d,b */	0,0,0,0,0,0,0,0,
	/* ld e,b */	0,0,0,0,0,0,0,0,
	/* ld h,b */	0,0,0,0,0,0,0,0,
	/* ld l,b */	0,0,0,0,0,0,0,0,
	/* ld (hl),b */	0,0,0,0,0,0,0,0,
	/* ld a,b */	0,0,0,0,0,0,0,0,
	/* add b */		0,0,0,0,0,0,0,0,
	/* adc b */		0,0,0,0,0,0,0,0,
	/* sub b */		0,0,0,0,0,0,0,0,
	/* sbc b */		0,0,0,0,0,0,0,0,
	/* and b */		0,0,0,0,0,0,0,0,
	/* xor b */		0,0,0,0,0,0,0,0,
	/* or b */		0,0,0,0,0,0,0,0,
	/* cp b */		0,0,0,0,0,0,0,0,
	/* ret nz */	0,0,M_CDST,M_CDST,M_CDSTS,0,0,0,
	/* ret z */		0,0,M_CDST,0,M_CDSTS,M_CDSTS,0,0,
	/* ret nc */	0,0,M_CDST,0,M_CDSTS,0,0,0,
	/* ret c */		0,0,M_CDST,0,M_CDSTS,0,0,0,
	/* ret po */	0,0,M_CDST,0,M_CDSTS,0,0,0,
	/* ret pe */	0,0,M_CDST,0,M_CDSTS,0,0,0,
	/* ret p */		0,0,M_CDST,0,M_CDSTS,0,0,0,
	/* ret m */		0,0,M_CDST,0,M_CDSTS,0,0,0,
};


static const MNEM_FLAGS mnemflags[256] = {
	{0,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{FLAGS_CF,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_NF | FLAGS_CF},
	{0,ALL_FLAGS},{0,ALL_FLAGS},{0,0},{0,0},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{FLAGS_CF,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_NF | FLAGS_CF},
	{0,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{FLAGS_CF,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_NF | FLAGS_CF},
	{0,0},{0,ALL_FLAGS},{0,0},{0,0},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{FLAGS_CF,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_NF | FLAGS_CF},
	{FLAGS_ZF,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{FLAGS_CF,ALL_FLAGS},
	{FLAGS_ZF,0},{0,ALL_FLAGS},{0,0},{0,0},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{FLAGS_CF,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_NF},
	{FLAGS_CF,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{0,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_NF | FLAGS_CF},
	{FLAGS_CF,0},{0,ALL_FLAGS},{0,0},{0,0},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{FLAGS_CF,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_NF | FLAGS_CF},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},

	{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},
	{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},
	{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},
	{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},
	{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},
	{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},
	{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},
	{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},

	{FLAGS_ZF,0},{0,0},{FLAGS_ZF,0},{0,0},{FLAGS_ZF,0},{0,0},{0,ALL_FLAGS},{0,0},
	{FLAGS_ZF,0},{0,0},{FLAGS_ZF,0},{0,0},{FLAGS_ZF,0},{0,0},{FLAGS_CF,ALL_FLAGS},{0,0},
	{FLAGS_CF,0},{0,0},{FLAGS_CF,0},{0,0},{FLAGS_CF,0},{0,0},{0,ALL_FLAGS},{0,0},
	{FLAGS_CF,0},{0,ALL_FLAGS},{FLAGS_CF,0},{0,0},{FLAGS_CF,0},{0,0},{FLAGS_CF,ALL_FLAGS},{0,0},
	{FLAGS_PF,0},{0,0},{FLAGS_PF,0},{0,0},{FLAGS_PF,0},{0,0},{0,ALL_FLAGS},{0,0},
	{FLAGS_PF,0},{0,0},{FLAGS_PF,0},{0,0},{FLAGS_PF,0},{0,0},{0,ALL_FLAGS},{0,0},
	{FLAGS_SF,0},{0,ALL_FLAGS},{FLAGS_SF,0},{0,0},{FLAGS_SF,0},{0,0},{0,ALL_FLAGS},{0,0},
	{FLAGS_SF,0},{0,0},{FLAGS_SF,0},{0,0},{FLAGS_SF,0},{0,0},{0,ALL_FLAGS},{0,0},
};

// CB prefix
const char *const mnemonicsCB[256] = 
{
	"rlc b", "rlc c", "rlc d", "rlc e", "rlc h", "rlc l", "rlc (hl)", "rlc a",
	"rrc b", "rrc c", "rrc d", "rrc e", "rrc h", "rrc l", "rrc (hl)", "rrc a",
	"rl b", "rl c", "rl d", "rl e", "rl h", "rl l", "rl (hl)", "rl a",
	"rr b", "rr c", "rr d", "rr e", "rr h", "rr l", "rr (hl)", "rr a",
	"sla b", "sla c", "sla d", "sla e", "sla h", "sla l", "sla (hl)", "sla a",
	"sra b", "sra c", "sra d", "sra e", "sra h", "sra l", "sra (hl)", "sra a",
	"sll b", "sll c", "sll d", "sll e", "sll h", "sll l", "sll (hl)", "sll a",
	"srl b", "srl c", "srl d", "srl e", "srl h", "srl l", "srl (hl)", "srl a",
	"bit 0,b", "bit 0,c", "bit 0,d", "bit 0,e", "bit 0,h", "bit 0,l", "bit 0,(hl)", "bit 0,a",
	"bit 1,b", "bit 1,c", "bit 1,d", "bit 1,e", "bit 1,h", "bit 1,l", "bit 1,(hl)", "bit 1,a",
	"bit 2,b", "bit 2,c", "bit 2,d", "bit 2,e", "bit 2,h", "bit 2,l", "bit 2,(hl)", "bit 2,a",
	"bit 3,b", "bit 3,c", "bit 3,d", "bit 3,e", "bit 3,h", "bit 3,l", "bit 3,(hl)", "bit 3,a",
	"bit 4,b", "bit 4,c", "bit 4,d", "bit 4,e", "bit 4,h", "bit 4,l", "bit 4,(hl)", "bit 4,a",
	"bit 5,b", "bit 5,c", "bit 5,d", "bit 5,e", "bit 5,h", "bit 5,l", "bit 5,(hl)", "bit 5,a",
	"bit 6,b", "bit 6,c", "bit 6,d", "bit 6,e", "bit 6,h", "bit 6,l", "bit 6,(hl)", "bit 6,a",
	"bit 7,b", "bit 7,c", "bit 7,d", "bit 7,e", "bit 7,h", "bit 7,l", "bit 7,(hl)", "bit 7,a",
	"res 0,b", "res 0,c", "res 0,d", "res 0,e", "res 0,h", "res 0,l", "res 0,(hl)", "res 0,a",
	"res 1,b", "res 1,c", "res 1,d", "res 1,e", "res 1,h", "res 1,l", "res 1,(hl)", "res 1,a",
	"res 2,b", "res 2,c", "res 2,d", "res 2,e", "res 2,h", "res 2,l", "res 2,(hl)", "res 2,a",
	"res 3,b", "res 3,c", "res 3,d", "res 3,e", "res 3,h", "res 3,l", "res 3,(hl)", "res 3,a",
	"res 4,b", "res 4,c", "res 4,d", "res 4,e", "res 4,h", "res 4,l", "res 4,(hl)", "res 4,a",
	"res 5,b", "res 5,c", "res 5,d", "res 5,e", "res 5,h", "res 5,l", "res 5,(hl)", "res 5,a",
	"res 6,b", "res 6,c", "res 6,d", "res 6,e", "res 6,h", "res 6,l", "res 6,(hl)", "res 6,a",
	"res 7,b", "res 7,c", "res 7,d", "res 7,e", "res 7,h", "res 7,l", "res 7,(hl)", "res 7,a",
	"set 0,b", "set 0,c", "set 0,d", "set 0,e", "set 0,h", "set 0,l", "set 0,(hl)", "set 0,a",
	"set 1,b", "set 1,c", "set 1,d", "set 1,e", "set 1,h", "set 1,l", "set 1,(hl)", "set 1,a",
	"set 2,b", "set 2,c", "set 2,d", "set 2,e", "set 2,h", "set 2,l", "set 2,(hl)", "set 2,a",
	"set 3,b", "set 3,c", "set 3,d", "set 3,e", "set 3,h", "set 3,l", "set 3,(hl)", "set 3,a",
	"set 4,b", "set 4,c", "set 4,d", "set 4,e", "set 4,h", "set 4,l", "set 4,(hl)", "set 4,a",
	"set 5,b", "set 5,c", "set 5,d", "set 5,e", "set 5,h", "set 5,l", "set 5,(hl)", "set 5,a",
	"set 6,b", "set 6,c", "set 6,d", "set 6,e", "set 6,h", "set 6,l", "set 6,(hl)", "set 6,a",
	"set 7,b", "set 7,c", "set 7,d", "set 7,e", "set 7,h", "set 7,l", "set 7,(hl)", "set 7,a"
};

const char *const mnemC_CB[256] =
{
	"b=rlc(b)", "c=rlc(c)", "d=rlc(d)", "e=rlc(e)", "h=rlc(h)", "l=rlc(l)", "memory[hl]=rlc(memory[hl])", "a=rlc(a)",
	"b=rrc(b)", "c=rrc(c)", "d=rrc(d)", "e=rrc(e)", "h=rrc(h)", "l=rrc(l)", "memory[hl]=rrc(memory[hl])", "a=rrc(a)",
	"b=rl(b)", "c=rl(c)", "d=rl(d)", "e=rl(e)", "h=rl(h)", "l=rl(l)", "memory[hl]=rl(memory[hl])", "a=rl(a)",
	"b=rr(b)", "c=rr(c)", "d=rr(d)", "e=rr(e)", "h=rr(h)", "l=rr(l)", "memory[hl]=rr(memory[hl])", "a=rr(a)",
	"b=sla(b)", "c=sla(c)", "d=sla(d)", "e=sla(e)", "h=sla(h)", "l=sla(l)", "memory[hl]=sla(memory[hl])", "a=sla(a)",
	"b=sra(b)", "c=sra(c)", "d=sra(d)", "e=sra(e)", "h=sra(h)", "l=sra(l)", "memory[hl]=sra(memory[hl])", "a=sra(a)",
	"b=sll(b)", "c=sll(c)", "d=sll(d)", "e=sll(e)", "h=sll(h)", "l=sll(l)", "memory[hl]=sll(memory[hl])", "a=sll(a)",
	"b=srl(b)", "c=srl(c)", "d=srl(d)", "e=srl(e)", "h=srl(h)", "l=srl(l)", "memory[hl]=srl(memory[hl])", "a=srl(a)",
	"Z=!!(b&0x01)", "Z=!!(c&0x01)", "Z=!!(d&0x01)", "Z=!!(e&0x01)", "Z=!!(h&0x01)", "Z=!!(l&0x01)", "Z=!!(memory[hl]&0x01)", "Z=!!(a&0x01)",
	"Z=!!(b&0x02)", "Z=!!(c&0x02)", "Z=!!(d&0x02)", "Z=!!(e&0x02)", "Z=!!(h&0x02)", "Z=!!(l&0x02)", "Z=!!(memory[hl]&0x02)", "Z=!!(a&0x02)",
	"Z=!!(b&0x04)", "Z=!!(c&0x04)", "Z=!!(d&0x04)", "Z=!!(e&0x04)", "Z=!!(h&0x04)", "Z=!!(l&0x04)", "Z=!!(memory[hl]&0x04)", "Z=!!(a&0x04)",
	"Z=!!(b&0x08)", "Z=!!(c&0x08)", "Z=!!(d&0x08)", "Z=!!(e&0x08)", "Z=!!(h&0x08)", "Z=!!(l&0x08)", "Z=!!(memory[hl]&0x08)", "Z=!!(a&0x08)",
	"Z=!!(b&0x10)", "Z=!!(c&0x10)", "Z=!!(d&0x10)", "Z=!!(e&0x10)", "Z=!!(h&0x10)", "Z=!!(l&0x10)", "Z=!!(memory[hl]&0x10)", "Z=!!(a&0x10)",
	"Z=!!(b&0x20)", "Z=!!(c&0x20)", "Z=!!(d&0x20)", "Z=!!(e&0x20)", "Z=!!(h&0x20)", "Z=!!(l&0x20)", "Z=!!(memory[hl]&0x20)", "Z=!!(a&0x20)",
	"Z=!!(b&0x40)", "Z=!!(c&0x40)", "Z=!!(d&0x40)", "Z=!!(e&0x40)", "Z=!!(h&0x40)", "Z=!!(l&0x40)", "Z=!!(memory[hl]&0x40)", "Z=!!(a&0x40)",
	"Z=!!(b&0x80)", "Z=!!(c&0x80)", "Z=!!(d&0x80)", "Z=!!(e&0x80)", "Z=!!(h&0x80)", "Z=!!(l&0x80)", "Z=!!(memory[hl]&0x80)", "Z=!!(a&0x80)",
	"b&=~0x01", "c&=~0x01", "d&=~0x01", "e&=~0x01", "h&=~0x01", "l&=~0x01", "memory[hl]&=~0x01", "a&=~0x01",
	"b&=~0x02", "c&=~0x02", "d&=~0x02", "e&=~0x02", "h&=~0x02", "l&=~0x02", "memory[hl]&=~0x02", "a&=~0x02",
	"b&=~0x04", "c&=~0x04", "d&=~0x04", "e&=~0x04", "h&=~0x04", "l&=~0x04", "memory[hl]&=~0x04", "a&=~0x04",
	"b&=~0x08", "c&=~0x08", "d&=~0x08", "e&=~0x08", "h&=~0x08", "l&=~0x08", "memory[hl]&=~0x08", "a&=~0x08",
	"b&=~0x10", "c&=~0x10", "d&=~0x10", "e&=~0x10", "h&=~0x10", "l&=~0x10", "memory[hl]&=~0x10", "a&=~0x10",
	"b&=~0x20", "c&=~0x20", "d&=~0x20", "e&=~0x20", "h&=~0x20", "l&=~0x20", "memory[hl]&=~0x20", "a&=~0x20",
	"b&=~0x40", "c&=~0x40", "d&=~0x40", "e&=~0x40", "h&=~0x40", "l&=~0x40", "memory[hl]&=~0x40", "a&=~0x40",
	"b&=~0x80", "c&=~0x80", "d&=~0x80", "e&=~0x80", "h&=~0x80", "l&=~0x80", "memory[hl]&=~0x80", "a&=~0x80",
	"b|=0x01", "c|=0x01", "d|=0x01", "e|=0x01", "h|=0x01", "l|=0x01", "memory[hl]|=0x01", "a|=0x01",
	"b|=0x02", "c|=0x02", "d|=0x02", "e|=0x02", "h|=0x02", "l|=0x02", "memory[hl]|=0x02", "a|=0x02",
	"b|=0x04", "c|=0x04", "d|=0x04", "e|=0x04", "h|=0x04", "l|=0x04", "memory[hl]|=0x04", "a|=0x04",
	"b|=0x08", "c|=0x08", "d|=0x08", "e|=0x08", "h|=0x08", "l|=0x08", "memory[hl]|=0x08", "a|=0x08",
	"b|=0x10", "c|=0x10", "d|=0x10", "e|=0x10", "h|=0x10", "l|=0x10", "memory[hl]|=0x10", "a|=0x10",
	"b|=0x20", "c|=0x20", "d|=0x20", "e|=0x20", "h|=0x20", "l|=0x20", "memory[hl]|=0x20", "a|=0x20",
	"b|=0x40", "c|=0x40", "d|=0x40", "e|=0x40", "h|=0x40", "l|=0x40", "memory[hl]|=0x40", "a|=0x40",
	"b|=0x80", "c|=0x80", "d|=0x80", "e|=0x80", "h|=0x80", "l|=0x80", "memory[hl]|=0x80", "a|=0x80",
};

static const int mnemtimeCB[256] =
{
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,12 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,12 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,12 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,12 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,12 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,12 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,12 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,12 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,15 ,8 ,
};

static const M_TYPE mnemtypeCB[256]= {0};

static const MNEM_FLAGS mnemflagsCB[256] = {
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
};

// ED prefix
const char *const mnemonicsED[256] = 
{
	"edh,00h", "edh,01h", "edh,02h", "edh,03h",	"edh,04h", "edh,05h", "edh,06h", "edh,07h",
	"edh,08h", "edh,09h", "edh,0ah", "edh,0bh",	"edh,0ch", "edh,0dh", "edh,0eh", "edh,0fh",
	"edh,10h", "edh,11h", "edh,12h", "edh,13h",	"edh,14h", "edh,15h", "edh,16h", "edh,17h",
	"edh,18h", "edh,19h", "edh,1ah", "edh,1bh",	"edh,1ch", "edh,1dh", "edh,1eh", "edh,1fh",
	"edh,20h", "edh,21h", "edh,22h", "edh,23h",	"edh,24h", "edh,25h", "edh,26h", "edh,27h",
	"edh,28h", "edh,29h", "edh,2ah", "edh,2bh",	"edh,2ch", "edh,2dh", "edh,2eh", "edh,2fh",
	"edh,30h", "edh,31h", "edh,32h", "edh,33h",	"edh,34h", "edh,35h", "edh,36h", "edh,37h",
	"edh,38h", "edh,39h", "edh,3ah", "edh,3bh",	"edh,3ch", "edh,3dh", "edh,3eh", "edh,3fh",
	"in b,(c)", "out (c),b", "sbc hl,bc", "ld (\4),bc",	"neg",		"retn", "im 0", "ld i,a",
	"in c,(c)", "out (c),c", "adc hl,bc", "ld bc,(\4)",	"edh,4ch", "reti", "edh,4eh", "ld r,a",
	"in d,(c)", "out (c),d", "sbc hl,de", "ld (\4),de",	"edh,54h", "edh,55h", "im 1", "ld a,i",
	"in e,(c)", "out (c),e", "adc hl,de", "ld de,(\4)",	"edh,5ch", "edh,5dh", "im 2", "ld a,r",
	"in h,(c)", "out (c),h", "sbc hl,hl", "ld (\4),hl",	"edh,64h", "edh,65h", "edh,66h", "rrd",
	"in l,(c)", "out (c),l", "adc hl,hl", "ld hl,(\4)",	"edh,6ch", "edh,6dh", "edh,6eh", "rld",
	"in f,(c)", "edh,71h", "sbc hl,sp", "ld (\4),sp",	"edh,74h", "edh,75h", "edh,76h", "edh,77h",
	"in a,(c)", "out (c),a", "adc hl,sp", "ld sp,(\4)",	"edh,7ch", "edh,7dh", "edh,7eh", "edh,7fh",
	"edh,80h", "edh,81h", "edh,82h", "edh,83h",	"edh,84h", "edh,85h", "edh,86h", "edh,87h",
	"edh,88h", "edh,89h", "edh,8ah", "edh,8bh",	"edh,8ch", "edh,8dh", "edh,8eh", "edh,8fh",
	"edh,90h", "edh,91h", "edh,92h", "edh,93h",	"edh,94h", "edh,95h", "edh,96h", "edh,97h",
	"edh,98h", "edh,99h", "edh,9ah", "edh,9bh",	"edh,9ch", "edh,9dh", "edh,9eh", "edh,9fh",
	"ldi", "cpi", "ini", "outi",	"edh,a4h", "edh,a5h", "edh,a6h", "edh,a7h",
	"ldd", "cpd", "ind", "outd",	"edh,ach", "edh,adh", "edh,aeh", "edh,afh",
	"ldir", "cpir", "inir", "otir",	"edh,b4h", "edh,b5h", "edh,b6h", "edh,b7h",
	"lddr", "cpdr", "indr", "otdr",	"edh,bch", "edh,bdh", "edh,beh", "edh,bfh",
	"edh,c0h", "edh,c1h", "edh,c2h", "edh,c3h",	"edh,c4h", "edh,c5h", "edh,c6h", "edh,c7h",
	"edh,c8h", "edh,c9h", "edh,cah", "edh,cbh",	"edh,cch", "edh,cdh", "edh,ceh", "edh,cfh",
	"edh,d0h", "edh,d1h", "edh,d2h", "edh,d3h",	"edh,d4h", "edh,d5h", "edh,d6h", "edh,d7h",
	"edh,d8h", "edh,d9h", "edh,dah", "edh,dbh",	"edh,dch", "edh,ddh", "edh,deh", "edh,dfh",
	"edh,e0h", "edh,e1h", "edh,e2h", "edh,e3h",	"edh,e4h", "edh,e5h", "edh,e6h", "edh,e7h",
	"edh,e8h", "edh,e9h", "edh,eah", "edh,ebh",	"edh,ech", "edh,edh", "edh,eeh", "edh,efh",
	"edh,f0h", "edh,f1h", "edh,f2h", "edh,f3h",	"edh,f4h", "edh,f5h", "edh,f6h", "edh,f7h",
	"edh,f8h", "edh,f9h", "edh,fah", "edh,fbh",	"edh,fch", "edh,fdh", "edh,feh", "edh,ffh"
};

const char *const mnemC_ED[256] =
{
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"b=in(c)", "out(c,b)",	"hl-=bc+C", "memoryw[\4]=bc", "a=-a",	"retn(); return",	"im(0)",	"i=a",
	"c=in(c)", "out(c,c)",	"hl+=bc+C", "bc=memoryw[\4]", "",		"reti(); return",	"",			"r=a",
	"d=in(c)", "out(c,d)",	"hl-=de+C", "memoryw[\4]=de", "",		"",					"im(1)",	"a=i",
	"e=in(c)", "out(c,e)",	"hl+=de+C", "de=memoryw[\4]", "",		"",					"im(2)",	"a=r",
	"h=in(c)", "out(c,h)",	"hl-=hl+C", "memoryw[\4]=hl", "",		"",					"",			"a=rrd(a)",
	"l=in(c)", "out(c,l)",	"hl+=hl+C", "hl=memoryw[\4]", "",		"",					"",			"a=rld(a)",
	"f=in(c)", "",			"hl-=sp+C", "memoryw[\4]=sp", "",		"",					"",			"",
	"a=in(c)", "out(c,a)",	"hl+=sp+C", "sp=memoryw[\4]", "",		"",					"",			"",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"memory[de++]=memory[hl++]; bc--", "if (a==memory[hl++]) bc--", "memory[hl++]=in(c)", "", "", "", "", "",
	"memory[de--]=memory[hl--]; bc--", "if (a==memory[hl--]) bc--", "out(c,memory[hl++])", "", "", "", "", "",
	"do { memory[de++]=memory[hl++]; } while (--bc)", "while (--bc && a==memory[hl++])", "do { memory[hl++]=in(c); } while (--b)", "", "", "", "", "",
	"do { memory[de--]=memory[hl--]; } while (--bc)", "while (--bc && a==memory[hl--])", "do { out(c,memory[hl++]); } while (--b)", "", "", "", "", "",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
};

static const int mnemtimeED[256] =
{
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	12 ,12 ,15 ,20 ,8 ,14 ,8 ,9 ,
	12 ,12 ,15 ,20 ,8 ,14 ,8 ,9 ,
	12 ,12 ,15 ,20 ,8 ,14 ,8 ,9 ,
	12 ,12 ,15 ,20 ,8 ,14 ,8 ,9 ,
	12 ,12 ,15 ,16 ,8 ,14 ,8 ,18 ,
	12 ,12 ,15 ,16 ,8 ,14 ,8 ,18 ,
	12 ,12 ,15 ,20 ,8 ,14 ,8 ,8 ,
	12 ,12 ,15 ,20 ,8 ,14 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	16 ,16 ,16 ,16 ,8 ,8 ,8 ,8 ,
	16 ,16 ,16 ,16 ,8 ,8 ,8 ,8 ,
	(21<<8)|16 ,(21<<8)|16 ,(21<<8)|16 ,(21<<8)|16 ,8 ,8 ,8 ,8 ,
	(21<<8)|16 ,(21<<8)|16 ,(21<<8)|16 ,(21<<8)|16 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,8 ,8 ,
};

static const M_TYPE mnemtypeED[256]=
{
	0,0,0,0,	0,0,0,0,
	0,0,0,0,	0,0,0,0,
	0,0,0,0,	0,0,0,0,
	0,0,0,0,	0,0,0,0,
	0,0,0,0,	0,0,0,0,
	0,0,0,0,	0,0,0,0,
	0,0,0,0,	0,0,0,0,
	0,0,0,0,	0,0,0,0,
	/* in b,(c) */	0,0,0,M_DDSTW,	/* neg */		0,0,0,0,
	/* in c,(c) */	0,0,0,M_DSRCW,	/* db */		0,0,0,0,
	/* in d,(c) */	0,0,0,M_DDSTW,	/* db */		0,0,0,0,
	/* in e,(c) */	0,0,0,M_DSRCW,	/* db */		0,0,0,0,
	/* in h,(c) */	0,0,0,M_DDSTW,	/* db */		0,0,0,0,
	/* in l,(c) */	0,0,0,M_DSRCW,	/* db */		0,0,0,0,
	/* in f,(c) */	0,0,0,M_DDSTW,	/* db */		0,0,0,0,
	/* in a,(c) */	0,0,0,M_DSRCW,
	0
};

static const MNEM_FLAGS mnemflagsED[256] = {
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{0,ALL_FLAGS},{0,0},{0,ALL_FLAGS},{0,0},{0,0},{0,0},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{0,ALL_FLAGS},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{0,ALL_FLAGS},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{0,ALL_FLAGS},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{0,ALL_FLAGS},{0,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{0,ALL_FLAGS},{0,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{0,ALL_FLAGS},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{0,ALL_FLAGS},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_PF | FLAGS_NF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,0},{0,0},{0,0},{0,0},
	{0,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_PF | FLAGS_NF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,0},{0,0},{0,0},{0,0},
	{0,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_PF | FLAGS_NF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,0},{0,0},{0,0},{0,0},
	{0,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_PF | FLAGS_NF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
};

// DD/FD prefix (IX/IY instead of HL)
const char *const mnemonicsXX[256] = 
{
	"fdh", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh",
	"fdh", "add i%,bc", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh",
	"fdh", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh",
	"fdh", "add i%,de", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh",
	"fdh", "ld i%,\3", "ld (\4),i%", "inc i%", "inc i%h", "dec i%h", "ld i%h,\1", "fdh",
	"fdh", "add i%,i%", "ld i%,(\4)", "dec i%", "inc i%l", "dec i%l", "ld i%l,\1", "fdh",
	"fdh", "fdh", "fdh", "fdh", "inc (i%\6)", "dec (i%\6)", "ld (i%\6),\1", "fdh",
	"fdh", "add i%,sp", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh",
	"fdh", "fdh", "fdh", "fdh", "ld b,i%h", "ld b,i%l", "ld b,(i%\6)", "fdh",
	"fdh", "fdh", "fdh", "fdh", "ld c,i%h", "ld c,i%l", "ld c,(i%\6)", "fdh",
	"fdh", "fdh", "fdh", "fdh", "ld d,i%h", "ld d,i%l", "ld d,(i%\6)", "fdh",
	"fdh", "fdh", "fdh", "fdh", "ld e,i%h", "ld e,i%l", "ld e,(i%\6)", "fdh",
	"ld i%h,b", "ld i%h,c", "ld i%h,d", "ld i%h,e", "ld i%h,i%h", "ld i%h,i%l", "ld h,(i%\6)", "ld i%h,a",
	"ld i%l,b", "ld i%l,c", "ld i%l,d", "ld i%l,e", "ld i%l,i%h", "ld i%l,i%l", "ld l,(i%\6)", "ld i%l,a",
	"ld (i%\6),b", "ld (i%\6),c", "ld (i%\6),d", "ld (i%\6),e", "ld (i%\6),h", "ld (i%\6),l", "fdh", "ld (i%\6),a",
	"fdh", "fdh", "fdh", "fdh", "ld a,i%h", "ld a,i%l", "ld a,(i%\6)", "fdh",
	"fdh", "fdh", "fdh", "fdh", "add i%h", "add i%l", "add (i%\6)", "fdh",
	"fdh", "fdh", "fdh", "fdh", "adc i%h", "adc i%l", "adc (i%\6)", "fdh",
	"fdh", "fdh", "fdh", "fdh", "sub i%h", "sub i%l", "sub (i%\6)", "fdh",
	"fdh", "fdh", "fdh", "fdh", "sbc i%h", "sbc i%l", "sbc (i%\6)", "fdh",
	"fdh", "fdh", "fdh", "fdh", "and i%h", "and i%l", "and (i%\6)", "fdh",
	"fdh", "fdh", "fdh", "fdh", "xor i%h", "xor i%l", "xor (i%\6)", "fdh",
	"fdh", "fdh", "fdh", "fdh", "or i%h", "or i%l", "or (i%\6)", "fdh",
	"fdh", "fdh", "fdh", "fdh", "cp i%h", "cp i%l", "cp (i%\6)", "fdh",
	"fdh", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh",
	"fdh", "fdh", "fdh", "pfx_cb", "fdh", "fdh", "fdh", "fdh",
	"fdh", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh",
	"fdh", "fdh", "fdh", "fdh", "fdh", "pfx_dd", "fdh", "fdh",
	"fdh", "pop i%", "fdh", "ex (sp),i%", "fdh", "push i%", "fdh", "fdh",
	"fdh", "jp (i%)", "fdh", "fdh", "fdh", "pfx_ed", "fdh", "fdh",
	"fdh", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh", "fdh",
	"fdh", "ld sp,i%", "fdh", "fdh", "fdh", "pfx_fd", "fdh", "fdh"
};

const char *const mnemC_XX[256] =
{
	"","","","","","","","",
	"","i%+=bc","","","","","","",
	"","","","","","","","",
	"","i%+=de","","","","","","",
	"","i%=\3","memory[\4]=i%","i%++","i%h++","i%h--","i%h=\1","",
	"","i%+=i%","i%=memory[\4]","i%--","i%l++","i%l--","i%l=\1","",
	"","","","","memory[i%\6]++","memory[i%\6]--","memory[i%\6]=\1","",
	"","i%+=sp","","","","","","",
	"","","","","b=i%h","b=i%l","b=memory[i%\6]","",
	"","","","","c=i%h","c=i%l","c=memory[i%\6]","",
	"","","","","d=i%h","d=i%l","d=memory[i%\6]","",
	"","","","","e=i%h","e=i%l","e=memory[i%\6]","",
	"i%h=b", "i%h=c", "i%h=d", "i%h=e", "i%h=i%h", "i%h=i%l", "h=memory[i%\6]", "i%h=a",
	"i%l=b", "i%l=c", "i%l=d", "i%l=e", "i%l=i%h", "i%l=i%l", "l=memory[i%\6]", "i%l=a",
	"memory[i%\6]=b","memory[i%\6]=c","memory[i%\6]=d","memory[i%\6]=e","memory[i%\6]=h","memory[i%\6]=l","","memory[i%\6]=a",
	"","","","","a=i%h","a=i%l","a=memory[i%\6]","",
	"","","","","a+=i%h","a+=i%l","a+=memory[i%\6]","",
	"","","","","a+=i%h+C","a+=i%l+C","a+=memory[i%\6]+C","",
	"","","","","a-=i%h","a-=i%l","a-=memory[i%\6]","",
	"","","","","a-=i%h+C","a-=i%l+C","a-=memory[i%\6]+C","",
	"","","","","a&=i%h","a&=i%l","a&=memory[i%\6]","",
	"","","","","a^=i%h","a^=i%l","a^=memory[i%\6]","",
	"","","","","a|=i%h","a|=i%l","a|=memory[i%\6]","",
	"","","","","if (a==i%h)","if (a==i%l)","if (a==memory[i%\6])","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","","","","","","","",
	"","i%=pop()","","ex_sp_i%()","","push(i%)","","",
	"","goto i%","","","","","","",
	"","","","","","","","",
	"","sp=i%","","","","","","",
};

// DD/FD prefix (IX/IY instead of HL)
static const int mnemtimeXX[256] =
{
	4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,
	4 ,15 ,4 ,4 ,4 ,4 ,4 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,
	4 ,15 ,4 ,4 ,4 ,4 ,4 ,4 ,
	4 ,14 ,20 ,10 ,8 ,8 ,8 ,4 ,
	4 ,15 ,20 ,10 ,8 ,8 ,11 ,4 ,
	4 ,4 ,4 ,4 ,23 ,23 ,19 ,4 ,
	4 ,15 ,4 ,4 ,4 ,4 ,4 ,4 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	8 ,8 ,8 ,8 ,8 ,8 ,19 ,8 ,
	8 ,8 ,8 ,8 ,8 ,8 ,19 ,8 ,
	19 ,19 ,19 ,19 ,19 ,19 ,4 ,19 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	4 ,4 ,4 ,4 ,8 ,8 ,19 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,
	4 ,4 ,4 ,23 ,4 ,4 ,4 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,
	4 ,14 ,4 ,23 ,4 ,15 ,4 ,4 ,
	4 ,8 ,4 ,4 ,4 ,8 ,4 ,4 ,
	4 ,4 ,4 ,4 ,4 ,4 ,4 ,4 ,
	4 ,10 ,4 ,4 ,4 ,4 ,4 ,4 ,
};

static const M_TYPE mnemtypeXX[256]=
{
/* nop */			0,0,0,0,0,0,0,0,
/* ex af,af */		0,0,0,0,0,0,0,0,
/* djnz \2 */		M_CDSTR,0,0,0,0,0,0,0,
/* jr \2 */			M_CDSTR,0,0,0,0,0,0,0,
/* jr nz,\2 */		M_CDSTR,0,M_DDSTW,0,0,0,0,0,
/* jr z,\2 */		M_CDSTR,0,M_DSRCW,0,0,0,0,0,
/* jr nc,\2 */		M_CDSTR,0,M_DDSTB,0,0,0,0,0,
/* jr c,\2 */		M_CDSTR,0,M_DSRCB,0,0,0,0,0,
/* ld b,b */		0,0,0,0,0,0,0,0,
/* ld c,b */		0,0,0,0,0,0,0,0,
/* ld d,b */		0,0,0,0,0,0,0,0,
/* ld e,b */		0,0,0,0,0,0,0,0,
/* ld i%h,b */		0,0,0,0,0,0,0,0,
/* ld i%l,b */		0,0,0,0,0,0,0,0,
/* ld (i%^),b */	0,0,0,0,0,0,0,0,
/* ld a,b */		0,0,0,0,0,0,0,0,
/* add b */			0,0,0,0,0,0,0,0,
/* adc b */			0,0,0,0,0,0,0,0,
/* sub b */			0,0,0,0,0,0,0,0,
/* sbc b */			0,0,0,0,0,0,0,0,
/* and b */			0,0,0,0,0,0,0,0,
/* xor b */			0,0,0,0,0,0,0,0,
/* or b */			0,0,0,0,0,0,0,0,
/* cp b */			0,0,0,0,0,0,0,0,
/* ret nz */		0,0,M_CDST,M_CDST,M_CDSTS,0,0,0,
/* ret z */			0,0,M_CDST,0,M_CDSTS,M_CDSTS,0,0,
/* ret nc */		0,0,M_CDST,0,M_CDSTS,0,0,0,
/* ret c */			0,0,M_CDST,0,M_CDSTS,0,0,0,
/* ret po */		0,0,M_CDST,0,M_CDSTS,0,0,0,
/* ret pe */		0,0,M_CDST,0,M_CDSTS,0,0,0,
/* ret p */			0,0,M_CDST,0,M_CDSTS,0,0,0,
/* ret m */			0,0,M_CDST,0,M_CDSTS,0,0,0,
};

static const MNEM_FLAGS mnemflagsXX[256] = {
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_NF | FLAGS_CF},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_NF | FLAGS_CF},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS&~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{0,0},
	{0,0},{0,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_NF | FLAGS_CF},{0,0},{0,0},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,FLAGS_YF | FLAGS_HF | FLAGS_XF | FLAGS_NF | FLAGS_CF},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,0},
	{0,0},{0,0},{0,0},{0,0},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,0},
	{0,0},{0,0},{0,0},{0,0},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{FLAGS_CF,ALL_FLAGS},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,ALL_FLAGS},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
};

// DD CB / FD CB prefix
const char *const mnemonicsXXCB[256] = 
{
	"rlc (i%\6),b", "rlc (i%\6),c", "rlc (i%\6),d", "rlc (i%\6),e", "rlc (i%\6),h", "rlc (i%\6),l", "rlc (i%\6)", "rlc (i%\6),a",
	"rrc (i%\6),b", "rrc (i%\6),c", "rrc (i%\6),d", "rrc (i%\6),e", "rrc (i%\6),h", "rrc (i%\6),l", "rrc (i%\6)", "rrc (i%\6),a",
	"rl (i%\6),b", "rl (i%\6),c", "rl (i%\6),d", "rl (i%\6),e", "rl (i%\6),h", "rl (i%\6),l", "rl (i%\6)", "rl (i%\6),a",
	"rr (i%\6),b", "rr (i%\6),c", "rr (i%\6),d", "rr (i%\6),e", "rr (i%\6),h", "rr (i%\6),l", "rr (i%\6)", "rr (i%\6),a",
	"sla (i%\6),b", "sla (i%\6),c", "sla (i%\6),d", "sla (i%\6),e", "sla (i%\6),h", "sla (i%\6),l", "sla (i%\6)", "sla (i%\6),a",
	"sra (i%\6),b", "sra (i%\6),c", "sra (i%\6),d", "sra (i%\6),e", "sra (i%\6),h", "sra (i%\6),l", "sra (i%\6)", "sra (i%\6),a",
	"sll (i%\6),b", "sll (i%\6),c", "sll (i%\6),d", "sll (i%\6),e", "sll (i%\6),h", "sll (i%\6),l", "sll (i%\6)", "sll (i%\6),a",
	"srl (i%\6),b", "srl (i%\6),c", "srl (i%\6),d", "srl (i%\6),e", "srl (i%\6),h", "srl (i%\6),l", "srl (i%\6)", "srl (i%\6),a",
	"bit 0,(i%\6)", "bit 0,(i%\6)", "bit 0,(i%\6)", "bit 0,(i%\6)", "bit 0,(i%\6)", "bit 0,(i%\6)", "bit 0,(i%\6)", "bit 0,(i%\6)",
	"bit 1,(i%\6)", "bit 1,(i%\6)", "bit 1,(i%\6)", "bit 1,(i%\6)", "bit 1,(i%\6)", "bit 1,(i%\6)", "bit 1,(i%\6)", "bit 1,(i%\6)",
	"bit 2,(i%\6)", "bit 2,(i%\6)", "bit 2,(i%\6)", "bit 2,(i%\6)", "bit 2,(i%\6)", "bit 2,(i%\6)", "bit 2,(i%\6)", "bit 2,(i%\6)",
	"bit 3,(i%\6)", "bit 3,(i%\6)", "bit 3,(i%\6)", "bit 3,(i%\6)", "bit 3,(i%\6)", "bit 3,(i%\6)", "bit 3,(i%\6)", "bit 3,(i%\6)",
	"bit 4,(i%\6)", "bit 4,(i%\6)", "bit 4,(i%\6)", "bit 4,(i%\6)", "bit 4,(i%\6)", "bit 4,(i%\6)", "bit 4,(i%\6)", "bit 4,(i%\6)",
	"bit 5,(i%\6)", "bit 5,(i%\6)", "bit 5,(i%\6)", "bit 5,(i%\6)", "bit 5,(i%\6)", "bit 5,(i%\6)", "bit 5,(i%\6)", "bit 5,(i%\6)",
	"bit 6,(i%\6)", "bit 6,(i%\6)", "bit 6,(i%\6)", "bit 6,(i%\6)", "bit 6,(i%\6)", "bit 6,(i%\6)", "bit 6,(i%\6)", "bit 6,(i%\6)",
	"bit 7,(i%\6)", "bit 7,(i%\6)", "bit 7,(i%\6)", "bit 7,(i%\6)", "bit 7,(i%\6)", "bit 7,(i%\6)", "bit 7,(i%\6)", "bit 7,(i%\6)",
	"res 0,(i%\6),b", "res 0,(i%\6),c", "res 0,(i%\6),d", "res 0,(i%\6),e", "res 0,(i%\6),h", "res 0,(i%\6),l", "res 0,(i%\6)", "res 0,(i%\6),a",
	"res 1,(i%\6),b", "res 1,(i%\6),c", "res 1,(i%\6),d", "res 1,(i%\6),e", "res 1,(i%\6),h", "res 1,(i%\6),l", "res 1,(i%\6)", "res 1,(i%\6),a",
	"res 2,(i%\6),b", "res 2,(i%\6),c", "res 2,(i%\6),d", "res 2,(i%\6),e", "res 2,(i%\6),h", "res 2,(i%\6),l", "res 2,(i%\6)", "res 2,(i%\6),a",
	"res 3,(i%\6),b", "res 3,(i%\6),c", "res 3,(i%\6),d", "res 3,(i%\6),e", "res 3,(i%\6),h", "res 3,(i%\6),l", "res 3,(i%\6)", "res 3,(i%\6),a",
	"res 4,(i%\6),b", "res 4,(i%\6),c", "res 4,(i%\6),d", "res 4,(i%\6),e", "res 4,(i%\6),h", "res 4,(i%\6),l", "res 4,(i%\6)", "res 4,(i%\6),a",
	"res 5,(i%\6),b", "res 5,(i%\6),c", "res 5,(i%\6),d", "res 5,(i%\6),e", "res 5,(i%\6),h", "res 5,(i%\6),l", "res 5,(i%\6)", "res 5,(i%\6),a",
	"res 6,(i%\6),b", "res 6,(i%\6),c", "res 6,(i%\6),d", "res 6,(i%\6),e", "res 6,(i%\6),h", "res 6,(i%\6),l", "res 6,(i%\6)", "res 6,(i%\6),a",
	"res 7,(i%\6),b", "res 7,(i%\6),c", "res 7,(i%\6),d", "res 7,(i%\6),e", "res 7,(i%\6),h", "res 7,(i%\6),l", "res 7,(i%\6)", "res 7,(i%\6),a",
	"set 0,(i%\6),b", "set 0,(i%\6),c", "set 0,(i%\6),d", "set 0,(i%\6),e", "set 0,(i%\6),h", "set 0,(i%\6),l", "set 0,(i%\6)", "set 0,(i%\6),a",
	"set 1,(i%\6),b", "set 1,(i%\6),c", "set 1,(i%\6),d", "set 1,(i%\6),e", "set 1,(i%\6),h", "set 1,(i%\6),l", "set 1,(i%\6)", "set 1,(i%\6),a",
	"set 2,(i%\6),b", "set 2,(i%\6),c", "set 2,(i%\6),d", "set 2,(i%\6),e", "set 2,(i%\6),h", "set 2,(i%\6),l", "set 2,(i%\6)", "set 2,(i%\6),a",
	"set 3,(i%\6),b", "set 3,(i%\6),c", "set 3,(i%\6),d", "set 3,(i%\6),e", "set 3,(i%\6),h", "set 3,(i%\6),l", "set 3,(i%\6)", "set 3,(i%\6),a",
	"set 4,(i%\6),b", "set 4,(i%\6),c", "set 4,(i%\6),d", "set 4,(i%\6),e", "set 4,(i%\6),h", "set 4,(i%\6),l", "set 4,(i%\6)", "set 4,(i%\6),a",
	"set 5,(i%\6),b", "set 5,(i%\6),c", "set 5,(i%\6),d", "set 5,(i%\6),e", "set 5,(i%\6),h", "set 5,(i%\6),l", "set 5,(i%\6)", "set 5,(i%\6),a",
	"set 6,(i%\6),b", "set 6,(i%\6),c", "set 6,(i%\6),d", "set 6,(i%\6),e", "set 6,(i%\6),h", "set 6,(i%\6),l", "set 6,(i%\6)", "set 6,(i%\6),a",
	"set 7,(i%\6),b", "set 7,(i%\6),c", "set 7,(i%\6),d", "set 7,(i%\6),e", "set 7,(i%\6),h", "set 7,(i%\6),l", "set 7,(i%\6)", "set 7,(i%\6),a"
};

const char *const mnemC_XXCB[256] =
{
	"b=rlc(i%\6)","c=rlc(i%\6)","d=rlc(i%\6)","e=rlc(i%\6)","h=rlc(i%\6)","l=rlc(i%\6)","rlc(i%\6)","a=rlc(i%\6)",
	"b=rrc(i%\6)","c=rrc(i%\6)","d=rrc(i%\6)","e=rrc(i%\6)","h=rrc(i%\6)","l=rrc(i%\6)","rrc(i%\6)","a=rrc(i%\6)",
	"b=rl(i%\6)","c=rl(i%\6)","d=rl(i%\6)","e=rl(i%\6)","h=rl(i%\6)","l=rl(i%\6)","rl(i%\6)","a=rl(i%\6)",
	"b=rr(i%\6)","c=rr(i%\6)","d=rr(i%\6)","e=rr(i%\6)","h=rr(i%\6)","l=rr(i%\6)","rr(i%\6)","a=rr(i%\6)",
	"b=sla(i%\6)","c=sla(i%\6)","d=sla(i%\6)","e=sla(i%\6)","h=sla(i%\6)","l=sla(i%\6)","sla(i%\6)","a=sla(i%\6)",
	"b=sra(i%\6)","c=sra(i%\6)","d=sra(i%\6)","e=sra(i%\6)","h=sra(i%\6)","l=sra(i%\6)","sra(i%\6)","a=sra(i%\6)",
	"b=sll(i%\6)","c=sll(i%\6)","d=sll(i%\6)","e=sll(i%\6)","h=sll(i%\6)","l=sll(i%\6)","sll(i%\6)","a=sll(i%\6)",
	"b=srl(i%\6)","c=srl(i%\6)","d=srl(i%\6)","e=srl(i%\6)","h=srl(i%\6)","l=srl(i%\6)","srl(i%\6)","a=srl(i%\6)",
	"Z=!!(memory[i%\6]&0x01)","Z=!!(memory[i%\6]&0x01)","Z=!!(memory[i%\6]&0x01)","Z=!!(memory[i%\6]&0x01)","Z=!!(memory[i%\6]&0x01)","Z=!!(memory[i%\6]&0x01)","Z=!!(memory[i%\6]&0x01)","Z=!!(memory[i%\6]&0x01)",
	"Z=!!(memory[i%\6]&0x02)","Z=!!(memory[i%\6]&0x02)","Z=!!(memory[i%\6]&0x02)","Z=!!(memory[i%\6]&0x02)","Z=!!(memory[i%\6]&0x02)","Z=!!(memory[i%\6]&0x02)","Z=!!(memory[i%\6]&0x02)","Z=!!(memory[i%\6]&0x02)",
	"Z=!!(memory[i%\6]&0x04)","Z=!!(memory[i%\6]&0x04)","Z=!!(memory[i%\6]&0x04)","Z=!!(memory[i%\6]&0x04)","Z=!!(memory[i%\6]&0x04)","Z=!!(memory[i%\6]&0x04)","Z=!!(memory[i%\6]&0x04)","Z=!!(memory[i%\6]&0x04)",
	"Z=!!(memory[i%\6]&0x08)","Z=!!(memory[i%\6]&0x08)","Z=!!(memory[i%\6]&0x08)","Z=!!(memory[i%\6]&0x08)","Z=!!(memory[i%\6]&0x08)","Z=!!(memory[i%\6]&0x08)","Z=!!(memory[i%\6]&0x08)","Z=!!(memory[i%\6]&0x08)",
	"Z=!!(memory[i%\6]&0x10)","Z=!!(memory[i%\6]&0x10)","Z=!!(memory[i%\6]&0x10)","Z=!!(memory[i%\6]&0x10)","Z=!!(memory[i%\6]&0x10)","Z=!!(memory[i%\6]&0x10)","Z=!!(memory[i%\6]&0x10)","Z=!!(memory[i%\6]&0x10)",
	"Z=!!(memory[i%\6]&0x20)","Z=!!(memory[i%\6]&0x20)","Z=!!(memory[i%\6]&0x20)","Z=!!(memory[i%\6]&0x20)","Z=!!(memory[i%\6]&0x20)","Z=!!(memory[i%\6]&0x20)","Z=!!(memory[i%\6]&0x20)","Z=!!(memory[i%\6]&0x20)",
	"Z=!!(memory[i%\6]&0x40)","Z=!!(memory[i%\6]&0x40)","Z=!!(memory[i%\6]&0x40)","Z=!!(memory[i%\6]&0x40)","Z=!!(memory[i%\6]&0x40)","Z=!!(memory[i%\6]&0x40)","Z=!!(memory[i%\6]&0x40)","Z=!!(memory[i%\6]&0x40)",
	"Z=!!(memory[i%\6]&0x80)","Z=!!(memory[i%\6]&0x80)","Z=!!(memory[i%\6]&0x80)","Z=!!(memory[i%\6]&0x80)","Z=!!(memory[i%\6]&0x80)","Z=!!(memory[i%\6]&0x80)","Z=!!(memory[i%\6]&0x80)","Z=!!(memory[i%\6]&0x80)",
	"b=memory[i%\6]=memory[i%\6]&~0x01","c=memory[i%\6]=memory[i%\6]&~0x01","d=memory[i%\6]=memory[i%\6]&~0x01","e=memory[i%\6]=memory[i%\6]&~0x01","h=memory[i%\6]=memory[i%\6]&~0x01","l=memory[i%\6]=memory[i%\6]&~0x01","memory[i%\6]&=~0x01","a=memory[i%\6]=memory[i%\6]&~0x01",
	"b=memory[i%\6]=memory[i%\6]&~0x02","c=memory[i%\6]=memory[i%\6]&~0x02","d=memory[i%\6]=memory[i%\6]&~0x02","e=memory[i%\6]=memory[i%\6]&~0x02","h=memory[i%\6]=memory[i%\6]&~0x02","l=memory[i%\6]=memory[i%\6]&~0x02","memory[i%\6]&=~0x02","a=memory[i%\6]=memory[i%\6]&~0x02",
	"b=memory[i%\6]=memory[i%\6]&~0x04","c=memory[i%\6]=memory[i%\6]&~0x04","d=memory[i%\6]=memory[i%\6]&~0x04","e=memory[i%\6]=memory[i%\6]&~0x04","h=memory[i%\6]=memory[i%\6]&~0x04","l=memory[i%\6]=memory[i%\6]&~0x04","memory[i%\6]&=~0x04","a=memory[i%\6]=memory[i%\6]&~0x04",
	"b=memory[i%\6]=memory[i%\6]&~0x08","c=memory[i%\6]=memory[i%\6]&~0x08","d=memory[i%\6]=memory[i%\6]&~0x08","e=memory[i%\6]=memory[i%\6]&~0x08","h=memory[i%\6]=memory[i%\6]&~0x08","l=memory[i%\6]=memory[i%\6]&~0x08","memory[i%\6]&=~0x08","a=memory[i%\6]=memory[i%\6]&~0x08",
	"b=memory[i%\6]=memory[i%\6]&~0x10","c=memory[i%\6]=memory[i%\6]&~0x10","d=memory[i%\6]=memory[i%\6]&~0x10","e=memory[i%\6]=memory[i%\6]&~0x10","h=memory[i%\6]=memory[i%\6]&~0x10","l=memory[i%\6]=memory[i%\6]&~0x10","memory[i%\6]&=~0x10","a=memory[i%\6]=memory[i%\6]&~0x10",
	"b=memory[i%\6]=memory[i%\6]&~0x20","c=memory[i%\6]=memory[i%\6]&~0x20","d=memory[i%\6]=memory[i%\6]&~0x20","e=memory[i%\6]=memory[i%\6]&~0x20","h=memory[i%\6]=memory[i%\6]&~0x20","l=memory[i%\6]=memory[i%\6]&~0x20","memory[i%\6]&=~0x20","a=memory[i%\6]=memory[i%\6]&~0x20",
	"b=memory[i%\6]=memory[i%\6]&~0x40","c=memory[i%\6]=memory[i%\6]&~0x40","d=memory[i%\6]=memory[i%\6]&~0x40","e=memory[i%\6]=memory[i%\6]&~0x40","h=memory[i%\6]=memory[i%\6]&~0x40","l=memory[i%\6]=memory[i%\6]&~0x40","memory[i%\6]&=~0x40","a=memory[i%\6]=memory[i%\6]&~0x40",
	"b=memory[i%\6]=memory[i%\6]&~0x80","c=memory[i%\6]=memory[i%\6]&~0x80","d=memory[i%\6]=memory[i%\6]&~0x80","e=memory[i%\6]=memory[i%\6]&~0x80","h=memory[i%\6]=memory[i%\6]&~0x80","l=memory[i%\6]=memory[i%\6]&~0x80","memory[i%\6]&=~0x80","a=memory[i%\6]=memory[i%\6]&~0x80",
	"b=memory[i%\6]=memory[i%\6]|0x01","c=memory[i%\6]=memory[i%\6]|0x01","d=memory[i%\6]=memory[i%\6]|0x01","e=memory[i%\6]=memory[i%\6]|0x01","h=memory[i%\6]=memory[i%\6]|0x01","l=memory[i%\6]=memory[i%\6]|0x01","memory[i%\6]|=0x01","a=memory[i%\6]=memory[i%\6]|0x01",
	"b=memory[i%\6]=memory[i%\6]|0x02","c=memory[i%\6]=memory[i%\6]|0x02","d=memory[i%\6]=memory[i%\6]|0x02","e=memory[i%\6]=memory[i%\6]|0x02","h=memory[i%\6]=memory[i%\6]|0x02","l=memory[i%\6]=memory[i%\6]|0x02","memory[i%\6]|=0x02","a=memory[i%\6]=memory[i%\6]|0x02",
	"b=memory[i%\6]=memory[i%\6]|0x04","c=memory[i%\6]=memory[i%\6]|0x04","d=memory[i%\6]=memory[i%\6]|0x04","e=memory[i%\6]=memory[i%\6]|0x04","h=memory[i%\6]=memory[i%\6]|0x04","l=memory[i%\6]=memory[i%\6]|0x04","memory[i%\6]|=0x04","a=memory[i%\6]=memory[i%\6]|0x04",
	"b=memory[i%\6]=memory[i%\6]|0x08","c=memory[i%\6]=memory[i%\6]|0x08","d=memory[i%\6]=memory[i%\6]|0x08","e=memory[i%\6]=memory[i%\6]|0x08","h=memory[i%\6]=memory[i%\6]|0x08","l=memory[i%\6]=memory[i%\6]|0x08","memory[i%\6]|=0x08","a=memory[i%\6]=memory[i%\6]|0x08",
	"b=memory[i%\6]=memory[i%\6]|0x10","c=memory[i%\6]=memory[i%\6]|0x10","d=memory[i%\6]=memory[i%\6]|0x10","e=memory[i%\6]=memory[i%\6]|0x10","h=memory[i%\6]=memory[i%\6]|0x10","l=memory[i%\6]=memory[i%\6]|0x10","memory[i%\6]|=0x10","a=memory[i%\6]=memory[i%\6]|0x10",
	"b=memory[i%\6]=memory[i%\6]|0x20","c=memory[i%\6]=memory[i%\6]|0x20","d=memory[i%\6]=memory[i%\6]|0x20","e=memory[i%\6]=memory[i%\6]|0x20","h=memory[i%\6]=memory[i%\6]|0x20","l=memory[i%\6]=memory[i%\6]|0x20","memory[i%\6]|=0x20","a=memory[i%\6]=memory[i%\6]|0x20",
	"b=memory[i%\6]=memory[i%\6]|0x40","c=memory[i%\6]=memory[i%\6]|0x40","d=memory[i%\6]=memory[i%\6]|0x40","e=memory[i%\6]=memory[i%\6]|0x40","h=memory[i%\6]=memory[i%\6]|0x40","l=memory[i%\6]=memory[i%\6]|0x40","memory[i%\6]|=0x40","a=memory[i%\6]=memory[i%\6]|0x40",
	"b=memory[i%\6]=memory[i%\6]|0x80","c=memory[i%\6]=memory[i%\6]|0x80","d=memory[i%\6]=memory[i%\6]|0x80","e=memory[i%\6]=memory[i%\6]|0x80","h=memory[i%\6]=memory[i%\6]|0x80","l=memory[i%\6]=memory[i%\6]|0x80","memory[i%\6]|=0x80","a=memory[i%\6]=memory[i%\6]|0x80",
};

// DD CB / FD CB prefix
static const int mnemtimeXXCB[256] =
{
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,20 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,20 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,20 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,20 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,20 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,20 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,20 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,20 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
	23 ,23 ,23 ,23 ,23 ,23 ,23 ,23 ,
};

static const M_TYPE mnemtypeXXCB[256]= {0};

static const MNEM_FLAGS mnemflagsXXCB[256] = {
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},{FLAGS_CF, ALL_FLAGS},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},{0,ALL_FLAGS & ~FLAGS_CF},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
	{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},
};

const char *const *mnemtab[7] =
{
	/* CS_NORMAL */	mnemonics,
	/* CS_CB     */	mnemonicsCB,
	/* CS_ED	 */	mnemonicsED,
	/* CS_FD	 */	mnemonicsXX,
	/* CS_DD	 */	mnemonicsXX,
	/* CS_FDCB	 */	mnemonicsXXCB,
	/* CS_DDCB	 */	mnemonicsXXCB,
};

// % - IX or IY
// \1 - immediate byte
// \2 - immediate byte offset
// \3 - immediate word (load)
// \4 - immediate word (load relative)
// \5 - immediate word (call/jump)
// \6 - immediate byte offset from IX/IY

#pragma warning(push)
#pragma warning (disable: 4706)
//---------------------------------------------------------------------------------------
// Disassemble a single instruction and return the number of bytes consumed.
//---------------------------------------------------------------------------------------
static char *hex(unsigned char c, int useC)
{
	static char tmp[10];
	if(useC)
		sprintf_s(tmp, 10, "0x%02X", c);
	else
		sprintf_s(tmp, 10, "%02Xh", c);
	return tmp;
}
static char *hexw(int c, int useC)
{
	static char tmp[10];
	if (useC)
		sprintf_s(tmp, 10, "0x%04X", c);
	else
		sprintf_s(tmp, 10, "%04Xh", c);
	return tmp;
}
static int DAsm2(unsigned short address, DASM_RES *res, unsigned char (*peeker)(unsigned short), int useC)
{
	memset(res, 0, sizeof *res);
	char R[128], H[100];
	char C;
	char *P;
	const char *T;
	unsigned char offset;
	char have_offset = 0;
	unsigned short start_address;
	unsigned short pc = address;
	start_address = address;
	C = '\0';
	offset = 0;

	char *instr = res->instr;
	int *loc = &res->target;
	M_TYPE *code = &res->type;
	unsigned char *bytes = res->bytes;

	unsigned char ins = peeker(address++);
	*bytes++ = ins;

	res->address = address;

	switch(ins)
	{
		case 0xCB:
			ins = peeker(address++);
			*bytes++ = ins;
			*code = mnemtypeCB[ins];
			T = useC?mnemC_CB[ins]:mnemonicsCB[ins];
			res->cycles = mnemtimeCB[ins];
			res->flags = mnemflagsCB[ins];
			break;

		case 0xED:
			ins = peeker(address++);
			*bytes++ = ins;
			*code = mnemtypeED[ins];
			T = useC ? mnemC_ED[ins] : mnemonicsED[ins];
			res->cycles = mnemtimeED[ins];
			res->flags = mnemflagsED[ins];
			break;

		case 0xDD:
			C = 'x'; goto case_fdX;
		case 0xFD:
			C = 'y';
		case_fdX:
			ins = peeker(address++);
			*bytes++ = ins;
			if (ins != 0xCB)
			{
				*code = mnemtypeXX[ins];
				T = useC ? mnemC_XX[ins] : mnemonicsXX[ins];
				res->cycles = mnemtimeXX[ins];
				res->flags = mnemflagsXX[ins];
			}
			else
			{
				offset = peeker(address++);
				have_offset = 1;
				ins = peeker(address++);
				*bytes++ = offset;
				*bytes++ = ins;
				*code = mnemtypeXXCB[ins];
				T = useC ? mnemC_XXCB[ins] : mnemonicsXXCB[ins];
				res->cycles = mnemtimeXXCB[ins];
				res->flags = mnemflagsXXCB[ins];
			}
			break;

		default:
			*code = mnemtype[ins];
			T = useC ? mnemC[ins] : mnemonics[ins];
			res->cycles = mnemtime[ins];
			res->flags = mnemflags[ins];
	}

	// ix/iy+dd
	if (P = strchr(T, '\6'))
	{
		strncpy(R, T, P-T);
		R[P-T] = '\0';
		if (!have_offset)
			offset = peeker(address++);
		signed char d = offset;
		*bytes++ = (unsigned char)d;
		sprintf(H, "%c%s", d<0?'-':'+', hex(d<0?-d:d, useC));
		strcat(R, H);
		strcat(R, P+1);
	}
	else
	{
		strcpy(R, T);
	}

	// ix/iy (might be two)
	if (P = strchr(R, '%'))
		*P = C;
	if (P = strchr(R, '%'))
		*P = C;

	if (P = strchr(R, '\1'))
	{
		// imm8
		strncpy(instr, R, P-R);
		instr[P-R] = '\0';
		unsigned char d = peeker(address++);
		*bytes++ = d;
		sprintf(H, "%s", hex(d, useC));
		strcat(instr, H);
		strcat(instr, P+1);
	}
	else if (P = strchr(R, '\2'))
	{
		// imm8 offset (jr/djnz)
		strncpy(instr, R, P-R);
		instr[P-R] = '\0';
		offset = peeker(address++);
		*bytes++ = offset;
		*loc = (signed char)offset + pc + 2;
		sprintf(H, "%s", pc_to_label((unsigned short)*loc, (signed char)offset, 1, useC));
		sprintf_s(res->autocomment, 100, "REF %04Xh", *loc);
		strcat(instr, H);
		strcat(instr, P+1);
	}
	else if (P = strchr(R, '\5'))
	{
		// imm16 (call/jp)
		strncpy(instr, R, P-R);
		instr[P-R] = '\0';
		*loc = peeker(address) | ((unsigned short)peeker(address + 1) << 8); address += 2;
		*bytes++ = (unsigned char)*loc;
		*bytes++ = (unsigned char)(*loc>>8);
		sprintf(H, "%s", pc_to_label((unsigned short)*loc, 0, 0, useC));
		sprintf_s(res->autocomment, 100, "REF %04Xh", *loc);
		strcat(instr, H);
		strcat(instr, P+1);
	}
	else if (P = strchr(R, '\3'))
	{
		// imm16 (ld)
		strncpy(instr, R, P - R);
		instr[P - R] = '\0';
		*loc = peeker(address) | ((unsigned short)peeker(address + 1) << 8); address += 2;
		*bytes++ = (unsigned char)*loc;
		*bytes++ = (unsigned char)(*loc >> 8);
		sprintf(H, "%s", hexw(*loc,useC));
		strcat(instr, H);
		strcat(instr, P+1);
	}
	else if (P = strchr(R, '\4'))
	{
		// imm16 (ld, ref)
		strncpy(instr, R, P - R);
		instr[P - R] = '\0';
		*loc = peeker(address) | ((unsigned short)peeker(address + 1) << 8); address += 2;
		*bytes++ = (unsigned char)*loc;
		*bytes++ = (unsigned char)(*loc >> 8);
		sprintf(H, "%s", pc_to_label((unsigned short)*loc, 0, 0,useC));
		sprintf_s(res->autocomment, 100, "REF %04Xh", *loc);
		strcat(instr, H);
		strcat(instr, P+1);
		if (useC)
		{
			if (!strstr(instr, "memory[_0x") && !strstr(instr, "memoryw[_0x"))
			{
				char *c = strstr(instr, "memory");
				char *d = strchr(c, '[');
				memmove(c, d + 1, d-instr + strlen(instr));
				char *e = strchr(c, ']');
				memmove(e, e + 1, e-instr + strlen(instr));
			}
			else
			{
				char *u = strchr(instr, '_');
				*u = ' ';
			}
		}
	}
	else
	{
		strcpy(instr, R);
	}

	if (start_address >= address)
		return (int)address+0x10000-start_address;

	return address-start_address;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
int DAsm(char *instr, unsigned short address, M_TYPE *code, int *loc)
{
	DASM_RES res;
	int r = DAsm2(address, &res, peek_fast,0);
	strcpy(instr, res.instr);
	*code = res.type;
	*loc = res.target;
	return r;
}
#pragma warning(pop)

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
int DAsmEx(unsigned short address, DASM_RES *res)
{
	static DASM_RES null_res;
	if (res == NULL) res = &null_res;
	return DAsm2(address, res, peek_fast,0);
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
int DCEx(unsigned short address, DASM_RES *res)
{
	static DASM_RES null_res;
	if (res == NULL) res = &null_res;
	int size = DAsm2(address, res, peek_fast, 1);
	if (res->instr[0] != '\0')
		strcat_s(res->instr, 100, ";");
	return size;
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
void DAsmBlock(unsigned short address, int length)
{
	DASM_RES res;
	unsigned short pc = address;
	while (pc < address + length)
	{
		int size = DAsmEx(pc, &res);
		elog("%04X %-10s  ", pc, res.instr);
		for (int i = 0; i < size; i++)
			elog("%02X ", res.bytes[i]);
		elog("\n");
		pc += (unsigned short)size;
	}
}

//---------------------------------------------------------------------------------------
//
//---------------------------------------------------------------------------------------
static unsigned char *mem;
unsigned char peekmem(unsigned short a)
{
	//either will do, but *mem++ means that PC address need not be tied to instruction data
	//return mem[a];
	a; return *mem++;
}

int DAsmExMem(unsigned char *memory, unsigned short address, DASM_RES *res)
{
	mem = memory;
	static DASM_RES null_res;
	if (res == NULL) res = &null_res;
	return DAsm2(address, res, peekmem,0);
}

void test_dasm(void)
{
	DASM_RES res;
	unsigned char m[] = { 0xFD, 0x36, 0x31, 0x02 };
	int size = DAsmExMem(m, 0, &res);
	assert(size == 4);
	assert(strcmp(res.instr, "ld (iy+31h),02h")==0);

	m[0] = 0xDD;
	m[1] = 0xE1;
	m[2] = 0x00;
	m[3] = 0x00;
	size = DAsmExMem(m, 0, &res);
	assert(size == 2);
	assert(strcmp(res.instr, "pop ix") == 0);

	m[0] = 0xdd;
	m[1] = 0x21;
	m[2] = 0x56;
	m[3] = 0x78;
	size = DAsmExMem(m, 0, &res);
	assert(size == 4);
	assert(strcmp(res.instr, "ld ix,7856h") == 0);

	m[0] = 0xdd;
	m[1] = 0x22;
	m[2] = 0x56;
	m[3] = 0x78;
	size = DAsmExMem(m, 0, &res);
	assert(size == 4);
	assert(strcmp(res.instr, "ld (7856h),ix") == 0);

	m[0] = 0xdd;
	m[1] = 0x26;
	m[2] = 0x56;
	m[3] = 0x00;
	size = DAsmExMem(m, 0, &res);
	assert(size == 3);
	assert(strcmp(res.instr, "ld ixh,56h") == 0);

	m[0] = 0xdd;
	m[1] = 0xcb;
	m[2] = 0x56;
	m[3] = 0x0d;
	size = DAsmExMem(m, 0, &res);
	assert(size == 4);
	assert(strcmp(res.instr, "rrc (ix+56h),l") == 0);
}

