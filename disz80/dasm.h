//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

//code dest (absolute) jump (jp)
//data dest
//data src
//code dest relative
//data dest relative
//data src relative
//code dest (absolute) subroutine (call)
typedef enum
{
	M_UNKNOWN=0,
	M_CDST = 1,//jp
	M_DDSTB = 2,
	M_DSRCB = 4,
	M_DDSTW = 16,
	M_DSRCW = 32,
	M_CDSTR = 8,//jr/djnz
	M_CDSTS = 64,//call
} M_TYPE;

typedef struct
{
	unsigned char in, out;
} MNEM_FLAGS;

typedef struct
{
	unsigned short address;
	char instr[100];
	unsigned char bytes[10];
	M_TYPE type;
	int target;
	int cycles;
	MNEM_FLAGS flags;
	char autocomment[100];
} DASM_RES;

//try to use DAsmEx instead
int DAsm(char *instr, unsigned short address, M_TYPE *code, int *loc);
int DAsmEx(unsigned short address, DASM_RES *res);
void DAsmBlock(unsigned short address, int length);
int DAsmExMem(unsigned char *memory, unsigned short address, DASM_RES *res);

int DCEx(unsigned short address, DASM_RES *res);
void test_dasm(void);
