//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
#define _CRT_SECURE_NO_WARNINGS

#define _WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <richedit.h>

#include "resource.h"
extern HWND dis_dialog;

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <ctype.h>

#include "daos.h"
#include "disview.h"
#include "emu.h"
#include "machine.h"
#include "dasm.h"
#include "z80toC.h"
#include "logger.h"
#include "basic.h"
#include "coverage.h"
#include "trace.h"
#include "labels.h"
#include "breakpoint.h"
#include "gfxview.h"
#include "perf.h"
#include "z80toC.h"

//pc_entry contains the pc for each line number
typedef struct
{
	unsigned short pc;
} PC_ENTRY;

//line entry contains the line number for each pc
typedef struct
{
	int line_no;
} LINE_ENTRY;

static PC_ENTRY pc_entry[655360];
static LINE_ENTRY line_entry[65536];
static int pc_entry_count;

//given a pc, what is the line number
static int get_line_at_pc(unsigned short pc)
{
	return line_entry[pc].line_no;
}

static int get_line_at_current_pc(void)
{
	return get_line_at_pc(get_regs()->pc);
}

//given a line on the display, what is the pc
//comment/blank lines share the pc of the next instruction
unsigned short get_pc_at_line(int line)
{
	if (line >= 0 && line < pc_entry_count)
		return pc_entry[line].pc;
	return 0;
}

//used to put a temporary breakpoint at next instruction
unsigned short find_next_pc(void)
{
	unsigned short pc = get_regs()->pc;
	char ins[100];
	M_TYPE code;
	int loc;
	return pc + (unsigned short)DAsm(ins, pc, &code, &loc);
}

//----------------------------------------------------------------------------------------------------------
//
//----------------------------------------------------------------------------------------------------------

static DB_RES *labelres = NULL;
static DB_RES *typeres = NULL;
static char db_loaded = 0;
void disview_close(void)
{
	if (labelres != NULL)
		free_result(labelres);
	if (typeres != NULL)
		free_result(typeres);
	labelres = NULL;
	typeres = NULL;
}

void disview_database_changed(void)
{
	elog("[DIS] database change\n");
	disview_close();

	if (is_db_open())
	{
		fill_type_gaps();

		labelres = search_labels();
		typeres = search_types();
		db_loaded = 1;
	}
	else
	{
		db_loaded = 0;
	}

	set_database_caption(get_db_name());
	reset_coverage();
	disview_get_labels();
	disassemble_advanced();
	gfx_database_changed();
}

void reload_database(void)
{
	disview_close();
	labelres = search_labels();
	typeres = search_types();
	disview_get_labels();
	disassemble_advanced();
}

DB_RES *get_db_labels(void)
{
	if (labelres == NULL) return NULL;
	return labelres;
}

static void compute_data_access(void);

void disview_apply_coverage(void)
{
	if (db_loaded)
	{
		disview_close();
		db_loaded = 0;

		apply_coverage();

		REG_PACK *regs = get_regs();
		PC_TRACE *pct = NULL, *imt= NULL;

		pct = start_pc_trace(regs->pc);

		//if there's an im 2 interrupt running, disassemble that too
		if (regs->im == 2)
		{
			unsigned short iv = ((unsigned short)regs->i << 8) | 0xff;
			unsigned short im = peek_fastw(iv);
			im = peek_fastw(iv);

			imt = start_pc_trace(im);

			//the interrupt routine
			char imname[100];
			sprintf_s(imname, 100, "INT_%04X", im);
			make_label(imname, SR_PCTRACE, im, LT_JUMP);
			make_code(SR_PCTRACE, im);

			//the interrupt vector table
			iv &= 0xff00;
			sprintf_s(imname, 100, "INTVEC_%04X", iv);
			make_label(imname, SR_PCTRACE, iv, LT_DATA);
			make_dataw(SR_PCTRACE, iv, iv >= 0xff00 ? (65535 - iv) + 1 : 257);
		}

		compute_data_access();

		db_loaded = 1;

		labelres = search_labels();
		typeres = search_types();

		if (pct)
		{
			graph_trace(pct);
			free_trace(pct);
		}

		if (imt)
		{
			graph_trace(imt);
			free_trace(imt);
		}

		disview_get_labels();
		disassemble_advanced();
	}
}

void disview_refresh(void)
{
	disview_close();
	labelres = search_labels();
	typeres = search_types();
}

void disview_get_labels(void)
{
	SendDlgItemMessage(dis_dialog, IDC_LABELS, LB_RESETCONTENT, 0, 0);

	if (!db_loaded) return;

	ZX_LABEL *label = (ZX_LABEL *)labelres->rows;
	int count = labelres->count;
	while (count--)
	{
		if (label->type != LT_LOCAL)
			SendDlgItemMessage(dis_dialog, IDC_LABELS, LB_ADDSTRING, 0, (LPARAM)label->name);
		label++;
	}
}

static char *hex(unsigned char c, int useC)
{
	static char tmp[10];
	if (useC)
		sprintf_s(tmp, 10, "0x%02X", c);
	else
		sprintf_s(tmp, 10, "%02Xh", c);
	return tmp;
}
static char *hexw(int c, int useC)
{
	static char tmp[10];
	if (useC)
		sprintf_s(tmp, 10, "0x%04X", c);
	else
		sprintf_s(tmp, 10, "%04Xh", c);
	return tmp;
}
char *pc_to_label(unsigned short target, signed char offset, char is_offset, int hexfmt)
{
	static char labeltext[1000];

	if (!db_loaded)
		goto nolabelfound;

	ZX_LABEL *label = (ZX_LABEL *)labelres->rows;
	ZX_LABEL *nextlabel;
	int count = labelres->count;

	/*
	unsigned char t[10] = { 0 };
	wchar_t wc = 0x2191;
	WideCharToMultiByte(CP_UTF8, 0, &wc, 1, (LPSTR)t, 10, NULL, FALSE);
	//e2 86 91
	wc = 0x2193;
	WideCharToMultiByte(CP_UTF8, 0, &wc, 1, (LPSTR)t, 10, NULL, FALSE);
	//e2 86 93

	//+U2191 up arrow
	//+U2193 down arrow
	*/

	while (count--)
	{
		nextlabel = count == 0 ? NULL : label + 1;

		if (target >= label->address && (nextlabel == NULL || target < nextlabel->address))
		{
			char *dir = "";
			//if (target < pc) dir = "\xe2\x86\x91";
			//else if (target > pc) dir = "\xe2\x86\x93";
			//this will only be positive
			unsigned short targetdiff = target - label->address;

			if (hexfmt == 1)
			{
				if (targetdiff == 0)
					sprintf_s(labeltext, 1000, "%s%s", Csafe(label->name), dir);
				else
					sprintf_s(labeltext, 1000, "_%s", hexw(target, hexfmt));
			}
			else
			{
				if (targetdiff == 0)
					sprintf_s(labeltext, 1000, "%s%s", label->name, dir);
				else if (targetdiff < 256)
					sprintf_s(labeltext, 1000, "%s+%s%s", label->name, hex((unsigned char)targetdiff, hexfmt), dir);
				else
					sprintf_s(labeltext, 1000, "%s+%s%s", label->name, hexw((int)targetdiff, hexfmt), dir);
			}
			return labeltext;
		}
		label++;
	}

nolabelfound:
	if (is_offset)
		sprintf_s(labeltext, 1000, "%c%s", offset < 0 ? '-' : '+', hex((offset < 0 ? -offset:offset) & 0x7f, hexfmt));
	else
		sprintf_s(labeltext, 1000, "%s", hexw(target,hexfmt));

	return labeltext;
}

static void scroll_to_line(int line)
{
	RECT rect = { 0 };
	SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_GETRECT, 0, (LPARAM)&rect);
	
	POINTL pointl;
	pointl.x = 1;
	pointl.y = 4;
	int char_start = (int)SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_CHARFROMPOS, 0, (LPARAM)&pointl);
	pointl.y = rect.bottom - rect.top - 1;
	int char_end = (int)SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_CHARFROMPOS, 0, (LPARAM)&pointl);

	int firstline = (int)SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_EXLINEFROMCHAR, 0, char_start);
	int lastline = (int)SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_EXLINEFROMCHAR, 0, char_end);

	//don't scroll if it's already in view
	const int linesabovePC = 5;
	const int linesbottommargin = 10;

	int scroll_min = firstline + linesabovePC;
	int scroll_max = lastline - linesbottommargin;
	if (scroll_max < scroll_min)
		scroll_max = scroll_min;

	if (line >= scroll_min && line <= scroll_max)
		return;

	int linediff = line - firstline - linesabovePC + 1;
	SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_LINESCROLL, 0, linediff);
}

static void highlight_line(int line)
{
	CHARFORMAT charf = { 0 };
	static CHARRANGE last_range = { -1,0 };

	charf.cbSize = sizeof(CHARFORMAT);
	charf.dwMask = CFM_COLOR;

	//start of this line, start of next line
	int linestart = (int)SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_LINEINDEX, line, 0);
	int lineend = (int)SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_LINEINDEX, line + 1, 0);

	CHARRANGE range;
	range.cpMin = max(linestart, 1);
	range.cpMax = lineend - 1;

	//select the current selection, change it to black
	charf.crTextColor = 0;
	SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_EXSETSEL, 0, (LPARAM)&last_range);
	SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&charf);
	//SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_SETSEL, (WPARAM)-1, 0);

	//select the new selection, change it to red, clear the selection
	charf.crTextColor = 0x0000ff;
	SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_EXSETSEL, 0, (LPARAM)&range);
	SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_SETCHARFORMAT, SCF_SELECTION, (LPARAM)&charf);
	SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_SETSEL, (WPARAM)-1, 0);

	//remember the area we've coloured
	last_range = range;
}

void disview_skip_to_label(char *name)
{
	ZX_LABEL *label = (ZX_LABEL *)labelres->rows;
	int count = labelres->count;

	while (count--)
	{
		if (strcmp(name, label->name) == 0)
		{
			int line = get_line_at_pc(label->address);

			elog("skip to address %u %04X line %u\n", label->address, label->address, line);

			scroll_to_line(line);
			break;
		}
		label++;
	}
}

void disview_skip_to_pc(void)
{
	int line = get_line_at_current_pc();
	scroll_to_line(line);
	highlight_line(line);
}

void disview_skip_to_address(unsigned short pc)
{
	int line = get_line_at_pc(pc);
	scroll_to_line(line);
}

static void compute_data_access(void)
{
	elog("data access...");
	static ZX_TYPE *types[65536];
	memset(types, 0, sizeof types);
	{
		DB_RES *res = search_types();
		ZX_TYPE *type = res->rows;
		while (res->count--)
		{
			for (unsigned int i = type->address; i < min(65536, type->address + type->size); i++)
				types[i] = type;
			type++;
		}
	}

	bulk_insert_begin();

	int pc = 0;
	while (pc < 65536)
	{
		DASM_RES asm;
		unsigned short target;

		if (types[pc] == NULL)
		{
			//should never happen
			eloglevel(LOG_ERROR, "[DIS] there's no type for location %04X\n", pc);
			pc++;
		}
		else if (types[pc]->type == CODE)
		{
			if (peek_fast((unsigned short)pc) == 0xCF)//rst 8h, 207 - 'ERROR-1' error handler
			{
				if (pc+1 < 16384)
					make_data(SR_LIBRARY, (unsigned short)(pc + 1), 1);
				else
					make_data(SR_DATAACCESS, (unsigned short)(pc + 1), 1);
				pc += 2;
			}
			else if (peek_fast((unsigned short)pc) == 0xEF)//rst 28h, 239 - 'FP-CALC' fp math handler, ending with 38h end-calc
			{
				//todo:
				//stk-data 0x34 is followed by up to 5 data bytes forming a float
				//there are jumps (e.g. jump-true 0x00 which are followed by a data byte)
				//there are series
				pc++;
				unsigned int i = pc;
				while (i < 65536)
				{
					ZX_SOURCE source = i < 16384 ? SR_LIBRARY : SR_DATAACCESS;
					make_datac(source, (unsigned short)i, 1);
					unsigned char op = peek_fast((unsigned short)i);
					if (op == 0x34)//stk-data
					{
						//first byte / 0x40 + 1 is how many bytes follow
						unsigned char b0 = peek_fast((unsigned short)(i + 1));
						make_fpstknumber(source, (unsigned short)(i + 1), (b0 >> 6) + 2);
						i += (b0 >> 6) + 1;
						pc += (b0 >> 6) + 1;
					}
					else if (op == 0x86 || op == 0x88 || op == 0x8C)//series-06, series-08, series-0C
					{
						int count = op - 0x80;
						i++;
						pc++;
						while (count--)
						{
							unsigned char b0 = peek_fast((unsigned short)(i));
							make_fpstknumber(source, (unsigned short)(i), (b0 >> 6) + 2);
							i += (b0 >> 6) + 2;
							pc += (b0 >> 6) + 2;
						}
					}
					else if (op == 0x00)//jump-true
					{
						make_data(source, (unsigned short)(i + 1), 1);
						i++;
						pc++;
					}
					else if (op == 0x33)//jump
					{
						make_data(source, (unsigned short)(i + 1), 1);
						break;
					}
					else if (op == 0x38)//end-calc
					{
						break;
					}
					i++;
					pc++;
				}
			}
			else
			{
				int size = DAsmEx((unsigned short)pc, &asm);

				target = (unsigned short)asm.target;

				switch (asm.type)
				{
					//data src
					case M_DSRCW:
						make_dataw(SR_DATAACCESS, target, 2);
						break;
					//data src
					case M_DSRCB:
						make_data(SR_DATAACCESS, target, 1);
						break;
					//data dest
					case M_DDSTW:
						make_dataw(SR_DATAACCESS, target, 2);
						break;
					//data dest
					case M_DDSTB:
						make_data(SR_DATAACCESS, target, 1);
						break;
					case M_CDSTR://jr/djnz
					{
						if (target > 16384)
						{
							char tmp[10];
							sprintf_s(tmp, 10, "L%04X", target);
							make_label(tmp, SR_DATAACCESS, target, LT_LOCAL);
						}
					}
					break;
					case M_CDST://jp
					{
						if (target > 16384)
						{
							char tmp[10];
							sprintf_s(tmp, 10, "SUB%04X", target);
							make_label(tmp, SR_DATAACCESS, target, LT_JUMP);
						}
					}
					break;
					case M_CDSTS://call
					{
						if (target > 16384)
						{
							char tmp[10];
							sprintf_s(tmp, 10, "SUB%04X", target);
							make_label(tmp, SR_DATAACCESS, target, LT_SUB);
						}
					}
					break;
				}

				pc += size;
			}
		}
		else
		{
			pc++;
		}
	}

	bulk_insert_end();

	elog("done\n");
}

typedef enum
{
	XREF_RD,
	XREF_WR
} XREF_TYPE;
typedef struct XREF
{
	unsigned short from;
	XREF_TYPE rw;
	struct XREF *next;
} XREF;
#define MAX_XREFS 65536
static XREF xrefspace[MAX_XREFS];
static int n_xrefs = 0;
static XREF *cxrefs[65536];
static XREF *dxrefs[65536];

void add_cxref(unsigned short target, int pc)
{
	XREF *head = cxrefs[target];
	XREF *cxref = cxrefs[target] = &xrefspace[n_xrefs];
	cxref->from = (unsigned short)pc;
	cxref->next = head;
	n_xrefs++;
	assert(n_xrefs < MAX_XREFS);
}

void add_dxref(unsigned short target, int pc, XREF_TYPE type)
{
	XREF *head = dxrefs[target];
	XREF *dxref = dxrefs[target] = &xrefspace[n_xrefs];
	dxref->from = (unsigned short)pc;
	dxref->rw = type;
	dxref->next = head;
	n_xrefs++;
	assert(n_xrefs < MAX_XREFS);
}

static void compute_xrefs(void)
{
	memset(cxrefs, 0, sizeof cxrefs);
	memset(dxrefs, 0, sizeof dxrefs);
	n_xrefs = 0;

	static ZX_TYPE *types[65536];
	memset(types, 0, sizeof types);
	{
		ZX_TYPE *type = (ZX_TYPE *)typeres->rows;

		int count = typeres->count;
		while (count--)
		{
			for (unsigned int i = type->address; i < min(65536, type->address + type->size); i++)
				types[i] = type;
			type++;
		}
	}

	int pc = 0;
	while (pc < 65536)
	{
		DASM_RES asm;
		unsigned short target;

		if (types[pc] == NULL)
		{
			//should never happen
			eloglevel(LOG_ERROR, "[DIS] there's no type for location %04X\n", pc);
			pc++;
		}
		else if (types[pc]->type == CODE)
		{
			int size = DAsmEx((unsigned short)pc, &asm);

			target = (unsigned short)asm.target;

			switch (asm.type)
			{
				//code dest (absolute) jump (jp)
				case M_CDST:
					add_cxref(target, pc);
					break;
				//data src
				case M_DSRCW:
					add_dxref(target, pc, XREF_RD);
					break;
				//data src
				case M_DSRCB:
					add_dxref(target, pc, XREF_RD);
					break;
				//data dest
				case M_DDSTW:
					add_dxref(target, pc, XREF_WR);
					break;
				//data dest
				case M_DDSTB:
					add_dxref(target, pc, XREF_WR);
					break;
				//code dest relative
				case M_CDSTR:
					add_cxref(target, pc);
					break;
				//code dest (absolute) subroutine (call)
				case M_CDSTS:
					add_cxref(target, pc);
					break;
			}
			pc += size;
		}
		else
		{
			pc++;
		}
	}
}

//if there's no database called default, create it and load it
void load_default_db()
{
	if (open_db("default") == -1)
		create_db("default");
	disview_database_changed();
}

void disassemble_advanced(void)
{
	if (!db_loaded)
	{
		//disassemble_plain();
		//return;
		load_default_db();
	}

	disview_refresh();

	//where is the cursor?
	int start = 0, end = 0;
	SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_GETSEL, (WPARAM)&start, (LPARAM)&end);
	unsigned short linepc = get_pc_at_line((int)SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_LINEFROMCHAR, start, 0));

	ZX_LABEL *label = (ZX_LABEL *)labelres->rows;
	ZX_LABEL *finallabel = label + labelres->count;

	char *output = malloc(10000000);
	char *t = output;
	t += sprintf_s(t, 10000000, "\xef\xbb\xbf");//UTF-8 BOM
	//t += sprintf_s(t, 10000000, "{\\rtf1{");

	int pc = 0;
	int line = 0;

	struct
	{
		unsigned short address;
		char *label;
		char *asm;
		char *xrefs;
		char *comment;
		char *autocomment;
		char *cycles;
		char *breakpoint;
	} row = { 0 };

	static ZX_TYPE *types[65536];
	memset(types, 0, sizeof types);
	{
		//todo: optimise this
		ZX_TYPE *type = (ZX_TYPE *)typeres->rows;

		static ZX_TYPE builtin_code_type = { 0 };
		builtin_code_type.address = 0;
		builtin_code_type.size = 16384;
		builtin_code_type.type = CODE;
		builtin_code_type.source = SR_BUILTIN;

		static ZX_TYPE builtin_default_type = { 0 };
		builtin_default_type.address = 16384;
		builtin_default_type.size = 49152;
		builtin_default_type.type = DEFB;
		builtin_default_type.source = SR_BUILTIN;
		
		for (int i = 0; i < 16384; i++)
			types[i] = &builtin_code_type;
		for (int i = 16384; i < 65536; i++)
			types[i] = &builtin_default_type;

		int count = typeres->count;
		while (count--)
		{
			for (unsigned int i = type->address; i < min(65536,type->address + type->size); i++)
				types[i] = type;
			type++;
		}
	}

	compute_xrefs();

	//ui
	pc = 0;
	pc_entry_count = 0;
	line = 0;

	while (pc < 65536)
	{
		//address
		row.address = (unsigned short)pc;

		int size = 0;

		if (types[pc]->type == CODE)
		{
			DASM_RES asm;

			size = DAsmEx((unsigned short)pc, &asm);
			row.asm = asm.instr;

			//cycles
			static char cyc[8];
			if (asm.cycles & 0xff00) sprintf_s(cyc, 8, "%2d/%d", asm.cycles & 0xff, asm.cycles >> 8);
			else sprintf_s(cyc, 8, "%2d", asm.cycles);
			row.cycles = cyc;

			//autocomment
			row.autocomment = asm.autocomment[0] != '\0' ? asm.autocomment : NULL;

			//breakpoint
			row.breakpoint = (get_breakpoint_status((unsigned short)pc) & BP_ACTIVE) ? "*" : " ";
		}
		else if (types[pc]->type == DEFB || types[pc]->type == DEFM || types[pc]->type == STRUCT)
		{
			//asm
			static char defb[100];
			unsigned char b = peek_fast((unsigned short)pc);
			sprintf_s(defb, 100, "defb %02Xh", b);
			row.asm = defb;

			//cycles
			row.cycles = NULL;

			//autocomment
			static char autoc[100];
			if (b >= 32 && b <= 127)
			{
				sprintf_s(autoc, 100, "%c", b);
				row.autocomment = autoc;
			}
			else if (b >= 165)
			{
				row.autocomment = (char *)get_keyword_txt(b);
			}
			else
			{
				row.autocomment = NULL;
			}

			//breakpoint
			row.breakpoint = NULL;

			size = 1;
		}
		else if (types[pc]->type == DEFN)
		{
			//asm
			static char defn[100];
			unsigned char bytes[5];
			for (int i = 0; i < 5; i++)
				bytes[i] = peek_fast((unsigned short)(pc+i));
			sprintf_s(defn, 100, "defb %02Xh,%02Xh,%02Xh,%02Xh,%02Xh", bytes[0], bytes[1], bytes[2], bytes[3], bytes[4]);
			row.asm = defn;

			//cycles
			row.cycles = NULL;

			//autocomment
			static char number[100];
			sprintf_s(number, 100, "%24.12g", decodeFP(bytes));
			row.autocomment = number;

			//breakpoint
			row.breakpoint = NULL;

			size = 5;
		}
		else if (types[pc]->type == DEFFPSTK)
		{
			//first byte / 0x40 + 1 is how many bytes follow
			
			//asm
			static char defn[100];
			unsigned char bytes[6] = { 0 };
			bytes[0] = peek_fast((unsigned short)pc);
			unsigned char extrabytes = (bytes[0] >> 6) + 1;
			if ((bytes[0] & 0x3f) == 0) extrabytes++;
			for (int i = 1; i <= extrabytes; i++)
				bytes[i] = peek_fast((unsigned short)(pc + i));
			switch (extrabytes)
			{
				case 1: sprintf_s(defn, 100, "defb %02Xh,%02Xh", bytes[0], bytes[1]); break;
				case 2: sprintf_s(defn, 100, "defb %02Xh,%02Xh,%02Xh", bytes[0], bytes[1], bytes[2]); break;
				case 3: sprintf_s(defn, 100, "defb %02Xh,%02Xh,%02Xh,%02Xh", bytes[0], bytes[1], bytes[2], bytes[3]); break;
				case 4: sprintf_s(defn, 100, "defb %02Xh,%02Xh,%02Xh,%02Xh,%02Xh", bytes[0], bytes[1], bytes[2], bytes[3], bytes[4]); break;
			}
			row.asm = defn;
			size = extrabytes+1;

			//cycles
			row.cycles = NULL;

			//autocomment
			static char number[100];
			if ((bytes[0] & 0x3F) == 0)
			{
				bytes[1] += 0x50;
				sprintf_s(number, 100, "%24.12f", decodeFP(bytes+1));
			}
			else
			{
				bytes[0] &= 0x3F;
				bytes[0] += 0x50;
				sprintf_s(number, 100, "%24.12f", decodeFP(bytes));
			}
			row.autocomment = number;

			//breakpoint
			row.breakpoint = NULL;
		}
		else if (types[pc]->type == DEFC)
		{
			//asm
			static char defb[100];
			unsigned char b = peek_fast((unsigned short)pc);
			sprintf_s(defb, 100, "defb %02Xh", b);
			row.asm = defb;

			//cycles
			row.cycles = NULL;

			//autocomment
			row.autocomment = (char *)get_calculator_label(b);

			//breakpoint
			row.breakpoint = NULL;

			size = 1;
		}
		else if (types[pc]->type == DEFW)
		{
			//asm
			static char defw[100];
			unsigned short b = peek_fastw((unsigned short)pc);
			sprintf_s(defw, 100, "defw %04Xh", b);
			row.asm = defw;

			//cycles
			row.cycles = NULL;

			//autocomment
			row.autocomment = pc_to_label(b, 0, 0, 0);

			//breakpoint
			row.breakpoint = NULL;

			size = 2;
		}
		else if (types[pc]->type == DEFS)
		{
			ZX_TYPE *curr_type = types[pc];
			int defb = pc;
			while (defb < 65536 && types[defb++] == curr_type)
				size++;

			//asm
			static char defs[100];
			sprintf_s(defs, 100, "defs %d", size);
			row.asm = defs;

			//cycles
			row.cycles = NULL;

			//autocomment
			row.autocomment = NULL;

			//breakpoint
			row.breakpoint = NULL;
		}
		else if (types[pc]->type == GFX)
		{
			ZX_TYPE *curr_type = types[pc];

			size = get_gfx_size(curr_type->gfxtype, curr_type->gfxwidth, curr_type->gfxheight);

			//asm
			static char defs[100];
			sprintf_s(defs, 100, "gfx %d x %d %s (%d bytes) ", curr_type->gfxwidth, curr_type->gfxheight, get_gfx_type(curr_type->gfxtype), size);
			row.asm = defs;

			//cycles
			row.cycles = NULL;

			//autocomment
			row.autocomment = NULL;

			//breakpoint
			row.breakpoint = NULL;

		}
		else
		{
			assert(0);
		}

		//label
		//step through the labels as we step through the code, rather than looking from the start.
		while (label != NULL && pc > label->address)
		{
			label++;
			if (label == finallabel)
			{
				label = NULL;
				break;
			}
		}

		if (label != NULL)
		{
			if (pc == label->address)
				row.label = label->name;
			else
				row.label = NULL;
		}
		else
		{
			row.label = NULL;
		}

		//xrefs
		static char xrefs[10000];
		char *x = xrefs;
		int cnt = 0;
		if (cxrefs[(unsigned short)pc])
		{
			x += sprintf_s(x, 10000, "CODE XREF: ");
			XREF *cxref = cxrefs[pc];
			do
			{
				x += sprintf_s(x, xrefs - x + 10000, "%04Xh,%s ", cxref->from, pc_to_label(cxref->from, 0, 0, 0));
				cxref = cxref->next;
				cnt++;
			} while (cxref && cnt < 20);
		}
		if (dxrefs[(unsigned short)pc])
		{
			x += sprintf_s(x, 10000, "DATA XREF: ");
			XREF *dxref = dxrefs[pc];
			do
			{
				x += sprintf_s(x, xrefs - x + 10000, "%04Xh,%c,%s ", dxref->from, (dxref->rw==XREF_RD)?'r':'w', pc_to_label(dxref->from, 0, 0, 0));
				dxref = dxref->next;
				cnt++;
			} while (dxref && cnt < 20);
		}
		if (x != xrefs)
			row.xrefs = xrefs;
		else
			row.xrefs = NULL;

		//comment
		row.comment = NULL;

		t += sprintf_s(t, 2000, "%1s%04X %-15s %-20s ; %s %-5s %s %s", row.breakpoint?row.breakpoint:" ", row.address, row.label ? row.label : "", row.asm, row.cycles?row.cycles:"", row.autocomment ? row.autocomment : "", row.xrefs ? row.xrefs : "", row.comment ? row.comment : "");
		t += sprintf_s(t, 1000, "\r\n");
		//t += sprintf_s(t, 1000, "\\par");

		//ui
		//pc_entry is the PC for each line number (if multiple lines generated, set them all to the pc)
		pc_entry[pc_entry_count++].pc = (unsigned short)pc;

		//line_entry is the line number for each PC
		//(if multiple lines generated, set them all to the line containing the disassembly, which is usually the last one)
		for (unsigned short i = 0; i < size; i++)
			line_entry[(unsigned short)(pc+i)].line_no = line;

		pc += size;
		line++;


	}
	//t += sprintf_s(t, 1000, "{\\field{\\*\\fldinst{ HYPERLINK \"http://www.msn.com\"}}{\\fldrslt{MSN} }}\\par");
	//t += sprintf_s(t, 1000, "{\\field{\\*\\fldinst{ HYPERLINK \"#1234\"}}{\\fldrslt{#1234} }}\\par");
	//t += sprintf_s(t, 1000, "}");
	SendDlgItemMessage(dis_dialog, IDC_LISTING, WM_SETREDRAW, FALSE, 0);

	//SendDlgItemMessage(dis_dialog, IDC_LISTING, WM_SETTEXT, 0, (LPARAM)"");
	//SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_SETTEXTMODE, TM_PLAINTEXT, 0);
	
	//err = SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_AUTOURLDETECT, AURL_ENABLEURL, 0);

	//DWORD style = SES_HYPERLINKTOOLTIPS | SES_NOFOCUSLINKNOTIFY;
	//SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_SETEDITSTYLE, style, style);

	//LRESULT mask = SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_GETEVENTMASK, 0 ,0);
	//SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_SETEVENTMASK, 0, mask|ENM_LINK);

	LRESULT err;
	start_timer();
	//SETTEXTEX textex = { 0 };
	//textex.flags = ST_DEFAULT;
	//textex.codepage = CP_UTF8;
	//err = SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_SETTEXTEX, (WPARAM)&textex, (LPARAM)output);

	err = SendDlgItemMessage(dis_dialog, IDC_LISTING, WM_SETTEXT, 0, (LPARAM)output);
	time_stamp("1");

	//FILE *f = fopen("sample.txt", "wb");
	//fputs(output, f);
	//fclose(f);

	//size_t s = strlen(output);
	//wchar_t *mem = calloc(1, s * 2);
	//if (mem)
	//{
	//	wchar_t *m = mem;
	//	unsigned char *o = (unsigned char *)output;
	//	for (int i = 0; i < s; i++)
	//		*m++ = *o++;
	//	err = SendDlgItemMessageW(dis_dialog, IDC_LISTING, WM_SETTEXT, 0, (LPARAM)mem);
	//	free(mem);
	//}

	show_timers();

	//put us back where we were
	int currline = get_line_at_pc(linepc);
	scroll_to_line(currline);

	//put the caret there too
	int charatline = (int)SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_LINEINDEX, (WPARAM)currline, 0);
	CHARRANGE range = { charatline, charatline };
	SendDlgItemMessage(dis_dialog, IDC_LISTING, EM_EXSETSEL, 0, (LPARAM)&range);

	SendDlgItemMessage(dis_dialog, IDC_LISTING, WM_SETREDRAW, TRUE, 0);
	RedrawWindow(dis_dialog, NULL, NULL, RDW_ERASE | RDW_FRAME | RDW_INVALIDATE | RDW_ALLCHILDREN);

	free(output);

	GetC(labelres, typeres);
}

