//---------------------------------------------------------------------------------------
// (C) Copyright 2020 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------
#pragma once
char *update_basic(void);
double decodeFP(unsigned char *q);
char *get_sysvars(void);
char *get_vars(void);
char *tokenise(char *txt);
char *basic_sample(void);
const char *get_keyword_txt(int k);

void gen_sysvar_if1_labels(void);
void gen_sysvar_labels_48k(void);
