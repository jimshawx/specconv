//---------------------------------------------------------------------------------------
// (C) Copyright 2004 James Shaw. All Rights Reserved.
//---------------------------------------------------------------------------------------

enum
{
	EMU_STOP,
	EMU_RUN,
	EMU_STEPOVER,
	EMU_STEPINTO,
	EMU_STEPOUT
};

void set_rom_menu(int machine_no);
char *get_working_folder(void);
typedef enum
{
	DLG_MEM,
	DLG_REG,
	DLG_STK,
	DLG_FPU,
	DLG_COV,
	DLG_STATS,
	DLG_PCT,
	DLG_BASIC,
	DLG_SYSVAR,
	DLG_ZXGFX,
	DLG_TAPEINFO,
	DLG_CONSOLE,
	DLG_POKES,
	DIALOG_ID_MAX = DLG_POKES
} DIALOG_ID;

#define UI_FONT_NAME "Consolas"
